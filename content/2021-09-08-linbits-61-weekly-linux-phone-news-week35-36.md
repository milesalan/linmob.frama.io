+++
title = "LinBits 61: Weekly Linux Phone news / media roundup (week 35/36)"
date = "2021-09-08T21:07:00Z"
updated = "2021-09-09T15:55:00Z"
draft = false
[taxonomies]
tags = ["PinePhone","LINMOBapps","LinuxPhoneApps","MMS","Manjaro","Qt 6","Nitrokey","open source modem firmware","PineRoot","DeltaChat",]
categories = ["weekly update"]
[extra]
author = "Peter"
update_note="Edited note on Manjaro's Phosh Dev with MMS, added word \"Experimental\" to the subtitle."
+++

_It's Wednesday again! Now what happened since last Wednesday?_

Experimental MMS support landing in Manjaro, libhandy 1.4.0, NemoMobile needs translators and more!<!-- more --> _Commentary in italics._ 

### Software releases
* [libhandy 1.4.0 has been released](https://gitlab.gnome.org/GNOME/libhandy/-/commit/f8626427acebfa08b2b4ee1166d51e416d3d7407#9f621eb5fd3bcb2fa5c7bd228c9b1ad42edc46c8). To get a better idea of the new features, read this [file](https://gitlab.gnome.org/GNOME/libhandy/-/raw/f8626427acebfa08b2b4ee1166d51e416d3d7407/NEWS) and read the section about 1.3.90, too.
* Biktorgj has released yet another [testing release of his modem firmware](https://github.com/Biktorgj/pinephone_modem_sdk/releases/tag/0.3.5), hopefully finally fixing the wakeup issue(s)!
* If you've been desperately waiting for MMS support to land, but compiling stuff from source is not your thing, [check out Manjaro Phosh Dev](https://twitter.com/ManjaroLinux/status/1435217273984278535), which now has MMS support in Chatty - [you can even configure it easily!](https://twitter.com/ManjaroLinux/status/1435359413213220868) _This is essentially based on this [WIP branch by kop316](https://source.puri.sm/kop316/chatty/-/tree/wip/mms_settings), [although the PKGBUILD takes Purism's git master and applies a bunch of patches taken from MR's and pre-MR material](https://gitlab.manjaro.org/manjaro-arm/packages/community/phosh/chatty/-/blob/02dfef6d042444b485e6888e2912442089b2f62c/PKGBUILD)._ _kop 316 [points out](https://fosstodon.org/@kop316/106898597137221551) "that MMS support in Chatty is very much experimental and many things do not work yet". So **please, adjust your expectations accordingly** and don't flood Purism's repo with issues on features that they have not even merged yet!_ 
* [Manjaro have also released another Plasma Mobile beta release (No. 7).](https://forum.manjaro.org/t/manjaro-arm-beta7-with-plasma-mobile-pinephone/81679) _Now with Plasma Mobile Gear 21.08!_
* [pineroot, the buildroot environment for PinePhone, has seen another stable release (2021.08).](https://twitter.com/pineroot1/status/1435420376599875585)

### Worth noting
* Follow up: The Nitrokey NitroPhone [is just a Google Pixel 4a with Graphene OS pre-installed with optional microphone removal](https://shop.nitrokey.com/shop/product/nitrophone-1-199).
* If you like Nemo Mobile and know more than one language well enough, [please help them out with translations!](https://www.transifex.com/nemomobile/signin/?next=/nemomobile/)
* If you are using postmarketOS edge on your PinePhone, [be careful while updating](https://postmarketos.org/edge/2021/09/07/pp-full-boot-part/)!

### Worth reading

#### Software progress
* Qt Dev Loop: [Qt 6.2 vs. Qt 5.15 – The Feature Parity Comparison](https://www.qt.io/blog/qt-6.2-vs.-qt-5.15-the-feature-parity-comparison). _Qt 6.2 is certainly interesting._
* Nate Graham: [This week in KDE: gazillions of bugfixes](https://pointieststick.com/2021/09/03/this-week-in-kde-gazillions-of-bugfixes/). _Search for Kirigami to find out why I included this :-)_
* This Week in GNOME: [#8 Fresh Sketches](https://thisweek.gnome.org/posts/2021/09/twig-8/).
* xnux.eu log: [Pinephone HDMI hot-plug-detection HW bug](https://xnux.eu/log/#045). _I have been reading about people having HDMI issues lately, this should help!_
* Claudio Cambria: [Killing the dreaded hamburger menu — Kalendar devlog 13](https://claudiocambra.com/2021/09/04/killing-the-dreaded-hamburger-menu-kalendar-devlog-13/). _It gets better and better!_

#### Visual stuff
* dot.kde.org: [Announcing the Winner of the Plasma 25th Anniversary Edition Wallpaper Contest](https://dot.kde.org/2021/09/01/announcing-winner-plasma-25th-anniversary-edition-wallpaper-contest). _The next wallpaper of Plasma Desktop and Mobile... maybe it'll grow on me._
* Federico: [GNOME themes, an incomplete status report, and how you can help](https://people.gnome.org/~federico/blog/gnome-themes.html). _Theming is not my cup of tea, but it might be interesting for some of you._

#### Daily Driving
* Daily Linux Mobile: [How I use Linux Mobile as a daily driver](https://dailylinuxmobile.org.uk/how-i-use-linux-mobile-as-a-daily-driver.html). _Great first blog post! I still have not tried NixOS, maybe I should do so._
* Hackaday: [Pining For A De-Googled Smartphone](https://hackaday.com/2021/09/02/pining-for-a-de-googled-smartphone/). _Great to see a bigger outlet write something in depth on PinePhone!_

#### Apps and use cases
* Daily Linux Mobile: [Control Chromecast on Linux Mobile with cast_control](https://dailylinuxmobile.org.uk/control-chromecast-on-linux-mobile-with-castcontrol.html). _Interesting!_
* Gamey: [PINEPHONE: 5 more amazing Apps for Linux Phones \| Pinephone, Librem5 & OnePlus6](https://odysee.com/@gamey:c/best-pinephone-apps-3:2). _Nice!_

#### Software cataloguing
* Tobias Bernard: [Get your apps ready for Software 41](https://blogs.gnome.org/tbernard/2021/09/07/ready-for-software-41/).

#### Quectel EG-25 G Information
* Dylan van Assche: [PinePhone modem myths](https://dylanvanassche.be/blog/2021/pinephone-modem-myths/). _Dylan is the guy that made instant wake-up from sleep on incoming calls happen. I am pretty sure he knows what he's talking about._

#### Luxury Linux Phones
* TuxPhones: [The singular niche of luxury Linux smartphones](https://tuxphones.com/luxury-linux-phones/). _Wow, Necunos... Totally forget about that. If you, dear reader, have such a device, please get in touch!_

### Worth Watching

#### Regular PinePhone videos
* andy gaal: [Arch barebone image brought to become a desktop Plasma-Pinephone](https://www.youtube.com/watch?v=y0A34vfYHA0). _I don't know why you would want to do this (except for convergence), but on the other hand: Why not?_
* jesica: [Mobian on PinePhone](https://www.youtube.com/watch?v=vyVTUKNlZKY). _The Mobian software you know and love._
* Blue Square: [Pixel to PinePhone - Reverse Shell](https://www.youtube.com/watch?v=MrlJ69f5xTg).

#### Tutorials
* kldk.de: tech: [PinePhone Essentials: turn off haptic keyboard feedback - feedbackd - turn off vibration](https://www.youtube.com/watch?v=0IZ-_BlZxCI). _[This](https://social.librem.one/@agx/105928457362125063) might be a more precise approach, but what's demoed in this video definitely works too._

#### Nemo Mobile progress
* Сергей Чуплыгин: [Nemomobile boot and some tests. August 2021](https://www.youtube.com/watch?v=wU8WFIHEFcM). _Nice progress!_

#### Linux Phone Gaming
* Hackers Game: [Poodillion (An OS game)](https://odysee.com/@HackersGame:4/poodillion-(an-os-game):e). _Seems fun!_

#### Corrections
* Purism: [Librem 5 Getting Faster With Age (corrected graph)](https://www.youtube.com/watch?v=JCjQXRLZQGQ).

#### Ubuntu Touch Unboxings
* Swiss Homegarden: [Latest Privacy Phone Volla Phone X \| Ubuntu Touch \| ( Unboxing & Price )](https://www.youtube.com/watch?v=1lIXdacsfQY). _This one looks robust._


#### Shorts 
* NOT A FBI Honey pot: [pinephone user gonna run OpenSUSE or however you say it......](https://www.youtube.com/watch?v=KLFBsdqr_8o). _I guess I should play with OpenSUSE again, too._
* Maxjoker98: [PinePhone UI mockup](https://www.youtube.com/watch?v=EyJvSJ9q22Y). _Ambitious project!_
* Scientific Perspective: [Ubuntu Touch GNU/Linux on POCO F1 #shorts](https://www.youtube.com/watch?v=_3qnof_7MR0).

### Stuff I did

#### Website improvements
* I have been working on my [Minimola theme](https://github.com/1peter10/minimola), which now has a search feature on the [Archive](https://linmob.net/tags) and [the 404 page](https://linmob.net/this-post-does-not-exist) and also should be slightly better in terms of documentation.
* Speaking of search features: This is a [major issue](https://github.com/linuxphoneapps/linuxphoneapps.org/issues/11) with [LinuxPhoneApps](https://alpha.linuxphoneapps.org): Due to the current design of the app pages (which only consist of [front matter](https://github.com/linuxphoneapps/linuxphoneapps.org/blob/main/content/apps/chat.delta.KDeltaChat/index.md), rendered by a [page template](https://github.com/linuxphoneapps/linuxphoneapps.org/blob/main/templates/apps/page.html), the search (as it currently works) does not find any apps. This is a blocking issue: Either we'll find a way to search the front matter properly, or we are going to have to find a different approach for the individual pages, as proper search is a must. I've outlined my current ideas, but if you have better or different ideas please [comment on the issue](https://github.com/linuxphoneapps/linuxphoneapps.org/issues/11) or [send an email](https://lists.sr.ht/~linuxphoneapps/linuxphoneapps.org-devel).
* That aside, I have been working on more informative taxonomy overview pages, see the one for [Kirigami](https://alpha.linuxphoneapps.org/frameworks/kirigami) as an example. The (still terrible) "app cards" (that list the apps) are one of the next things I am going to work on.

#### Random tinkering
* I've been building [Chatty](https://source.puri.sm/kop316/chatty/-/tree/wip/mms_settings) (for Matrix and MMS, more on that next week) and [Calls](https://gitlab.gnome.org/GNOME/calls) (for VoIP, it works!) from git to see what's coming soon,
* signed up to a proprietary service to test the GTK4/libadwaita release of [Spot](https://github.com/xou816/spot) (it works great!),
* I've been creating [one more MR](https://gitlab.com/greenbeast/white_noise_mobian/-/merge_requests/4) on a PinePhone software project this week.

#### LINMOBapps

The following app was added in the past week, pushing the app count to 312:

* [KDeltaChat](https://git.sr.ht/~link2xt/kdeltachat), a Kirigami Delta Chat client.

It's also listed at [the new place](https://alpha.linuxphoneapps.org/apps/chat.delta.KDeltaChat/).

[Read here what (else) happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master) on LINMOBapps this week. And please [do contribute!](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/CONTRIBUTING.md)
