+++
title = "LinBits 49: Weekly Linux Phone news / media roundup (week 23/24)"
date = "2021-06-16T21:10:00Z"
[taxonomies]
tags = ["PINE64", "PinePhone", "LINMOBapps", "Librem 5", "Arch Linux ARM", "Maui", "Plasma Mobile", "Jing Pad"]
categories = ["weekly update"]
[extra]
author = "Peter"
+++

_It's Wednesday. Now what happened since last Wednesday?_

Plasma Mobile, PureOS and postmarketOS progess, DanctNIX with Plasma Mobile, PinePhone keyboard firmware and more!<!-- more --> _Commentary in italics._

### Software releases
* [Calls 0.3.4 has been released](https://source.puri.sm/Librem5/calls/-/releases#v0.3.4).
* [Phoc 0.7.1 has been released](https://source.puri.sm/Librem5/phoc/-/releases/v0.7.1).
* [DanctNIX Mobile](https://github.com/dreemurrs-embedded/Pine64-Arch/releases/tag/20210613) has seen a new release, for the first time featuring Plasma Mobile images. _Great work, Danct12!_

### Worth noting

* [Maemo Leste Translation progress](https://twitter.com/maemoleste/status/1402959691853058049).
* [Maemo Leste in an Android chroot](https://twitter.com/maemoleste/status/1404825056493158403). _The video linked is too old and I sadly missed it. BTW, if you should watch it: Mute it, the music might mess you up!_
* u/ppafin: [Our Weekend project: buildroot environment for pinephone](https://old.reddit.com/r/embeddedlinux/comments/ny2unw/our_weekend_project_buildroot_environment_for/). _Definitely something I need to take a look at!_

### Worth reading 

* Vice Motherboard: [The $149 Smartphone That Could Bring The Linux Mobile Ecosystem to Life](https://www.vice.com/en/article/v7e77y/the-dollar149-smartphone-that-could-bring-the-linux-mobile-ecosystem-to-life). _This essentially a re-post of the excellent [Tedium post](https://tedium.co/2021/05/26/pinephone-mobile-linux-review/) we had in [LinBits 47](https://linmob.net/linbits-47-weekly-linux-phone-news-week21-22/), but it definitely hit a wider audience here, which is great, especially for such a nice article. The only issue I have is that my Sxmo video is definitely not the best video on the topic &mdash; [this video is](https://diode.zone/videos/watch/b52e7c40-87cb-4479-a4cc-c11b1bfa8806) ;-)_
* xnux.eu log: [Hints on debugging HDMI output on Pinephone](https://xnux.eu/log/#039). _If you have problems with HDMI out on your PinePhone, this might help._

#### Software Progress
* NxOS: [Maui Weekly Report 12](https://nxos.org/maui/maui-weekly-report-12/). _I like this!_
* Plasma Mobile: [Plasma Mobile: More Applications and an Improved Homescreen](https://www.plasma-mobile.org/2021/06/10/plasma-mobile-update-june/). _Nice update! The only thing I miss progress on is the work on an email client for Plasma Mobile, but then many people likely use (hopefully friendly) webmail, that can be dealt with with Angelfish's web app feature._
  * Linux Smartphones: [Plasma Mobile gains shell and home screen improvements, app updates and new apps](https://linuxsmartphones.com/plasma-mobile-gains-shell-and-home-screen-improvements-app-updates-and-new-apps/). _Brad's take on this update._
* Purism: [Making a Platform Adaptive for Everyone](https://puri.sm/posts/making-a-platform-adaptive-for-everyone/). _This is truly a great effort. Make sure to watch those videos on what's coming for libadwaita._
* Janet Blackquill: [Yet Another Week In Tok](https://blog.blackquill.cc/yet-another-week-in-tok). _Tok is really coming along nicely!_
* Claudio Cambra: [First week of Google Summer of Code 2021](https://claudiocambra.com/2021/06/14/first-week-of-google-summer-of-code-2021/). _[Kalendar](https://invent.kde.org/carlschwan/kalendar) now definitely is a project to watch._

#### Hardware news
* Purism: [Librem 5 News Summary: May 2021](https://puri.sm/posts/librem-5-news-summary-may-2021/). _One note regarding the Products and Availability Chart: Given that there are people with orders from late 2017 waiting for the non-USA Librem 5, I would not exactly bet on this._
* Purism: [Toward a Respectful Hardware Production](https://puri.sm/posts/toward-a-respectful-hardware-production/). 
* TuxPhones: [Linux tablet maker JingLing secures $10M funding, starts JingPad crowdfunding campaign](https://tuxphones.com/linux-tablet-jingling-jingpad-a1-startup-10m-funding-crowdfunding-pricing-indiegogo/). _Except for the displays aspect ratio which I like a lot, I really don't think this is for me._

#### Slightly unrelated, but worth reading
* pointiestick.com: [Bug triaging is the foundation of quality and we need more of it](https://pointieststick.com/2021/06/15/bug-triaging-is-the-foundation-of-quality-and-we-need-more-of-it/)
* scarpino.dev: [I'm back in the boat](https://scarpino.dev/posts/im-back-in-the-boat.html). _A blog post about Sailfish OS with a few app recommendations that may be helpful._

#### Distro news
* TuxPhones: [postmarketOS in 2021: over 300 devices, growing technical basis and extensive "mainline" support](https://tuxphones.com/postmarketos-2021-linux-300-phones-glacier-powervr/). _Great post about the massive progress postmarketOS has been making lately._
* PineGuild: [Arch Linux ARM 20210613 – Kernel 5.12.7, Phosh 0.11 and Plasma Mobile images](https://pineguild.com/arch-linux-arm-20210613-kernel-5-12-7-phosh-0-11-and-plasma-mobile-images/).
* Purism: [Pureos 10 (Byzantium) Snapshot: June 2021](https://puri.sm/posts/pureos-10-byzantium-snapshot-june-2021/). _The progress is noteworthy. I really look forward to a properly working version of Megapixels being packaged!_
  * Linux Smartphones: [Purism shows off new features coming to PureOS for the Librem 5 smartphone (camera software, wireless toggles, screen rotation and more)](https://linuxsmartphones.com/purism-shows-off-new-features-coming-to-pureos-for-the-librem-5-smartphone-camera-software-wireless-toggles-screen-rotation-and-more/). _Brad's take._

#### Akademy (KDE Conference) previews
* Volker Krause: [KDE Akademy 2021](https://www.volkerkrause.eu/2021/06/12/kde-akademy-2021.html)
* Kevin Ottens: [Akademy 2021 at Home](https://ervin.ipsquad.net/blog/2021/06/15/akademy-2021-at-home/).

#### LinBits competition
* Linux Smartphones: [News roundup: Hacking the PinePhone keyboard accessory, Phoc updates, and a new convergent web browser](https://linuxsmartphones.com/news-roundup-hacking-the-pinephone-keyboard-accessory-phoc-updates-and-a-new-convergent-web-browser/)

#### PINE64 Community Update
* PINE64: [June update: new hardware and more on the way](https://www.pine64.org/2021/06/15/june-update-new-hardware-and-more-on-the-way/). _Another nice update by PINE64. SoQuartz really interests me._
  * Linux Smartphones: [The PinePhone keyboard case will support hardware add-ons for wireless charging, LoRa,GNOME on Mobile and more](https://linuxsmartphones.com/the-pinephone-keyboard-case-will-support-hardware-add-ons-for-wireless-charging-lora-and-more/).

#### PinePhone Keyboard development
* xnux.eu log: [PinePhone keyboard - HW testing](https://xnux.eu/log/#036)
* xnux.eu log: [Pinephone keyboard firmware](https://xnux.eu/log/#037). _Make sure to also check the video section below._
* xnux.eu log: [Pinephone keyboard input daemon released](https://xnux.eu/log/#040)

### Worth listening
* postmarketOS Podcast: [#6.1 Alpineconf, MMS, Lomiri, PureMaps, postmarketOS-tweaks](https://cast.postmarketos.org/episode/06.1-AlpineConf-MMS-Lomiri-PureMaps-pmOStweaks/).
* postmarketOS Podcast: [#6.2 AsteroidOS, PinePhone Battery/RAM Freq, RetroArch, Firefox](https://cast.postmarketos.org/episode/06.2-AsteroidOS-PinePhone-Battery-RAM-RetroArch-Firefox/). _Two great episodes at once!_

### Worth watching
#### PINE64 Community Update
* PINE64: [June update: new hardware and more on the way](https://www.youtube.com/watch?v=mzCIhq17b78). _Great video by PizzaLovingNerd!_

#### Software news
* Camilo Higuita: [Maui Weekly Report](https://www.youtube.com/watch?v=JS8qZwKoMFk). _Impressive progress. I really need to use these apps more – not just on the phone._
* Purism: [Pureos 10 (Byzantium) Snapshot: June 2021](https://www.youtube.com/watch?v=JPrGF5ML-qI).

#### Video Review
* LearnLinuxTV: [Pinephone Review (and why we NEED this phone!)](https://www.youtube.com/watch?v=FqWLbjov-bQ). _Good, thoughtful review._

#### Impressions of postmarketOS
* Tamás Vándorffy: [postmarketOS is booting on my N900](https://www.youtube.com/watch?v=92IRhkiyTCE).
* VegaData: [Conhecendo o postmarketOS com o Phosh](https://www.youtube.com/watch?v=nSfkrDtDaV4). _Nice video about postmarketOS with Phosh running on the Xiaomi Pocophone F1 - in Portuguese._

#### Tutorial corner
* (RTP) Privacy & Tech Tips: [Linux Class: Create Backup Image Files + Copy w/the dd Command- Here's How (Pinephone Backing up Ex)](https://www.youtube.com/watch?v=pq3nL2ZWqUg). _Great tutorial!_
* kdlk.de: tech: [PinePhone Essentials: How to open a pinephone](https://www.youtube.com/watch?v=wyIiu0LKtfU). _If you didn't know, now you know!_
* kdlk.de: tech: [PinePhone Essentials: Samsung Battery in your PinePhone](https://www.youtube.com/watch?v=RVrm0LFHdi8).

#### Convergence fun
* Nat Tuck: [Librem 5 Convergence](https://www.youtube.com/watch?v=uqCWy2gDukk). _Fun video!_

#### PinePhone keyboard development
* sadfwesv: [Pinephone keybaord - Free Firmware Project](https://www.youtube.com/watch?v=hj8DIqD74IM).
* sadfwesv: [PinePhone keyboard demo](https://www.youtube.com/watch?v=Kx6B_OL4OJ4). _Lukasz says that they are likely to ship the keyboard case with Megi's firmware._

#### JingOS corner
* JingOS: [JingPad A1 Tablet Mode running GIMP, LibreOffice, Chromium, Firefox...](https://www.youtube.com/watch?v=fhJ10dsslQU).


### Stuff I did

#### Content

None, sorry. Busy times :(

#### Random

* I've been having no luck with relisting this blog on Bing (a search engine only relevant because of DuckDuckGo), and both their tools and communication are rather opaque. Apparently, this blog is accidentally doing something listed in "Things to avoid", if only I knew what it is.

#### LINMOBapps
LINMOBapps got much love this week, either. I have added a few apps: 

* [Baconer](https://github.com/TomBebb/Baconer), a Kirigami/Qt Reddit Client,
* [Sol](https://invent.kde.org/maui/sol), a convergent web browser,
* [Image Roll](https://github.com/weclaw1/image-roll), a simple and fast GTK image viewer with basic image manipulation tools, written in Rust,
* [OpenTodoList](https://gitlab.com/rpdev/opentodolist), a Todo List and Note Taking Application, and
* [Seahorse](https://gitlab.gnome.org/GNOME/seahorse), a password and encryption key manager for GNOME.

[Read here what (else) happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master). 
Please [do contribute!](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/CONTRIBUTING.md)
