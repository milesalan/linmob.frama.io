+++
title = "Get a shell on A910(i)"
aliases = ["2008/04/get-shell-on-a910i.html"]
date = "2008-04-13T20:54:00Z"
[taxonomies]
tags = ["A910", "A910i", "ezxterm", "howto", "OpenEZX", "SBF_Backup"]
categories = ["howto",]
[extra]
author = "Peter"
+++
_I am writing this howto as "SBF Backup" seems to have failed._ This tutorial explains how to get a shell right on your phone. If you want to get a shell on your phone while it is connected to your computer, have a look at <a href="http://wiki.openezx.org/Get_a_shell">this explanations at openezx.org</a>.
<!-- more -->
What you do need:

<a href="http://www.e2mod.com/content/view/254/28/">ezxterm.mpkg</a>

And to get a more comfortable setup: 

<a href="http://www.fachsemester.de/diverses/a910tools.tar.bz2">a910-tools</a>

Just put ezxterm.mpkg on your phones memory card, find it via your phones filemanager and install it. After installing it, connect your phone in memory card mode again (or put your phones memory card in a cardreader) and edit _.system/java/CardRegistry_; there will be an entry which ezxterm created. At a new line looking like that (somewhere in the middle of that entry):

~~~~
IMEI = # Enter your IMEI here, if you don't know it type *#06# in your phone
~~~~

If you want to have a root terminal, change GroupID and UserID value from _&#8220;ezx&#8221;_ to _&#8220;root&#8221;_ (without &#8220;&#8221;), if you want to have one root terminal and a normal one, just copy the entry in _CardRegistry_, edit the upper values and the &#8220;_Name_&#8221; value; don't forget to edit this:
~~~~
[1234abd2-2def-3456-a7bc-d8ef1g90hi12]
AniIcon =
AppID = {1234abd2-2def-3456-a7bc-d8ef1g90hi12}
~~~~

(Just change some numbers around, but []-value and {}-value have to be the same).

As you are now done with this, just extract a910tools.tar.bz2 to your memory cards root. 

If you restart your phone with memory card after that, you'll have one or two entries in your phones' main menu, just try them out. If you want to have a more comfortable bash-feeling, just enter `/mmc/mmca1/root/start` (deleting wrong characters works then, and you have a bash history.

But the font will still look bad, as ezxterm needs a special font. Simply enter

~~~~
cp /mmc/mmca1/.system/QTDownLoad/ezxterm/lucon.ttf /ezxlocal/lucon.ttf
~~~~

and this problem will be history after a restart of your phone.

By the way: If you enter `./acmattach.lin` after that, your phone should be in USBLan mode (it will be easier to get a shell via your PC then).

Enjoy!
