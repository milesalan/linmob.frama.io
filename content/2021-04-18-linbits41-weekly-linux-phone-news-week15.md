+++
title = "LinBits 41: Weekly Linux Phone news / media roundup (week 15)"
aliases = ["2021/04/18/linbits41-weekly-linux-phone-news-week15.html"]
date = "2021-04-18T20:30:00Z"
author = "Peter"
layout = "post"
[taxonomies]
tags = ["PINE64", "PinePhone", "Librem 5", "Manjaro", "LINMOBapps", "RUHAcam", "OpenSUSE", "DanctNIX", "Arch Linux ARM", "PinePhone Modem Firmware", "Whisperfish", "Signal", "Ubuntu Touch", "PINE64 Community Update", "Glacier UX", "postmarketOS",]
categories= ["weekly update"]
+++

_It's sunday. Now what happened since last sunday?_

Phosh and Squeekboard, Manjaro, DanctNIX and OpenSUSE see new releases, PINE64 show more PinePhone accessories and more! _Commentary in italics._
<!-- more -->


### Software development and releases
* [phosh 0.10.1](https://social.librem.one/@agx/106051864014799204) has been released: Bug fixes auto rotation and more! Make sure to read the [full release notes](https://source.puri.sm/Librem5/phosh/-/releases/v0.10.1).
* The GNOME on Mobile virtual keyboard, Squeekboard, have [announced](https://forums.puri.sm/t/squeekboard-1-13-0-released/13097) a new release, 1.13.0. New features include Caps Lock, locale-flavoured terminal layouts and more. Read the [full release notes](https://source.puri.sm/Librem5/squeekboard/-/merge_requests/451).
* Arch Linux ARM/DanctNIX Mobile has also seen [a new release](https://github.com/dreemurrs-embedded/Pine64-Arch/releases/tag/20210418), shipping, among other things, the new releases mentioned above.
* Manjaro have released new beta images for the PinePhone: [Phosh beta 8](https://twitter.com/ManjaroLinux/status/1382063011012214785) and [Plasma Mobile beta 4](https://twitter.com/ManjaroLinux/status/1382063364969525249).
* [Manjaro have also brought](https://twitter.com/ManjaroLinux/status/1383498747464753153/) their package manager [Pamac](https://gitlab.manjaro.org/applications/pamac) to the PinePhone by using libhandy.
* OpenSUSE have silently published [new PinePhone images with Phosh and Plasma Mobile](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/?C=N;O=D).
* [Biktor](https://twitter.com/biktorgj/status/1383471499047104523) has a [released a new version of his alternative, FOSS PinePhone Modem firmware](https://github.com/Biktorgj/pinephone_modem_sdk/releases). _Make sure to also read the section on the Modem in PINE64's community update._

### Hardware: 
* TuxPhones: [The RUHAcam is an open-source, 3D-printed Linux camera based on Raspberry Pi](https://tuxphones.com/open-source-ruhacam-3d-printed-raspberry-linux-hq-camera-imx477/). _Not a phone, but still interesting!_
* [Andrew M. designed a weighted PinePhone stand](https://fosstodon.org/@silverhax/106073284362532312). _Nice!_


### Worth noting
* An effort for a new _Signal client_ for Linux Phones based on the Rust library [libsignal-client](https://github.com/signalapp/libsignal-client) was announced on the [forums](https://forum.pine64.org/showthread.php?tid=13610&highlight=signal) and on [Reddit](https://old.reddit.com/r/linux/comments/mp2j0j/starting_a_native_adaptive_linux_client_for_signal/), then [started as a Plasma Mobile app](https://invent.kde.org/being/flare) but eventually becoming [a Kirigami adaption of Whisperfish, the Signal client for SailfishOS](https://gitlab.com/whisperfish/whisperfish/-/issues/318).
* If you are a fan of both, RetroArch and postmarketOS, [rejoice!](https://fosstodon.org/@postmarketOS/106071868693483297)


### Worth reading 
* twongkee: [Pinephone Megapixels vs cli ffmpeg](https://twongkee.wordpress.com/2021/04/12/pinephone-megapixels-vs-cli-ffmpeg/). _ffmpeg experiments are always fun._
* Gamey: [My Pinephone daily driver setup!](https://odysee.com/@gamey:c/my-pinephone-setup:e?src=open&r=GrpMQANLB82BBbQoMRsoz2UWomgtwTjw). _Nice setup!_
* Bhushan Shah: [Walled Gardens](https://blog.bshah.in/2021/04/12/walled-gardens/). _A few good thoughts on the new Signal effort above._
* FOSS2go: [Manjaro ARM 21.04 released: new application for flashing images](https://foss2go.com/manjaro-arm-21-04-released-new-application-flashing-images/). 
* Purism: [App Showcase: Drawing](https://puri.sm/posts/app-showcase-drawing/). _Nice app!_
* Han Young: [Season of KDE 2021 Status Report](https://han-y.gitlab.io/posts/sok-2021-report/). _Great!_
* FOSS2go: [TELEports for Pinephone has been fixed](https://foss2go.com/teleports-for-pinephone-has-been-fixed/). _Great to see improvements here!_
* Qt Dev Loop: [Introducing Qt Quick 3D Particles](https://www.qt.io/blog/introducing-qt-quick-3d-particles?utm_source=atom_feed). _Something to look forward to in Qt 6._
* PINE64: [April Update: New Developments](https://www.pine64.org/2021/04/15/april-update-new-developments/). _Another big update. Sticking to PinePhone, there's news on the Beta Edition (on schedule), and it's accessories: The keyboard and fingerprint reader/Qi charging back cover are getting closer to release._ 
* Linux Smartphones: [Ubuntu Touch for the PinePhone is moving to a new kernel for better hardware support](https://linuxsmartphones.com/ubuntu-touch-for-the-pinephone-is-moving-to-a-new-kernel-for-better-hardware-support/). _Brad highlights an important aspect of PINE64's community update here._
* UBports: [Ubuntu Touch Q&A 98 blog post](https://ubports.com/blog/ubports-news-1/post/ubuntu-touch-q-a-98-3751). _The written and audio release of last weeks Q&A._


### Worth watching
* PINE64: [April Update: New Developments](https://tilvids.com/videos/watch/7c5a1277-b39b-49a5-84a9-eecc7e343661). _Once again, a great video by PizzaLovingNerd!_
* Privacy & Tech Tips: [Pine64 LoRa Radio Texting Long Range Using Low Power: Linux Gateway + Modules Including On Pinephone](https://www.youtube.com/watch?v=cJ0wpANpbyc). _A first look at PINE64's new LoRa products!_
* Martijn Braam: [Glacier WIP for postmarketOS](https://www.youtube.com/watch?v=Gxin52CVq24) _Note that a [few bugs have already been fixed since](https://mastodon.fam-ribbers.com/@bart/106082131738392641). I recently tried the "official" Manjaro based-devel image and had a slightly better, overall similar experience._
* Sk4zZi0uS: [PinePhone Challenge 2 week update (Moving to Manjaro Phosh) - 2021-04-08](https://www.youtube.com/watch?v=4ZQPQKeBA7I) _Nice ramblings, looking forward to his experience with Manjaro Phosh!_
* E. Matt Armstrong: [PinePhone Arch Distro Spin Part 5 More Apps](https://www.youtube.com/watch?v=tv88fPN2B8M). _Nice continuation of a series._
* Hammad Farooq: [Signal-Desktop-5.0.0 running on Pinephone](https://www.youtube.com/watch?v=jlmWeP6V2yE). _Running yes, working no. Make sure to read a related [Reddit thread](https://old.reddit.com/r/pinephone/comments/mqx7z0/the_status_of_signaldesktop500_running_on/)._


### Stuff I did

#### Content
None, sorry. But there's hope for next week!

#### Random
* I tried [to do something with the PineCube](https://fosstodon.org/@linmob/106070598603118848) PINE64 sent me. _And I am going to use it as a camera in my next live stream._
* Work on this blog's [Zola](https://getzola.org) migration continues: There are still years of posts to be looked at and frontmatter editing, the theme needs work (and a few decisions on features and questions like modern vs. minimal), and... and... But: I managed to get [Zola](https://getzola.org) running on my [current hoster](https://uberspace.de) by compiling it in a CentOS 7 virtual machine and most importantly, I am still on schedule. 

#### LINMOBapps

I added three apps to LINMOBapps, so that we have a total of 252 apps right now: Pamac (see above); Telegrand, a WIP GTK4/libadwaita Telegram client written in Rust; and Lith, a WeeChat client. 
I also a removed few things from the LINMOBapps repository and put them in their own repositories: [APKBUILDs](https://framagit.org/linmobapps/apkbuilds), [PKGBUILDs](https://framagit.org/linmobapps/pkgbuilds) and [tweaks](https://github.com/1peter10/linuxphone-tweaks).
[See here what (else) happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master). Please [do contribute](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/tasks.md)! 
