+++
title = "MeeGo 1.2 Tablet UX on Nokia N900"
aliases = ["2011/03/meego-1-2-tablet-ux-on-nokia-n900.html"]
date = "2011-03-29T13:02:00Z"
[taxonomies]
categories = ["shortform"]
tags = ["MeeGo", "MeeGo tablet UX", "Nokia N900", "video"]
[extra]
author = "Peter"
+++
Oh, you can't imagine how disappointed when I didn't manage to run Meego 1.1.90 on my Samsung GT-i8320. While I'll attempt that once more as soon as I am done with the non tech blog stuff I have to do right now, here is a nice video demo of the MeeGo Tablet UX on the Nokia N900 shot by <a href="http://sagestechblog.blogspot.com/2011/03/nokia-n900-meego-12-development-release.html">Sage's Tech Blog.</a>

Marko Saukko on YouTube: [Nokia N900 + MeeGo 1.2 dev + Tablet UX](https://www.youtube.com/watch?v=xjQukhXYt-U)
