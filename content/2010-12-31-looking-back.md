+++
title = "Looking back"
aliases = ["2010/12/looking-back.html"]
date = "2010-12-31T00:07:00Z"
[taxonomies]
tags = ["Android", "looking back", "MeeGo", "new year", "tech", "WP7"]
categories = ["commentary", "personal"]
[extra]
author = "Peter"
+++
<p>2011 is close, and it will be there in a glimpse. 2010 will be over,  and people on twitter will write &#8220;oh, that's so 2010&#8221;, whenever they  stumble on something that isn't bleeding edge.

2011  will be the way it' ll be. I won't write down what I expect for 2011,  all I can say is that I am going to do better in 2011, as 2010 wasn't a  great year for me. I don't want to complain though, because complaining  doesn't help anybody and because I think of complaining as a way of &#8220;not  admitting ones own guilt/fault&#8221;. 

I could try to talk about tech  here, but well, we all know what came up in 2010 - considering mobile  devices it was interesting, but not exactly mind blowing. 1GHz  superphones became a thing, Microsoft released it's new mobile OS, Android,  iOS, webOS evolved, MeeGo for handhelds didn't make it in 2010, some  tablets have been released, cheap and expensive, bad and good, but the  real tablet business will start - you name it - in 2011.

Netbooks  are still there and now dual core powered, but honestly: The Netbook  business has become boring, prices are relatively high and there are  just a few devices, that are outstanding in one way or the other - I  won't name them here, you have to find them on your own.

So, overall, I believe that 2011 will be better. I am quite optimistic, and that's unusual. 

I wish you all the best!
