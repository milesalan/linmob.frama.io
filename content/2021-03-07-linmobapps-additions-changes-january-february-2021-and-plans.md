+++
title = "LINMOBapps: Additions (and changes) in January and February 2021 (and future plans)"
aliases = ["2021/03/07/linmobapps-additions-changes-january-february-2021-and-plans.html"]
author = "Peter"
date = "2021-03-07T00:57:18Z"
layout = "post"
[taxonomies]
tags = ["PinePhone", "Librem 5", "Apps", "LINMOBapps"]
categories = ["projects"]
+++

_Lately, I did not manage to properly promote every app that I added to LINMOBapps, the app list you can now find under [appl.ist](http://appl.ist).[^1] In this post I will list the apps that have been added to the list. I also want to share my further plans for this list._

<!-- more -->

### Apps that have been added

#### January

* [Metadata Cleaner](https://gitlab.com/rmnvgr/metadata-cleaner), _a GTK GUI to view and clean metadata in files_,
* [Ambience](https://github.com/LukaJankovic/Ambience), _a GTK/libhandy GUI to control LIFX lights_,
* [Cawbird](https://github.com/IBBoard/cawbird), _a GTK twitter client that works great on PinePhone_,
* [Decoder](https://gitlab.gnome.org/bilelmoussaoui/decoder/), _a GTK4/libadwaita app to scan and generate QR codes_,
* [Health](https://gitlab.gnome.org/Cogitri/Health), _a GTK4/libadwaita health tracking app_,
* [Mt](https://gitlab.com/Miridyan/terminal), _a terminal app written in GTK and Rust_,
* [Dino](https://github.com/dino/dino/tree/feature/handy), _the libhandy branch of a great XMPP client_,
* [Cuttlefish](https://gitlab.shinice.net/artectrex/Cuttlefish), _a GTK4/libhandy PeerTube app_,
* [Lord Almighty’s Modern Bible (LAMB)](https://gitlab.com/The3DmaN/lord-almightys-modern-bible)[^2], _a QtQuick bible app_,
* [Media Server Connect (MSC)](https://gitlab.com/The3DmaN/media-server-connect)[^2], _a simple QtWebEngine web app to connect to Emby servers_,
* [Home Assistant Connect (HAC)](https://gitlab.com/The3DmaN/ha-connect)[^2], _a simple QtWebEngine web app to connect to Home Assistant servers_, and
* [ABV Calculator](https://gitlab.com/The3DmaN/abv-calc)[^2], _something with alcohol_.

#### February

* [Pikasso](https://invent.kde.org/graphics/pikasso), _a Kirigami drawing app_,
* [Kairo](https://invent.kde.org/utilities/kairo), _a programmable timer_,
* [Qwertone](https://gitlab.com/azymohliad/qwertone), _a fun little GTK musical instrument that's rusty_,
* [Castor](https://git.sr.ht/~julienxx/castor), _a Gemini, Gopher, Finger browser_,
* [Master Key](https://gitlab.com/guillermop/master-key/), _a password generator (GTK and libhandy)_,
* [GPodder Adaptive](https://github.com/tpikonen/gpodder#branch=hdy), _a fork of the GTK gpodder app that uses libhandy to fit phone screens_,
* [PassUi](https://github.com/mickenordin/passui), _a GTK gui for pass_, 
* [pass-manager-compact](https://gitlab.com/chrisu281080/pass-manager-compact), _another GTK gui for pass,
* [caerbannog](https://git.sr.ht/~craftyguy/caerbannog), _yet another GTK gui for pass, and my favourite of the ones listed_,
* [Weather Mobile](https://github.com/Tiggilyboo/weather-mobile), _a GTK4/libadwaita weather app written in Rust_,
* [Notejot](https://github.com/lainsce/notejot/), _a note taking app_,
* [Spot](https://github.com/xou816/spot), _a Spotify Premium spotify music player that by now has a flawless mobile UI and is written in Rust_,
* [eOVPN](https://github.com/jkotra/eOVPN), _a GTK Open VPN setup GUI that also works ok on phones_,
* [Crow Translate](https://github.com/crow-translate/crow-translate), _a Kirigami translation frontend for use with Google, Bing or Yandex translate_ and
* [siglo](https://github.com/alexr4535/siglo), _a WIP GUI for [InfiniTime](https://github.com/JF002/InfiniTime) sync._,


### Changes

I did a few adjustments to ratings, added a few package names, and more. You can look them up by going through the commit history. That aside, I tried to add appstream:// links to allow easier install via Discover or GNOME Software, but ... see below.

### What's coming?

Aside from a lot of apps listed in [tasks.md](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/tasks.md) that are waiting to be added, there are quite a lot of things I want to do.

#### Content tasks

I think that it's necessary to go through all the listed apps again, to check for broken links (and maybe add some), review the descriptions and check whether they have been packaged meanwhile.

Also, I want to add freedesktop.org [categories](https://specifications.freedesktop.org/menu-spec/latest/apa.html) and [Additional categories](https://specifications.freedesktop.org/menu-spec/latest/apas02.html). Transitioning the "License" field over to [SPDX identifiers](https://spdx.org/licenses/) is also planned.
Adding URLs for appstream yaml files (per project) is another task, but I am not sure whether I will take this on with this revision.

#### Development tasks

As mentioned above, I tried to add appstream:// links. Even after reading the [documentation of the linkify JavaScript library](https://soapbox.github.io/linkifyjs/docs/) that generates links on LINMOBapps, I have no clue how to accomplish this. _Help is highly appreciated!_

That aside, it's time to transition over to something different. There's been a plan for a long time, if you're interested in helping _cahfofpai_[^3] and me, [check the WIP project](https://framagit.org/app-directory/app-directory.frama.io) out. As with all things LINMOBapps, help is highly appreciated! 

In fact, I might be able to even "pay" major contributors for this – I'll use the LBRY fantasy money and potential BTC donations (yes, LINMOBapps has a Bitcoin address now) for this.

### Conclusion

A lot has been added, and there's a lot to do. I hope to make this a tradition and share progress and plans again in two months.

[^1]: I initially planned to add screenshots for everyone of these apps, but I sadly did not have the time to do so.
[^2]: Thanks to The3DmaN for adding these apps!
[^3]: You may remember cahfofpai from [MGLapps](https://mglapps.frama.io).
