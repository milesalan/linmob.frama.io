+++
title = "Tiny robot: Android ported to the HP Veer"
aliases = ["2012/05/14/tiny-robot-android-ported-to-hp-veer.html"]
author = "peter"
date = "2012-05-14T17:12:00Z"
layout = "post"
[taxonomies]
tags = ["Android", "Android ports", "HP Veer", "webOS",  "Open webOS", "platforms", "Video"]
categories = ["shortform"]
+++
<p>Tiny-Droid: Installing Android On The HP Veer (by <a href="https://www.youtube.com/watch?v=-6hbmUWKwtU">pocketnowvideo</a>)</p>

This is really interesting: A port of Android to webOS hardware. Surely, most HP/Open webOS enthusiasts would totally prefer this happening the other way round, but still it's great to see an almost working port created by nice folks from China. The best bit: You've got the good old webOS gestures!

If you want to install this on your HP Veer (it does not replace webOS (dual boot installation), check the above linked source of the video or the <a href="http://xndcn.github.com/android-on-veer/english.html">project page</a>.
