+++
title = "Rethinking my current computing line up (Part I)"
aliases = ["2010/03/10/rethinking-my-current-computing-line-up-part-i"]
author = "peter"
date = "2010-03-10T14:57:00Z"
layout = "post"
[taxonomies]
tags = ["Asus Eee PC 1001p", "computing", "decisions", "HP 6715b", "htc", "Neo Freerunner", "One_A110", "openmoko", "personal", "T-Mobile G1"]
categories = ["personal", "hardware"]
+++
Let's skip the introduction, here's what I currently have (heavy usage leads to bold fonts):

* HP Compaq 6715b (15.4 inch WSXGA+ AMD Turion64 X2 notebook)
* __Asus Eee PC 1001p (10.1 inch WSVGA Pine Trail netbook)__
* One A120 (7 inch VIA C7 netbook)

* __T-Mobile G1 / HTC Dream (3.2 inch HVGA smartphone)__
* Openmoko FreeRunner (2.8 inch VGA open source smartphone)

So why am I writing this? Actually I ask myself whether I really need all these devices. During the last weeks, I just used the HP 6715b just as a video playing device, meaning I used it to watch TV or DVDs, as it has a great screen and better than average speakers. I didn't really use it as a computer besides for short websurfing sessions&mdash;since I've got my new netbook, or better since I am used to my netbooks keyboard, I 've found myself using the netbook for websurfing and office work&mdash;I really love that thing, especially since I managed to make Linux work on it just perfectly. 
Yeah, I have to admit it: I am set to sell the HP 6715b again, which I bought used in december. While it is a nice device, I do not like the keyboard that much (it is ok, but worse than the keyboard of my previous notebook (HP Compaq nx6325)&mdash;and Linux does not run that well. While I managed to silence the roaring fans (I cleaned them and replaced the heat paste), I have no idea how to fix the really bad USB performance of the device&mdash;I don't think this is a hardware issue, as USB performance is ok in Vista (which I find horrible, otherwise). 
So it is very likely that I will sell this notebook pretty soon. But what will I do then? Living without a notebook, just with my Eee PC as only computing device? Actually though I really like my 1001P, I doubt that I will do that, just because of the screen size: 10.1 inch is not the screen resolution you want to work with for several hours. Besides: I am not really sure whether the Atoms performance is really enough for me. It might very well be, but I have my doubts.

Then I've got this old, 2008 VIA powered netbook, which is, as you might imagine, pretty slow&mdash;and 7 inch is really small. I don't know what to do with that one. I most likely won't make much money by selling it, and as I kind of like it  (and still have parts I wanted to solder into it once), I might finally apply the touchscreen mod and use it as music player (or even to create music using software synthesizers) or W-Lan router / home server.

While I really love my HTC Dream (even though it could be slimmer, have a better camera and ... well, it could be a _Milestone_, you know ;-) ), I don't know what to do with that Openmoko FreeRunner. It is a nice device, and I got to be able to try out the various flavours of mobile linux distributions (like Hackable:1, QtMoko, and SHR), and back then the Openmoko world was different, as GTA03/3D7K, meant as a follow up to the FreeRunner was not cancelled yet. Buying a broken FreeRunner to fix it was a start into the world of Free Software telephony&mdash;on an open device. Today I have to admit that I barely use the Freerunner. It feels nice and solid when you hold in your hand, but as standby times are short and phone calls work, but still could work better and standby battery life is rather dissapointing (I didn't apply the #1024 bug fix yet) I rather use my Motorola A910 for my secondary SIM, which runs Linux as well (though the rest of the software is rather proprietary). If I really wanted to use this phone, I would have to sell my HTC Dream (without getting a &#8220;milestony&#8221; replacement)&mdash;because right now I have no real use case for that one besides trying out some distributions every quarter. 

Now that is what I've got. What to change? (Suggestions welcome!) Part 2 will be about options.
