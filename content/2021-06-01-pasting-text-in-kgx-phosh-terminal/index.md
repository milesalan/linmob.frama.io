+++
title = "Pasting text in kgx"
aliases = ["2021/06/01/pasting-text-in-kgx.html"]
date = "2021-06-01T22:26:18Z"
[taxonomies]
tags = ["PinePhone", "Phosh", "Librem 5", "postmarketOS", "kgx", "fun",]
categories = ["howto"]
[extra]
author = "Peter"
+++
I've come across this question quite often, and today is the day to finally answer it, once and for all.
<!-- more -->
"kgx" is the name of the default terminal app for Phosh. On Mobian, Fedora and OpenSUSE it's referred to as "Kings Cross" (and thus often causing confusion for new users), on other distributions it comes with a standard terminal icon and is aptly named "Terminal".


### Textual explanation

There may be more ways to do this (e.g. just _long pressing_ until that Paste menu comes up), but I usually go over to the special set of keys of the terminal layout (behind the `>_` key), hit the menu button and guess what? A context menu appears, allowing you to paste whatever you may have in your clipboard!

### Visual explanation

If that was not clear enough, check out this GIF:

[![GIF demoing how to paste in kgx, the Phosh terminal](terminalpasting_small.gif)](terminalpasting.gif)

