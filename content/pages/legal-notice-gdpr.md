+++
title = "Legal Notice and GDPR conformity"
template = "single.html"
path = "legal-notice-gdpr"
date = "2021-01-01"
+++
_German law requires this, please don't ask and don't be creepy._

## IMPRESSUM | Legal notice

### Angaben gemäß § 5 TMG:

Peter Mack

Clemensstr. 74

80796 München

#### Kontakt:
Telefon:	089/21550345

E-Mail:		legal@linmob.net

### Verantwortlich für den Inhalt nach § 55 Abs. 2 RStV:

Peter Mack

Clemensstr. 74

80796 München

### Quellenangaben für die verwendeten Bilder und Grafiken:

Bildquellen sind unter den Bildern vermerkt, sofern diese nicht selbsterstellt sind.

### GDPR 

The web server (the website is hosted on [Uberspace.de](https://uberspace.de/) and a data processing agreement (DPA) is in place) writes log files with the accesses and the IP addresses of the accessers. These are used for troubleshooting in case of a crisis and are otherwise not evaluated. Since there are no user accounts and no tracking cookies or similar, I do not consider these log files as personal.
 
Apart from that, no data is collected, stored or evaluated.

### Datenschutzerklärung (German)

Der Webserver (die Webseite wird bei [Uberspace.de](https://uberspace.de/) gehostet und ein Auftragsverarbeitungsvertrag liegt vor) schreibt Logdateien mit den Zugriffen und den IP-Adressen der Zugreifer. Diese dienen der Fehlersuche im Krisenfall und werden ansonsten nicht ausgewertet. Da es hier keine Benutzerkonten und keine Tracking-Cookies o.ä. gibt, betrachte ich diese Logdateien nicht als personenbezogen.
 
Abgesehen davon werden keine Daten erhoben, gespeichert oder ausgewertet.

