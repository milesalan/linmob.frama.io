+++
title = "About" 
path = "about" 
template = "single.html"
pagenate_by = 0
+++

### What is LINMOB.net?

LINMOB.net is a blog on LINux on MOBile devices, that has been around for more than 10 years - on and off. Now, with new exciting products like the Pine64 PinePhone and Purisms Librem 5 scheduled to be shipped later this year, it has come back to report on all things GNU/Linux on mobile devices such as smartphones, tablets and smaller notebooks.

This site is currently hosted on [Framagit](https://framagit.org/linmob/linmob.frama.io). 

### Who am I?

I am Peter, 36, and have been following Linux on Smartphones for quite some time. My day job has nothing to do with IT. I have a business diploma and my thesis discussed whether Bitcoin has a "fundamental value".
If you want to learn more about me, maybe give the first episode of the PineTalk podcast a listen.

### Disclosures

I was a co-host for thirteen episodes on the then  bi-weekly [PineTalk Podcast](https://www.pine64.org/pinetalk), a PINE64 community podcast, from January to July 2021. This was pro-bono community work and it should not affect my view on the PINE64 products or competing Linux Phones/handheld devices in any way.

### More Content

#### Shortform
You can follow updates and shortform related content that is not worth a blog post on [Twitter](https://twitter.com/linmobblog) `@linmobblog` or in the [Fediverse](https://fosstodon.org/@linmob) `@linmob@fosstodon.org`. 

#### Videos
If you like visual content, make sure to check out my [video content](https://linmob.net/videos/)[^1].

#### App List
I also maintain a list of Apps for GNU/Linux Smartphone distributions at [LINMOBapps.frama.io](https://linmobapps.frama.io). Work on a better website is going on, you can find the current state at [alpha.linuxphoneapps.org](https://alpha.linuxphoneapps.org).

### Why do I do this?
After first trying Linux in the early 2000s, I switched to mostly using Linux around 2005. I was just amazed by all the great Free Software tools. During these years, mobile phones became more interesting: Cameras, colored displays and Smartphones. 

Of course I wanted to have all the great tools of my Linux desktop on my phone, too![^2] Unfortunately, Motorola's EZX phones only shipped a very few native apps. That's why I was very excited when I first heard of OpenMoko.

Unfortunately, OpenMoko eventually failed, unable to ship GTA03, which would have been a massive improvement, fixing most of the flaws of the previous devices. Also, Android took off. Back then, Google felt less problematic to me, and I was glad that it was Android that managed to succeed and not the less user-friendly alternatives like LiMo (which eventually became Tizen) or evil Microsoft's Windows Phone.

Purism's Librem 5 got me really excited when I heard the first rumors before announcement. I backed it in October 2017, and unfortunately Purism's time frame turned out to have been overly optimistic. PINE64 managed to ship their PinePhone earlier, and when I received my first PinePhone I immediately knew that I had to create content about this project and revive this blog.

TL,DR: This is a passion project!

### How can I contribute?

First: I am not in this for the money. I do not expect to make even one Dollar or Euro out of this project – I have a day-job, that has nothing to do with what I am working on here, which pays well enough that I can spend my free time and some money on this project.[^3]

If you want to contribute money, please support the people that develop software for the PinePhone and other Platforms, e.g.

* [UBports, who build Ubuntu Touch](https://ubports.com/donate),
* [Ondřej Jirman (Megi)](https://xnux.eu/contribute.html#toc-donations), who works on the PinePhone kernel,
* [Mobian, a great Debian-based distribution](https://liberapay.com/mobian/donate),
* [postmarketOS, the real Linux distribution for phonesi](https://postmarketos.org/donate.html), that runs (well, boots) on more phones every day,
* [Plasma Mobile](https://www.plasma-mobile.org/findyourway/#!/rootgroup/outreach), who build a great GUI for phones,
* [Lune OS](https://pivotce.com/author/webosports/), who are keeping webOS alive, and
* individual app developers!

If I've missed a great project that has a donation link and should be added here in your opinion, please get in touch.

#### Content

If you have an idea for an article or have actually written something, you are welcome to send it to me via [email](mailto:articles@linmob.net). I can't offer payment, but you'll be credited. I am going to get back to you as soon as I can.

#### App List: LINMOBapps/LinuxPhoneApps.org

See [CONTRIBUTING.md](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/contributing.md) or check for open [issues](https://framagit.org/linmobapps/linmobapps.frama.io/-/issues) for what you could help out with.
If you want to help with LinuxPhoneApps, please check the [README](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/blob/main/README.md) and the open [issues](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/issues) on that project and [its subprojects](https://framagit.org/linuxphoneapps).



####  Otherwise...

Please lend your time to the projects you like, try to communicate in a friendly way and be helpful. There are a million ways to contribute to opensource, by reporting, triaging bugs, by improving documentation or translations. Thanks!

### Current Issues with Bing (and Bing-based search engines)

Since May 2021 this website has been delisted by Microsoft Bing (and importantly Bing-based search engines, e.g. ecosia, DuckDuckGo and more). I have appealed this twice without success (seemingly unrelated to that linmob.net was listed again for five days in July), and [the second time the process was so dissatisfying](https://fosstodon.org/@linmob/106987445312737202) that I doubt I'll try again. 

So if you use a Bing-based search engine, keep in mind that you won't find my content on it. You can search this site on the [archive page](https://linmob.net/tags/) if you have JavaScript enabled.



[^1]: If the audio volume is too low on my older videos on LBRY or YouTube, please check these videos out on PeerTube, where I carefully re-uploaded my videos after increasing the volume.

[^2]: Bad J2ME app support and horrible platforms like Qualcomm's BREW on BenQ/Siemens phones contributed to this. 

[^3]: If you would like to hire me for a job that is closer to what I am working here, that would still allow me to continue with this, feel free to get in touch.
