+++
title = "Videos" 
description = "This page lists all videos I created for my LINMOB channels about Linux on Smartphones"
path = "videos" 
template = "single.html"
updated = "2022-03-03"
[extra]
update_note = "Added links to 'Updating PinePhone (Pro) Modem Firmware with GNOME Firmware Updater'."
+++

This page lists all my videos about Linux on Smartphones.

## By Distribution

### Arch Linux ARM/DanctNIX mobile

* PinePhone Software Progress: Phosh's App Drawer, Video Acceleration, Kasts and more (July 30th, 2021): [DevTube](https://devtube.dev-wiki.de/videos/watch/095bc753-7cf3-42cf-94a6-8ee7c289377e), [Odysee](https://odysee.com/@linmob:3/pinephone-software-progress-phosh's-app:3), [YouTube](https://www.youtube.com/watch?v=C4RC3Miuo2A).
* DanctNIX Mobile/Arch Linux ARM for PinePhone and LINMOBapps (October 24th, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/76e3029b-4f1f-4db5-a70e-7a1009062154), [Odysee](https://odysee.com/@linmob:3/danctnix-mobile-arch-linux-arm-for:e), [YouTube](https://www.youtube.com/watch?v=dxup2c9aNzE).
* PinePhone: Installing Anbox on Arch Linux ARM, trying WhatsApp and Signal (September 6th, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/219f7716-f00e-470f-b490-553d01bfe16a), [Odysee](https://odysee.com/@linmob:3/pinephone-installing-anbox-on-arch-linux:f), [YouTube](https://www.youtube.com/watch?v=Y-9Wmki7DsU).
* PinePhone Convergence with Phosh and Editing .desktop files: [DevTube](https://devtube.dev-wiki.de/videos/watch/25d2e06b-ff3f-4ffa-a8cf-56f034e37199), [Odysee](https://odysee.com/@linmob:3/pinephone-convergence-with-phosh-and:f), [YouTube](https://www.youtube.com/watch?v=y9pOCXy0Q94).


### AVMultiPhone (discontinued)

* AVMultiPhone - (MATE) Desktop Linux on the PinePhone (August 23rd, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/4ee7c8e7-7d01-4863-98d1-3965477567d9), [Odysee](https://odysee.com/@linmob:3/avmultiphone-mate-desktop-linux-on-the:3), [YouTube](https://www.youtube.com/watch?v=UNaftRCCRP8).

### Fedora

* Fedora on the PinePhone: Pipewire calling! (February 4th, 2021): [DevTube](https://devtube.dev-wiki.de/videos/watch/982d6f1c-91be-4426-be9e-6f0ecfe9b3ad), [Odysee](https://odysee.com/@linmob:3/fedora-on-the-pinephone-pipewire-calling:1), [YouTube](https://www.youtube.com/watch?v=TcLnC74Vo7c).


### Gentoo

* :( (No video yet.)

### GloDroid

* Android on the PinePhone: Installing GloDroid (June 1st, 2021): [DevTube](https://devtube.dev-wiki.de/videos/watch/c001bbad-1010-495c-957f-56d84f24e7f1), [Odysee](https://odysee.com/@linmob:3/android-on-the-pinephone-installing:9), [YouTube](https://www.youtube.com/watch?v=ZdmBq0rFPa8).

### KDE Neon

* The current state of Plasma Mobile on the PinePhone (October 23rd, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/a4f1e6ef-41f5-41ab-a8bf-0e80e3c3d513), [Odysee](https://odysee.com/@linmob:3/the-current-state-of-plasma-mobile-on:3), [YouTube](https://www.youtube.com/watch?v=Pz9DgFRmOMQ).
* KDE Neon Plasma Mobile on the PinePhone (July 28th, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/ba179b60-b386-48c1-b2ea-e499c5981f51), [Odysee](https://odysee.com/@linmob:3/kde-neon-plasma-mobile-on-the-pinephone:b), [YouTube](https://www.youtube.com/watch?v=oVcYaCgwN-U).

### LuneOS

* webOS in 2020 - LuneOS on the PinePhone (August 22th, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/71801e9b-bd0e-432f-ad52-a02019306724), [Odysee](https://odysee.com/@linmob:3/webos-in-2020-luneos-on-the-pinephone:c), [YouTube](https://www.youtube.com/watch?v=IQB1QACgZOU).

### Maemo Leste

* Best of LINMOB Live #1: Maemo Leste on the PinePhone (December 4th, 2029): [DevTube](https://devtube.dev-wiki.de/videos/watch/aec95d0e-b8cf-4091-ba8e-4f2d487c7a97), [Odysee](https://odysee.com/@linmob:3/best-of-limmob-live-1-maemo-leste-on-the:a), [YouTube](https://www.youtube.com/watch?v=xN5HTNcWyL0).
* Looking at some PinePhone distributions and a closer look at postmarketOS (June 25th, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/8fb14a1e-2587-4ae5-b680-002bf045d8ae), [Odysee](https://odysee.com/@linmob:3/looking-at-some-pinephone-distributions:a), [YouTube](https://www.youtube.com/watch?v=JzazgbL4u6U).

### Manjaro Lomiri

* Manjaro Daily Dev Builds for PinePhone - a look at Lomiri, Phosh and Plasma Mobile (February 19th, 2021): [DevTube](https://devtube.dev-wiki.de/videos/watch/95834c0c-7416-46cf-ac7f-46677a92acc2), [Odysee](https://odysee.com/@linmob:3/manjaro-daily-dev-builds-for-pinephone-a:1), [YouTube](https://www.youtube.com/watch?v=cW224e-4ZYE).
* Another look at Manjaro Lomiri on the PinePhone (January 12th, 2021): [DevTube](https://devtube.dev-wiki.de/videos/watch/094d745f-afcd-413d-b72b-1fd5ea0338e6), [Odysee](https://odysee.com/@linmob:3/another-look-at-manjaro-lomiri-on-the:c), [YouTube](https://www.youtube.com/watch?v=nGU6_Lxjw0k).
* PinePhone: Manjaro Lomiri Alpha 2 (November 25th, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/571cfca0-2bc5-4c79-9568-96f919ad7af7), [Odysee](https://odysee.com/@linmob:3/pinephone-manjaro-lomiri-alpha-2:0), [YouTube](https://www.youtube.com/watch?v=KJHn-Oec-YI).
* Manjaro Lomiri Alpha 1 on the PinePhone (September 20th, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/0c0929a2-ed18-4aa4-bfbe-2b13a1ea6d5b), [Odysee](https://odysee.com/@linmob:3/manjaro-lomiri-alpha-1-on-the-pinephone:9), [YouTube](https://www.youtube.com/watch?v=sOinLw0xP3c).


### Manjaro Phosh

* Manjaro Daily Dev Builds for PinePhone - a look at Lomiri, Phosh and Plasma Mobile (February 19th, 2021): [DevTube](https://devtube.dev-wiki.de/videos/watch/95834c0c-7416-46cf-ac7f-46677a92acc2), [Odysee](https://odysee.com/@linmob:3/manjaro-daily-dev-builds-for-pinephone-a:1), [YouTube](https://www.youtube.com/watch?v=cW224e-4ZYE).
* Manjaro Phosh Alpha 4: Anbox Community Service (Anbox on PinePhone III) (September 30th, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/bfc84568-27eb-43e5-8315-d26460206eed), [Odysee](https://odysee.com/@linmob:3/manjaro-phosh-alpha-4-anbox-community:3), [YouTube](https://www.youtube.com/watch?v=8Sha3R4PKSs).
* PinePhone: Manjaro Phosh Alpha 4 Walkthrough + GNOME 3.38 App improvements (September 23rd, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/b7df80ad-f751-43a2-bf92-79653fa8ac23), [Odysee](https://odysee.com/@linmob:3/pinephone-manjaro-phosh-alpha-4:b), [YouTube](https://www.youtube.com/watch?v=VefFyTF3c-I).


### Manjaro Plasma Mobile

* Unboxing the PinePhone Pro Explorer Edition (Febuary 1st, 2022): [DevTube](https://devtube.dev-wiki.de/videos/watch/7dc2ea65-2561-42af-a9d6-57b65edcad1b), [Odysee](https://odysee.com/@linmob:3/unboxing-the-pinephone-pro-explorer:1), [YouTube](https://www.youtube.com/watch?v=h6C2UatW8tQ).
* Manjaro Daily Dev Builds for PinePhone - a look at Lomiri, Phosh and Plasma Mobile (February 19th, 2021): [DevTube](https://devtube.dev-wiki.de/videos/watch/95834c0c-7416-46cf-ac7f-46677a92acc2), [Odysee](https://odysee.com/@linmob:3/manjaro-daily-dev-builds-for-pinephone-a:1), [YouTube](https://www.youtube.com/watch?v=cW224e-4ZYE).
* Getting started with the KDE Community Edition PinePhone (January 22th, 2021): [DevTube](https://devtube.dev-wiki.de/videos/watch/e32832c3-55e5-4483-b906-5164be4c7839), [Odysee](https://odysee.com/@linmob:3/getting-started-with-the-kde-community:1), [YouTube](https://www.youtube.com/watch?v=1uUMiNS5rlw).
* PinePhone: Manjaro Plasma Mobile (soon on the KDE Community Edition) (December 2nd, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/5c73aab1-cb41-4354-81fe-a56a36324806), [Odysee](https://odysee.com/@linmob:3/pinephone-manjaro-plasma-mobile-soon-on:d), [YouTube](https://www.youtube.com/watch?v=eJ8V0lRxKgM).
* The current state of Plasma Mobile on the PinePhone (October 23rd, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/a4f1e6ef-41f5-41ab-a8bf-0e80e3c3d513), [Odysee](https://odysee.com/@linmob:3/the-current-state-of-plasma-mobile-on:3), [YouTube](https://www.youtube.com/watch?v=Pz9DgFRmOMQ).
* PinePhone - Manjaro Plasma Mobile Alpha 6 (August 13th, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/78d04170-015f-4955-a650-8d400ff491c4), [Odysee](https://odysee.com/@linmob:3/pinephone-manjaro-plasma-mobile-alpha-6:4), [YouTube](https://www.youtube.com/watch?v=Wn7bliZrHJ8).

### Mobian

* Mobian on the Purism Librem 5 Evergreen (March 20th, 2021): [DevTube](https://devtube.dev-wiki.de/videos/watch/2addeee5-0352-4c7b-b7ed-f44a140b9dfb), [Odysee](https://odysee.com/@linmob:3/mobian-on-the-purism-librem-5-evergreen:f), [YouTube](https://www.youtube.com/watch?v=7_4zCOTELlU).
* Anbox on Mobian on the PinePhone (August 14th, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/a5fc224c-004f-406f-bb85-f2262636b352), [Odysee](https://odysee.com/@linmob:3/anbox-on-mobian-on-the-pinephone:d), [YouTube](https://www.youtube.com/watch?v=v06KUrfs69k).
* Looking at some PinePhone distributions and a closer look at postmarketOS (June 25th, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/8fb14a1e-2587-4ae5-b680-002bf045d8ae), [Odysee](https://odysee.com/@linmob:3/looking-at-some-pinephone-distributions:a), [YouTube](https://www.youtube.com/watch?v=JzazgbL4u6U).
* PinePhone Daily Driver Challenge / Taking pictures on Mobian (July 9th, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/3caf242e-c215-4ed6-a073-59c6e163ecc8), [Odysee](https://odysee.com/@linmob:3/pinephone-daily-driver-challenge-taking:c), [YouTube](https://www.youtube.com/watch?v=0Ju6szQebPk).



### Nemo Mobile

* Nemo Mobile on the PinePhone (October 29th, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/08b7d4e6-f736-47cc-a25e-c05be5c2165b), [Odysee](https://odysee.com/@linmob:3/nemo-mobile-on-the-pinephone:6), [YouTube](https://www.youtube.com/watch?v=mKHuZch0kF4).

### Nix OS

* :( (No video yet.)

### OpenMandriva

* :( (No video yet, I am waiting for their second image.)

### openSUSE

* OpenSUSE Plasma Mobile on the PinePhone (April 21st, 2021): [DevTube](https://devtube.dev-wiki.de/videos/watch/672dde9a-772f-41ad-a09f-5128e235fb89), [Odysee](https://odysee.com/@linmob:3/opensuse-plasma-mobile-on-the-pinephone:6), [YouTube](https://www.youtube.com/watch?v=eCrGi8hsGhY).
* Revisiting openSUSE on the PinePhone (January 11th, 2021): [DevTube](https://devtube.dev-wiki.de/videos/watch/ee717e12-5db7-496f-9a21-15eea82e6d9c), [Odysee](https://odysee.com/@linmob:3/revisiting-opensuse-on-the-pinephone:e), [YouTube](https://www.youtube.com/watch?v=Zac6WgYIn28).
* slem.os: OpenSUSE on the PinePhone (November 12th, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/ee924f40-b82e-435d-ac4a-70dd7e6bdfc2), [Odysee](https://odysee.com/@linmob:3/slem-os-opensuse-on-the-pinephone:a), [YouTube](https://www.youtube.com/watch?v=G8mR52ulDvw).

### postmarketOS Phosh

* Updating PinePhone (Pro) Modem Firmware with GNOME Firmware Updater (February 26th, 2022): [TilVids](https://tilvids.com/w/2htMubjLy6Wh6yeX3jaQJa), [Odysee](https://odysee.com/@linmob:3/updating-pinephone-(pro)-modem-firmware:3), [YouTube](https://www.youtube.com/watch?v=IsFbVZsQJX4).
* postmarketOS with Phosh on the bq Aquaris X5 (November 6th, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/59bafeef-9942-4c40-ac39-390995982e5a), [Odysee](https://odysee.com/@linmob:3/postmarketos-with-phosh-on-the-bq:c), [YouTube](https://www.youtube.com/watch?v=ZZR3yAamwXA).
* Unboxing the Pine64 PinePhone postmarketOS Community Edition Convergence Package (September 18th, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/78d04170-015f-4955-a650-8d400ff491c4), [Odysee](https://odysee.com/@linmob:3/unboxing-the-pine64-pinephone:6), [YouTube](https://www.youtube.com/watch?v=g2w0p_gzpds).
* Looking at some PinePhone distributions and a closer look at postmarketOS (June 25th, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/8fb14a1e-2587-4ae5-b680-002bf045d8ae), [Odysee](https://odysee.com/@linmob:3/looking-at-some-pinephone-distributions:a), [YouTube](https://www.youtube.com/watch?v=JzazgbL4u6U).

### postmarketOS Plasma Mobile

* Installing postmarketOS with Plasma Mobile on the Motorola Moto G4 Play (Harpia) (May 21st, 2021): [DevTube](https://devtube.dev-wiki.de/videos/watch/c3d3cbf0-e3c5-43ed-86a0-84d90c0ba9bc), [Odysee](https://odysee.com/@linmob:3/installing-postmarketos-with-plasma:0), [YouTube](https://www.youtube.com/watch?v=C4LKllQT_GQ).
* The current state of Plasma Mobile on the PinePhone (October 23rd, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/a4f1e6ef-41f5-41ab-a8bf-0e80e3c3d513), [Odysee](https://odysee.com/@linmob:3/the-current-state-of-plasma-mobile-on:3), [YouTube](https://www.youtube.com/watch?v=Pz9DgFRmOMQ).
* Looking at some PinePhone distributions and a closer look at postmarketOS (June 25th, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/8fb14a1e-2587-4ae5-b680-002bf045d8ae), [Odysee](https://odysee.com/@linmob:3/looking-at-some-pinephone-distributions:a), [YouTube](https://www.youtube.com/watch?v=JzazgbL4u6U).

### PureOS

* Librem 5 with PureOS Byzantium: Work in Progress + AppStream Metadata nerdery (May 22nd, 2021): [DevTube](https://devtube.dev-wiki.de/videos/watch/9ccf19be-92f9-4096-907f-eeace8f11563), [Odysee](https://odysee.com/@linmob:3/librem-5-with-pureos-byzantium-work-in:a), [YouTube](https://www.youtube.com/watch?v=zEsu9GFGzns).
* Unboxing the Purism Librem 5 Evergreen (January 28th, 2021): [DevTube](https://devtube.dev-wiki.de/videos/watch/66ea3360-2b2a-4e3e-8796-de7ae271ac1b), [Odysee](https://odysee.com/@linmob:3/unboxing-the-purism-librem-5-evergreen:e), [Youtube](https://www.youtube.com/watch?v=5WdnPw537nU).
* PureOS on the PinePhone? Sure! (December 20th, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/278f6f88-cfc8-405d-9efb-fbbaef8fef50), [Odysee](https://odysee.com/@linmob:3/pureos-on-the-pinephone-sure:5), [YouTube](https://www.youtube.com/watch?v=oT9XUkui4zs).
* SuperTuxKart on PureOS on the PinePhone (December 20th, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/b7cc9c6a-f16d-442e-97b3-a686129f2662).
* Looking at some PinePhone distributions and a closer look at postmarketOS (June 25th, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/8fb14a1e-2587-4ae5-b680-002bf045d8ae), [Odysee](https://odysee.com/@linmob:3/looking-at-some-pinephone-distributions:a), [YouTube](https://www.youtube.com/watch?v=JzazgbL4u6U).

### Sailfish OS

* Sailfish OS 4.0.1.48 on the PinePhone (March 5th, 2021): [DevTube](https://devtube.dev-wiki.de/videos/watch/b8810def-644e-4448-b24e-4d33d6a6fa9a), [Odysee](https://odysee.com/@linmob:3/sailfish-os-4.0.1.48-on-the-pinephone:9), [YouTube](https://www.youtube.com/watch?v=DcdNwlmG1yk). 
* Sailfish OS on the PinePhone (October 18th, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/3fb19774-6956-4ce3-ab40-f4ff50ce0c64), [Odysee](https://odysee.com/@linmob:3/sailfish-os-on-the-pinephone:0), [YouTube](https://www.youtube.com/watch?v=hZevZqXlNXc).
* Looking at some PinePhone distributions and a closer look at postmarketOS (June 25th, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/8fb14a1e-2587-4ae5-b680-002bf045d8ae), [Odysee](https://odysee.com/@linmob:3/looking-at-some-pinephone-distributions:a), [YouTube](https://www.youtube.com/watch?v=JzazgbL4u6U).

### Sxmo

* Sxmo: Simple X Mobile on PinePhone (November 12th, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/91c96859-0a65-401a-bc11-8aa0645ea676), [Odysee](https://odysee.com/@linmob:3/sxmo-simple-x-mobile-on-pinephone:5), [YouTube](https://www.youtube.com/watch?v=c0-JatWCDuo). 
* Looking at some PinePhone distributions and a closer look at postmarketOS (June 25th, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/8fb14a1e-2587-4ae5-b680-002bf045d8ae), [Odysee](https://odysee.com/@linmob:3/looking-at-some-pinephone-distributions:a), [YouTube](https://www.youtube.com/watch?v=JzazgbL4u6U).

### Ubuntu Touch

* Looking at some PinePhone distributions and a closer look at postmarketOS (June 25th, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/8fb14a1e-2587-4ae5-b680-002bf045d8ae), [Odysee](https://odysee.com/@linmob:3/looking-at-some-pinephone-distributions:a), [YouTube](https://www.youtube.com/watch?v=JzazgbL4u6U).
* PinePhone UBPorts Community Edition Unboxing (June 9th, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/9154dc88-3d5d-468c-a640-cad19f68cea4), [Odysee](https://odysee.com/@linmob:3/pinephone-ubports-community-edition:1), [YouTube](https://www.youtube.com/watch?v=uhx7XelZuZk).

## Topical videos [^1]

### Anbox

* Manjaro Phosh Alpha 4: Anbox Community Service (Anbox on PinePhone III) (September 30th, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/bfc84568-27eb-43e5-8315-d26460206eed), [Odysee](https://odysee.com/@linmob:3/manjaro-phosh-alpha-4-anbox-community:3), [YouTube](https://www.youtube.com/watch?v=8Sha3R4PKSs).
* PinePhone: Installing Anbox on Arch Linux ARM, trying WhatsApp and Signal (September 6th, 2020):[DevTube](https://devtube.dev-wiki.de/videos/watch/219f7716-f00e-470f-b490-553d01bfe16a), [Odysee](https://odysee.com/@linmob:3/pinephone-installing-anbox-on-arch-linux:f), [YouTube](https://www.youtube.com/watch?v=Y-9Wmki7DsU).
* Anbox on Mobian on the PinePhone (August 14th, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/a5fc224c-004f-406f-bb85-f2262636b352), [Odysee](https://odysee.com/@linmob:3/anbox-on-mobian-on-the-pinephone:d), [YouTube]https://www.youtube.com/watch?v=v06KUrfs69k).

### Convergence

#### Plasma Mobile
* Manjaro Plasma Mobile on the PinePhone: Convergence (Dev build 201107) (November 10th, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/1d7249d4-e577-4c8c-9f64-74855792f019), [Odysee](https://odysee.com/@linmob:3/manjaro-plasma-mobile-on-the-pinephone:e), [YouTube](https://www.youtube.com/watch?v=OgEnSd5w4cQ).

#### Phosh
* PinePhone Convergence with Phosh and Editing .desktop files: [DevTube](https://devtube.dev-wiki.de/videos/watch/25d2e06b-ff3f-4ffa-a8cf-56f034e37199), [Odysee](https://odysee.com/@linmob:3/pinephone-convergence-with-phosh-and:f), [YouTube](https://www.youtube.com/watch?v=y9pOCXy0Q94).

### PinePhone Accessories
* PinePhone (Pro) External Batteries: PinePhone Keyboard vs. 8500 mAh S 20 Ultra Battery case: [TilVids](https://tilvids.com/w/w3qXbr31SE21jGxnM8A1r7), [Odysee](https://odysee.com/@linmob:3/pinephone-(pro)-external-batteries:2), [YouTube](https://www.youtube.com/watch?v=M5T_Wol-tHQ).

### Fake-Convergence: Running proper desktops on the Phone while it's connected

#### GNOME
* Best of LINMOB Live #1: postmarketOS GNOME on the PinePhone (December 3rd, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/8ffb56cb-31bb-4998-925c-d8cfb6f7b0ae), [Odysee](https://odysee.com/@linmob:3/best-of-limmob-live-1-postmarketos-gnome:0), [YouTube](https://www.youtube.com/watch?v=1TZm-ZPlToE).

#### Plasma Desktop
* PinePhone: Plasma Desktop and the Convergence Dock (December 5th, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/d96f2590-31f7-4928-abbc-79e3526d877c), [Odysee](https://odysee.com/@linmob:3/pinephone-plasma-desktop-and-the:2), [YouTube](https://www.youtube.com/watch?v=f2Mmd3gRaTs).


### Flashing the Phone 
* PinePhone: Flashing the PinePhone from the PinePhone (December 28th, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/010aa5d1-ecdf-434e-aa92-ebad781d75af), [Odysee](https://odysee.com/@linmob:3/pinephone-flashing-the-pinephone-from:a), [YouTube](https://www.youtube.com/watch?v=VPV024bRZ_4)

### Flashing Modem Firmware
* Updating PinePhone (Pro) Modem Firmware with GNOME Firmware Updater (February 26th, 2022): [TilVids](https://tilvids.com/w/2htMubjLy6Wh6yeX3jaQJa), [Odysee](https://odysee.com/@linmob:3/updating-pinephone-(pro)-modem-firmware:3), [YouTube](https://www.youtube.com/watch?v=IsFbVZsQJX4).

### Firefox: Advanced Configuration
* Firefox Profiles and Site Specific Browsers on the PinePhone (January 9th, 2021): [DevTube](https://devtube.dev-wiki.de/videos/watch/165ca7c9-aa35-4b07-867f-74064542c9fd), [Odysee](https://odysee.com/@linmob:3/firefox-profiles-and-site-specific:c), [YouTube](https://www.youtube.com/watch?v=h7aLE8jBOPc).


### Launcher clean-up (removing non-needed launchers)

* PinePhone Convergence with Phosh and Editing .desktop files: [DevTube](https://devtube.dev-wiki.de/videos/watch/25d2e06b-ff3f-4ffa-a8cf-56f034e37199), [Odysee](https://odysee.com/@linmob:3/pinephone-convergence-with-phosh-and:f), [YouTube](https://www.youtube.com/watch?v=y9pOCXy0Q94).

### Getting started
* Getting started with the KDE Community Edition PinePhone (January 22th, 2021): [DevTube](https://devtube.dev-wiki.de/videos/watch/e32832c3-55e5-4483-b906-5164be4c7839), [Odysee](https://odysee.com/@linmob:3/getting-started-with-the-kde-community:1), [YouTube](https://www.youtube.com/watch?v=1uUMiNS5rlw).

### Maps and Navigation

* PinePhone Daily Driver Challenge - Maps and Navigation (July 17th, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/ad95eb65-64bb-4c7c-83a9-b05a16f3c80d), [Odysee](https://odysee.com/@linmob:3/pinephone-daily-driver-challenge-maps:3), [YouTube](https://www.youtube.com/watch?v=WvGNXBntkp0).

### Qt/Plasma Mobile apps on Phosh (mixing worlds)

* PinePhone DDC - Qt/Plasma Mobile Apps on Phosh (August 1st, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/fd220d83-c880-488b-bf0b-9d243331a25c), [Odysee](https://odysee.com/@linmob:3/pinephone-ddc-qt-plasma-mobile-apps-on:2), [YouTube](https://www.youtube.com/watch?v=Ka9zh0Vt3zM).

## Live Stream recordings[^2] 

* LINMOBlive 3: Librem 5 impressions and PinePhone news (February 17th, 2021): [Odysee](https://odysee.com/@linmob:3/linmoblive3-librem5-and-pinephone:4)
* LINMOBlive 2: Manjaro Lomiri and random unboxing (January 12th, 2021): [Odysee](https://odysee.com/@linmob:3/linmoblive-2-manjaro-lomiri-and-random:d).
* LINMOBlive 1: Fun with Megi's 2020/11/23 p-boot-demo image and the Lap Dock (November 30th, 2020): [DevTube](https://devtube.dev-wiki.de/videos/watch/ff0b9166-e57f-419d-9bc4-95c39a896814).

[^1]: Limited to videos that are not just about distributions, but show stuff that might be helpful or focusses on a cross-distribution topic.

[^2]: There have been more Live Streams  (they all happen on YouTube for now) that I did not make available. Future uncut recordings are going to be Odysee exclusives.
