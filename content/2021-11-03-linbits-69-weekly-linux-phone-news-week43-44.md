+++
title = "LinBits 69: Weekly Linux Phone news / media roundup (week 43/44)"
date = "2021-11-03T21:52:00Z"
draft = false
[taxonomies]
tags = ["PinePhone", "PinePhone Pro", "Maemo Leste", "SailfishOS", "Nemo Mobile","Phosh",]
categories = ["weekly update"]
[extra]
author = "Peter"
+++

_This Week in Mobile Linux:_ Phosh 0.14.0 delivers a bunch of quality of life improvements, Maemo Leste shares a massive progress report, Jolla present Sailfish OS 4.3.0 and more!<!-- more --> _Commentary in italics._ 


### Software releases
* [Phosh 0.14 has been released](https://gitlab.gnome.org/World/Phosh/phosh/-/releases#v0.14.0), delivering launch splash support, media player widget improvements, a wifi hotspot mode indicator and music player pausing on headphone disconnect. Its also making the mobile friendly launcher feature more understandable by adding a "Show all apps" toggle, alongside other visual improvements `scale-to-fit has been reworked so that it seems to work with proper app IDs (e.g. scale-to-fit org.gnome.Evolution on instead of scale-to-fit evolution on previously).
* [GNOME Calls 41.1 is out](https://gitlab.gnome.org/GNOME/calls/-/tags/41.1), delivering multiple important fixes.

### Worth noting
* Kasts is implementing even more awesome features, [this time synchronization features to fit your multi device lifestyle!](https://fosstodon.org/@mogwai/107192263697737595).
* Martijn Braam made a [postmarketOS Cheatsheet](https://fosstodon.org/@martijnbraam/107213998586359237). _Nice!_
* Nemo Mobile have added [two apps](https://twitter.com/neochapay/status/1455580281168179201)!
* If you are dailying your PinePhone (or are close to it), maybe chime in in this [Reddit thread](https://www.reddit.com/r/PINE64official/comments/ql8dio/what_os_is_everyone_using_as_a_daily_driver_on/).

### Worth reading

#### Software progress
* Maemo Leste: [Fifteenth Update: Januari - October 2021](https://maemo-leste.github.io/maemo-leste-fifteenth-update-januari-october-2021.html). _A huge update with lots of big and smaller things in it, including work on phone and messaging GUI. Make sure to read this, if you long for a N900 revival!_
* Jolla on Sailfish OS Forum: [[Release notes] Suomenlinna 4.3.0](https://forum.sailfishos.org/t/release-notes-suomenlinna-4-3-0/8495). _Lots of changes. Watch Leszek's video below to get a better idea which of these changes make a difference._
* This Week in GNOME: [#16 Card-Carrying](https://thisweek.gnome.org/posts/2021/10/twig-16/). _More movement toward GTK4+libadwaita!_
* Christopher Davis: [Announcing Solanum](https://blogs.gnome.org/christopherdavis/2021/11/02/announcing-solanum/). 
* Carl Schwan: [KDE PIM in September and October](https://carlschwan.eu/2021/11/02/kde-pim-in-september-and-october/).
* Phoronix: [Firefox 94.0 Released With Linux Build Using EGL For Better Performance, Power Savings](https://www.phoronix.com/scan.php?page=news_item&px=Firefox-94.0-Released). _I think we've been having this acceleration for quite a while on PinePhone, nice to see it land for everybody!_

#### PINE64 Announcements
* PINE64: [October Update Follow-up](https://www.pine64.org/2021/10/29/october-update-follow-up/). _Great to see the keyboard accessory being available soon! Now stop teasing and take my money! Also, I am delighted to hear that "new PineTalk is doing great, it's well deserved!_

#### Repair reports
* xnux.eu log: [Fixing broken Pinephone, aka Pinephone repairability](https://xnux.eu/log/#051). _Nice repair report by Megi!_

#### Tiny SBCs for fun projects
* Liliputing: [Raspberry Pi Zero 2 W is a tiny $15 computer with 5X the performance of its predecessor](https://liliputing.com/2021/10/raspberry-pi-zero-2-w-is-a-tiny-15-computer-with-5x-the-performance-of-its-predecessor.html). _Honestly, I almost find the Radxa Zero more interesting - but before you start dreaming, remember that this does can't drive an internal display._

#### Purism and repairability
* Kyle Rankin for Purism: [Beyond Right to Repair](https://puri.sm/posts/beyond-right-to-repair/).

### Worth listening
* PEBKAC: [S01E03 Manjaro ARM Interview](https://pebkac.show/2021/11/02/pebkac-s01e03-manjaro-arm-interview/). _Another great news round-up followed by a fun conversation. Give it a listen!_

### Worth watching
#### PinePhone Pro
* PizzaLovingNerd: [Everything We Know About The PinePhone Pro](https://odysee.com/@pizzalovingnerd:5/everything-we-know-about-the-pinephone:4). _Nice roundup!_

#### App Videos
* Niccolò Ve: [KDE Telegram Client? Meet TOK!](https://tube.kockatoo.org/w/kmsaS5tJTaB5AZNRujdRAd).

#### Sailfish OS
* Leszek Lesner: [SailfishOS 4.3 - What's new!? Short Overview](https://www.youtube.com/watch?v=ERoMw1e-qnc). _Nice overview of the new Early Access release of SailfishOS 4.3._
* Jolla: [Sailing for 10 years Berlin, October 14, 2021](https://www.youtube.com/watch?v=Z6bGOHhwO7w). _This is the nice video that previously was only available on Facebook._

#### JingOS
* The Linux Experience: [JingPad A1 Review: Flagship Hardware, but that software...](https://www.youtube.com/watch?v=LIKfXbwzfXE). _The upside is: Software can still be easily improved after shipping the hardware out._
* JingOS: [Running Android Apps on JingOS, JingPad A1](https://www.youtube.com/watch?v=YTuAIaSHh8Y). _Not bad!_

#### Gaming
* u/stopcomputing: [Morrowind running on my Pinephone](https://www.reddit.com/r/PINE64official/comments/qichm5/morrowind_running_on_my_pinephone/).

#### Shorts
* Tino Tech Tips: [Librem 5 flackert an externem Bildschirm](https://www.youtube.com/watch?v=akg_mMYEugw). _Flickering!_

### Stuff I did

#### Content

None :( Well, I went live last thursday, but I likely won't manage to edit the recording any time soon. The "Linux Phone Setup" post is still in the works (and already quite long), I hope to finish it this week. I also plan to update [this post](https://linmob.net/pinephone-setup-scaling-in-phosh/) to reflect the changes Phosh 0.14.0 brings.

#### Random

* There's a fun (?) [opportunity to vote](https://fosstodon.org/@linmob/107214138457810806) on my near Linux future!

#### LINMOBapps and Linux Phone Apps

Nothing, sadly. This is what happens when you wind down for annual celebratorial live events (and have a lot to do at the day job): You get nothing done!
