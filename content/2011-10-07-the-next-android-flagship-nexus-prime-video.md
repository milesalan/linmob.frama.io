+++
title = "The next Android flagship: Nexus Prime (Video)"
aliases = ["2011/10/next-android-flagship-nexus-prime-video.html"]
date = "2011-10-07T11:00:00Z"
layout = "post"
[taxonomies]
tags = ["Android", "Android 2.3 Gingerbread", "Android 3.0 (Honeycomb)", "flagship device", "Ice Cream Sandwich", "mobile devices", "nexus prime", "video"]
[extra]
author = "peter"
2021_commentary = "Sadly, the video is no longer available."
+++


_I haven´t been writing much lately, especially not about Android devices&mdash;I felt _really bored_ by all the new devices that did not change much._ 
<!-- more -->
__Update (10/08/2011):__ The launch of the next Android flagship has been postponed because of Steve Jobs death&mdash;a gentle act. The launch will take place in London, October 27th. 

BTW: _I don't feel like writing a tribute to Steve Jobs, so many others have and there is not a word that I could add to praise this great visionary without whom we likely wouldn't be where we are in computing today. When I read that message in the morning on my way to work in the subway, I almost started to cry&mdash;something that happens really rarely to me._
_Thanks for everything, Steve. Thanks for reinventing computing every once in a while, making the use of computers a joyful thing to everybody!_

Most of the new devices are pretty good, at least those that you can consider flagship devices&mdash;and if you are not into spending much money for such a useless thing as a smartphone (or simply don´t have that much money at hand), there are plenty of OK mid range devices.

Soon this age of Android boredom is going to end, new devices are going to be released, but more importantly, the Android platform (the software, to make it perfectly clear what I mean) is going to be renewed with the next iteration of Android that is supposed to be numbered 4.0 and has the code name / branch name Ice Cream Sandwhich.

Ice Cream Sandwhich will reintegrate the two branches of Android we have right now: 3.* Honeycomb, which was newly developed for the special needs of tablet devices (bigger screen ;) ) and Android 2.3.* Gingerbread, which feels like it´s been around forever (actually, this is almost true: It was announced in December 2010). 

New software&mdash;this implies new flagship devices. One of these will be the device rumored to be called the Nexus Prime (other rumors say the Samsung made devices will be named &#8220;Samsung Galaxy Nexus&#8221; (_what a stupid name!_)).

Specs are rumored <a href="http://lmgtfy.com/?q=nexus+prime+specs">everywhere in the internets</a> and honestly, I believe that they are accurate, but don´t feel like spreading them before the actual announcement at Samsungs Unpacked event on monday (October 11th, 2011).

Here however, is something more interesting a video of <b>ICS</b> running on the smartphone believed to be the next android flagship smartphone.

That´s what I wrote all this for. More information and opinion after the official announcement!

<b>VIA:</b> <a href="http://thisismynext.com/2011/10/07/galaxy-nexus-ice-cream-sandwich-video/">thisismynext.com</a></p>
