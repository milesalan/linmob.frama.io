+++
title = "Another device"
aliases = ["2008/01/another-device.html"]
author = "peter"
date = "2008-01-14T10:03:00Z"
layout = "post"
[taxonomies]
tags = ["E28", "E2831", "neuf", "platforms"]
categories = ["hardware", "personal"]
+++
Well, I'm pretty sure that you're able to guess, what I did.
I just got my self another Linux-based cellphone.
Yes, it supports WLan.
Correct, it is the E28 E2831.
<!-- more -->

It isn't here yet, I assume it is on it's way to me from france.
Needless to mention I'm exited.

Having had a first look at the devices firmware, I have to say, that applications development for this device will be even more difficult than it is with Motorola's EZX-phones.
The applications appear to be packed in *.elf files, and I found no libraries sounding like they would belong to GTK+ or Qt. I found traces of &#8220;Montavista&#8221;.. but that isn't a real surprise.

The kernel, BTW, is a 2.4.20, and it's a TI OMAP 730 based device.
