+++
title = "Android on Netbooks"
aliases = ["2009/01/05/android-on-netbooks"]
author = "peter"
date = "2009-01-05T15:42:00Z"
layout = "post"
[taxonomies]
tags = ["Android", "FSO", "netbooks", "One_A120", "OpenEZX"]
categories = ["software", "commentary",]
+++
I know that there are some people who dislike Android and exspecially the idea of cloud computing, which is due to Google Docs, which is in turn in a way a part of Android. But Android has the potential to be a very powerful platform, as you can use the same app for Android on x86, ARM and other platforms, as far as the Android platform is ported to it&mdash;of course recompiling can do the same with any open source programs as well, but we all know that for people used to propriety software this is a great problem, and as most people are used to that this is a barrier to market share. On the other hand the SDK is made for all popular pc platforms, so anybody who is ready to invest time in learning how to program for Android just needs to download and install SDK&mdash;which can mean, that more apps are created and apps may make a platform more attractive&mdash;which leads to more market share as well.

So, making a long story short: I like other platforms better than Android, that's for sure, I like OpenMoko's efforts, as they base on the „classical“ open source software, the good old free software GNU tools and such. But Android has the potential to become a pretty powerful platform (if it isn't yet), and the potential to great market share&mdash;not only on handsets and pda's (I admit: PDAs without phone functions are a pretty dead thing) and, as we've seen <a href="http://www.netbooknews.de/index.php?s=android&amp;x=0&amp;y=0">recently</a> netbooks.

I would like to run Android on my EZX phones (since the Cupcake branch is the main branch, it is able to work on my E680i, since it has a touchscreen keyboard) and my HTC Universal, and on my VIA C7 powered Quanta IL1-based ONE A120*, as an alternative to my currently used Sidux/Debian XFCE desktop. But I don't know whether I have the time to even try[^1] to port it. 

[^1]: As far as it survives my soldering attempts (USB Hub, BT, Touchscreen, USB SD-Card Reader with 16GB SD).
