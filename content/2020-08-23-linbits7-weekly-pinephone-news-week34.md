+++
title = "LinBits 7: Weekly PinePhone news / media roundup (week 34)"
aliases = ["2020/08/23/linbits7-weekly-pinephone-news-week34.html", "linbits7"]
author = "Peter"
date = "2020-08-23T15:45:00Z"
layout = "post"
[taxonomies]
tags = ["Pine64", "PinePhone", "Ubuntu Touch", "Arch Linux ARM", "Manjaro", "Anbox", "SPURV", "adb", "Emacs", "Fedora", "U-Boot", "SailfishOS", "Mer", "Nemo Mobile", "community ports", "debconf", "kill-switches"]
categories = ["weekly update"]
+++

_It's sunday. Now what happened since last sunday?_

This is what happened around the PinePhone this week. _Commentary in italics._
<!-- more -->
### Software: releases and improvements
* New [Arch Linux ARM release](https://github.com/dreemurrs-embedded/Pine64-Arch/releases/tag/20200822). _Gnome Camera, Geary and Gnome Usage have been added, sudo is now a thing, and lz4 compression for zram is now available, making the zram-feature more useful. Also, there is a barebones image for all those who want to build their own little fun distro atop arch._
* [Manjaro have released images with Phosh that feature Anbox preinstalled](https://osdn.net/projects/manjaro-arm/storage/pinephone/phosh/alpha2-20200822/).  _Unfortunately, with `adb` currently broken in Manjaro (and Arch), this is nothing more than a demo on which you can't really install apps without jumping through a ton of hoops. At least Manjaro comes with a swap partition by design, which certainly helps._
* [Scardracs](https://www.scardracs.com/fedora-per-pinephone-parte-2/), fellow Italian blogger, has picked up Fedora for the PinePhone and released a couple builds this week on a [Telegram channel](http://t.me/fdxpp). 
* [EmacsはOS](https://twitter.com/maeda_/status/1295414523537453056). _Unfortunately, there don't seem to be any images to just simply get this up and running. It seems to be an amalgam of Arch Linux ARM, sway, Squeekboard, Waybar and Emacs._


### Worth reading
* Blogger Bust: [How To Update U-Boot For PostmarketOS On The Pine Phone ](https://bloggerbust.ca/post/how-to-update-uboot-for-postmarketos-on-the-pinephone/). _This is a really technical long read, but it is totally worth your time._
* SailfishOS Forums: [Changes needed to merge the project names to Sailfish OS](https://forum.sailfishos.org/t/changes-needed-to-merge-the-project-names-to-sailfish-os/1672). _Jolla is going to turn off a lot of their infrastructure, including build servers, in the process of moving projects over to GitHub. This will affect the open source base project [Mer](http://www.merproject.org/), which has been in the process of being rebranded to Sailfish; and thus affect projects like [NemoMobile](https://nemomobile.net/) which are based on Mer and share the infrastructure. It is also likely to negatively affect SailfishOS community ports. Sad._
* FOSSphones: [How to Change Operating Systems on Your PinePhone](http://fossphones.com/changing-pinephone-os/). _Helpful!_
* Phoronix: [Mozilla's Incredible Speech-To-Text Engine Is At Risk Following Layoffs](https://www.phoronix.com/scan.php?page=news_item&px=DeepSpeech-At-Risk). _Another sad bit of fall-out from the Mozilla Layoffs, which might impact Speech-to-Text solutions for the Pinephone._

### Worth watching
* silver: [Installing silvercam on a PinePhone](https://vimeo.com/448379583).
* John Sullivan: [Void on pinephone demo](https://www.youtube.com/watch?v=VjjHmefdNuI). _Great watch, nice to see that the HP Lap Dock x3 works with the PinePhone._
* Дмитрий Куртуков: [Inside PINEPHONE keyboard](https://www.youtube.com/watch?v=8lcKObWnAKA). _A look at how the keyboard that we've seen a couple times now has been hacked together._
* PizzaLovingNerd: [6 Essential Ubuntu Touch Apps](https://www.youtube.com/watch?v=dVz0aJG4Gac). _A great, well produced little video, featuring some must-have apps for Ubuntu Touch._ 
* TWiT Tech Podcast Network: [The Linux-Based PinePhone Has DIP Switches](https://www.youtube.com/watch?v=Mm3HqtrXiOk). _It's always great to see more mainstream outlets pick up on the PinePhone._
* Howto: Privacy & Infosec: [Pinephone Killswitch Tool- Household Item!](https://www.youtube.com/watch?v=VFX4WbtpGRI). _How to kill-switch!_
* Camden Bruce: [Running a Java Minecraft server off a pinephone](https://www.youtube.com/watch?v=P9Qj9kY9G4Y), [Running a SuperTuxKart server off a pinephone](https://www.youtube.com/watch?v=Gj7bB8G2gxg). _Yes, the PinePhone is just another Linux computer, so it totally can run your Minecraft or SuperTuxKart server._

### Coming up
* [debconf 20, the 21st Debian annual conference](https://debconf20.debconf.org/) is starting today, with a ton of interesting talks. Watch out for [this one](https://debconf20.debconf.org/talks/13-my-phone-runs-debian-and-it-does-phone-calls/) on Wednesday if you like Phosh.
* [Linux Plumbers Conference](https://linuxplumbersconf.org/event/7/) happens from August 24th to 28th. On Friday, the 28th at 15:30 UTC [Bushan Shah will give a talk about Plasma on Mobile devices](https://linuxplumbersconf.org/event/7/contributions/835/).

### Stuff I did
* I failed to build Maliit on Arch Linux ARM and at getting Lomiri to run on Manjaro. 
* Also, not much progress on the App List/App DB front. I am really not sure how to set this up &mdash; as a part of this blog or as a seperate thing, maybe running Wiki software to make adding apps easier for those who are not familiar with GitHub? I hope I will figure this out this week. 
* At least I managed to upload [two](https://www.youtube.com/watch?v=UNaftRCCRP8) [videos](https://www.youtube.com/watch?v=oVcYaCgwN-U).
* That's not all I want to do, I will try to test some Android apps in Anbox on camera, namely Whatsapp and NewPipe. I might try GloDroid 0.3.0 finally, and then someone on reddit told me about [SPURV](https://gitlab.collabora.com/spurv), an effort to run Android 9 alongside Wayland by Collabora, for which they really [picked a bad announcement date](https://www.collabora.com/news-and-blog/blog/2019/04/01/running-android-next-to-wayland/) with April Fools day. Unfortunately, building SPURV appears to be a bit beyond my abilities, so I hope I can avoid wasting time on it.

