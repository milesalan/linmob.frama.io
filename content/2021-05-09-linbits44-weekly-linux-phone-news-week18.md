+++
title = "LinBits 44: Weekly Linux Phone news / media roundup (week 18)"
aliases = ["2021/05/09/linbits44-weekly-linux-phone-news-week18.html"]
date = "2021-05-09T21:59:00Z"
[taxonomies]
tags = ["PINE64", "PinePhone", "Purism", "Librem 5", "LINMOBapps", "postmarketOS", "camera", "Megapixels", "Mobian", "Ubuntu Touch",]
categories = ["weekly update"]
[extra]
author = "Peter"
+++

_It's sunday. Now what happened since last sunday?_

Megapixels gets a lot better, postmarketOS releases another service pack, Mobian runs on Snapdragon devices, and more!<!-- more --> _Commentary in italics._


### Software development and releases
* Megapixels has been ported to GTK4 and released in [1.0](https://git.sr.ht/~martijnbraam/megapixels/refs/1.0.0), with version  [1.0.1](https://git.sr.ht/~martijnbraam/megapixels/refs/1.0.1) quickly following.  _It's so much better, do give it a try!_
* postmarketOS: [v21.03 service pack 1 release](https://postmarketos.org/blog/2021/05/09/v21.03.1-release/). _They are bringing some new software to their beta channel, so that it does not feel too stale._

### Worth noting
* Librem 5 takes [photos](https://social.librem.one/@dos/106173399329034794) [now](https://social.librem.one/@dos/106170001244660464)! 
* [Caleb](https://nitter.nixnet.services/calebccff/status/1389623485568327691#m): Mobian is now running the [OnePlus 6](https://images.mobian-project.org/oneplus6/)/[6t](https://images.mobian-project.org/oneplus6t/), [Pocofone F1](https://images.mobian-project.org/pocof1/). 

### Worth reading 
* Purism: [Librem 5 News Summary: April 2021](https://puri.sm/posts/librem-5-news-summary-april-2021/). _Good news with regards to software development, but on the hardware side? I quote "we are pausing Librem 5 production until October". Because of the component shortage you may have heard of. This is really bad, and it also affects my original October 23rd, 2017 order._
* TuxPhones: [postmarketOS is finally coming to wearables](https://tuxphones.com/postmarketos-linux-targets-smartwatches-wearables-watch-friendly-asteroid-os-ui/). _This is really cool. I won't try it though, I already have more smartwatches than I can wear._
#### Software corner
* Mobile NixOS: [March and April 2021 round-up](https://mobile.nixos.org/news/2021-05-04-march-april-2021-round-up.html). _A nice update by the NixOS people._
* Linux Smartphones: [Megapixels 1.0 released, bringing hardware accelerated graphics to the camera app for Linux phones](https://linuxsmartphones.com/megapixels-1-0-released-bringing-hardware-accelerated-graphics-to-the-camera-app-for-linux-phones/). _A post on the release mentioned above._
* Linux Smartphones: [Ubuntu Touch OTA-17 brings initial support for NFC and other improvements (coming May 12, available for testing now)](https://linuxsmartphones.com/ubuntu-touch-ota-17-brings-initial-support-for-nfc-and-other-improvements-coming-may-12-available-for-testing-now/). _Great write up of OTA-17 features. Remember, the PinePhone is not on the OTA cycle._
* Julian's Code Code: [The Internals of Fractal-Next](https://blogs.gnome.org/jsparber/2021/05/07/the-internals-of-fractal-next/). _A nice post discussing the future of Fractal. Give it a read if you are interested in GTK4, Rust or Matrix!_
* Foss Adventures: [Trying out Phosh shell on PinePhone and openSUSE Tumbleweed](https://www.fossadventures.com/trying-out-phosh-shell-on-pinephone-and-opensuse-tumbleweed/). _Fun read._

#### Hardware corner
* DEV.to/pcvonz: [I²C on the Pinephone](https://dev.to/pcvonz/i-c-on-the-pinephone-5090). _For those who like to tinker!_


### Worth listening
* PineTalk [008: After √64 (O'Clock)](https://www.pine64.org/podcast/008-after-sqrt64-oclock/). _This was fun to record, even though it was past 1 am on my end. Ezra and I talk about many topics that have been in past LinBits or are in this one, and share our first impressions of the PineTime smartwatch._

### Worth watching
* Niccolò Ve: [Kirigami Application for Recording, Alarms and Timers, RSS and Much More!](https://www.youtube.com/watch?v=vjFy_EwfE-Y). _If you still don't know what Kirigami is and why it is great, maybe this video is going to help you!_
* Pako St: [PinePhone FluffyChat demo](https://www.youtube.com/watch?v=6FUPBnyYcwo). _Now you could use that [Flathub build](https://flathub.org/apps/search/fluffychat), but this is more fun!_
* Sk4zZi0uS: [PinePhone Challenge Continued - One Phone Many Distros with P-Boot](https://www.youtube.com/watch?v=4_N-InBQ68w). _It really saddens me that the p-boot-demo has not been updated since November, and thus really can't be recommended to getting an idea of the current state of most projects._
* Pear Crew: [How to Install postmarketOS on a Poco F1](https://www.youtube.com/watch?v=Rh0tA9-tVXE). _Well, they use Windows to do that, but other than that it's a nice video!_
* Martijn Braam: [From 0 to Phosh on the LG K10](https://www.youtube.com/watch?v=cZKmmp36OBo). _Nice video by Martijn! Fun fact: I was planning to do a similar video with a different device this week, but gladly did not get to it._
* UBports: [Ubuntu Touch Q&A 100](https://www.youtube.com/watch?v=CIX-a-i6B1w). _This time, the community answers questions. They also discuss future challenges they expect to happen._

#### PinePhone Beta Impressions
* Geotechland: [PinePhone BETA Edition Review](https://www.youtube.com/watch?v=IJEoWR3trNg). _Great 
* HyperVegan: [First Impressions of Pinephone with Manjaro KDE Plasma](https://www.youtube.com/watch?v=YjrWeyULq3M). _Nice video!_


### Stuff I did

#### Content
I wrote [one blog post](https://linmob.net/linmobapps-additions-changes-march-april-2021/), that I should have split up in hindsight, as it discusses the apps added to LINMOBapps in the past months _and_ the future.


#### LINMOBapps
I did a lot of work on LINMOBapps this week (but promoted none of it on Social Media. I added links to AppStream metadata files and fixed the appstream:// links, so if you're lucky, you can now open GNOME Software or Discover from your web browser. I also added the following apps this week: 
* Keepassk, a WIP Kirigami KeePass client written in Rust,
* FluffyChat, a Matrix client that uses Flutter,
* Kasts, a Kirigami Podcast client that saves where you paused playback and thus is the best PinePhone podcast app already,
* Amazfish, a smartwatch client I believed to have added a long time ago,
* Tinge, a GTK Hue lamp client, 
* Hermes Messenger, for those who want to use (Facebook) Messenger,
* Stream, a nice GTK based YouTube client, developed by Purism's CEO, and
* Wike (a Wikipedia browser).

[A lot else here what (else) happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master). 
Please [do contribute!](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/CONTRIBUTING.md) _One task that would be important is checking whether all these License identifiers in about the first half of the list are actually correct._
