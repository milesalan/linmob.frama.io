+++
title = "Mer on N900"
aliases = ["2011/11/06/mer-on-n900.html", "2011/11/06/having-an-n900-and-already-trying-out-nitdroid-i.html"]
author = "peter"
date = "2011-11-06T19:44:00Z"
layout = "post"
[taxonomy]
tags = ["hacking", "MeeGo", "Mer", "N900", "Nemo handset UX", "Nokia N900", "platforms"]
categories = ["shortform"]
+++
Having an N900 (and already trying out NitDroid (I recommend <a href="http://talk.maemo.org/showthread.php?t=72365&amp;page=1">this how to</a> if you&#8217;re eager to check NitDroid out) I came around this video, which shows about the latest stuff that we used to call MeeGo before Tizen was started. It&#8217;s now <a href="http://www.merproject.org">Mer</a> again, and the handset UX has the new name Nemo.

* Carsten Munk (YouTube): [Nemo Handset UI on Mer 0.20111021.1 on Nokia N900](https://www.youtube.com/watch?v=Q9HAyU1krJc)
