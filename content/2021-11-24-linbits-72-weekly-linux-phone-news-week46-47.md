+++
title = "LinBits 72: Weekly Linux Phone news / media roundup (week 46/47)"
date = "2021-11-24T21:57:00Z"
draft = false
[taxonomies]
tags = ["PinePhone", "PinePhone Pro","DanctNIX", "Nemo Mobile", "Librem 5", "Purism", "JingPad", "LINMOBapps"]
categories = ["weekly update"]
[extra]
author = "Peter"
+++
This week in Linux Phones: Android 12 coming to PinePhone, NemoMobile 0.7, Ubuntu Touch OTA 20, postmarketOS on the Pocket P.C., and more!
<!-- more --> _Commentary in italics._ 

### Software releases
* [Ubuntu Touch OTA-20 Release](https://ubports.com/blog/ubports-news-1/post/ubuntu-touch-ota-20-release-3790#).
* Danct12 has [released new builds](https://github.com/dreemurrs-embedded/Pine64-Arch/releases/tag/20211121) of his Arch Linux ARM based distribution, including a build for the PinePhone Pro, bringing all the software up to the latest version. 
* NemoMobile have released a new [image](https://img.nemomobile.net/2021.10/Manjaro-ARM-nemomobile-pinephone-0.7.img.xz), dubbed 0.7. _While it's not ready for daily use yet, they've made major progress! [Do support them!](https://liberapay.com/NemoMobile/)_
* [GloDroid v. 0.7.0](https://github.com/GloDroid/glodroid_manifest/releases) brings Android 12 to the PinePhone!


### Worth noting
* Martijn Braam has brought [postmarketOS with Phosh to the Pocket P.C.](https://fosstodon.org/web/@martijnbraam/107327809947282123), [and also Plasma Mobile and GNOME 41](https://fosstodon.org/web/@martijnbraam/107328327046742273).

### Worth reading

#### Software progress
* This Week in GNOME: [#19 Updated Calculations](https://thisweek.gnome.org/posts/2021/11/twig-19/).
* tchx84: [Flatseal 1.7.5](https://blogs.gnome.org/tchx84/2021/11/20/flatseal-1-7-5/). _Nice update!_
* Janet Blackquill: [Porting Tok To Not Linux: The Journey Of Incessant Failure](https://blog.blackquill.cc/porting-tok-to-not-linux-the-journey-of-incessant-failure). _Not about mobile Linux specifically, but still fun._
* GabMus: [Gtk4, LibAdwaita and the new Feeds](https://gabmus.org/posts/gtk4_libadwaita_and_the_new_feeds/). _Great app, great post!_
* Phoronix: [Box86 + Box64 Updated For Running Linux x86/x86_64 Programs On Other Architectures](https://www.phoronix.com/scan.php?page=news_item&px=Box86-0.2.4-Box64-0.1.6). _While Box* is something you only really need for proprietary stuff, it's awesome to see it making progress._
* Phoronix: [Alpine Linux 3.15 Released - Builds Off Linux 5.15, Drops MIPS64, SimpleDRM For FB](https://www.phoronix.com/scan.php?page=news_item&px=Alpine-Linux-3.15). _Why is this relevant? Well, the next postmarketOS stable is going to be based in this!_
* itfoss: [Distribution kit for mobile phones NemoMobile 0.7 Released](https://www.itsfoss.net/distribution-kit-for-mobile-phones-nemomobile-0-7-released/).

#### Librem 5 Corner
* Purism: [True Convergence is Here: PureOS 10 is Released for all Librem Products](https://puri.sm/posts/true-convergence-is-here-pureos-10-is-released-for-all-librem-products/).
* Liliputing: [Purism’s Librem 5 Linux smartphone costs $1199 after the latest price hike](https://liliputing.com/2021/11/purisms-librem-5-linux-smartphone-costs-1199-after-the-latest-price-hike.html). _While I get this from a accrued costs perspective, it surely won't help with sales._

#### PinePhone Pro Development
* xnux.eu log: [Pinephone Pro LCD panel](https://xnux.eu/log/#055). 
* xnux.eu log: [Pinephone Pro – audio and modem power up support](https://xnux.eu/log/#056). _Nice progress reports by Megi!_

#### Hardware: RISC-V on Phones and Tiny Intel Convertibles
* TuxPhones: [The world's first RISC-V phone might be just behind the corner](https://tuxphones.com/sipeed-rv64-first-risc-v-rv64-phone-linux-2022-2023/). _I don't want to be negative, but I doubt this RISC-V phone is something you are actually going to want to use if you're not totally psyched by everything RISC-V._
* Liliputing: [Running Linux on the GPD Pocket 3 mini-laptop](https://liliputing.com/2021/11/running-linux-on-the-gpd-pocket-3-mini-laptop.html). 

#### Tutorials
* Samuel Norbury: [A usability hack for convergent devices](http://whatshouldyoueat.com/2021/11/21/multi-wm-linux-phone/). _This is something I thought about for a long time, and Samuel just did it. Awesome!_
* Neil Brown: [Installing Mobian with full disk encryption on PinePhone](https://neilzone.co.uk/2021/11/installing-mobian-with-full-disk-encryption-on-pinephone). 
* Han Young: [Cross Compile to PinePhone with Clang](https://www.hanyoung.uk/blog/cross-compile-to-pinephone-with-clang/). _Great tutorial!_

#### Ethical Tech Giving 
* Tux Phones: [Freedom and phone advice](https://tuxphones.com/freedom-and-phone-advice/). _Great post by Martijn!_

### Worth listening
* PEBKAC: [PEBKAC S01E04 Mid November Updates!](https://pebkac.show/2021/11/15/pebkac-s01e04-mid-november-updates/) _Well done, and thanks for the mention! (I felt that I got nothing done lately, this nugded me to revisit that assumption)._[^1]
* PineTalk: [S2E03: PineTalk - Performance, UIs, the Keyboard, Oh My!](https://www.pine64.org/2021/11/23/s2e03-pinetalk-performance-uis-the-keyboard-oh-my/) _Come for the news, stay for a great discussion! I love how Brian and Justin are doing this now!_
* postmarketOS Cast: [#11 INTERVIEW: kop316 (of mmsd-tng Fame)](https://cast.postmarketos.org/). _Great interview!_

### Worth watching

#### Learn something
* Avisando: [PinePhone "Desktop Mode" Manjaro Phosh (2021)](https://www.youtube.com/watch?v=5EK7Ua2efbE). _I love that voice!_

#### Ubuntu Touch Corner
* UBports: [OTA-20 Released | Ubuntu Touch Q&A 112](https://www.youtube.com/watch?v=PKWHX4Twu34). _After taking a break, Dalton and Marius are back to talk about OTA 20, Clickable 7 (the tool to build Ubuntu Touch apps), new apps, 20.04 progress, how they need help with device support and more._ 
* Review for Nerds: [One Month using a Linux SmartPhone](https://www.youtube.com/watch?v=4rQMMIvuDYA).

#### Android 12 on PinePhone
*  蕭博文powen: [Android 12 on pinephone,flash with Glodroid 0.7.0(still in development)](https://www.youtube.com/watch?v=ZnrZSnTBK7o). _Nice!_


#### JingPad 
* Niccolò Ve: [I Brought The JingPad To Venice! And The PineBook as well! Oh, right, my girlfriend too](https://tube.kockatoo.org/w/aeuenwrcarabMZnJ1eSiwn).

####  Nemo Mobile Shorts
*  Сергей Чуплыгин: [Voicecall work on Nemomobile PinePhone](https://www.youtube.com/watch?v=HegimsdVw_o). _Awesome!_
* Jozef Mlich: [glacier-weather demo](https://www.youtube.com/watch?v=wK_99lds7GA). _Nice app!_


### Stuff I did

#### Content
Nothing.

#### Random
I've played with Sxmo 1.6.0 and Nemo Mobile 0.7 this week, and what shall I say: Both are really smooth. Do give them a try.

#### LINMOBapps and Linux Phone Apps

This week I added 
* [Space Launch](https://gitlab.com/elescoute/spacelaunch), a GTK/libhandy app that tracks rocket launches. 

[Read here what (else) happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master) on LINMOBapps this week. And please [do contribute!](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/CONTRIBUTING.md)

[^1]: This should have been included last week (it is now), but since it was added so late it's in here, two. Just subscribe to these podcasts so that you don't have to rely on me!
