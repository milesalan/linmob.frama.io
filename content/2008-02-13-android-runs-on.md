+++
title = "Android runs on E28 E2831 aka Twin Tact"
aliases = ["2008/02/android-runs-on.html"]
date = "2008-02-13T11:16:00Z"
layout = "post"
[taxonomies]
tags = ["Android", "E28", "E2831", "MWC2k8", "platforms"]
categories = ["shortform",]
[extra]
author = "Peter"
+++
No, I don't have Android on my phone, I don't know if everything works yet, but some people of E28 ported it to this device, as the guys of <a href="http://www.phonemag.com/e28-show-working-android-cellphone-video-from-mwc-02950.php">phonemag.com</a> found out at MobileWorldCongress (I'm not there, but I'll be at CeBIT).

(via <a href="http://www.mobilelinuxinfo.com/507/google-android-touchscreen-demo/">MobileLinuxInfo</a>)

_(Maybe I'll write more on that subject later, but ATM I have no time.)_
