+++
title = "LinBits 12: Weekly PinePhone news / media roundup (week 39)"
aliases = ["2020/09/27/linbits12-weekly-pinephone-news-week39.html", "linbits12"]
author = "Peter"
date = "2020-09-27T13:00:00Z"
layout = "post"
[taxonomies]
tags = ["Pine64", "PinePhone", "Librem 5", "GloDroid", "Android 11", "GNOME Maps", "Anbox", "Peertube", "App Lists", "Camera", "Manjaro", "Plasma Mobile", "Arch Linux ARM", "Convergence", "Mumble", "LINMOBapps", "Lomiri", "Phosh", "postmarketOS", "Megapixels", "Unboxing"]
categories = ["weekly update"]
+++

_It's sunday. Now what happened since last sunday?_

A new release of #HuongTramLinux, GloDroid 0.5.1, Camera Adventures and much more. _Commentary in italics._
<!-- more -->


### Software releases and improvements
* [Arch Linux ARM/Huong Tram Linux](https://github.com/dreemurrs-embedded/Pine64-Arch/releases/tag/20200927) has had a new release, replacing Pinhole with Megapixels (which is now in version 0.9.0 and working better than ever) and fixing the GNOME Software app listings among other things. If you already have this distribution installed, a full upgrade should bring you all these new features without reinstalling. Also, [danct12 played a bit with Nemo Mobile's Glacier UX](https://nitter.net/RealDanct12/status/1307718376354054146) this week.
* [Manjaro Phosh Alpha 4 has been released](https://forum.manjaro.org/t/manjaro-arm-developer-release-with-plasma-mobile-pinephone/26733).
* [Plasma Mobile](https://www.plasma-mobile.org/) have rebased on Ubuntu 20.04, at least initially causing some regressions. Work on [working keyboard input in GTK apps](https://matrix-client.matrix.org/_matrix/media/r0/thumbnail/kde.modular.im/037c9b106f933a229f8220f544650205dbb84958?width=800&height=600) is also progressing, which is great news!
* [Phosh 0.4.4 has been released](https://social.librem.one/@agx/104901905763931217) with minor improvements.
* GloDroid, the Android <del>10</del><ins>11</ins> distribution for the PinePhone and several ARM single board computers, have [released version 0.5.1](https://github.com/GloDroid/glodroid_manifest/releases/tag/v0.5.1).


### Worth reading
* LinuxSmartphones: [Ubuntu Touch OTA-13 released with browser improvements, new features, support for more phones](https://linuxsmartphones.com/ubuntu-touch-ota-13-released-with-browser-improvements-new-features-support-for-more-phones/). _Great write up by Brad._
* LinuxSmartphones: [PinePhone news roundup (9-25-2020)](https://linuxsmartphones.com/pinephone-news-roundup-9-25-2020/). _If I should get hit by a bus or loose interest, here is a more than capable backup ..._
* TuxPhones: [Using a Google Nexus 5 as a Linux phone, in 2020](https://tuxphones.com/google-nexus-5-as-linux-phone-2020-ubports-postmarketos/). _An excellent post on using the Nexus 5 with "true Linux" distributions._
* BrixIT Blog: [PinePhone camera adventures, part 3](https://blog.brixit.nl/pinephone-camera-adventures-part-3/). _Martijn Braam, developer of [megapixels](https://git.sr.ht/~martijnbraam/megapixels) has written his third post on the PinePhone cameras. If you want to learn about the difficulties of camera stuff, this is a must read!_
* xnux.eu log: [Some ways to improve Pinephone safety](https://xnux.eu/log/#018) and [Let's talk about safety of Pinephone](https://xnux.eu/log/#017) ([hacker news comments](https://news.ycombinator.com/item?id=24596248). _The second post is older and is decidedly inflamatory – as if there is not enough hardship. That said, safety matters, although I am not aware of any house burning down because of badly configured PinePhone kernels. Also, if you look at the world as a whole, you'll find much larger health/life risks that no one seems to care about enough to effect change._
* David Field: [The PinePhone is a world of possibility thats not ready for you just yet](https://tech.davidfield.co.uk/the-pinephone-is-a-world-of-possibility-thats-not-ready-for-you-just-yet/). _If you are not sure whether to get a PinePhone or not, this post might help making a decision._

### Worth watching
* Avisando: [PinePhone - run without battery])(https://www.youtube.com/watch?v=4Urbu1Wb9CU). _A new channel that makes videos that are very much to my liking (as they focus on the device and not a talking head). I managed to become the second subscriber!_
* PineTime: [BLE HID mouse with the Pinephone running Mobian](https://video.codingfield.com/videos/watch/2989c888-6c3c-4e0a-8428-4d5af3e03f21). _I was not going to buy a PineTime, but projects like this one..._
* Fabrixxm's things: [PinePhone + BangleJS Experiment](https://peertube.uno/videos/watch/032cc2f4-8afd-46c7-aa74-91fdcf03fa6f).
* Howto: Privacy & Security: 
[Megapixel: Dramatic Pinephone Camera Software Improvements](https://www.youtube.com/watch?v=rsDP1-D8Ys4). _Megapixels is making impressive progress._
* GeoTechLand: [What’s New In Ubuntu Touch OTA-13](https://www.youtube.com/watch?v=N2HQwoE8N7U).
* UBports: [Ubuntu Touch Q&A 85](https://www.youtube.com/watch?v=h3kRM_8Wvjs). _I like watching these sessions, although I never made it live, yet._
* QtCon Brasil 2020: [Convergence with Qt](https://youtu.be/l7Rrx74W1fA). _If you want to know more about [MauiKit](https://mauikit.org/), this is a must watch!_
* Bits inside by René Rebe: [The USB-C complexities: But I found a Pinephone USB dock charging workaround!](https://www.youtube.com/watch?v=pSY7N3jIB8E) _René's streams are quite informative and technical._

### Stuff I did

* I published two videos that cover [Manjaro ARM](https://forum.manjaro.org/tag/pinephone) on the PinePhone with [Lomiri](https://www.youtube.com/watch?v=sOinLw0xP3c) and [Phosh](https://www.youtube.com/watch?v=VefFyTF3c-I), and a [blog post on the new features GNOME 3.38 brings](https://linmob.net/2020/09/25/gnome-3.38-and-linux-smartphones.html). Also, I started uploading my videos to [a platform that is not YouTube](https://devtube.dev-wiki.de/videos/watch/0c0929a2-ed18-4aa4-bfbe-2b13a1ea6d5b), adjusting loudness on the way. This process can hopefully be finished in the next week.
* Back to Manjaro Lomiri: After making that video, I managed to get Plasma Mobile apps to work by installing breeze and editing their .desktop files (in /usr/share/applications): Adding `env QT_SCREEN_SCALE_FACTORS=2 QT_STYLE_OVERRIDE=breeze` between `Exec=` and the command to execute the app [made them work just fine](https://nitter.net/linmobblog/status/1309377856972165121#m) ([short video](https://nitter.net/linmobblog/status/1308073587534946304#m)). Also, the terminal can be fixed by just choosing the `monospace` font in the settings of the app.
* Another thing that can hopefully be (initially) finished next week is a new "How to …" page that explains basic stuff for the PinePhone by linking to helpful content on this blog and elsewhere. 
* Back to last week: On Tuesday I listened live to [Linux Unplugged](https://github.com/dreemurrs-embedded/Pine64-Arch/releases/tag/20200927) and took part in the Mumble room before and after the show. I did so on my PinePhone (running Arch Linux ARM by danct12) using the "convergence dongle", my Thinkpad USB keyboard and my USB audio interface. Phosh is not really great at convergence yet and there is at least [one known issue](https://source.puri.sm/Librem5/phosh/-/issues/365) that is being worked on. I hope I can find the time to write about how to get to "convergence" and also try [sway](https://swaywm.org/) instead of phosh for convergence soon. Other people have had good results with GNOME shell, but that might be a bit heavy for the limited hardware.
* Three apps have been added to the [LINMOBapps list](https://linmobapps.frama.io): Social, Workflow and hackgregator.

