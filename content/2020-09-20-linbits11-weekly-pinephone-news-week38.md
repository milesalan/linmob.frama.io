+++
title = "LinBits 11: Weekly PinePhone news / media roundup (week 38)"
aliases = ["2020/09/20/linbits11-weekly-pinephone-news-week38.html", "linbits11"]
author = "Peter"
date = "2020-09-20T13:10:00Z"
layout = "post"
[taxonomies]
tags = ["Pine64", "PinePhone", "Librem 5", "GloDroid", "Android 11", "GNOME Maps", "Social", "gaming", "Anbox", "App Lists", "Camera", "Manjaro", "Lomiri", "Phosh", "postmarketOS", "Megapixels", "Unboxing"]
categories = ["weekly update"]
+++

_It's sunday. Now what happened since last sunday?_

Manjaro Lomiri Alpha 1, GloDroid 0.4.0, more camera improvements and a ton of videos. _Commentary in italics._
<!-- more -->

### Software releases and improvements
* [GloDroid 0.4.0](https://github.com/GloDroid/glodroid_manifest/releases/tag/v0.4.0). The Android port is progressing. _They have also released an [early build of Android 11](https://github.com/GloDroid/glodroid_manifest/issues/58#issuecomment-670354906), and [bauner in the Pine64 forums](https://forum.pine64.org/showthread.php?tid=10613&pid=78278#pid78278) is working on enabling GSM and GPS._
* [megapixels](https://git.sr.ht/~martijnbraam/megapixels) has seen a lot of progress and now has a really fast viewfinder, takes pictures at 5 megapixels, rendering the PinePhone back camera a lot more usable. _The front camera also works in postmarketOS now, but it is very dim currently and only usable in daylight._
* Manjaro [Lomiri Alpha 1](https://forum.manjaro.org/t/manjaro-arm-alpha1-with-lomiri-pinephone/26454) has been released. _I have made a [little video](https://www.youtube.com/watch?v=sOinLw0xP3c) demoing the release._ [^1]

### Other Hardware [^1]
* bunnie:studios, run by the famous bunny who created awesome hardware like the open source [Novena laptop](https://www.bunniestudios.com/blog/?p=3657), have announced a new [handheld device](https://www.bunniestudios.com/blog/?p=5921). It's called "Precursor" and is going to be sold on [Crowdsupply](https://www.crowdsupply.com/sutajio-kosagi/precursor). _If you are looking for a faster device than the PinePhone, this FPGA-based device likely is not for you. But it should be more secure._


### Worth reading
* CNX-Software: [Pinephone Multiboot Image Boots 13 Different Linux Distributions](https://www.cnx-software.com/2020/09/16/pinephone-multiboot-image-boots-13-different-linux-distributions/). _This image is really something. I managed to break it though..._
* Pine64: [September Update: Let it sink in…](https://www.pine64.org/2020/09/15/september-update-let-it-sink-in/). _
* Purism: [Adventures of porting postmarketOS to the Librem 5](https://puri.sm/posts/adventures-of-porting-postmarketos-to-the-librem-5/). _A great read!_
* Koen Vervloesem: [Exploring the PinePhone with the multi-distro demo image](https://koen.vervloesem.eu/blog/exploring-the-pinephone-with-the-multi-distro-demo-image/). _This is a little review of Megi's p_boot image, which takes a very different perspective than I would have taken: E.g. I like it when there is not too much software pre-installed, because I know how to install software on pretty much every one of these distros. PSA: Don't report bugs experienced with this image to the respective distro maintainers, as this image uses a very different kernel. Also: Screenshots on Phosh based distros can be created via a little command line utilty called 'grim'._ [^1]

### Worth watching

* Matthew Higgins: [Running OpenSUSE Linux on the Pinephone (Braveheart)](https://youtu.be/tEU0y-BDi5w). _I still did not try OpenSUSE myself, but it does not seem to bad._
* Howto: Digital Privacy & Infosec: [Pinephone (Mobian Linux) Video/Audio Proof Phone Calls Do Work Both Incoming/Outgoing](https://www.youtube.com/watch?v=Vdx-vp1doB0). _This video may seem strange, but maybe it helps some people believing me that this stuff is incredibly hard to demo while maintaining privacy._
* Howto: Digital Privacy & Infosec: [Pinephone Mobian Inkscape + Camera + Firefox, Gpodder, SMS/Phone/Cell Data/Wifi/Hotspot All Working](https://www.youtube.com/watch?v=yhgQk6wYYeg)
* sadwesv: [Accelerated video decoding on Pinephone using gstreamer 0.18 and cedrus](https://www.youtube.com/watch?v=6P76lQX70iI), [Pinephone playing 1440p h264 encoded video to external display](https://www.youtube.com/watch?v=yyRm8kccyG4), [Pinephone: 1080p@60 video playback scaled to 1440p@60 display](https://www.youtube.com/watch?v=dHOgVmxH_dA). _Megi published a couple of video showing promising progress with regard to video acceleration on the PinePhone. Looks great!_
* Josh Moore: [Pine64 PinePhone: Linux Users Remain a Persecuted Minority (For Now)](https://www.youtube.com/watch?v=F1ewLsIjqao). _The video title is weird, this is just a negative review._

#### Android Corner
* HomebrewXS: [How to install Android 11 on PinePhone](https://www.youtube.com/watch?v=WbqzMXsnEEA)
* HomebrewXS: [Android 11 on PinePhone, customizing and installing apps](https://www.youtube.com/watch?v=w9Oqlwb2iLo)
* Purism: [Anbox on the Librem 5](https://www.youtube.com/watch?v=wAk72UnwU4E). _This is a great demo of Anbox, not showing all the pain I showed in my videos. Please note that they are usinghttps://forum.manjaro.org/t/manjaro-arm-alpha1-with-lomiri-pinephone/26454 virtboard here, which is a terrible method of text input even on the slightly larger PinePhone._

#### Dev streaming corner
* Marius Gripsgård: [Hacking on Lomiri: Overview](https://youtu.be/SJ_K6XFRk84). _A longer stream for people who want to help the development of Lomiri (a.k.a. Unity 8)_
* Bits inside by René Rebe: [t2 ARM64 Linux on a Pine64 Pinephone](https://www.youtube.com/watch?v=AneXavrp8MA). 

#### Gaming corner
* Doctor Cranium: [Quake on Pinephone](https://www.youtube.com/watch?v=zUS3bg3l6XU)
* Iron Oxidizer: [PinePhone playing Super Mario 64 natively at 30fps](https://www.youtube.com/watch?v=nICNyCosbCU)
* Danct12: [Undertale on the PinePhone](https://nitter.net/RealDanct12/status/1306988588773515264)

#### Privacy corner
* Rob Braxman Tech: [Privacy. That's LinuxPhone. True? False?](https://www.youtube.com/watch?v=lTC28AWnCyk)

### Stuff I did

* I finally migrated my blog over to different hosting. It still uses Jekyll, but should be GDPR compliant now. Sadly, this will negatively impact my GitHub activity statistics. ;-)
* On friday I received my postmarketOS CE convergence package PinePhone and [made an unboxing video](https://www.youtube.com/watch?v=g2w0p_gzpds).
* I tried to build some more software. [Social](https://gitlab.gnome.org/World/Social) took long to build, but I could not authorize it to use it with my Mastodon account. Also, I build GNOME Maps 3.38, which [works a lot better on the PinePhone than the previous version](https://fosstodon.org/@linmob/104888229662272223). What's notable: The new PinePhone ran a lot cooler than my UBports CE while building these apps.

[^1]: The link to my Lomiri video, the Other Hardware section and last Worth Reading link were added after initial publication on Monday, September 21st at 8:14 AM CEST.
