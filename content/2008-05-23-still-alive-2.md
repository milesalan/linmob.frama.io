+++
title = "Still alive ;)"
aliases = ["2008/05/still-alive.html"]
date = "2008-05-23T11:00:00Z"
[taxonomies]
tags = ["LinuxTag", "openmoko", "platforms"]
categories = ["personal", "internal"]
[extra]
author = "Peter"
+++
I am still alive. I am preparing an article on how I think graphical user interfaces on (small ~3&#8221;) mobile devices should look like for about two weeks now and i am unable to get it ready, as there are innovations every day&mdash;and interesting news like <a href="http://gettingstartedopenmoko.wordpress.com/2008/05/19/openmoko-software-update/">OpenMokos' April Software Update</a>, which change some things around concerning existing solutions.
<!-- more -->

On the other hand I didn't spend much time with any of my handset devices, I upgraded my laptops hardware (RAM+harddisk) instead and switched back to linux-x86 after a short x64 episode, just because of these stupid closed source programs I use from time to time, especially <a href="http://zattoo.com/en">Zattoo</a>.

And while I'll have to travel a lot next week due to family troubles, I think of going to Berlin for <a href="http://www.linuxtag.org/2008/">LinuxTag</a>. But I am not sure whether I can really do that &#8230;</p>
