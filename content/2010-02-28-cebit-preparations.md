+++
title = "CeBIT preparations"
aliases = ["2010/02/28/cebit-preparations.html"]
author = "peter"
date = "2010-02-28T15:59:00Z"
layout = "post"
[taxonomies]
tags = ["T-Mobile G1", "Eee PC"]
categories = ["events", "personal",]
+++
I will go to CeBIT the next tuesday (or call it the day after tomorrow) and it is time for the last preparations. As I didn't switch this blog to WordPress yet (I didn't even start this process), blogging from CeBIT will be tough, though not impossible. My gear setup is ready (and not too special), it includes my new <em>ASUS Eee PC 1001P</em>, my <em>T-Mobile G1</em> and as Camera my <em>Kodak Z1085 IS</em>, as the G1's camera is just not that good. I will be 3G connected on my netbook trough my smartphone, so I might even upload some pictures from Hanover to my new flickr account (which I won't link here yet because it is still empty)&mdash;and I do believe that CeBIT will be fun. Depending on how funny (and interesting) it will be, I will spend one or two days there.

So stay tuned for CeBIT impressions ;) 

