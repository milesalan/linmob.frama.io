+++
title = "Building Software from the AUR, Part 2: Reddit clients for the PinePhone"
aliases = ["2020/10/06/reddit-clients-for-mobile-linux.html"]
author = "Peter"
date = "2020-10-06T17:32:00Z"
layout = "post"
[taxonomies]
tags = ["PinePhone", "Apps", "Gnome on Mobile", "Phosh", "Arch Linux ARM", "AUR", "reddit", "Giara", "Quickddit", "Ubuntu Touch", "Rust"]
categories = ["howto"]
+++
Reddit may not be the social network that has the friendliest reputation, but at least since Google sunset Google+, Reddit's many technical subreddits have become one of the best places to talk about new software, hardware and other nerdy things. So let's have a short overview about software that may or may not work great on a phone.

<!-- more -->

## General instructions

You will need to have an AUR helper installed or know how to install packages from the AUR without such a software. For that, you can follow the instructions of my [previous post](https://linmob.net/2020/09/05/pinephone-building-plasma-mobile-apps-from-the-aur.html#preparations), although I would now recommend to now clone `git clone https://aur.archlinux.org/yay-bin.git ` instead, because it is just faster to not build the `yay` binary on device.

## GTK based

### Giara

[Giara](https://gitlab.gnome.org/GabMus/giara) is a relatively new client developed by [Gabriele Musco](https://gabmus.org/), who has [already developed](https://gitlab.com/gabmus) a number of phone-compatible GTK+ apps. Giara will eventually be available on Flathub and thus be easily installable on pretty much every other distribution, but for now, you will have to follow one of the ways to install it below:

#### Installing from the AUR

Install is easy enough on Arch Linux (ARM) and Manjaro, as you will just have to install `giara-git` from the AUR. 

#### Installing from Flathub [^1]
Giara is now also [available from Flathub](https://flathub.org/apps/details/org.gabmus.giara). Flathub has instructions on how to enable it, and if these aren't enough, I wrote a [blog post](https://linmob.net/2020/07/27/pinephone-daily-driver-challenge-part2-flatpak-and-scaling-in-phosh.html) a while back. 

#### Screenshots

![Giara: Overview](giara_overview.jpg)
![Giara: Overview with Post menu](giara_overview_post.jpg)
![Giara: Thread View](giara_thread_view.jpg)
![Giara: New Post](giara_new_post.jpg)
### Something for Reddit

[Something for Reddit](https://github.com/samdroid-apps/something-for-reddit) is an older client and does not work too well on mobile, but it still is worth a mention, as it is pretty nice on the desktop. 

It is available on the AUR (`something-for-reddit`) and can be installed on OpenSUSE, Fedora and as a Flatpak. As the screenshots show, it works after a `scale-to-fit reddit-is-gtk on`, but everything is quite small.


#### Screenshots

![Something for Reddit: Overview](reddit-is-gtk_overview.jpg)
![Something for Reddit: Overview with Thread (landscape)](reddit-is-gtk_landscape.jpg)
![Something for Reddit: New Post](reddit-is-gtk_post.jpg)

### Work-in-Progress: reddit-client-gtk

This is another project, written in Rust, which is not feature complete yet. You can't login yet and thus not do anything which would require logging in. You can find instructions to build it [on the projects GitHub repository](https://github.com/johan-bjareholt/reddit-client-gtk).
I actually build it by running `cargo build` which led to a binary called `reddit-client-gtk-rs` in the target/debug/ subfolder.	

#### Screenshots

![reddit-client-gtk: Overview](reddit-client-gtk_overview.jpg)
![reddit-client-gtk: Going to Subreddit](reddit-client-gtk_going_to_subreddit.jpg)
![reddit-client-gtk: Thread View](reddit-client-gtk_thread_view.jpg)
![reddit-client-gtk: Not implemented](reddit-client-gtk_not_implemented.jpg)


## Qt based

### Quickddit

_Please note: Quickddit is now available on the [AUR](https://aur.archlinux.org/packages/quickddit/). If you are running Arch Linux ARM, you won't need to follow the following instructions and can just install it with_ `yay -S quickddit`_. I also made [newer screenshots](https://fosstodon.org/@linmob/105424788379486065).[^2]_

Finally, we will try to build [Quickddit](https://github.com/accumulator/Quickddit) ([OpenStore](https://open-store.io/app/quickddit)) for Ubuntu Touch on ArchLinuxARM. There's also a more advanced version of [this client for Sailfish OS](https://openrepos.net/content/accumulator/quickddit-0), but we would need to use the proprietary UI components of Sailfish, which as far as I know don't even exist for ARM64/aarchv8 systems. 

First, dependencies: All of the packages I installed for [my last building tutorial](https://linmob.net/2020/09/05/pinephone-building-plasma-mobile-apps-from-the-aur.html#installing-software-from-the-aur) were still installed. Not all of them may be needed, but unless your SD card is tiny: Just install them.

Next, were going to need `qqc2-suru-style`. This is how you get installed: 

~~~~
git clone https://github.com/ubports/qqc2-suru-style
cd qqc2-suru-style
qmake
make -j4
sudo make install
~~~~

Now that we have all the dependencies, we can get started with building Quickddit. We are building the Ubuntu Touch variant, reasons explained above.

~~~~~
git clone --recurse-submodules https://github.com/accumulator/Quickddit
vim [or nano] Quickddit/src/quickdditmanager.cpp #add API keys here, the project readme describes how to get them
~~~~~ 

After adding the API keys (which unfortunately did not lead to a working result for me), we can now build the app:

~~~~~
cd Quickddit/ubuntu-touch
qmake
make -j4
~~~~~

Normally we would run `sudo make install` now. But this install script is made for UBports Ubuntu Touch, which packages apps as _click_ packages (a precursor to snaps), and I did not even look into fixing it for "normal" distributions. Just run `./quickddit` to start the app and try signing into Reddit. To install it permanently, you would have to create a desktop file and you might want to move it to `/opt/quickddit` or something.

#### Screenshots

![Quickddit: Overview](quickddit_overview.jpg)
![Quickddit: Subreddit Menu](quickddit_menu.jpg)
![Quickddit: Thread View](quickddit_info.jpg)
![Quickddit: Sad face](quickddit_sadface.jpg)


## Conclusions

While there aren't too many reddit clients for mobile linux OS, the situation is not too bad. Giara is shaping up to be really good, and Quickddit is pretty great too (assuming logging-in would be possible like on Ubuntu Touch). "Something for Reddit" is rather a client for the desktop power-redditor than a good mobile alternative, and reddit-client-gtk, (being Rust based) is super fast and hopefully sees more development in the future. 

What do you think? Did I miss a client? If so, please get in touch on Reddit, via Twitter or Mastodon or just send me an email.   

[^1]: This section was added on October 15th, 2020.
[^2]: This section was added on December 25th, 2020.
