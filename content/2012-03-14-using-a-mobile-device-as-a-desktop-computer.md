+++
title = "Using a Mobile Device as a Desktop Computer"
aliases = ["2012/03/14/using-amobile-device-as-a-desktop-computer.html", "2012/03/14/this-is-a-cool-video-as-this-is-pretty-close-to.html"]
author = "peter"
date = "2012-03-14T18:54:00Z"
layout = "post"
[taxonomies]
tags = ["desktop android", "phones", "Video", "Samsung Galaxy Nexus", "stationary mobility", "tech"]
categories = ["shortform"]
+++

<a href="https://www.youtube.com/watch?v=_--zcmqIyRI">This</a> is a cool video, as this is pretty close to what I dreamt of years ago. Finally, phones (as the Samsung Galaxy Nexus) are powerful enough (and have the connectivity options) to do this. And it will only become easier, with ARM Cortex A15 and further, more powerful SoCs.
Make sure not to miss <a href="http://www.youtube.com/watch?v=bB-icTl2J-c">part 2</a>.
