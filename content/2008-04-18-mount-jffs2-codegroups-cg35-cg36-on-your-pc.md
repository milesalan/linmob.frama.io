+++
title = "Mount jffs2 codegroups (CG35, CG36) on your PC"
aliases = ["2008/04/mount-jffs2-codegroups-cg35-cg36-on.html"]
author = "peter"
date = "2008-04-18T14:42:00Z"
layout = "post"
[taxonomies]
tags = ["CG35", "CG36", "ezxlocal", "ezx_user", "howto", "modding"]
categories = ["projects", "howto"]
+++
_I just write this how to, as it was quite hard to find an explanation for this._
<!-- more -->

While mounting cramfs (filesystem of the other codegroups of Motorola firmware, which are Linux related) is quite easy as you can use the loop device, mounting CG35 (/ezx_user/) and CG36 (/ezxlocal/) is rather hard&mdash;jffs2 can't be mounted using a simple _mount -o loop_.

At first you have to be root. On some Linux distributions you can do that by simply entering _su_ and root's password, on Ubuntu you use _sudo su_ and your user password.

Keep in mind that in this howto the codegroup file is _cg35.smg_, it is stored in _/home/user/_

~~~~
mkdir /dev/mtdblock
cd /dev/mtdblock_
mknod 0&#160;b 31&#160;0

modprobe mtdblock

# 32768 = 32&#160;MB
modprobe mtdram total_size=32768
modprobe jffs2

cd /home/user
dd if=./cg35.smg of=/dev/mtdblock/0 bs=0 skip=1
mkdir /mnt/test1
mount -t jffs2 /dev/mtdblock/0 /mnt/test1

# copy files to a folder
mkdir /home/user/cg35
cp -d -r /mnt/test1/ /home/user/cg35/
~~~~

That's it, now you can edit the files. I did not find out yet how to repack these files; when I know how to do that, I will extend this how to.

Credits go to <a href="http://www.linuxontour.de/modules/newbb/viewtopic.php?topic_id=5273&viewmode=flat&order=ASC&type=&mode=0&start=10&PHPSESSID=658cb115cf370602630502b3615a4199">linuxontour.de</a>.


__Addendum: Old comments__

_Unknown_, March 4, 2009 at 5:35 PM

Thanks a lot, when you post how to repack?


_[Gopi Aravind](https://www.blogger.com/profile/02725014039788046653)_, January 14, 2010 at 4:13 AM

Command to repack:

~~~~
mkfs.jffs2 -l -e 0x20000 --pad -r/mnt/usb/ -o my-fs.jffs2
~~~~

From here:
[http://plugcomputer.org/plugforum/index.php?topic=29.0](http://plugcomputer.org/plugforum/index.php?topic=29.0)
