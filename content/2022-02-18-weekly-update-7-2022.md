+++
title = "Weekly Update (7/2022): Call and chat with +015550199999 or put Ubuntu Touch OTA 22 on your libhybris phone!"
date = "2022-02-18T17:52:00Z"
updated = "2022-02-19T11:15:00Z"
draft = false
[taxonomies]
tags = ["PinePhone","PINE64 Community Update","Librem 5","Ubuntu Touch","XMPP","3d printing",]
categories = ["weekly update"]
[extra]
author = "Peter"
update_note = "Added some more stories and announced the post on social media."
+++

A rather boring week: People love handhelds with keyboards, an unofficial PinePhone battery case, a translucent backcover and a little bit of software news. Oh, and PINE64 shared another community update!
<!-- more -->

_Commentary in italics._
### Editorial: Inside baseball
This one landed early for RSS people (additions have been marked with a footnote). The big, hidden news with this update is that this blog is now hosted on a [public code hosting repository](https://framagit.org/linmob/linmob.frama.io) again. You can now [file issues](https://framagit.org/linmob/linmob.frama.io/-/issues) if a post is outdated or easily prepare a merge request for an outdated page (I am also working on a way to enable this kind of easy editing for the posts that need it). I also plan to start future updates and lengthy posts in seperate branches going forward, hoping that they will then get published eventually and not rot in my private Nextcloud notes. _Will this change anything? I doubt it, but I look forward to your contributions!_

### Hardware news
* [Penkesu Computer - A Homebrew Retro-style Handheld PC](https://github.com/penk/penkesu) (via [Liliputing](https://liliputing.com/2022/02/penkesu-computer-is-a-diy-mini-laptop-powered-by-a-rasberry-pi-zero-2-w.html)). _If a PinePhone in a keyboard case is not enough wide-screen or just not enough of a project for you, this thing might be it – if only that Raspberry Pi Zero 2 W packed more RAM than it does._

### Hardware accessory news (PinePhone cases, mostly)
* lllsondowlll: [Unofficial Pinephone Battery Case](https://forum.pine64.org/showthread.php?tid=16094). _A similar looking case is sold by a different brand in Europe, but I have not ordered one yet. Should I?_
  * [5ilver designed a Spacer](https://fosstodon.org/web/@silverhax/107806191563288107) for that case to make it fit properly
* If you always wanted a translucent, 3d printed case by for your PinePhone, you may want to get in touch with [James (@jthecoder)](https://nitter.net/jthecoder/status/1492914671569063949#m)

### Software news
#### GNOME ecosystem
* Phoronix: [Clutter Is Being Officially Retired](https://www.phoronix.com/scan.php?page=news_item&px=Clutter-Being-Retired). _Farewell, and thanks for all the clutter!_
* Phoronix: [GNOME Shell 42 Lands Redesigned OSD Notifications](https://www.phoronix.com/scan.php?page=news_item&px=GNOME-Shell-42-New-OSD). _Assuming that these are going to land in Phosh, I am very much looking forward to less giant OSD notifications._
* Dino: [Dino 0.3 Release: Video calls and conferences - encrypted and peer-to-peer](https://dino.im/blog/2022/02/dino-0.3-release/). _Please note: The main branch of Dino is not mobile-friendly yet and the [feature/handy branch](https://github.com/dino/dino/tree/feature/handy) is a tad behind, but with Dino [being ported to GTK4/libadwaita](https://github.com/dino/dino/issues/178#issuecomment-981101970) this app should work fine OOTB on our Linux Phones eventually!_
* This Week in GNOME: [#31 Bit Windy](https://thisweek.gnome.org/posts/2022/02/twig-31/).[^1] _GTK4/libadwaita ports going strong!_
* GabMus: [Swatch: a color palette manager](https://gabmus.org/posts/swatch_a_color_palette_manager/).[^1] _Interesting read, great to hear that libadwaita makes things easier for developers!_
* Guido Günther shared some bits about [future improvements coming to Phosh with regards to text input](https://social.librem.one/@agx/107779635575184450) and [lockscreen](https://social.librem.one/@agx/107817790186641703).[^1]


#### Plasma/Maui ecosystem
* TSDgeos: [Okular: Signature verification User Interface added to Okular mobile interface](https://tsdgeos.blogspot.com/2022/02/okular-signature-verification-user.html)

#### Ubuntu Touch
* UBports: [Ubuntu Touch OTA-22 Release](https://ubports.com/blog/ubports-news-1/post/ubuntu-touch-ota-22-release-3835).
* UBports: [Press release: More and more smartphone users want alternative operating systems](https://ubports.com/blog/ubports-news-1/post/press-release-more-and-more-smartphone-users-want-alternative-operating-systems-3834).

#### Sailfish OS
A new [minor release, 4.3.0.15,](https://nitter.net/JollaHQ/status/1494645438061940768#m) was published, fixing that polkit vulnerability and preparing things for a seamless upgrade to the upcoming 4.4.0 release.[^1]

#### Capyloon rising from Firefox OS's ruins
Just in case you don't use social media, [these toots](https://fosstodon.org/web/@linmob/107784286967430394) still contain everything I know.[^1]

#### Rust-native UI
[Slintcan apparently work on Linux Phones](https://github.com/slint-ui/slint/pull/956).[^1]

#### Distro releases
* Danct12 has [released another round of images](https://github.com/dreemurrs-embedded/Pine64-Arch/releases/tag/20220212) for his Arch Linux ARM-based distribution.
* [Manjaro ARM Beta 22 with Phosh](https://forum.manjaro.org/t/manjaro-arm-beta-22-with-phosh-pinephone-pinephonepro/102557) is out, delivering some new (partially [pre-release](https://gitlab.gnome.org/GNOME/calls/-/issues/412)) software (via [PineGuild](https://pineguild.com/manjaro-arm-beta-22-with-phosh-pinephone-pinephonepro/).
* An unofficial [Kali Linux port to the PinePhone](https://wiki.pine64.org/index.php?title=PinePhone_Software_Releases#Kali_Linux) has also [appeared](https://nitter.net/Shubhamvis98/status/1494500225716678659#m). _Hello, fellow hackers!_


#### Alternative Modem Firmware
Biktorgj released version [0.5.9 of his modem firmware](https://github.com/Biktorgj/pinephone_modem_sdk/releases/tag/0.5.9). _It's getting better and better, and you can call your modem now._

#### Cross-project Linux Mobile issue tracking
Tom wanted a [cross-project, general issue tracker](https://gitlab.com/linux-mobile/tracker/-/issues?sort=created_date&state=opened) and set one up. _I don't know how helpful this is to fix actual problems, but it might help with keeping track of overarching issues eventually getting fixed._

### Worth reading

#### Megapixels improvements
* Martijn Braam: [PinePhone Camera pt5: Exploring more advanced post-processing for Megapixels pictures](https://blog.brixit.nl/pinephone-camera-pt5/). _Another superb post by Martijn! I look forward to this landing on my PinePhone!_

#### PINE64 Community Update
* PINE64: [February update: chat with the machine](https://www.pine64.org/2022/02/15/february-update-chat-with-the-machine/). _There's a lot in there, and as always all is news to readers of this Weekly Update. So make sure to watch the little video clips interspersed detailing Plasma Mobile, the interactions with Biktorgj's modem firmware and if you're in the EU, PINE64 EU sounds great (and [more details](https://nitter.net/pine64eu/status/1493760154189316096#m) were shared on Twitter since). Also, if eInk tablets excite you, that PineNote progress sure is something – the fact that I already have a 10,3" eInk device I don't use enough helps me be patient and keep holding off, as there are still some major issues to be solved._[^1]

#### PinePhone Call Quality
* Jonathan Dieter: [PinePhone call quality](https://www.jdieter.net/posts/2022/02/07/pinephone-call-quality/).[^1] _Actual measurements. Take that, anecdata!_

#### PinePHone flashing
* TechHut: [How to Flash Linux on the PinePhone](https://techhut.tv/how-to-flash-linux-on-the-pinephone/).[^1] _Just as a reminder: It's really easy. And if Tow-Boot shipped as a default on the PinePhone Pro, it would be even easier there._

#### Software for ARM devices
* It's FOSS News: [KDE’s Latest Move Will Help Raspberry Pi and PinePhone Pro Users Immensely](https://news.itsfoss.com/kde-apps-arm/).[^1] _Ok, ARM Snaps by KDE now. Luckily, their Flatpaks have (at those I tried) been built for ARM as well._

### Worth listening
* Linux After Dark: [Episode 11: Why Linux operating systems on small Arm devices almost all feel half-finished, and what we can do to improve things.](https://linuxafterdark.net/linux-after-dark-episode-11/). _That guy Dalton sure knows his stuff, and explains Tow-Boot really well!_
* Linux User Space: [Episode 2:17: XXL MX](https://www.linuxuserspace.show/217). _Looking forward to more PinePhone impressions!_

### Worth watching

#### PINE64 Community Update
* PINE64: [February update: chat with the machine](https://www.youtube.com/watch?v=zGgt3dGdJzg). _Great video summary by PizzaLovingNerd!_

#### PinePhone Keyboard
* BlandManStudios: [Shim your PinePhone (Pro) Keyboard - Fix Broken](https://www.youtube.com/watch?v=4ixPjz6SPIA). _Good to see this demoed on video!_
* Nat Tuck: [PinePhone Keyboard Case](https://odysee.com/@NatTuck:9/nat-pinephone-keyboard-case:7).

#### How's that Librem 5 doing?
* Nat Tuck: [How's the Librem 5 in Feb 2022?](https://odysee.com/@NatTuck:9/nat-librem5-feb-2022:2). _Actually, you could update this like you do update your regular Debian by editing that sources.list, but then you would miss out on new features like full disk encryption. Also: The camera app works for me, you just need to move these sliders – it's all manual. Maybe setting gain to zero by default is not a good idea._

#### Sailfish OS corner
* Leszek Lesner: [SailfishOS App Podcast: Nightish - Blue light filter](https://www.youtube.com/watch?v=bEc2FxgTtdU).[^1]
* Some Jolla stuff: [MMS on Sailfish OS fails if there's no text in Subject field](https://www.youtube.com/watch?v=2wysF4jvD6A).[^1]

#### Nemo Mobile
* Jozef Mlich: [glacier-alarm demo](https://www.youtube.com/watch?v=W2wTbajeqVo).

#### Tablets
* Niccolò Ve: [The Linux Tablet That Could've Been](https://tube.kockatoo.org/w/e8cbc20b-f8f2-48f8-9deb-f720a5a5b351).[^1] _Sad._
* Linux Lounge: [The £10 Linux Tablet Experience](https://tilvids.com/w/8500a5e2-1f80-4586-9df4-ce51a624640a).[^1] _I blame nVidia._

#### Minecraft on Waydroid on postmarketOS on the Pocofone F1
* Ivon Huang: [Waydroid + postmarketOS讓Linux也能執行Android APP玩Minecraft](https://www.youtube.com/watch?v=WRVYTsN97Fs).[^1]

#### WayDroid on Ubuntu Touch on the Pocofone F1
* Ivon Huang: [Ubuntu Touch + Waydroid + Libertine安裝教學 (小米Pocophone F1)](https://www.youtube.com/watch?v=1ronTatQb0g).[^1]

### Something missing?
If your projects' cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email!


[^1]: This post or comment was added after initial publication on February 18th, 2022.
