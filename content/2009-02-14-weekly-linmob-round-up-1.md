+++
title = "Weekly linmob round up (1)"
aliases = ["2009/02/14/weekly-linmob-round-up-1"]
author = "peter"
date = "2009-02-14T20:54:00Z"
layout = "post"
[taxonomies]
tags = ["Android", "MotoMAGX", "Motorola", "MWC2k9", "OpenEZX", "Qt Extended", "Qtopia"]
categories = ["weekly update"]
+++

As I decided to do weekly round ups with a focus on the mobile linux world recently, I will now try to give this idea a good start, even though i don't have much time right now.

Let's start with Android. Besides the 1.1 SDK was released, <a href="http://www.archos.com/corporate/press/press_releases/ARCHOS-TI_020909_Final.pdf">Archos announced</a> an internet and multimedia tablet device supposed to run Android and featuring phone functions, powered by chips from TI. It isn't too unlikely, that we will see some more Android running devices on next weeks MWC at Barcelone, Spain - but don't expect a flood, as Motorola and Samsung are unlikely to announce any Android devices there.
Besides this, there are some efforts to jailbreak the european T-Mobile G1 devices, as Linux kernel developer Pavel Machek <a href="http://pavelmachek.livejournal.com/72674.html">states on his blog</a>.

As Motorola will release there first Android smartphones in Q4&#160;2009, they need something to fill the gap until then - considering they don't want to loose the rest of their highly decreased market share. <a href="http://www.unwiredview.com/2009/02/13/motorola-qa4-ve66lx-and-qa1-for-att-in-official-pics/">Unwired View</a> has pictures of some new Motorola phones: While the VE66LX is just a luxury edition of the already announced 5MP MotoMAGX slider VE66 and the QA4 seems to be pretty close to the CDMA Qualcomm BREW powered Krave ZN4, the somehow QA30 resembling GSM/3G (aimed at AT&amp;T) QA1 seems to be the most interesting: What kind of OS it is running? Has Motorola tweaked good old p2k05 so far? Or has it finally tought MotoMAGX 3G (and probably touchscreen capability? Or is it BREW? Well, nobody seems to know yet (hopefully besides Motorola ;-) ) so we have to keep our eyes on this, as it would be a great finish for MotoMAGX.

From Motorola to OpenEZX - support is becoming better and better, and thanks to FSO OpenEZX efforts seem to be close to become really usable. There is a <a href="http://img264.imageshack.us/my.php?image=screenshotphonetoolsphoqw0.png">flash tool with GUI</a> on the way.. So those who are not that skilled or just fear terminal emulations will be able to mess around with this, too.

One platform to run on top of OpenEZX is QT Extended, we shouldn't forget about that. If you are interested in programming for this platform, you might be interested in <a href="http://www.youtube.com/user/ICSNetwork">ICSNetworks Youtube Videos</a>.

That's it for this week, see you again for the next little round up next saturday, hopefully with lots of MWC news!
