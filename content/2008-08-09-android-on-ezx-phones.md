+++
title = "Android on EZX-phones"
aliases = ["2008/08/android-on-ezx-phones.html"]
date = "2008-08-09T19:33:00Z"
[taxonomies]
categories = ["shortform"]
tags = ["Android", "EZX/LJ", "OpenEZX", "platforms"]
[extra]
author = "Peter"
+++
Just a short video session:
<!-- more -->
Android on A780:

* Jorge SMM on YouTube: [Android on a780](https://www.youtube.com/watch?v=ntycTJosBHA)

* Android on E680: 

* androidezx on YouTube: [Android running on E680](https://www.youtube.com/watch?v=wUkf0EDVfQU)

Android on A1200:

* androidezx on YouTube: [Android running on A1200(new)](https://www.youtube.com/watch?v=SWi2y5o2Bgs)



_None of these videos were created by me, so don't ask me, how they did it. Make sure to visit the <a href="http://www.motorolafans.com/forums/android/">Motorolafans.com ANDROID forums</a>_

As you can see, Android will definitely run on the EZX phones&mdash;let's hope that Google will release some newer stuff soon. Of course there are some adjustments to be made (and as OpenEZX support for the phones becomes better and better, Android will run better) but I am pretty sure some of the guys out there will make it possible.
