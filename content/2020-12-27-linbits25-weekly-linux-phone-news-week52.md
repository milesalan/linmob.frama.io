+++
title = "LinBits 25: Weekly Linux Phone news / media roundup (week 52)"
aliases = ["2020/12/27/linbits25-weekly-linux-phone-news-week52.html", "linbits25"]
author = "Peter"
date = "2020-12-27T21:55:00Z"
layout = "post"
[taxonomies]
tags = ["Pine64", "PinePhone", "Arch Linux ARM", "DanctNIX Mobile", "Manjaro", "Jumpdrive", "Plasma Mobile", "Phosh", "Librem 5", "Purism", "openSUSE", "slem.os"]
categories = ["weekly update"]
+++

_It's sunday. Now what happened since last sunday?_

New releases of openSUSE and DanctNIX, plenty Librem 5 videos and more. _Commentary in italics._
<!-- more -->

### Software development and releases

* [Minetest has been ported so that it is playable without attaching keyboard or mouse](https://teddit.net/r/PinePhoneOfficial/comments/kh33m8/minetest_ported_to_the_pinephone/).
* [hadrianweb](https://nitter.net/hadrianweb/status/1341459384778559489/photo/1) announced a christmas openSUSE release for the Pinephone with kernel 5.10, improved battery life, wireless and modem.  _Go and [try it out!](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/)_ 
* [DanctNIX mobile/Arch Linux ARM has seen a new release](https://github.com/dreemurrs-embedded/Pine64-Arch/releases/tag/20201223). It features new software releases like an overhauled version of the Geary email client, the Portfolio File Manager and a lot more.
* [Libhandy 1.0.3](https://gitlab.gnome.org/GNOME/libhandy/-/releases/1.0.3) has been released &mdash; it fixes a few bugs.

### Worth reading 
* LinuxHint: [Linux Based Operating Systems for Mobile and Tablet Devices](https://linuxhint.com/linux_based_operating_systems_mobile_tablet_devices/). _Nice overview. I really wonder what that Lenovo Device in the Phosh photo is!_
* tchx84: [Portfolio: manage files in your phone](https://blogs.gnome.org/tchx84/2020/12/21/portfolio-manage-files-in-your-phone/). _Portfolio is a simple, mobile first GTK+libhandy, and it's fairly nice. [More info.](https://nitter.net/linmobblog/status/1341144976550490113#m)_
* TuxPhones: [New GNOME "Text Editor" app is a mobile-friendly, GTK4 alternative to Gedit](https://tuxphones.com/gnome-text-editor-mobile-friendly-libhandy-gtk4-gedit/). _In case you were wondering where the source to that new app resides, [wonder no more](https://gitlab.gnome.org/chergert/gnome-text-editor)._
* LinuxSmartphones: [Manjaro ARM Plasma Mobile Beta 1 is now available for the PinePhone](https://linuxsmartphones.com/manjaro-arm-plasma-mobile-beta-1-is-now-available-for-the-pinephone/).
* Purism: [Why FSF Endorsing PureOS Matters](https://puri.sm/posts/why-fsf-endorsing-pureos-matters/). _This post has caused quite a [discussion on Twitter](https://nitter.net/marcan42/status/1341416520392404993)._
* Barry's News: [ The PinePhone continues to evolve](https://bkhome.org/news/202012/the-pinephone-continues-to-evolve.html). _Read it with a grain of salt, while the title is certainly true, keep in mind the author does not have a PinePhone yet._
* Purism App Showcases
  * [Animatch](https://puri.sm/posts/app-showcase-animatch/). 
  * [Lollypop](https://puri.sm/posts/app-showcase-lollypop/).

#### Please contribute
PINE64 Wiki: [Feature Matrix](https://wiki.pine64.org/wiki/PinePhone_Software_Releases/Feature_Matrix). _/u/StructurallySound started something that could be really helpful. Please help with this effort._


### Worth watching
* Gardiner Bryant: [I can't believe VS Code is USABLE on the Librem 5!!!](https://lbry.tv/@TheLinuxGamer:f/gb-code-server:1). _Please note that Gardiner is not really running VS Code on the Librem 5 here, but just connects to a [Code Server](https://github.com/cdr/code-server) instance in Firefox._
* HackersGame: [Terminal Touch Interface &mdash; Multilsh](https://lbry.tv/@HackersGame:4/terminal-touch-interface-multilsh:a). _Nice work by HackersGame._
* Chris Titus Tech: [The Future of Mobile Phones](https://www.youtube.com/watch?v=4wcDkSl1wJ4). _Nice overview, once I swallowed my anger over the inaccurate representation of history of webOS, it was quite fun to watch._
* Privacy & Tech Tips: [Howto: Sdcard As Extra Storage For Pinephone/Linux Devices](https://www.youtube.com/watch?v=GW5RUcu6rbU). _Another really helpful video._
* everdaypine: [Holiday PinePhone Thoughts](https://lbry.tv/@everydaypine:a/Holiday-PinePhone-Thoughts-:c). _Nice short-term impressions of Manjaro Phosh Beta 4 including Portfolio and Quickddit (which to my knowledge never ran on Blackberry 10)._
* Notta Pro: [Librem 5: Production Evergreen Model Impression](https://youtu.be/Nu53AHdvwtM). _This video is too long for what it offers. It has been [praised on Reddit as an unbiased, honest review](https://teddit.net/r/Purism/comments/kj1lm6/first_ever_librem5_review_which_is_not_biased/), but as he tells in his later video, part of this was recorded in November. As I learned while making videos about the PinePhone: It's not a good idea to even delay the upload of videos by days. Therefore, much of his criticism does not necessarily reflect the current situation of PureOS on the Librem 5. That said, Purism has (at least from my experience with PureOS on the PinePhone) failed to deliver the IP native communication stack it promised; so some of the criticism is fair._
* Notta Pro: [Res Librem 5: Was it fair?](https://youtu.be/mkrOuZgeEeQ). _Follow-up video to the video above, calling out Purism's flawed marketing. This had to happen. If the Android boot-up and Android/iOS standy data-use comparison had been uploaded since I started LinBits, I would have called them out, too._
* u/redwisdomlight: Librem 5 encounters [part 1](https://teddit.net/r/Purism/comments/khjvcr/librem_5_first_encounter_part_1_not_really_but/), [2](https://teddit.net/r/Purism/comments/khjwfn/librem_5_encounters_part_2/), [3](https://teddit.net/r/Purism/comments/ki24jq/librem_5_encounters_part_3/), [4: Bluetooth Bose Headphones](https://teddit.net/r/Purism/comments/ki28uu/librem_5_encounters_p4bluetooth_bose_headphones/), [5](https://teddit.net/r/Purism/comments/ki2a5y/librem_5_encounters_p5bluetooth_bose_headphones/), [6](https://teddit.net/r/Purism/comments/kiykj4/librem_5_encounters_part6/), [7](https://www.loom.com/share/654233aa7a8f4d2ba1fd35b808026466), [8: Calls and Texts work](https://teddit.net/r/Purism/comments/kizlph/librem_5_encounters_part_8_phone_call_and_text_it/), [9: Installing applications](https://teddit.net/r/Purism/comments/kizrw9/librem_5_encounters_part_9_installing/), [9: Note apps and foliate](https://teddit.net/r/Purism/comments/kjcbts/librem_5_encounters_part_9_installed_note_apps/), [10: Librem 5 goodies](https://teddit.net/r/Purism/comments/kjcgv9/librem_5_encounters_part_10_installing/), [11](https://teddit.net/r/Purism/comments/kjck4e/librem_5_encounters_part_11_is_this_the_final_one/), [12](https://teddit.net/r/Purism/comments/kkpqnf/librem_5_encounters_part_12_were_really_making/). _An interesting series of videos with a guy discovering his new Librem 5._
* Pedro Francisco: [Manjaro Plasma y Phosh en el PinePhone](https://video.hardlimit.com/videos/watch/9c1811be-e059-416c-83ed-cde0c4e46976). _Spanish video about current Manjaro releases._


#### rC3: remote Chaos experience
* Tox talks [An Introduction to Tox ](https://media.ccc.de/v/rc3-730010-an_introduction_to_tox). 
* Cory Doctorow: [What the cyberoptimists got wrong - and what to do about it](https://media.ccc.de/v/rc3-11337-what_the_cyberoptimists_got_wrong_-_and_what_to_do_about_it). _Get more rC3 content at [media.ccc.de](https://media.ccc.de/)!_

### Stuff I did

#### Content
While I streamed twice the past week, I don't have much to show otherwise. The holidays weren't actually productive. With [rC3](https://rc3.world/rc3/) starting today, this is unlikely to change. 

#### LINMOBapps

Not much happened here, just [a few additions and some maintenance](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master). Please [do contribute](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/tasks.md)!

_See you all again next year!_
