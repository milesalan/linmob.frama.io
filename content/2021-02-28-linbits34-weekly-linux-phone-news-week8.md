+++
title = "LinBits 34: Weekly Linux Phone news / media roundup (week 8)"
aliases = ["2021/02/28/linbits34-weekly-linux-phone-news-week8.html", "linbits34"]
author = "Peter"
date = "2021-02-28T20:42:00Z"
layout = "post"
[taxonomies]
tags = ["PINE64", "PinePhone", "Purism", "Librem 5", "LINMOBapps", "PineTalk", "SailfishOS", "Manjaro", "Phosh", "NeoChat", "FreeBSD", "Lomiri", "Potabi"]
categories = ["weekly update"]
+++

_It's sunday. Now what happened since last sunday?_

A new Manjaro release, Plasma Mobile tarballs, Sailfish 4.0 on the PinePhone and more! _Commentary in italics._
<!-- more -->

### Software development and releases
* [NeoChat 1.1](https://carlschwan.eu/2021/02/23/neochat-1.1/) has been released. _No E2EE yet._
* [Manjaro Phosh Beta 6](https://github.com/manjaro-pinephone/phosh/releases/tag/beta6) has been released, featuring all the new releases. Read more [on their forums](https://forum.manjaro.org/t/manjaro-arm-beta6-with-phosh-pinephone/55679).
* Plasma Mobile blog: [Plasma Mobile applications tarball releases](https://www.plasma-mobile.org/2021/02/23/Plasma-Mobile-applications-tarball-release.html). _Not much to read, but I hope that this will help Plasma Mobile get packaged in more distributions._

### Worth noting
* /u/mrtiger69: [Spotify on the PinePhone!](https://www.teddit.net/r/PINE64official/comments/lszxrv/spotify_on_the_pinephone/). _I added this to LINMOBapps last week, and now it even scales properly. Spotify Premium only._
* @adampigg: [SailfishOS 4.0 now works on the PinePhone with video playback in the default Gecko-based browser.](https://twitter.com/adampigg/status/1365328542117093378). _I am going to look at this in the coming week._
* @drago_senpai: [ExpidusOS now has a six months roadmap.](https://twitter.com/drago_senpai/status/1364091065834164225) _Roadmaps are always confidence-inspiring._
* @Fivnex: [Potabi is going to be based on FreeBSD, and will be using Lomiri.](https://twitter.com/Fivnex/status/1365105202806710272) _Keep in mind: There is no FreeBSD working on the PinePhone just yet (although it works on the [Pine A64](https://wiki.freebsd.org/arm/pine64)), and I don't recall that Lomiri has ever been run on FreeBSD. Now that's what I call a gigantic endeavour._
* /u/realSpaetz: [Librem 5 vs PinePhone eMMC benchmarking](https://www.reddit.com/r/Purism/comments/lt2fdg/i_received_my_l5_and_it_is_heavy_beautiful_and/goz36if/?utm_source=reddit&utm_medium=web2x&context=3). _That's why apps launch faster on the Librem 5._

### Worth reading 
* Purism: [Purism and Linux 5.11](https://puri.sm/posts/purism-and-linux-5-11/). _An overview of Purism's contributions to the most recent Linux release._
* StealthGun's blog: [Gentoo on a PinePhone](https://stealthgun.tweakblogs.net/blog/19336/gentoo-on-a-pinephone). _Nice guide to getting started with Gentoo on the PinePhone._
* MoukTech: [Season Of KDE – Project Update 1](https://mouktech.wordpress.com/2021/02/22/season-of-kde-project-update-1/). _foKus seems to be an interesting app._
* UBports: [Call for Testing: Ubuntu Touch OTA-16](https://ubports.com/de/blog/ubports-blogs-nachrichten-1/post/call-for-testing-ubuntu-touch-ota-16-3742).
* LinuxSmartphones: [Open Source phone news roundup: FreeBSD, Google-free Android, and Ubuntu Touch](https://linuxsmartphones.com/open-source-phone-news-roundup-freebsd-google-free-android-and-ubuntu-touch/). _Open source, not Linux, because of Potabi &mdash; see above._

### Worth listening
* PineTalk [003: Quartzy Keyboards](https://www.pine64.org/2021/02/26/003-quartzy-keyboards/). _Another Episode of [Ezra](https://elatronion.com) and me talking about a few things. I already have some ideas for the next episode, but please keep feedback and questions coming!_
* Nephitejnf: [More Thoughts on the Current State of the Pinephone and Her Linux Distros](https://www.youtube.com/watch?v=lkR0KbcioPo). _Yes, this is a YouTube video, but there's nothing to see, so just listen._


### Worth watching


* Сергей Чуплыгин: [QtQuickControls-Nemo: Compare Spiner in QML and C++](https://www.youtube.com/watch?v=d-jHFa8YOpI). _New Nemo PinePhone image when?_
* Peter Hazenberg: [PinePhone - WoWLAN - Wake on Wireless LAN](https://www.youtube.com/watch?v=8wMlACfEO0A). _Really cool work!_
* Privacy & Tech Tips: [Sandbox Apps With Firejail: Protecting Pinephone/Pinetab/Linux Box (Firefox Example)](https://www.youtube.com/watch?v=7Q57Nj6Az3U). _Great to see Firejail in action, I haven't been using it lately._
* TalkNTech: [What I think of the Pine64 Pinephone KDE CE](https://www.youtube.com/watch?v=H_Nn7wJhr0s). _Good thoughts._
* IziacNix: [Ubuntu Touch on the Pinephone](https://www.youtube.com/watch?v=MYWe6L4hGNQ). _Two devices running Ubuntu Touch._
* UBports: [Ubuntu Touch Q&A 95](https://www.youtube.com/watch?v=5AWxWgs0CN0). _Highlights include: News on Anbox, Xperia X mainline and a lot more. Do watch it!_

#### Mobian unboxings
* TechHut: [Unboxing the PinePhone by Pine64! - First look at Mobian (Debian Mobile)](https://www.youtube.com/watch?v=KqzAjEWfrAc). _Nice video, looking forward to more!_
* Kevin Veroneau: [Unboxing the PinePhone Mobian community edition](https://www.youtube.com/watch?v=XLq9qINIybg), [Mobian first boot on the PinePhone](https://www.youtube.com/watch?v=kV02IK-1UMM). 

#### PineTab corner
* Privacy & Tech Tips: [Pinetab/Pinephone Arch (DanctNIX) Full D Encryption Install Script + Bonus (Option) BlackArch Linux](https://tube.tchncs.de/videos/watch/f5e41da9-3d95-4c5d-b2d0-2c96d165105e). _Nice to see that FDE script demoed._
* LinuxLounge: [PineTab Unboxing and First Impressions!](https://odysee.com/@LinuxLounge:b/pinetab-unboxing-and-first-impressions:6).

### Stuff I did

#### Content

I published no video and no blog post this week. I did not even manage to do much on social media or improve a previous blog post. _I hope to do better next week!_


#### Random
With `--ssb` being removed in Firefox 86, I [updated my launcher file](https://framagit.org/linmobapps/linmobapps.frama.io/-/commit/9cffb330b5dc50d37d1372597e7f6d61703cc4ba) to use `--kiosk` instead. It works ok once you have one more app open in Phosh.

#### LINMOBapps

This week I added two apps to LINMOBapps, siglo (a WIP GTK client for connecting to InfiniTime), and Craw Translate, a Kirigami translation frontend. I also updated the rating of Spot, the GTK/Rust Spotify frontend, which now works great on mobile. [A little more maintenance happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master). Please [do contribute](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/tasks.md)! 


