+++
title = "41 minutes of PinePhone content"
aliases = ["2020/06/27/41-minutes-of-pinephone-content.html"]
author = "Peter"
date = "2020-06-27T00:05:18Z"
layout = "post"
[taxonomies]
tags = ["Video content", "PinePhone", "UBports", "Ubuntu Touch", "SailfishOS", "PureOS", "Mobian", "Sxmo", "postmarketOS", "Maemo Leste", "Plasma Mobile", "Phosh", "necessary apps"]
categories = ["videos", "article",]
+++
_It took me 15 days to create and upload another video to YouTube, and that made it way too long._

I actually find it hard to create content sometimes, and I guess that the [PinePhone][https://www.pine64.org/pinephone] and its plethora of emerging software/distributions may have just overwhelmed me.<!-- more -->
If you still want to watch the video in question, in which I ramble on about the PinePhone, starting with corrections on my last video, then mentioning that this blog exists, to then finally go through UBports, SailfishOS, Mobian, PureOS, MaemoLeste, Smxo all in order to actually talk about postmarketOS a bit, you can do so at
 * [YouTube](https://www.youtube.com/watch?v=JzazgbL4u6U).

The next video (max. length 15 minutes) is going to be focussed [Maemo Leste][https://leste.maemo.org/Main_Page] (who actually shared an [update](https://maemo-leste.github.io/maemo-leste-thirteenth-update-april-may-june-2020.html)&mdash;including new [images](http://maedevu.maemo.org/images/droid4/20200624/)&mdash;just after my [blog post](https://linmob.net/2020/06/21/maemoleste-on-the-droid4.html)).
After that, I think of embarking on a series of short video that show little things (like installing `adwaita-qt` and setting the environment variable (`QT_STYLE_OVERRIDE=adwaita`) to apply it) on the PureOS port or the seemingly more popular [Mobian](https://www.mobian-project.org). I will also selfhost or put these videos on a PeerTube instance, in order to help those, who refuse to touch YouTube.
Every little video is going to be accompanied by short blog post.

The rough idea behind all this is to outline a way towards me being able to use the PinePhone as a daily driver (or at least a secondary phone), while ignoring issues I cannot fix, like battery life.

Now: What would I need?
* access to my Nextcloud instance,
* a secure (as in encrypted) storage on the PinePhone/SD Card,
* a way to open/use my KeePass password database,
* XMPP+OMEMO messaging (already kind of works),
* encrypted Matrix chats,
* GPG encrypted email,
* a way to open text documents that are not PDF (Abiword still has that `--enable-embedded` `./configure` flag),
* something usable for `org-mode`,
* an application to view maps and my approximate location.

I would also like
* navigation,
* a `quassel`-client,
* a way to run Android apps (like Threema) if necessary (= working anbox),
* and a lot more I can't even think of now.

So there will be a lot of ground to cover, especially if I decide to also broadcast failed attempts. Stay tuned!
