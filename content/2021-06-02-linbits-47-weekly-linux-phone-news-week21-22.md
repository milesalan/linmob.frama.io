+++
title = "LinBits 47: Weekly Linux Phone news / media roundup (week 21/22)"
aliases = ["2021/06/02/linbits47-weekly-linux-phone-news-week2122.html"]
date = "2021-06-02T20:32:00Z"
updated = "2021-06-03T07:54:00Z"
[taxonomies]
tags = ["PINE64", "PinePhone", "LINMOBapps", "Volla", "Phosh", "Jumpdrive", "Crust", "GloDroid",]
categories = ["weekly update"]
[extra]
author = "Peter"
update_note = "Added Purism Librem 5 USA posts that my feed reader did not catch previous to initial publication."
+++

_It's Wednesday. Now what happened since last Wednesday?_

New Crust, Phosh and Jumpdrive releases, Volla Community days and more!<!-- more --> _Commentary in italics._

### Software development and releases
* Crust, the low-level power-saving firmware, [has seen its 0.4 relaese](https://github.com/crust-firmware/crust/releases/tag/v0.4).
* [Kaidan 0.8](https://www.kaidan.im/2021/05/28/kaidan-0.8.0/) has been released, featuring typing notifications & message synchronization.
* [Neochat 1.2](https://carlschwan.eu/2021/06/01/neochat-1.2-bubbles-better-text-editing-and-more/) has been released, featuring bubbles, better text editing and more! _Make sure to watch the video, since it's not demoing how it works on smaller screens, I did not add it below._
* [Phosh 0.11.0](https://social.librem.one/@agx/106328941414170520) has been released. Changes include Wifi/WWAN/BT quick settings now toggle on/off, long press opens Settings, initial support for `gnome-session --systemd` and a torch brightness slider. Make sure to read the [full release notes](https://source.puri.sm/Librem5/phosh/-/releases/v0.11.0)!
* [Jumpdrive 0.8](https://github.com/dreemurrs-embedded/Jumpdrive/releases/tag/0.8) has been released, now supporting a few Snapdragon 845 devices.
* [JingOS 0.9](https://jingos.feishu.cn/docs/doccnFhsqYIUT2ozejeOlfvMfsf) has been released for x86 tablets.

### Worth reading 

#### Software development news
* Janet's Shenanigans: [A Switchy Week In Tok](https://blog.blackquill.cc/a-switchy-week-in-tok). _Tok is a Kirigami Telegram client._
* Volker Krause: [April/May in KDE Itinerary](https://www.volkerkrause.eu/2021/06/02/kde-itinerary-april-may-2021.html). _Progress on Itinerary, which is great and our best chance for public transport fares on Linux Phones._
* Visvesh's blog: [A Brief about my GSoC project](https://visveshs.blogspot.com/2021/05/redesigning-healths-mainview.html). _This will be work on [Health](https://gitlab.gnome.org/Cogitri/Health)._

#### What is a PinePhone?
* Tedium: [A Home for Mobile Linux](https://tedium.co/2021/05/26/pinephone-mobile-linux-review/). _A really nice review! It highlights what makes the PinePhone so important._

#### Getting started with things
* Jozef Mlich: [Developing Nemomobile with PinePhone](https://blog.mlich.cz/2021/05/developing-nemomobile-with-pinephone/). _If you want to help with Nemo Mobile development, read this  post!_
* FOSSingularity: [Installing Manjaro Phosh Beta 10 on The Pinephone](https://fossingularity.wordpress.com/2021/05/31/installing-manjaro-phosh-beta-10-on-the-pinephone/). _This is simpler. Note to self: I really need to hightlight Etcher alternatives more!_ 

#### PinePhone Keyboard corner
* xnux log: [PinePhone keyboard](https://xnux.eu/log/#034). _Very technical, but worth it!_
* xnux log: [PinePhone keyboard – more observations](https://xnux.eu/log/#035)

#### Hardware corner
* TuxPhones: [Volla expands line-up, teases new "Phone X" model with Android and Linux](https://tuxphones.com/volla-phone-x-ubuntu-linux-phone/). _Volla's 2nd phone is going to be quite similar, but rugged._
* Purism: [Purism Launches a Privacy-first, Made in USA Smartphone: Librem 5 USA, as an Alternative to Big Tech Offerings](https://puri.sm/posts/purism-launches-a-privacy-first-made-in-usa-smartphone-librem-5-usa-as-an-alternative-to-big-tech-offerings/). _Just a press release._
* Purism: [Manufacturing the Librem 5 USA Phone in the United States of America](https://puri.sm/posts/manufacturing-the-librem-5-usa-phone-in-the-united-states-of-america/). _This one is actually an interesting read in comparison._

#### Cooperation corner
* KDE.news: [PINE64 becomes a KDE Patron](https://dot.kde.org/2021/06/02/pine64-becomes-kde-patron).
* PINE64: [We are KDE patrons](https://www.pine64.org/2021/06/02/we-are-kde-patrons/).

### Worth watching

#### PinePhone Software intersection
* Avisando: [PinePhone - How to install Android 11](https://www.youtube.com/watch?v=uwgUVXuP18E). _Of course, Avisando did a better job at this than I did._
* Privacy & Tech Tips: [Shortwave Has 25,500 Radio Stations: Check It Out! (Pinephone/Linux)](https://odysee.com/@RTP:9/pinephone-linux-25,500-radio-stations:1). _Great app!_

#### Hardware corner
* Glenn Hancock: [Pinephone Motherboard Replacement](https://odysee.com/@GPilot:5/pinephone-motherboard-replacement:1). 
* mutantC: [Rebooted mutantC v3 and all works](https://www.youtube.com/watch?v=Xamhxax1nW8). _This is an interesting little DIY handheld computer, make sure to watch the video!_

#### Volla Community days
* Manjaro: [Manjaro took part of Volla Community Days 2021](https://www.youtube.com/watch?v=vAWKFUF9bXk)
* More recordings of this event can be found on [Volla's channel](https://www.youtube.com/channel/UCtU5z1rfssFUbC_WOw9yhyA), which currently is only useful if you cross-reference it with their [schedule](https://volla.online/en/blog/files/community-days-2021.html). _There's a lot in here, about Ubuntu Touch, Droidian, Nemo Mobile and more projects._


### Stuff I did

#### Content
I made one video about the PinePhone in the past week: 

* Android on the PinePhone: Installing GloDroid (June 1st, 2021): [DevTube](https://devtube.dev-wiki.de/videos/watch/c001bbad-1010-495c-957f-56d84f24e7f1), [Odysee](https://odysee.com/@linmob:3/android-on-the-pinephone-installing:9), [YouTube](https://www.youtube.com/watch?v=ZdmBq0rFPa8).

I also published a silly blog post about [howto paste into kgx, the Phosh terminal app](https://linmob.net/pasting-text-in-kgx-phosh-terminal/).

#### LINMOBapps
LINMOBapps got some love this week, too. I have added a few apps: 

* [Tokodon](https://invent.kde.org/network/tokodon), a work-in-progress Mastodon client for Plasma Desktop and Mobile,
* [Almond](https://github.com/stanford-oval/almond-gnome), a virtual assistant developed by Stanford,
* [Pyrocast](https://github.com/jnetterf/pyrocast), a work-in-progress GTK/libhandy podcast client written in Rust,
* [pods](https://github.com/dskleingeld/pods), another podcast client written in Rust,
* [GNOME Weather](https://gitlab.gnome.org/GNOME/gnome-weather), the GNOME Weather app that is mobile friendly since it's 40 release,
* [SBB](https://github.com/chefe/sbb), a simple GTK+  app written in Rust to get timetable information for swiss public transport, and
* [Scrum Poker](https://github.com/chefe/scrumpoker), also created with GTK+ and Rust.

[Read here what (else) happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master). 
Please [do contribute!](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/CONTRIBUTING.md)
