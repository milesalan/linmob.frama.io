+++
title = "Why do they always deliver crap?"
aliases = ["2008/02/why-do-they-always-deliver-crap.html"]
date = "2008-02-02T23:24:00Z"
[taxonomies]
tags = ["criticism", "E28", "E2831", "EZX/LJ", "platforms"]
categories = ["commentary"]
[extra]
author = "Peter"
2021_commentary = "This post clearly shows some misconceptions I had back then, but it also felt very true. Linux Phones were hold back by their software back then, as numerous open source efforts made painfully obvious."
+++
Sounds bad. But if you ask me, it's even worse. What the h*** he is talking about? Right. Linux software stacks.
<!-- more -->

There are some companys out there, that are selling Linux powered phones. I know the results of this for Motorola and E28, and, if you want me to be honest, they didn't their best.
No SDK, weird platforms&#8230; But why?

Some people say, they do it like that, because the operators want it like that. Ok, maybe. But: It's possible to install software on mobile phones powered by Windows Mobile or Symbian.

So what is the reason to cripple nice hardware with a bad software stack, as there are other ways to go (as FIC/OpenMoko and maybe Android show)? Maybe it's to hide security vulnerabilities. Or incompetence.

We should really free these phones.

(I was angry while writing this&#8230;)
