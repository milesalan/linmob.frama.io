+++
title = "CeBIT 2k9: report of a short visit"
aliases = ["2009/03/07/cebit-2k9-report-of-a-short-visit.html"]
author = "peter"
date = "2009-03-07T16:24:00Z"
layout = "post"
[taxonomies]
tags = ["ALP", "Android", "Azingo",  "cebit", "CeBIT2k9", "Garmin", "htc", "LiMo",]
categories = ["events", "commentary"]
+++

After I didn't manage to get up early enough for a <a href="http://cebit.de/">CeBIT</a> visit on while going to my parents, I was up quite early on friday, so that I could be at the CeBIT gates at 9:30 a.m. after two hours of train journey.
 
I won't write about all the halls I visited, as I guess that this doesn't help anybody&mdash;and often I just walked through those halls without looking at lots of things. But I can tell you, that I stayed in hall 6 for the most time, as the OpenSource area and the so called Webciety are located there.  
 
Webciety is rather boring, not that interesting, even good old T-Systems is among the companys that have a stand there.</p> <p style="margin-bottom: 0cm">Some of the talks there (pre:publica) were not at all uninteresting, but you have to say that it were often the same opinions you've heard before in podcasts or blogs&mdash;it is aimed at normal people, that don't read blogs all day&mdash;I'd guess.
 
OpenSource Area was nice, no <a href="http://www.openmoko.com/">OpenMoko</a> booth (or something like a booth for mobile Linux with OpenMoko, just desktop stuff, but hey, better than nothing. Even Linpus has just netbooks, last year they had a PDA like device, too.  
 
In Hall 9, B39 I found the (probably) only OpenMoko devices at CeBIT, presented by some guys of the<a href="http://www.tfh-wildau.de/index.html"> TFH Wildau</a>, a technical university of applied sciences, located in Brandenburg. I showed them my keyboard and we had a short talk about Openmoko, the potentials and weaknesses of the platform.   
 
The talk about <a href="http://www.limofoundation.org/">LiMo</a> i enjoyed more or accidentally than planned was great, as I finally understood what the LiMo Foundation really does: Just a middleware. Just a place to share patents. So you can't really compare LiMo Foundation to OHA or Symbian Foundation from a user perspective, but you can do so from a business perspective. From the user perspective can compare LiMo compliant platforms like <a href="http://alp.access-company.com/">Access Linux Platform</a> or <a href="http://www.azingo.com/">Azingo's platform</a> to others like like Android, not LiMo itself, which is rather a place for companys to share knowledge.  </p>   <p style="margin-bottom: 0cm">I talked to Garmin officials about the<a href="http://www.garminasus.com/garminasus/cms/site/home/lang/en/phones"> garmin-asus nüviphone G60</a>, which is confirmed to be Linux powered, but I couldn't find out whether they use „Software we know“&mdash;the officials didn't know. From the looks it isn't something I know, to say the least. Most likely a proprietary UI, like the one garmin runs on it's navigation solutions, which did run well besides hickups which were to appear when you wanted to use the keyboard&mdash;at least that's what I could observe. Start up time wasn't very fast too. But the thing is about to be launched in september (or even later) so they'll surely fix the issues until then, for those who really want the G60 (I wouldn't buy it as WQVGA on 3,5“ isn't looking great and is outdated in my opinion). And I was told (as others were told before) that we will probably see Android powered Garmin/Asus devices in the future.
 
Went to ASUS booth, guys over there (at least those I talked to) didn't know more than the Garmin guys&mdash;unsurprisingly. </p> <p style="margin-bottom: 0cm">Short talks to people at OS booths, just saying thanks for their great work, as I believe that this important.
 
Met the „german netbook king“ (he was called so by <a href="http://twitter.com/Scobleizer/status/1108438732">Robert Scoble</a>) <a href="http://twitter.com/sascha_p">Sascha Pallenberg</a> (<a href="http://www.netbooknews.de">netbooknews.de</a>). We talked about netbooks and stuff, then &#8220;mobile meetup&#8221; at 5pm at hall 6&#160;5pm. It is a funny situation to sit there with lots of guys you've already read, heared on podcasts or seen on video reviews. Showed my Openmoko keyboard to <a href="http://twitter.com/chippy">Steven Paine</a>  (<a href="http://www.umpcportal.com/">umpcportal.com</a>), while 3 UMPCs are on the table, later 3 UMPCs vs. 3. netbooks&#8230;</p><p style="margin-bottom: 0cm">Then I had a nice time at a party (Barcamp?) in the webciety area, taking most of the time to the guys that run <a href="http://ndevil.com/">ndevil.com</a> before I had to leave in order to get the last train home&mdash;a good end for a day full of walking from one location to another.
 
After all I have to say, that I didn't see lots of devices, I missed a hands-on on the <a href="http://ndevil.com/12659/">Umid M1</a> and the <a href="http://www.road.de/en/index.html">Road handyPC S101</a> (I didn have it on my list, as it was announced in 2006) e.g., but like this CeBIT wasn't like hell, it was more fun. I saw what I absolutely had to see (the garmin-asus G60 and the HTC Magic which might become a top seller, listened to a talk about LiMo which helped me to understand the LiMo platform a lot better than before&mdash;that's it, basically. I tried to stay relaxed all the time, taking the time to talk to people, to look at some things&mdash;if I hurried, I could have seen more things, e.g. the Green IT area I missed too, but I believe that this wouldn't have made the whole thing more awful.  
 
Next year  I will visit the CeBIT again, possibly for more than one day, even if it might be even less interesting considering mobile devices. And I might visit some more fairs&#8230; We'll see.

_(Pictures will follow tomorrow.)_
