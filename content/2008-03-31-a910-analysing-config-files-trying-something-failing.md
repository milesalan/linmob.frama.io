+++
title = "A910: Analysing config files, trying something, failing again."
alias = ["2008/03/universal-gpe-finally-runs-on-my-mda.html"]
date = "2008-03-31T17:36:00Z"
[taxonomies]
tags = ["A910", "A910i", "EZX/LJ", "ezx_dhcp.cfg", "ezx_flexbit.cfg", "ezx_ipsec.cfg", "ezx_wlan.cfg", "modding"]
categories = ["software", "projects"]
[extra]
author = "peter"
+++

The day before yesterday I had a closer look at CG43 contents&mdash;CG43 contains configuration files.
<!-- more -->

I compared all the files, and many were just the same on both firmwares, though there is a year between them (the A910 firmware I have got is a year older). The change I did to ezx_wlan.cfg, if you remember, had no effect&mdash;both files are the same on both firmwares. The greatest difference I found (besides ezx_flexbit.cfg) was in ezx_ipsec.cfg. A910i firmware doesn't contain a signature, in contrast to A910. So I replaced that file in A910 firmware, created a new CG43, made a flash file and flashed it on my phone&mdash;no change. It still doesn't connect to WiFi-networks successfully. As you may imagine, I am a kind of sad now. But&#8230; well... There has to be a solution, and I'll find it.

Maybe the problem is the flexbit, it differs a lot from A910 to A910i&mdash;if I only knew, which flexbits do what, I could try do something here&mdash;it might be the solution. So if anybody knows something about which flexbit is responsible for what on A910 or E2 (and maybe E6, A1200), it would be really nice if you could help me. Because as I realized on my old A780, changing the flexbit file changes things around, if you change the other files, it's just luck that something happens.

_UPDATE, April 6th, 2008:_ I tried to work on the latest A910 firmware I have, fixed a java-app issue (CG34), found another configuration file in CG43, which might be important: ezx_dhcp.cfg&mdash;this file does not exist in A910i FW and mentions something provider related&mdash;but no change after flashing modified firmware&mdash;since I added show* files to CG44, at least most E2 apps should work on (my) A910 as well. In addition to this I'll have a closer look at A910-Orange files I have on my hard disk next weekend (as I now have a working USBLan setup ;-) ), and I might even begin work on a wireless-lan-app (as the first try to port A910i's one failed, but I might try that again first).
