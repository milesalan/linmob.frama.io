+++
title = "Happy Easter!"
aliases = ["2008/03/happy-easter.html"]
date = "2008-03-23T11:52:00Z"
layout = "post"
[taxonomies]
categories = ["impressions"]
tags = ["HTC Universal", "MDA Pro", "modding", "OpenEZX", "Qtopia", "Windows Mobile"]
[extra]
author = "Peter"
+++
Well, this is really happy Easter. Yesterday I received my MDA Pro (HTC Universal) and I finally received some read out firmware of A910i. _Merci beaucoup, gama!_
<!-- more -->

OK, there are (as always) some bad points. I tried to run <a href="http://linuxtogo.org/%7Ehtcpxa/htcuniversal/index.html">Linux/QTopia</a> on my (still T-Mobile branded, but unlocked) MDA Pro, and I always get some &#8220;Kernel panic&#8221;message, what makes me kinda sad, as Windows Mobile isn't that good, Internet Explorer for example is just a horrible browser. But the device is a nice one, it is huge and heavy, but the  keyboard is just wonderful.
I think I'll flash it to some English &#8220;Crossbow&#8221;, and maybe I'll be able to run some Linux on it successfully (though the error might be due to my 4GB SD (not SDHC) card&#8230; but I don't know. BTW: does HTC-Linux support SDHC?

The bad thing about A910i firmware, is that I don't have much spare time ATM to really work on a FW&#8230; I might have time for trying to flash my A910 with parts of that A910i FW, but that's it, due to lots of University work.

I wish you all a happy Easter!

Boot messages of my Universal before Kernel panic:

~~~~~
Mounting SD card
Mounting root fs via loopback
Switching to new root
switch_root: bad init &#8216;/sbin/init'
[3.100000] Kernel panic - not syncing: Attempted to kill init!
~~~~~

And (repeated installation on &#8220;Crossbow&#8221;): While creating the partition, I get a warning: &#8220;Invalid tar magic&#8221;.

