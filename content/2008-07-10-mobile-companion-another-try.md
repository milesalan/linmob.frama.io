+++
title = "Mobile companion... Another try"
aliases = ["2008/07/mobile-companion-another-try.html"]
date = "2008-07-10T07:10:00Z"
[taxonomies]
categories = ["personal"]
tags = ["HTC Universal", "MDA Pro", "modding", "One A110", "One A120"]
[extra]
author = "Peter"

+++
The thing I am searching for a long time right now is a suitable &#8220;Mobile Companion&#8221; device, which is portable enough to carry it &#8220;always&#8221; with me (my 15&#8221; HP nx6325 is being used as a desktop). 
<!-- more -->

After my experience with the HTC Universal I knew I was needing something faster and bigger, a netbook (the most prominent one of this relatively new category is certainly the ASUS Eee PC).

I was thinking and thinking, and finally I found something: A device called <a href="http://www.a110wiki.de/wiki/One_A120">ONE A120</a>, which will cost me less than a Acer Aspire One A110 including an additional 16GB SDHC, a touchscreen + internal USB hub and a small bluetooth dongle, as I will sell the included Windows XP license (I don't need it, I am a student. I can get MSDN-AA licenses (if I need) and I already have a Win XP license).

Why on earth did I choose this VIA C7M (ULV) + VIA VX800U powered device?
First of all, it was relatively cheap. It has a reasonable battery life (reported minimum at 100% load: 3&#160;1/2 hours, maximum nearly 5 hours). Linux is reported to run well on the devices more popular Linux brother, the <a href="http://www.a110wiki.de/wiki/One_A110">A110</a>&mdash;there are some issues, but they are fixable.

Additionally, as I want a secure solution, it is good to know that the devices CPU has VIA Padlock technology, which I'll use to secure my /home partition, which I want to put onto the SDHC (using my great and fast SanDisk USB SDHC reader which I will place in the device, as the devices' inbuilt card-reader isn't that well supported, yet (Additionally this eliminates the risk of loosing the SD).

If this all works out, I will sell my HTC Universal&mdash;if it doesn't I will sell my modded A120 ;)
