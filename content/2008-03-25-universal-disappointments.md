+++
title = "Universal disappointments"
aliases = ["2008/03/universal-dissapointments.html"]
date = "2008-03-25T18:28:00Z"
layout = "post"
[taxonomies]
categories = ["impressions", "software"]
tags = ["GPE", "HTC Universal", "Qtopia", "Windows Mobile"]
[extra]
author = "Peter"
+++
First of all, I want to thank the guys on #htc-linux for their hard work and help.
<!-- more -->

But all in all I am a bit disappointed&mdash;I am unable to start one of goboxlives GPE or OpenMoko images on my Universal (MDA Pro), as I was told this is due to a &#8220;watchdog&#8221;, which kills the haret/Linux starting process after some time. On WM5 it was possible to kill that watchdog, but on WM6 it isn't.

Nevertheless QTopia starts and works fine (it does not become killed, WHY?), but there is only a rather unintuitive way to get QTopia running landscape mode (which would be attractive because of Universals nice keyboard), and I was told it doesn't run weird in that mode. OK, you think, well, then don't care and use QTopia. You're right, I could do that, QTopia is a nice phone software. But I want to use Universal as a kind of micro notebook, as my 15&#8221; HP one is to big and to heavy for having it always with me. Universals size is just right for that kind of usage, using it as a little text-machine and mobile internet device is fun&mdash;but QTopia hasn't any interesting applications for that (no office tool, no browser) and I'm not that crazy to use that brick for phone calls and short messages only, just because I can use free software to do so.

I was told that GPE is rather slow. Well, it might be, but as fast as QTopia is, a little bit less speed is absolutely OK for me. Really, no problem with that&#8230; and maybe it is possible to use QVGA, to make it faster? Would be no problem for me. Really&mdash;I just want to use it for a little bit surfing and writing some stuff with [Abiword](http://abiword.org).

As QTopia is not killed I believe that there must be a way to run some GPE version&mdash;problem is: I have got no experience in creating images, and ATM I have got no time to learn it. I would pay money for a working GPE image, really&#8230;

But on the other hand: I was able to discover that Windows Mobile (6) isn't too bad. At least there is plenty of software out there, including some free software.
