+++
title = "Ubuntu - soon on your mobile phone"
aliases = ["2013/01/02/ubuntu-soon-on-your-mobile-phone"]
author = "peter"
date = "2013-01-02T17:19:00Z"
layout = "post"
[taxonomies]
tags = ["competition", "Firefox OS", "Jolla", "linux", "platforms", "QML", "Sailfish OS", "Samsung Galaxy Nexus", "Tizen", "ubuntu", "ubuntu for smartphones"]
categories = ["article", "software"]
+++
<a href="http://www.ubuntu.com/devices/phone">Ubuntu - soon on your mobile phone</a>
<div class="link_description">

Ubuntu just announced that it will be on smartphones soon, too. While there have been many players announcing to enter the (very) mobile space with their own OS (in 2013, Samsung is believed to bring Tizen to market, Mozilla’s Firefox OS will launch (at least in Brazil) and Jolla will release their first Sailfish OS phone), Ubuntu is one of the most popular desktop Linux distributions and quite popular.

Being an Ubuntu user on my notebook, I am really excited, especially after I have seen Mark Shuttleworth’s announcement video.

As an Android Linux kernel is all that is said to be needed to run the new QML-powered Ubuntu mobile OS, porting it to your Android smartphone should be as easy as with Firefox OS, but most likely a lot more tempting than the latter (which is tempting, too).

That’s all I have to say for now, head over to the link above and watch the announcement or just get your Galaxy Nexus ready.. ;-)

Update:

<a href="http://www.engadget.com/2013/01/02/ubuntu-for-smartphones/">Engadget</a> have a nice hands on video, that makes the whole thing a lot more believable (<a href="http://engadget.a.ec.viddler.com/engadget_u3umf13n927m2ob3fc94e1g310pltr.mp4?fd9f2a1c14aadf1069f047c663f41e2b2d6f94fa7a2ed6f31f96347a31b14fc03ca0d74f2cf3134ac343c6a523657cca27172f58bde597d4e7fc14a17bc4b1acf526a23b27754153f6f8ef45fb89ac7e1e0f42ad3aa983266550732775415f55edb2cea9">MPEG4 video</a> for those on desktops without flash)

<a href="http://www.theverge.com/2013/1/2/3828266/ubuntu-phone-os-hands-on">The Verge</a> have another hands-on (this time without the “benevolent dictator”)

<a href="http://arstechnica.com/gadgets/2013/01/canonical-unveils-ubuntu-phone-os-that-doubles-as-a-full-pc/">ArsTechnica</a> have a nice news posting, too.
