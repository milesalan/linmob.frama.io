+++
title = "LinBits 3: Weekly PinePhone news / media roundup (week 30)"
aliases = ["2020/07/26/linbits3-weekly-pinephone-news-week30.html", "linbits3"]
author = "Peter"
date = "2020-07-26T16:00:18Z"
layout = "post"
[taxonomies]
tags = ["Manjaro", "Lomiri", "UBports", "Ubuntu Touch", "PureOS", "PinePhone", "Librem 5", "Soldering", "Phosh", "Debian", "Mobian", "soldering", "postmarketOS", "Game Emulation"]
categories = ["weekly update"]
+++

_It's sunday, it's Linbits time!_

This is some the best stuff that happened around the PinePhone this week. _Commentary in italics._
<!-- more -->

### Software: releases and improvements
* [Manjaro has released newer images with phosh.](https://osdn.net/projects/manjaro-arm/storage/pinephone/phosh/) _I am still waiting for an alpha image with Lomiri._
* [Also, new OpenSUSE images!](https://mirror.alefnode.com/#browse/browse:opensuse-pinephone-images:openSUSE-Pinephone-0.0.2-1.img).
* [Phosh 0.4.2 has been released.](https://source.puri.sm/Librem5/phosh/-/releases/v0.4.2). Features include a restart option and new translations.


### Worth reading
* HomebrewXS: [PinePhone UBports edition](http://homebrewx.bplaced.net/pinephoneub.html). _Just a little review._
* Kevin's musings: [pinephone: initial thoughts.](https://www.scrye.com/wordpress/nirik/2020/07/25/pinephone-initial-thoughts/) _Another collection of experiences with the PinePhone &mdash; by a Fedora user._
* Liliputing: [ DragonBox Pyra handheld computer inches closer to release ](https://liliputing.com/2020/07/dragonbox-pyra-handheld-computer-inches-closer-to-release.html). _The Pyra is one of these community built devices, that just is taking ages until it will hopefully finally ship to the people that ordered one. The heart of it is a TI OMAP 5, Cortex A15, ARMv7 CPU, which was still bleeding edge when it was originally announced._ 
* Pine64: [All about the PineTab](https://www.pine64.org/2020/07/24/all-about-the-pinetab-update/). _The PineTab, which is basically a PinePhone without the EG25-G and a 10" display, will ship soon to the people that ordered one._ 
* __Special: [GUADEC 2020](https://events.gnome.org/event/1/)__: Due to COVID-19, the yearly GNOME users and developers conference is a virtual event this year. The overall [schedule](https://events.gnome.org/event/1/timetable/) has been quite interesting, so check it out. Hopefully recordings will be made available later. Here are a few slide decks that might be of interest:
  * [Matthias Clasen: Port your widgets to GTK 4](https://events.gnome.org/event/1/contributions/77/).
  * [Philip Withnall: How can I make my project more environmentally friendly?](https://events.gnome.org/event/1/contributions/79/).
  * [State Of The Shell](https://events.gnome.org/event/1/contributions/86/)
* __Added 07/28/2020__ (meant to include it, but forgot): Madaidans-insecurities: [Linux Phones &mdash; Comparison with Other Phones](https://madaidans-insecurities.github.io/linux-phones.html). _This is a critique of the claim that the new Linux phones were more secure than iOS or Android. [I partially agree, as you can read on fosstodon](https://fosstodon.org/@linmob/104565130213304275)._

### Worth watching
* Tyler's Tech: [Mobian &mdash; The Best Mobile Linux Distro](https://www.youtube.com/watch?v=CUBXNJRZ5aE&feature=youtu.be). _A nice review of Mobian._
* Howto: Privacy & Infosec: [Pinephone Manjaro Linux: Working SMS/Calls/Cell Data/WiFi/Cam! Demo](https://www.youtube.com/watch?v=0oIF9z6Bwzw), [Pinephone Mobian (Debian) - Termshark Sniffer Working, Apps, Firefox Tor Setup](https://www.youtube.com/watch?v=N9Zhb5nb9XU). _This channel might be one to watch._
* Dalton Durst: [Fixing the PinePhone USB-C VCONN issue the equally stupid way](https://www.youtube.com/watch?v=ZqOb45N2sMc). _A long stream which is fun to watch. Also, [UBports is supporting convergence now](https://nitter.net/Mariogrip/status/1286430885085155329) (well, soon)._
* PizzaLovingNerd: [Answering Your PinePhone Questions](https://www.youtube.com/watch?v=ZBqFiUjM5OY) _Maybe this video answers some of your questions, too._
* Duncan Bayne: [Firefox, YouTube, and Bluetooth on the PinePhone!](https://vimeo.com/439433113)
* nas: [AOSP Android 10 demo on Pinephone 2/2](https://peertube.co.uk/videos/watch/2f49721b-9620-4810-a428-1be8fc855714), [Dreamcast emulation on Pinephone](https://peertube.co.uk/videos/watch/384dde13-80a5-435e-aefe-fd9655cb83ce) _While the first video is more than a week old, I figured to include it anyway; as it's the only video I am aware of that shows how GloDroid 0.3.0 runs. The Dreamcast emulation video is just fun. nas have some more game emulation videos on their channel._
* Hackers Game: [Mobile IP-native Communication](https://www.youtube.com/watch?v=1t4L_QNZ74A). _This video shows how to get IP native calls working on the Librem 5 &mdash; it should work on the PinePhone, too._

### Stuff I did
_I had a very busy week at work and as the weekend was kind of busy too, as a new episode of the NerdZoom podcast had to be recorded. Also, as I managed to forget my microSD card box, I couldn't do much PinePhone related stuff this weekend._ 

I [noticed that incoming calls no longer work for me](https://nitter.net/linmobblog/status/1286297854152253441) on any Phosh/modem-manager based distribution (they still work on Plasma Mobile, which uses ofono), and apparently I am at least not totally alone with that issue. 

I now run postmarketOS with Phosh on the eMMC. I chose postmarketOS because of their support for full disk encryption and installed using one of their prebuilt images. The installer is really something! 

My current secondary OS is Arch Linux ARM Huong Tram Edition. On Arch and pmOS, I mainly experimented with software that is all about reading: RSS readers, Read-it-Later clients and so on. Furthermore, I have played with Qt/Plasma Mobile Apps on Phosh &mdash; I am already drafting a blog post on all this, so stay tuned.

PS: _If you love the fediverse as much as I do, you can follow me there [@linmob@fosstodon.org](https://fosstodon.org/@linmob)._
