+++
title = "LinBits 1: Weekly PinePhone news / media roundup (week 28)"
aliases = ["2020/07/12/linbits1-weekly-pinephone-news-week28.html", "linbits1"]
author = "Peter"
date = "2020-07-12T00:45:18Z"
layout = "post"
[taxonomies]
tags = ["Mobian", "PinePhone", "Librem 5", "Phosh", "Gnome Camera", "epiphany", "Arch Linux ARM", "Lomiri", "Cameras", "Flutter", "CutiePi", "Raspberry Pi", "Convergence"]
categories = ["weekly update"]
+++
_One day late, sorry!_

This is some the best stuff that happened around the PinePhone this week. <!-- more -->_Commentary in italics._

### Software: releases and improvements
* postmarketOS developer Martijn Braam made a [Camera script](https://git.sr.ht/~martijnbraam/python-pinecamera) for the Pinephone, and [wrote about it on his blog](https://blog.brixit.nl/camera-on-the-pinephone/) [twice](https://blog.brixit.nl/pinephone-camera-part-2/). _(This should have been in linbits 0, sorry.)_
* [Mobian](https://www.mobian-project.org) has integrated a new, working [Gnome Camera app](https://gitlab.gnome.org/jwestman/camera). It's available in the current nightly images and works. The developer [attributes](https://fosstodon.org/@flyingpimonster/104476040503554037) the slowness to the app, not the sensor.  _The camera is slow, but it is great to have a camera app now. If it does not work on your Mobian install yet, check out this [thread](https://forum.pine64.org/showthread.php?tid=10578)._
* [Huang Tram Linux, the Arch Linux ARM](https://github.com/dreemurrs-embedded/Pine64-Arch/releases/tag/20200711) build for the Pinephone, has made some progress and comes with an [adjusted keyboard layout](https://nitter.net/RealDanct12/status/1282022262455930881#m) for terminal usage. _I really look forward to trying this keyboard. It should be relatively trivial to bring it to other Phosh-based distributions if I am not completely mistaken._
* In other Arch/Manjaro news, there are efforts to port over Lomiri (which used to be called Unity 8 and is well known from Ubuntu Touch) to [Arch](https://nitter.net/RealDanct12/status/1281576483610189829)/[Manjaro](https://nitter.net/DMConstantino/status/1279797058334048257).

### Worth reading
* Slashdot: [Linus Torvalds: 'I Do No Coding Any More'](https://linux.slashdot.org/story/20/07/03/2133201/linus-torvalds-i-do-no-coding-any-more)
* omg!ubuntu!: [Google & Ubuntu Team Up to Bring Flutter Apps to Linux](https://www.omgubuntu.co.uk/2020/07/flutter-sdk-linux-desktop). _This might proove useful for the PinePhone, Librem5 and similar projects. If Flutter should be supported on Linux Desktops with Touch interfaces in mind, easy ports of existing flutter apps might become a thing and help with app availability._
* [Phoronix: GNOME 3.37.3 Released With More Features, Code Improvements](https://www.phoronix.com/scan.php?page=news_item&px=GNOME-3.37.3-Released) _GNOME 3.37 is the development branch that is going to become GNOME 3.38 in September. What's notable is the progress on GTK 4, and the new features Epiphany/Gnome Web is going to get, most notably the 'running in the background' feature for web apps which will certainly come in handy on the PinePhone. Eye of Gnome improvements for phone form factors are certainly welcome, too._
* TuxPhones: [New CutiePi Shell "Sidekick" mode brings Linux convergence to the next level](https://tuxphones.com/cutiepi-shell-sidekick-linux-convergent-dual-screen-touchpad-keyboard/) _Convergent computing on Raspberry Pi 3 powered hardware? I am not getting my hopes up, but it is interesting nontheless._
* Purism has posted two blog posts this week. The [first one by CSO Kyle Rankin](https://puri.sm/posts/mobile-app-stores-and-the-power-of-incentives/) is an interesting read about the incentives on mobile app stores. The [second one](https://puri.sm/posts/librem-5-dogwood-update-3/) shares some details about their forthcoming fourth Librem 5 production batch, which is the last one before the Evergreen batch, which will be the real, high number production run the majority of backers are going to receive.  _I like that they decided to include a larger battery compared to their previous iterations. This should really help with getting decent enough battery life._

### Worth watching
*  Дмитрий Куртуков: [Keyboard for PinePhone](https://www.youtube.com/watch?v=K1XQ5Hv0sZo). _This is a weird one, but you are going to see a (rare!) Nokia N950 being used as a PinePhone keyboard which is quite cool._
*  PizzaLovingNerd: [PinePhone OSes: Mobian](https://www.youtube.com/watch?v=seLfbIIAycc). _Better than my video, PizzaLovingNerd actually prepare their videos and it shows. Also: The Pure Maps suggestion is a good one._
*  PizzaLovingNerd: [Taking Photos on the PinePhone](https://www.youtube.com/watch?v=TbAkoRTHl-U). _Good video. Minor nitpick: Upgrading Mobian generally works, if you are stuck on an old kernel, run_ `sudo dpkg-reconfigure linux-image-5.7-pinephone` _, as the [Mobian Wiki](https://wiki.mobian-project.org/doku.php?id=troubleshooting) suggests. Worked for me!_

### Stuff I did
_This really was a busy week for me. Aside from [thursdays blog post](https://linmob.net/2020/07/09/pinephone-daily-driver-challenge-part0.html), I did not really manage to do anyting at all._
