+++
title = "Weekly Update (6/2022): Plasma Mobile Gear 22.02, postmarketOS v21.12 SP2 and a lot of FOSDEM!"
date = "2022-02-11T22:56:00Z"
draft = false
[taxonomies]
tags = ["PinePhone","FOSDEM","eInk","OpenMandriva","openSUSE","Librem 5","Sailfish OS","PinePhone Pro","Ubuntu Touch",]
categories = ["weekly update"]
[extra]
author = "Peter"
+++

Plasma Mobile Gear 22.02, Maui 2.1.1., SailfishOS make progress on the PinePhone Pro, booting the Librem 5 from microSD, help UBports to test OTA 22, Signal Desktop as an unofficial Flatpak for ARM64 and a lot of FOSDEM 2022 videos!

<!-- more --> _Commentary in italics._ 
### Editorial
It's been a packed week: FOSDEM 2022 was amazing for everyone interested in FOSS on mobile, Plasma Mobile Gear 22.02 was released among other software.

That said, my personal highlight was something else: [Working suspend on the PinePhone Pro](https://nitter.net/linmobblog/status/1490443256605417475) (make sure to read the entire discusssion). _Caveat:_ I was very busy with the day job this week, so much so that I did not manage to test this properly (with a SIM card and all) yet. So take this brief progress report with a lot of salt and have fun reading what happened in the past (almost) seven days!


### Hardware news
* [Mobian is coming to the Fairphone 4](https://twitter.com/MobianLinux/status/1489927707035066368).

### Hardware enablement news
* The [Librem 5 can now boot from microSD card](https://fosstodon.org/web/@joao@librem.one/107757942402779749)! _It's still experimental, you need to mess with uuu and u-boot, but still!_
* Phoronix: [Qualcomm Open-Source Vulkan Driver Aims To Run More Windows Games Via Wine / Proton](https://www.phoronix.com/scan.php?page=news_item&px=Qualcomm-TURNIP-2022).

### Software news
#### GNOME ecosystem
* hadess: [“Videos” de-clutter-ification](https://www.hadess.net/2022/02/videos-de-clutter-ification.html).
* haeckerfelix: [The road to Fragments 2.0](https://blogs.gnome.org/haeckerfelix/2022/02/07/the-road-to-fragments-2-0/).
* This Week in GNOME: [#30 Fragmented](https://thisweek.gnome.org/posts/2022/02/twig-30/).
* Chatty 0.6.1 [has been released](https://source.puri.sm/Librem5/chatty/-/commit/d86b6bc2f613118218d4f4638e7dee21bd096e40), delivering support for per chat draft messages and a few fixes and translations.

#### Plasma/Maui ecosystem
* MauiKit: [Maui 2.1.1 Release](https://mauikit.org/blog/maui-2-1-1-release/). _Minor improvements mainly, but little details do matter!_
* KDE: [Plasma 5.24 – Perfect Harmony](https://kde.org/announcements/plasma/5/5.24.0). _This is quite desktop centric, but Mobile has seen quite some improvements, too.
* Plasma Mobile: [Plasma Mobile Gear 22.02 is Out](https://plasma-mobile.org/2022/02/09/plasma-mobile-gear-22-02/). _Great set of updates!_

#### Ubuntu Touch
* UBports: [Your biweekly UBports newsletter is here. No more teasers. It's time to code!](http://ubports.com/blog/ubports-news-1/post/your-biweekly-ubports-newsletter-is-here-no-more-teasers-it-s-time-to-code-3818).
* UBports: [Ubuntu Touch OTA-22 Call for Testing](http://ubports.com/blog/ubports-news-1/post/ubuntu-touch-ota-22-call-for-testing-3803). _Do test!_
* UBports: [Valentine's day. Show your love (for) Ubuntu Touch!](http://ubports.com/blog/ubports-news-1/post/valentine-s-day-show-your-love-for-ubuntu-touch-3833).

#### Sailfish OS
* flypig: [Sailfish Community News, 10th February, Sailfish SDK, PinePhone Pro](https://forum.sailfishos.org/t/sailfish-community-news-10th-february-sailfish-sdk-pinephone-pro/10243). _Regarding to Sailfish OS on PinePhone Pro, Adam Pigg shared more [progress](https://twitter.com/adampigg/status/1490796383858827269) [since](https://twitter.com/adampigg/status/1491339290210873346)._

#### Distro releases
* postmarketOS have [released v21.12 Service Pack 2](https://postmarketos.org/blog/2022/02/11/v21.12.2-release/), including the latest in Phosh, Chatty and Sxmo!
* New openSUSE images for the PinePhone are [also available](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/), for details make sure to read [this](https://nitter.net/hadrianweb/status/1491071230249926658#m), [this](https://nitter.net/hadrianweb/status/1491360500126720001#m) and [that](https://nitter.net/hadrianweb/status/1492115476918456325#m)!

#### App Availability
* If you've been longing to run Signal Desktop on your Linux Phone on a distribution it's not being packaged for (e.g. PureOS or postmarketOS), you can now try this [unofficial Flatpak build](https://elagost.com/flatpak/). _It actually [looks quite usable](https://forums.puri.sm/t/building-and-running-signal-desktop-on-the-librem-5/14999/54)!_

### Worth reading

#### Ways to use a PinePhone
* DistroWatch: [UBports as home server with media sharing](https://distrowatch.com/weekly.php?issue=20220207#tips). _I get using the PinePhone as a home server, but why would you do that with a distribution that needs to be 'mutilated' (rw mounting) to do so, and not one of the tons of desktop distributions?_

#### PinePhone Impressions

lwn.net: [PinePhone: trying out a Linux-based smartphone](https://lwn.net/SubscriberLink/883073/5cee003ff43af767/). _Nice read! (Make sure to subscribe to lwn!)_

#### PinePhone malware
* Mobian blog: [Script kiddy strikes back](https://blog.mobian-project.org/posts/2022/02/11/pinephone-malware-analysis/). _Fun read!_

#### Nemo Mobile at FOSDEM 2022
* Nemo Mobile: [NemoMobile at FOSDEM 2022(https://nemomobile.net/pages/fosdem/). _Nice to have the Q&A preserved like this!_


#### New Apps
* TuxPhones: [A new wave of Linux applications](https://tuxphones.com/convergent-linux-phone-apps/). _Not to be a buzzkill, but just like with libhandy apps, not every libadwaita app is automatically going to fit great on a phone screen._

#### PinePhone Keyboard
* Neil Brown: [PinePhone (Pro) keyboard case v. Planet Gemini v GPD Pocket 2: a photo comparison](https://neilzone.co.uk/2022/02/pinephone-pro-keyboard-case-v-planet-gemini-v-gpd-pocket-2-a-photo-comparison)

#### PinePhone Games and Game Development
* /u/thsalvo: [Final Fantasy XI on PinePhone Pro](https://www.reddit.com/r/pinephone/comments/sm96qv/final_fantasy_xi_on_pinephone_pro/).
* Rachel Singh: [Doing some stress testing & building games for my PinePhone Pro (C++/SFML)](https://www.youtube.com/watch?v=nUtBo5OQK9w). _Impressive performance!_



#### Purism at FOSDEM
* Evangelos Ribeiro Tzaras for Purism: [Our experiences hosting the FOSS on Mobile Devices Devroom at FOSDEM 2022](https://puri.sm/posts/our-experiences-hosting-the-foss-on-mobile-devices-devroom-at-fosdem-2022/).

#### Firmware stuff
* Phoronix: [Open-Source Firmware Foundation Established For Advancing Open Firmware](https://www.phoronix.com/scan.php?page=news_item&px=Open-Source-Firmware-Foundation).
* Phoronix: [Fwupd 1.7.5 Adds New Hardware Support, Prepares For EOL/Alternative Firmware](https://www.phoronix.com/scan.php?page=news_item&px=fwupd-1.7.5).

### Worth listening
* postmarketOS podcast: [#14 FOSDEM 2022 Special](https://cast.postmarketos.org/episode/14-FOSDEM-2022-special/). _Listen to this for first hand FOSDEM experiences and talk recommendations!_

### Worth watching

_Let's start this section with FOSDEM 2022:_

_If you're wondering, why there's so little Plasma Mobile the FOSDEM section: [KDE had its own stand](https://stands.fosdem.org/stands/kde_community/) and [handled recording differently](https://fosstodon.org/web/@bshah/107765245089526752) – I'll include recordings once they land!_

#### FOSDEM 2022: Linux Mobile - bigger picture
* Oliver Smith: [Linux Mobile vs. The Social Dilemma](https://fosdem.org/2022/schedule/event/mobile_social_dilemma/). _Really important talk! I love the idea for a "not-App Store"!_

#### FOSDEM 2022: Distributions

* Bernhard Rosenkränzer: [Taking a desktop OS to mobile phones: OpenMandriva now runs on the PinePhone - what did we have to do?](https://fosdem.org/2022/schedule/event/mobile_mandriva/). _OpenMandriva, a RPM based distribution with Mandrake heritage that published their first Plasma Mobile + Mobile Manager-based image in January of 2021, did some more things on their own that make this talk quite interesting to watch. Also, I look forward to the landing of OpenMandriva Lx 4.3 image for PinePhone – I've been waiting for another image to make a video since early 2021. If you're impatient and want to try it out right now, this [image builder](https://github.com/OpenMandrivaSoftware/os-image-builder) is your best bet right now._
* Arnaud Ferraris: [2 Years of Mobian: Birth and growth of a mobile Linux distribution](https://fosdem.org/2022/schedule/event/mobile_mobian/).

#### FOSDEM 2022: Nemo Mobile
* Jozef Mlich: [Nemomobile - Bringing freedom to mobile](https://fosdem.org/2022/schedule/event/mobile_nemomobile/). _Great talk!_

#### FOSDEM 2022: Mobile kernel development/Mainlining devices
* Martin Kepplinger: [Librem 5 phone kernel report](https://fosdem.org/2022/schedule/event/mobile_kernel_l5/).
* Luca Weiss: [Porting mainline Linux to mobile phones](https://fosdem.org/2022/schedule/event/mobile_kernel_mainline/).
* Nikita Travkin: [Running Mainline Linux on Snapdragon 410: How we support over 25 devices in postmarketOS](https://fosdem.org/2022/schedule/event/mobile_kernel_snapdragon_410/).
* Caleb Connolly: [From Android to mainline on the Snapdragon 845: Extending the life of Android devices with upstream kernels and postmarketOS](https://fosdem.org/2022/schedule/event/mobile_kernel_snapdragon_845/).

#### FOSDEM 2022: Low level: Camera and Modem middleware
* Aleksander Morgado: [ModemManager in your phone](https://fosdem.org/2022/schedule/event/mobile_modemmanager/).
* Jacopo Mondi: [Status of camera support on mobile FOSS devices: An open discussion about the state of cameras on Linux-powered mobile devices](https://fosdem.org/2022/schedule/event/mobile_camera/).

#### FOSDEM 2022: GNOME on Mobile
* Tobias Bernard: [Welcome to Libadwaita](https://fosdem.org/2022/schedule/event/mobile_adwaita/).
* Evangelos Ribeiro Tzaras: [Anatomy of GNOME Calls: What goes into making a call in GNOME Calls](https://fosdem.org/2022/schedule/event/mobile_calls/).

#### FOSDEM 2022: Lisp on Mobile
* Sebastian Crane: [Portable Parens: Graphical Lisp programs for mobile devices: Techniques for building responsive, touch-friendly GUI applications with functional programming paradigms](https://fosdem.org/2022/schedule/event/mobile_graphical_lisp/). _While I don't know nothing about Lisp, I loved watching this!_

#### FOSDEM 2022: Linux on eInk!
* Alistair Francis: [Mainlining the reMarkable 2 eInk tablet](https://fosdem.org/2022/schedule/event/mobile_kernel_tablet/).
* Andreas: Kemnade: [The road towards using regular linux on ebook readers: Experiences and progress with Kobo/Tolino readers](https://fosdem.org/2022/schedule/event/mobile_kernel_ebook_reader/).

#### FOSDEM 2022: Get togethers
* Guido Günther: [Phosh Contributors Get Together: Get to meet the other Phosh contributors](https://fosdem.org/2022/schedule/event/mobile_phosh/).
* Evangelos Ribeiro Tzaras: [Closing session](https://fosdem.org/2022/schedule/event/mobile_closing_session/). _Continuation of the Phosh get together!_


#### FOSDEM 2022: Genode on PinePhone
* Norman Feske: [Genode meets the Pinephone](https://fosdem.org/2022/schedule/event/nfeske/).

#### PinePhone Pro
* Wolf Fur Programming: [PinePhone Pro Review](https://www.youtube.com/watch?v=SaDDCmiVF0Q)
* Wolf Fur Programming: [Flutter running on the pine-phone pro (FluffyChat)](https://www.youtube.com/watch?v=SqteKEmXXB8).

#### PinePhone Keyboard
* BlandManStudios: [PinePhone (Pro) Keyboard Unboxing - Linux Smartphone](https://www.youtube.com/watch?v=vx1e0kcrPrY). _Nice!_

#### Ubuntu Touch Game Dev
* Lumpology: [How To Make An Ubuntu Touch Game](https://www.youtube.com/watch?v=T7IHyDd4KKs).

#### Shorts
* A Sicilian in Sweden: [Parolottero on PinePhone](https://www.youtube.com/shorts/bPqMaRqLYYw).
* calebccff: [Plasma Mobile on postmarketOS on the OnePlus 6](https://fosstodon.org/web/@calebccff/107769567428088898). _Smooth!_

### Something missing?
If your projects' cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email!
