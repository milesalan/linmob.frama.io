+++
title = "Unboxing the PinePhone UBports Community Edition"
aliases = ["2020/06/11/unboxing-the-pinephone-ubports-edition.html"]
author = "Peter"
date = "2020-06-11T18:10:18Z"
layout = "post"
[taxonomies]
tags = ["video content", "Pine64", "PinePhone", "UBports", "Ubuntu Touch", "Droid 4", "first impressions", "unboxing"]
categories = ["impressions"]
+++

Two days ago I unboxed my Pine64 PinePhone:
* watch on [PeerTube](https://devtube.dev-wiki.de/videos/watch/9154dc88-3d5d-468c-a640-cad19f68cea4),
* watch on [LBRY](https://lbry.tv/@linmob:3/pinephone-ubports-community-edition:1),
* watch on [YouTube](https://www.youtube.com/watch?v=uhx7XelZuZk).
<!-- more -->
### Some notes

Since unboxing the PinePhone, I played around with a few of the many [software projects][pinephone-software] that already support the device by booting them from microSDHC/microSDXC cards, which, when bootable, are being used instead on the internal eMMC.

Generally, it seems, that this is still very much a device aimed at developers and enthusiasts&mdash;none of the software products I tried felt quite ready to be used as a daily driver.

As the UBports community edition has some [slight differences in hardware to the earlier iterations][pinephone-revisions] of the PinePhone (namely the Developer batch and the Braveheart edition), this may have let to some of the issues that i experienced&mdash;it always takes some time to adjust in software to the improvements in revised hardware.

Therefore I will just note down what I really liked:

- Sailfish OS feels quite responsive,
- Phosh (developed by Purism) is pretty great already,
- PostmarketOS (with phosh) is quite a bit quicker than Debian based Mobian,
- UBports has certainly matured since I last tested it on a Nexus 4.

[pinephone-revisions]: https://wiki.pine64.org/index.php?title=PinePhone#Hardware_Revisions
[pinephone-software]: https://wiki.pine64.org/index.php?title=PinePhone_Software_Releases
