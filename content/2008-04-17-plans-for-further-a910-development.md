+++
title = "Plans for further A910 development"
aliases = ["2008/04/plans-for-further-a910-development.html"]
date = "2008-04-17T19:43:00Z"
[taxonomies]
tags = ["A910", "crosscompiling", "i-geekmod", "OpenEZX", "vpnc", "weekly release"]
categories = ["projects", "software"]
[extra]
author = "Peter"
2021_commentary = "I am not sure for how many weeks this plan lasted. Maybe for two weeks, but I am pretty sure it wasn't for much longer."
+++
I don't have too much time, no. But I want the A910 to become popular (as it is quite cheap and does well as a Linux WiFi phone now), this is why I decided yesterday that I will try to spend some time at A910 &#8220;testing&#8221; firmware every week and rollout a weekly release on Wednesday. Why Wednesday? It's just the best day, I guess&mdash;of course I will not create that firmware on Wednesday, but while the weekend&mdash;it shouldn't be to hard to have a working version on Wednesday. 
<!-- more -->
If you want to get informed every wednesday about &#8220;testing&#8221; release&mdash;just leave me your e-mail address. And if you're afraid about loosing your data everytime&mdash;I will try to include a working backup/restore solution in my next release. Loosing data every time is too anoying for me, and I think it is for you as well.

Well, lets leave this topic now. Today I tried to cross-compile &#8220;vpnc&#8221; (just scroll down if you want to know, what it is / I will not integrate it into my firmware, because it is too big, but I think of releasing it as mpkg if there is demand). I can't say that it works (will have to test this tomorrow), but at least cross-compiling was successful&mdash;on first attempt. Maybe I'll port some apps ;)

There is another task for me: Become more familiar to Linux, shell-scripting and programming&mdash;it would be the best to work on this every day, but this does not seem possible to me&mdash;I want to try to improve my knowledge a little bit every week.

At the end I have got on advise: Make sure to have a look at the <a href="http://www.openezx.org/">OpenEZX project</a>, as one of their developers works on A910&mdash;he does great work. And running a completely free and open system on this cellphone sounds very good and preferable to me.
