+++
title = "My first days with a Chromebook"
aliases = ["2013/07/09/my-first-days-with-a-chromebook.html"]
author = "peter"
date = "2013-07-09T06:17:00Z"
layout = "post"
[taxonomies]
tags = ["Chrome OS", "Chromebook", "first impressions", "needs image work",]
categories = ["impressions"]
+++

<a href="http://brimborium.net/my-first-days-with-a-chromebook/img_20130709_005135/" rel="attachment wp-att-3168"><img src="http://brimborium.net/wp-content/uploads/2013/07/IMG_20130709_005135-500x375.jpg" alt="" width="500" height="375" class="alignnone size-medium wp-image-3168" /></a>

The evening before my final exams (which BTW went really well) I bought a Samsung Chromebook, to be precise the 303C12 A01 (the non-3G one). It was one of these eBay bids which you do when you are nervous and try to get your mind of things&mdash;210€ for a Chromebook, which retails for 299 Euros in Germany (which btw is grotesquely high compared to the US pricing) is what I consider quite a deal. But that's not so important.

What do I think of the device? It is a great little laptop. Keyboard, Display and Touchpad seem better to me than the ones of my Lenovo Thinkpad Edge E320, which wasn't exactly super expensive either, but still, this is impressive. Then there is the software, which is beautiful, but not too feature rich. Chrome OS still lacks a few apps for me, but it is pretty much ok, and I knew before that it would be difficult to do the switch. And then, there is always the option to [Crouton](https://github.com/dnschneid/crouton) and have "a real OS" like Ubuntu.

What is disappointing though is the following:
* that Google Docs isn't enabling offline mode per default on your Chromebook,
* that offline GMail is strangely broken on my unstable dev-version of ChromeOS ;),
* that I couldn't find a way yet to get a decent frontend for Feedly (I strongly dislike their website), which is for now my feed reader of choice  (and which is great via Reeder on my iPad, I use the iPhone version with FullForce on my jailbroken iPad2),
* that I couldn't figure out yet how to use Evernote offline,
* that I couldn't figure out yet how to use Wordpress offline.

Maybe I will upgrade to the 3G version, but even with Germanys 2nd best mobile network, Vodafone, you face some offline time here and there. So many of the problems described may remain.

Still, it's a good first impression with plenty more to figure out and even more to try out.
 
