+++
title = "CeBIT 2k8 - short coverage"
aliases = ["2008/03/cebit-2k8-short-coverage.html"]
author = "peter"
date = "2008-03-07T16:07:00Z"
layout = "post"
[taxonomies]
categories = ["events"]
tags = ["cebit", "CeBIT2k8", "OLPC XO", "UMPC",]
+++

I have to say that when I arrived at my parents yesterday evening, I was really tired. Somehow these trade fairs are really killing me, I guess it's all the devices to look for, all these people and the constant noise.
<!-- more -->
<img style="margin: 0pt 0pt 10px 10px;float: right;" src="20080306145346.jpg" alt="OLPC XO-1"  border="0" />
I don't have to talk much about interesting Linux-powered smartphones, as I didn't see any. Wistron NeWeb was there, but they didn't show their GW4. The other stuff was quite boring, though hands on (e.g. Sony-Ericsson Xperia X1) is always nice. 
Concerning &#8220;UMPCs&#8221;, I was able to get my hands on EEE-PC (at ASUS they where running Windows on it&#8230;), Via Nanobook (at Linpus) and OLPC XO (OLPC Austria was a part of _Linux Park_). It was really nice to play around with XO, which is a really well engineered, beautiful device, though I soon realized that the keyboard isn't made for my hands.
