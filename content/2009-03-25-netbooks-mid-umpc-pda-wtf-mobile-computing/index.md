+++
title = "Netbooks, MID, UMPC, PDA - WTF? (Mobile Computing - The Hardware)"
aliases = ["2009/03/netbooks-mid-umpc-pda-wtf-mobile.html"]
date = "2009-03-25T19:30:00Z"
[taxonomies]
tags = ["Android", "ARM", "form factors", "Intel", "keyboard", "MIDs", "mini notebooks", "netbooks", "news", "OLPC", "Palm", "UMPCs", "VIA", "WebKit", "webos", "x86"]
categories = ["hardware", "commentary",] 
[extra]
author = "Peter"
+++
_Some (personal?) thoughts about the different classes of mobile devices_
<!-- more -->
### Introduction
First of all, what do I mean when I say mobile devices? I personally believe that being mobile isn't being mobile. There may be some guys out there, saying that a 18.4&#8221; notebook is still mobile - I don' t agree to that. I believe that a 15.4&#8221; 3kg heavy notebook is certainly the end of mobility, to name the upper end (and this isn't comfortable but training, if you ask me). Now what's the lower end? Well, considering just mobile devices, I'd say it can be dead small, as long as you are able to use it,  but considering mobile computing I would say it is the screen size of the Openmoko Neo FreeRunner, 2.8&#8221;, and in fact I would d love it even more if it had a slightly larger screen, like the 3.5&#8221; of the HTC Universal - even though this certainly makes a device less pocketable.<a href="100_0407.JPG"><img style="margin: 0pt 10px 10px 0pt;float: right;cursor: pointer;width: 326px;height: 266px" src="100_0407.JPG" alt="" border="0" /></a>

When you worked with such a small device on a table like you would do with a notebook, you would become tired soon, as  such a solution isn't very ergonomic (if you had a keyboard on your table and the display somewhere close to your eyes, like mounted to your head, it might be not that bad., btw..).  So there are devices beyond this PDA class, Intel calls them MIDs. I have to admit that I never had a longer hands on with a MID, only some short ones on CeBIT. More powerful MIDs running Windows XP or Vista are called UMPCs, and then there are mini notebooks, called netbooks by Intel. The first one of these was the ASUS Eee PC 701, and we have seen many following since then. Compared to UMPCs, mini notebooks are a lot cheaper. This and the fact that they look familiar to notebooks (and that you use them basically the same way, besides they're smaller, made these devices a huge success on the markets, even (or exspecially?!) in times of a economic crisis.

### Netbooks or mini notebooks - 7&#8221;-10&#8221; (12&#8221;)

<a href="20080306145346.jpg"><img style="margin: 0pt 10px 10px 0pt;float: right;cursor: pointer;width: 240px;height: 240px" src="20080306145346.jpg" alt="" border="0" /></a>
While I was fascinated by all these mini notebooks first (I liked the Eee PC 701 a lot, and the OLPC even more, before), I dislike the fact that while they were first 7-9&#8221; devices, they are now in a 10&#8221;-12&#8221;range (looking at the announcements, at least). This makes netbooks just a kind of cheap subnotebooks. The often heard argumentation for this screen bump (which has the positive side effect of bigger keyboards) is that people would request it - and I believe that there is a mayority that really does so.  But nontheless, I don' t like this trend, as the devices arent only bigger, but heavier, too - they are still mobile but in no way you can continue calling them ultramobile. And anyway: Calling a 12&#8221; device a &#8220;mini notebook&#8221; or &#8220;netbook&#8221; is just <del>marketing</del> bullshit, we've had those around for a long time, they were called subnotebooks and that is, what these devices are.

Writing and editing for a netbook site since two weeks, I really start hating some of the announcements you have to publish if you want to cover everything: Most devices are just the same. 10.2&#8221; or 10.1&#8221; screen (sometimes LED backlit), Intel Atom N270 (or N280, isn't that different) processor, two chipsets, that's it. Of course, minor differences in build quality, keyboards, screens (glossy or not), time they run on battery and thickness. My bad, I was close to forget that some have Bluetooth, while others don't and that we have some with 802.11&#160;b/g and some with 802.11&#160;b/g/n. And I shouldn't forget to mention that there are different designs and brandings ;)  

#### List of todays &#8220;netbook&#8221;-platforms (seen in device announcements or real devices)

##### x86:
* <a href="http://en.wikipedia.org/wiki/Intel_Atom">Intel Atom N270 / N280 (&#8220;Diamondville&#8221;) + Intel 945GSE</a>
* <a href="http://en.wikipedia.org/wiki/Intel_Atom">Intel Atom N280 (&#8220;Diamondville&#8221;) + Intel GN40&#160;</a>
* <a href="http://en.wikipedia.org/wiki/Intel_Atom">Intel Atom Z5xx (&#8220;Silverthorne&#8221;) + Intel SCH US15W (&#8220;Poulsbo&#8221;) _(&#8220;Menlow&#8221; Platform)_</a>

* Intel Celeron M (ULV353) + Intel 915GML
* Intel Atom* + <a href="http://www.nvidia.com/object/sff_ion.html">NVidia Ion</a>
* <a href="http://www.via.com.tw/en/products/processors/c7-m/">VIA C7-M ULV</a> / <a href="http://www.via.com.tw/en/products/processors/nano/">Nano Uxxxx</a> + <a href="http://www.via.com.tw/en/products/chipsets/v-series/vx855/index.jsp">VIA VX855</a>
* <a href="http://www.via.com.tw/en/products/processors/c7-m/">VIA C7-M ULV</a> / <a href="http://www.via.com.tw/en/products/processors/nano/">Nano Uxxxx</a> + <a href="http://www.via.com.tw/en/products/chipsets/v-series/vx800u/index.jsp">VIA VX800</a>
* <a href="http://www.via.com.tw/en/products/processors/c7-m/">VIA C7-M ULV</a> + <a href="http://www.via.com.tw/en/products/chipsets/v-series/vx700/index.jsp">VIA VX700</a>
* <a href="http://www.via.com.tw/en/products/processors/nano/">VIA Nano</a> + <a href="http://www.nvidia.com/object/sff_ion.html">NVidia Ion</a>
* <a href="http://www.amd.com/us-en/ConnectivitySolutions/ProductInformation/0,,50_2330_9863_13022,00.html">AMD Geode LX</a> + <a href="http://www.amd.com/us-en/ConnectivitySolutions/ProductInformation/0,,50_2330_9863_13022%5E13054,00.html">AMD CS5536</a>
* <a href="http://www.vortex86dx.com/">DM&amp;P Vortex86DX SoC</a> + <a href="http://www.xgitech.com/products/products_2.asp?P=23">XGI Volari Z9s</a>

##### MIPS 
* <a href="http://www.st.com/stonline/products/literature/bd/13577.htm">ST Micro Loongson 2F</a> + <a href="http://www.amd.com/us-en/ConnectivitySolutions/ProductInformation/0,,50_2330_9863_13022%5E13054,00.html">AMD CS5536</a> +SMI712
* <a href="http://www.ingenic.cn/eng/productServ/XBurst/pfCustomPage.aspx">Ingenic Xburst</a> 400 + unknown

##### ARM 
(we will see ARM11 and ARM Cortex A8/A9 SoC's in various flavours)

* <a href="http://www.freescale.com/webapp/sps/site/prod_summary.jsp?code=i.MX515">FreeScale i.MX515</a>
* <a href="http://focus.ti.com/general/docs/wtbu/wtbuproductcontent.tsp?templateId=6123&amp;navigationId=11989&amp;contentId=4682">Texas Instruments OMAP3</a>

I almost start to feel happy when I see a device using a Intel Atom Z-series + &#8220;Poulsbo&#8221; chipset combination and I am close to freak out when I see a VIA Nano / C7M + VX800 / VX855 combination or a so called &#8220;netvertible&#8221;. Of course there are some &#8220;outsiders&#8221; as the Loongson 2F powered Emtec Gdium netbook, which uses a USB-Stick and has no inbuilt SSD/HD. Or that small 7&#8221;, 128MB, 400MHz Ingenic-MIPS device, marketed as <a href="http://www.letux.org/wiki/index.php?title=Letux_400">&#8220;Letux 400&#8221;</a> in Germany, which might be a nice &#8220;mobile companion&#8221; (remember the cancelled <a href="http://en.wikipedia.org/wiki/Palm_Foleo">Palm Foleo</a>&#160;?), if would have a more modern operating system (among the software it ships with is a beta version of Firefox 2.0). Maybe you could call the <a href="http://www.laptop.org/">OLPC XO-1</a> a special netbook, too.

### 6&#8221;-8&#8221; - Mobile Companions?

I have to say that I like these &#8220;smaller than normal netbooks&#8221; range, as long as such a device has still a keyboard I am able to type on with my fingers (like my Quanta IL1 based &#8220;One A120&#8221;, its keyboard measures 21 x7.8&#160;cm), or one that is really thumb typeable, though these devices are usually a lot smaller - so let's go down slowly and think about devices like the Letux 400 for a second, as it is still wide enough for a keyboard not much (sizewise) worse than the one of my current mini notebook.

Of course a device with a 400MHz MIPS CPU and a 128MB RAM is not a great performer, more like a PDA (and I believe that there are more powerful PDAs). But think of a device like that with a fast ARM-SoC (like the TI OMAP 3 (or OMAP 4 in the future)) and 256MB or more RAM. Or imagine it at a form factor similar to the <a href="http://www.psionteklogix.com/products/discontinued-products.htm">Psion netBook (Pro)</a>.

And let's think of more innovation, or something you could call so in our boring PC worls, and think of such a mobile companion, which is a convertible (netvertible) and uses display technologies developed for the OLPC (<a href="http://www.pixelqi.com/">PixelQi</a>) - as long as it is rather lightweight (HTML5 (even though it doesn't include Ogg Theora) we will see less (Adobe) Flash video on the web, the system has to be powerful enough to play these videos in order to give the customer a rich internet experience.
In fact I believe that watching Flash videos is one of the most common power hungry applications in a world of cloud computing&#8230;

### UMPCs - 5-7&#8221;

Now forget my mobile companion dreams and let's head over to UMPCs.
<a href="Wibrain_B1_UMPC.jpg"><img style="margin: 0pt 0pt 10px 10px;float: right;cursor: pointer;width: 200px;height: 132px" src="Wibrain_B1_UMPC.jpg" alt="Wibrain B1 UMPC (CC) VIA Gallery" title=" Picture of the Wibrain B1 UMPC: (CC) VIA Gallery" border="0" /></a>
Actually I have to apologize first, that I am not that much into UMPCs. First of all, UMPC is a term describing a certain device class introduced by Microsoft in 2006. UMPC stands for Ultra Mobile Personal Computer, as they run &#8220;Origami&#8221; enriched Windows systems (like XP, Vista or &#8220;7&#8221;) you might be able to guess, that we talk of x86 based systems

#### List of common UMPC hardware platforms:

* Intel Atom Z5xx (&#8220;Silverthorne&#8221;) + Intel SCH US15W (&#8220;Poulsbo&#8221;)
* <a href="http://www.intel.com/products/mid/ultramobile2007.htm">Intel A1x0 (&#8220;Stealey&#8221;) + Intel 945GU</a>
* VIA C7-M ULV / Nano Uxxxx + VIA VX855
* VIA C7-M ULV / Nano Uxxxx + VIA VX800
* VIA C7-M ULV + VIA VX700

It must be due to the higher price tags of UMPCs and the fact that they have to run Windows, to be called that way that I am not to much into that.
But there are some devices, that look almost the same, but don't have to run Windows: MIDs.

### Mobile Internet Devices - 3.8&#8221; to 6&#8221; (estimate)

<blockquote>Mobile Internet Device (MID) is a term used by several vendors to describe a multimedia-capable handheld computer providing wireless Internet access. It is designed to provide entertainment, information and location-based services for the consumer market (rather than the enterprise). The MID is a larger form factor than a smartphone but smaller than the Ultra Mobile PC (UMPC). As such, the device has been described as filling a consumer niche between smartphones and Tablet PCs.

<cite><a href="http://en.wikipedia.org/wiki/Mobile_Internet_Device">Wikipedia</a></cite></blockquote>

<a href="http://farm4.static.flickr.com/3632/3369347617_5411d267dc.jpg?v=0"><img style="margin: 0pt 0pt 10px 10px;float: right;cursor: pointer;width: 259px;height: 199px" src="3369347617_5411d267dc.jpg" alt="UMID mbook M1 (CC) UMPCportal" title="UMID mbook M1 (CC) UMPCportal" border="0" /></a>

Looking at possible _MID hardware platforms_, it's a mixture of what I listed up for netbook hardware, removing the Diamondville and the VIA Nano and the more power hungry C7-M versions, but adding some ARM11 based SoCs, which are used in the Pocket PC/Smartphone range as well. As these devices are too small to feature &#8220;real&#8221; keyboards, some feature hardware thumb keyboards, others just feature a softkeyboard - I believe that the MID range, that I see as the range of devices between small netbooks and &#8220;really pocketable devices&#8221; is a somewhat &#8220;gadgety&#8221; one. You wouldn't usually use a MID to work hard, and it hasn't real advantages over a netbook while seated (unless you are the kind of girl/guy that types faster with two thumbs than with two hands). But when you want to be online on the go and even thinking of reading a longer text or watching a video on a sub 4&#8221; screen makes you feel pretty tired, they are just the right thing for you. Depending on the horsepower you imagine to require, your MID might be called UMPC, at least if you want to use Windows (and I admit that there might be some reasons (e.g. business software, fun with malware ;) ) for doing so). Of course you might consider using your MID like a netbook / mini notebook / subnotebook, using a <a href="http://linmob.net/2009/03/keyboard-for-freerunner.html">&#8220;pocketable&#8221; keyboard</a> ;) And we shouldn't forget the &#8220;Pandora&#8221; gaming handheld, which features a MID like form factor.

### Finally: &#8220;Smartphones&#8221; / &#8220;internet phones&#8221; - 2.6&#8221; to 3.8&#8221;

After a long walk on the &#8220;mobile devices&#8221; countryside, I finally have to find my keyring to open my home's door. And guess what: Finding the key isn't that easy. Well, first of all I want to define what I call &#8220;internet phones&#8221;. As you might realize looking at the screen size, I talk want to have some screen size, and additionally you should start thinking of touchscreen devices, at best running a software that fits to your fingertips. A classical PDA/Pocket PC form factor. Most likely it uses a WebKit based browser to show you the web (iPhone (OS), Palm Pre (WebOS), Android &amp; S60 phones, they all use WebKit as default rendering engine and there is a WebKit browser for Windows Mobile as well, TorchMobiles &#8220;IrisBrowser&#8221; - so as long as you use a platform that is expandable beyond J2ME (and as this is still about &#8220;Mobile Computing&#8221; I would really recommend you to do so). I have to say that I need a QWERTY hardware keyboard on such devices - as &#8220;mobile computing&#8221; implies to me that you are able to write longer texts, e.g. for finishing your first book or just for editing office documents.

Today these little computers are almost all running their operating systems on a variety of ARM (9,11, Vortex A8) SoCs - low power consumption, but good performance and faster than all PCs we had 15 years ago. As I described above, you won't use such a device (even if you are able to connect a keyboard over Bluetooth or USB host mode) as a real computer for work tasks unless you are a real enthusiast, just because the screens are too small. But imagining little projectors in these devices or foldable screens, they can become more and more usable for more than just typical SMS, browsing, twitter and phoning stuff. And all this while keeping their nice and pocketable form factor - but we enter the future now, so I better stop.

_See you again soon for &#8220;Mobile Computing - The Software&#8221;_


__Image sources:__
* Picture of the Wibrain B1 UMPC: <a href="http://www.flickr.com/photos/15932083@N05/2176875531/">(CC) VIA Gallery</a>
* Picture of the UMID mbook M1: <a href="http://www.flickr.com/photos/umpcportal/3369347617/">(CC) UMPCportal</a>
