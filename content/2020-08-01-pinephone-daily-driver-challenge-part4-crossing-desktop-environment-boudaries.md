+++
title = "PinePhone DDC pt. 4: Crossing (Desktop Environment) Boundaries"
aliases = ["2020/08/01/pinephone-daily-driver-challenge-part4-crossing-desktop-environment-boundaries.html"]
author = "Peter"
date = "2020-08-01T18:21:18Z"
layout = "post"
[taxonomies]
tags = ["PinePhone", "Video content", "Daily Driver Challenge", "postmarketOS", "Arch Linux ARM", "Huong Tram Linux", "Angelfish browser", "Chromium", "Ktrip", "KDE Itinerary", "Flatpak", "Flathub", "Scaling", "Phosh",]
categories = ["impressions", "howto", "videos",]
+++
This week is going to be about Flatpak, Reading apps and the intricacies of running (non-)Flatpak Qt and GTK apps on the respective "other side". Part 3: Crossing (Desktop Environment) Boundaries &mdash; running Qt/Plasma Mobile apps in Phosh.

<!-- more -->

__Caveat:__ _This post is going to be a one-way street. While GTK/Libhandy apps can also run on Plasma Mobile, they turned out to be utterly useless, as the software keyboard would refuse to work with them. Therefore, this is only about running Qt/PlasmaMobile apps on Phosh._

### Introduction
The worlds of Desktop Linux have been carried over to the Mobile Linux phones. The KDE project have their own [Plasma Mobile](https://www.plasma-mobile.org/) effort, and Purism started a GNOME Mobile effort for their Librem 5 smartphone.

Both are efforts are still in their early stages, both equally lack a few apps. That's why mixing and matching sounds like a good idea. As quite a few of these apps are packaged in distributions that support both efforts like postmarketOS and some of the apps are available as Flatpaks in Flathub or KdeApps, mixing and matching is just a few package installs away.

### Theming for non Flatpak apps
#### Option 1: qt5ct
If you want to set your theme in a detailed way using a GUI, just install `qt5ct` and add the line `QT_QPA_PLATFORMTHEME=qt5ct` to `/etc/environment` (as root).
#### Option 2: Manually setting a theme
Add the line `QT_STYLE_OVERRIDE=themename` with themename being `adwaita` if you have Qt adwaita theme installed (warning: This may leed to missing icons, [as explained in the "Theming Qt Apps" section of my video](https://www.youtube.com/watch?v=Ka9zh0Vt3zM?t=252s) or `breeze` (make sure it is installed) to `/etc/environment`.


### Apps you really should try
* Calindori (kdeapps, pmOS, AUR)
* KDE Itinery and Ktrip (kdeapp	s, pmOS) 
* Angelfish (kdeapps, pmOS, AUR)

kdeapps refers to the KDE Apps Flatpak repo, which contains nightly releases of KDE Apps. I explained using/installing flatpaks [in an earlier post](https://linmob.net/2020/07/27/pinephone-daily-driver-challenge-part2-flatpak-and-scaling-in-phosh.html).

### Figuring out missing dependencies for non-Flatpak apps
On postmarketOS, but also on the AUR, you might run into missing dependencies. Just try to run the app from the terminal and try to make sense of the output.

Now go and watch the [video](https://www.youtube.com/watch?v=Ka9zh0Vt3zM)!
