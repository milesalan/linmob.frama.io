+++
title = "LinBits 59: Weekly Linux Phone news / media roundup (week 33/34)"
date = "2021-08-25T08:00:00Z"
draft = false
[taxonomies]
tags = ["PinePhone","LINMOBapps","LinuxPhoneApps","GTK4","Cutie Shell","Maemo Leste","Librem 5",]
categories = ["weekly update"]
[extra]
author = "Peter"
+++

_It's Wednesday again! Now what happened since last Thursday?_

GTK 4.4, GSoC reports, Cutie Shell, Maemo Leste phone calls and more!<!-- more --> _Commentary in italics._ 

### Software releases
* [GTK 4.4.0 is out!](https://gitlab.gnome.org/GNOME/gtk/-/tags/4.4.0). _Read the blog post linked below!_
* [Biktorgj released 0.3.1 of the open source PinePhone modem firmware](https://github.com/Biktorgj/pinephone_modem_sdk/releases/tag/0.3.1), adding persistent USB audio support among other things. _No, I can't explain that feature._
* Manjaro have released [Phosh Beta 13 for PinePhone](https://forum.manjaro.org/t/manjaro-arm-beta13-with-phosh-pinephone/79665).

### Worth noting
* [MMS progress in Chatty is happening!](https://fosstodon.org/@kop316/106811879356261823)
* [Linux turned thirty today!}](https://twitter.com/n1mh/status/1430462815987437569) _Happy birthday!_
* [Maemo Leste is making progress with regards to phone calls!](https://twitter.com/maemoleste/status/1428354797590044675)

### Worth reading

#### Software progress 
* GTK Development Blog: [GTK 4.4](https://blog.gtk.org/2021/08/23/gtk-4-4/). _Having run 4.3.2 for a few weeks, I can confidentally attest that 4.4.0 is quite the improvment over 4.2.*._
* This Week in GNOME: [#6 Sharing, Caring](https://thisweek.gnome.org/posts/2021/08/twig-6/). _The audiobook player Cozy now supports phones properly, GNOME Podcasts gets important improvements and progress on the messenger front with Telegrand and Fractal Next!  
* Janet Blackquill: [Way More Than A Week In Tok](https://blog.blackquill.cc/way-more-than-a-week-in-tok/). _Impressive!_

#### GSoC progress reports
* Claudio Cambria: [Say hello to your new todo app! – Kalendar week 11 (GSoC 2021)](https://claudiocambra.com/2021/08/21/say-hello-to-your-new-todo-app-kalendar-week-11-gsoc-2021/). 
* Swapnil's Blog: [GSOC: Second Phase](https://swaptr.github.io/my-site/blog-gsoc/gsoc/2021/08/22/second-phase.html). _Kasts is going to get chapter marker support. Awesome!_
* Manuel Genovés: [Wrapping up GSoC 2021](https://blogs.gnome.org/manugen/2021/08/23/wrapping-up-gsoc-2021/). _Manuel worked on libadwaita._
* Alejandro Domínguez: [Multi-account support in Fractal-next: GSoC final report](https://aledomu.github.io/gnome/fractal-next-gsoc-2021-final-report/).

#### Pure(OS) Propaganda
* Purism: [Librem 5 Getting Faster With Age](https://puri.sm/posts/librem-5-getting-faster-with-age/). _Do read this, despite the headline I put above this, and watch the video, also linked below. BTW: The same is true for the PinePhone, and I have had the idea to do a similar video in February – for August (which is now, but I might actually do this in September). The Year Over Year improvement is quite impressive. But: What does this mean? Is it just, that the devices and their mainline kernels and user-space software just weren't ready way back when? Or is it an actual improvement that can continue, and the hardware is going to feel faster (in a way that's benchmarkable) in a year from now? How does this compare to vendor-kernel land, where a kernel is cobbled together and then it's mostly security patches, if anything? I can't quite say, but I am glad that there's progress, and I generally hope we'll achieve longer device lifetimes with Linux Phones._

#### Yet another Phone Shell
* Linux Smartphones: [Cutie Shell is a mobile phone UI inspired by Sailfish OS](https://linuxsmartphones.com/cutie-shell-is-a-mobile-phone-ui-inspired-by-sailfish-os/). _Cutie Shell looks quite nice, [Danct12 since managed to get it to run on Arch Linux ARM](https://twitter.com/RealDanct12/status/1430588652531306496), so I might get first hand experience with this soon!_

#### Software landscaping
* Emacsen's Blog: [The Search for a FLOSS Mobile OS (Aug 2021)](https://blog.emacsen.net/blog/2021/08/23/floss-mobile-os-aug-2021/). _Interesting read! Obviously, I am on the "to not Android"-side, but still._

#### Developer relations
* PINE64: [DevZone rollout](https://www.pine64.org/2021/08/23/devzone-rollout/). _Initially, this is for the PineNote eInk tablet only, but it will be for 

#### How to
* Spaetzblog: [Flashing the FOSS modem firmware on the Pinephone](https://sspaeth.de/2021/08/flashing-the-foss-modem-firmware-on-the-pinephone/). _Great tutorial!_

#### Worth listening
* DLN Xtend: [70: Pining about Pine64 \| DLN Xtend](https://dlnxtend.com/70). 

### Worth Watching
#### Byzantium awesomeness
* Purism: [Librem 5 Getting Faster With Age](https://www.youtube.com/watch?v=Xgbp6FdRxIQ). _My favourite bit is the part where it turns out that Firefox is way faster than GNOME Web._

#### Various PinePHne videos
* Jozef Mlich: [Nemomobile sounds settings prototype](https://www.youtube.com/watch?v=KDIZb9Vdqf4). _Nice!_
* Niccolò Ve: [PineBook and PinePhone: Why I LOVE Them!](https://tube.kockatoo.org/w/24aef84a-b8e7-4701-91db-0c0bb446c955). 
* NOT A FBI Honey pot: [pinephone users after they find out they can RUN CMATRIX](https://www.youtube.com/watch?v=GK12mjpqW3g). 
* NOT A FBI Honey pot: [hello postmarketOS #pine64](https://www.youtube.com/watch?v=hkyjc9pjC3w). 
* PizzaLovingNerd: [YouTube on the #PinePhone](https://tilvids.com/videos/watch/b9ab1e16-1060-45cb-9418-c464039c0649). _Ironically I am linking to a PeerTube instance here ;-)._


#### GPL enforcement fun
* @RealSexyCyborg: [It’s all fun and games until Naomi Wu shows up at your office to get GPLv2 compliance](https://twitter.com/RealSexyCyborg/status/1428706989274583049). _Remember: Umidigi is who likely make the hardware of that so-called "Freedom Phone"._

#### JingOS
* JingOS: [JingPad A1 Improved Virtual Keyboard Interacting with Terminal \| JingPad A1虚拟键盘与终端交互体验 \| JingOS](https://www.youtube.com/watch?v=ggLM4iIj0ok)

#### 3D printed PDA fun
* mutantC: [mutantC v4 - Ultimate, modular Handheld PC for everyday work](https://www.youtube.com/watch?v=BESuyftW3oY).



### Stuff I did
#### Random

* LinuxPhoneApps.org has made a little bit of progress (one more app, one issue closed). [Also, let me thank you for this contribution](https://github.com/linuxphoneapps/linuxphoneapps.org/pull/6), Serranya!
* I also did go did [something Firefox related](https://fosstodon.org/@linmob/106811013846424578). The change is not just visual, I am also [changing the User Agent here](https://gitlab.com/1peter10/mobile-config-firefox/-/commits/proton-tab-hack/). There's one particularily annoying bug: When Squeekboard is activated, the tab switcher shows up below Squeekboard – I don't know how whether/how this could be fixed with more CSS or JS, but then I don't quite know what I am doing anyway ;-) 

#### LINMOBapps

The following app was added in the past week, pushing the App count to 311:

* [Gtkeddit](https://gitlab.com/caveman250/Gtkeddit), a GTK4 Reddit client written in C++.

It's also listed at [the new place](https://alpha.linuxphoneapps.org/apps/io.gitlab.caveman250.gtkeddit/).

[Read here what (else) happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master), as I did some maintenance on other app listings. As with LinuxPhoneApps.org, progress with LINMOBapps will likely be even slower in the coming week, but I will happily accept Merge Requests! So please [do contribute!](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/CONTRIBUTING.md)
