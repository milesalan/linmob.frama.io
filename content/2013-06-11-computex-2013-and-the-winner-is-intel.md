+++
title = "Computex 2013: And the winner is: Intel"
aliases = ["/2013/06/11/computex-2013"]
author = "peter"
date = "2013-06-11T07:05:00Z"
layout = "post"
[taxonomies]
tags = ["acer", "ARM", "ASUS", "Intel", "platforms", "Computex"]
categories = ["shortform", "events",]
+++
There is not much to write about this years Computex, as almost every news has spread a thousand times throughout the web. This is thus more a personal note: Intel have finally managed to win quite a few smartphone and tablet designs, and at the same time they have managed to really lower the consumption of their performance platform with "Haswell". Finally, thin, light, powerful and long lasting mobile computing solutions are possible. _Thumbs up!_
