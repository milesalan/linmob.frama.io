+++
title = "An email with helpful information"
aliases = ["2007/10/an-email-with-helpfull-information.html"]
date = "2007-10-14T11:38:00Z"
layout = "post"
[taxonomies]
tags = ["EZX/LJ", "modding", "Motorola A910",]
categories = ["projects"]
[extra]
2021_commentary = "Back then this wasn't as helpful for me as you would maybe think, as I did not know how to successfully exit Vi yet."
author = "Peter"
+++
As I haven't posted here for nearly a month, I will now quote an email I got by a guy I contacted. All copyrights go to him, I haven't had time to test his explanations, but maybe it helps some of you that want their A910 to connect to every wireless network._
<!-- more -->

<blockquote>
I'll try to make some explainations. We can discuss together in order to
improve the text and make a correct howto we can put on the web&#8230;
(contains and form (I'm an old vi dinosaur from 1994 ;) ) + the english,
as it's not my mother language&#8230;)
4 parts: telnetd, swap, wifi and mackconnectivitypack/fake_dsm

1) Telnetd/login
Motorola didn't install any telnetd nor login. (thanks, Moto :-)
Mount your phone as an usb mass storage, then edit
[path]/.system/javaCardRegistr<div style="direction: ltr">y
and set lines as follow (you can install a dummy java application then
adapt the values (IMEI, AppID,&#8230;) JavaId must be -1.

~~~~
[1bf504e3-a1e3-d1e8-b96a-40f3a7f60087]
AppID = {1bf504e3-a1e3-d1e8-b96a-40f3a7f60087}
Args =
Attribute = 2
BigIcon = inetd.png
Daemon = 0
Directory = /mmc/mmca1/scripts
Eraseable = 1
Exec = inetd_start.lin
FixedAllPosition = 0
FixedPosition = 0
GroupID = ezx
IMEI = 353300010213731
Icon = inetd.png
InstalledDate = 2007/6/6
JavaId = -1
LockValue = 0
MassStorageStatus = 1
MpkgFile =
Name = Inetd_Start.lin
OsVersion = 1.0
Shared = 1
Size = 64K
Type = 2
UserID = ezx
Visible = 1
~~~~
with /mmc/mmca1/scripts/inetd_start.lin

~~~~
#!/bin/bash
export QTDIR=/usr/lib/ezx
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/ezxlocal/sbin
export PATH=$PATH:/ezxlocal/sbin
my_alert='/usr/SYSqtapp/phone/alertprocess'
my_mid1='/ezxlocal/sbin/done.mid'
cp -f /mmc/mmca1/sbin/inetd.conf /tmp/inetd.conf
echo -n &#8220;Starting internet superserver:&#8221;
echo -n &#8221; inetd&#8221;&#160;; start-stop-daemon &#8212;start &#8212;quiet &#8212;chuid root
&#8212;pidfile /var/run/inetd.pid &#8212;exec /mmc/mmca1/sbin/inetd
$my_alert -playvol 1 -playfile $my_mid1
~~~~
/mmc/mmca1/sbin/inetd.conf contains:
~~~
telnet  stream  tcp     nowait  root    /mmc/mmca1/sbin/in.telnetd
in.telnetd -L /mmc/mmca1/sbin/login
~~~
for the in.telnetd daemon, login exec, those from my A780 are ok,
or google search Telnet_Samba_preload.rar on a russian moto site.
A780-AddOns.tgz is also very usefull for login and many more
(was available on the old  motorolafan download section, but no more today)
<a href="http://big.pcpm.ucl.ac.be/A780-AddOns.tgz" target="_blank"></a> (install without diska
sub-directory, into your mmc card)

Then after a A910 reboot, you see into games, a &#8220;Inetd_Start.lin&#8221; you
can activate (hear a sound).

you can connect your linux box (A910 usb in modem mode)
~~~~
usb 4-2: new full speed USB device using uhci_hcd and address 4
cdc_acm 4-2:1.0: ttyACM0: USB ACM device
usb 4-2: USB disconnect, address 4
usb 4-2: new full speed USB device using uhci_hcd and address 5
~~~~
see <a href="http://www.courville.org/mediawiki/index.php/EZX" target="_blank"></a><a href="http://www.courville.org">http://www.courville.org</a>/mediawiki/index.php/EZX
~~~
echo &#8220;AT+mode=13&#8221; &gt; /dev/ttyACM0
echo &#8220;AT+mode=99&#8221; &gt; /dev/ttyACM0
~~~
sometimes more than one time (why? :-), until you get something like
this in the kernel message (dmesg):
~~~
usb0: register usbnet at usb-0000:00:1d.2-2, pseudo-MDLM (BLAN) device,
12:ea:79:65:60:6f
usbcore: registered new driver usbnet
ifconfig usb0 192.168.1.1 netmask 255.255.255.0 mtu 900
telnet 192.168.1.2
Trying 192.168.1.2&#8230;
Connected to 192.168.1.2
Escape character is &#8216;^]'.

MontaVista(R) Linux(R) Consumer Electronics Edition 3.1
Linux/armv5tel 2.4.20_mvlcee31-mainstone_pxa27x

(none) login: root

MontaVista(R) Linux(R) Consumer Electronics Edition 3.1

No directory /root!
Logging in with home = &#8220;/&#8221;.
~~~~
I set a more convenient environment with

#  /mmc/mmca1/root/start
~~~
root@a910 ~ 11:38:16 &gt; cat /mmc/mmca1/root/start
#!/bin/sh
/etc/init.d/ezxenvr.sh
export PATH=$PATH:/mmc/mmca1/.system/bin:/mmc/mmca1/.system/sbin
export
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$QTDIR/lib:/lib:/usr/lib:/usr/lib/ezx/lib:/mmc/mmca1/.system/lib
export EZX_RES_FONT_PATH=/usr/lib/ezx/lib/fonts
eval &#8220;`dircolors -b`&#8221;
alias ls='ls &#8212;color=auto'
export HOME=/mmc/mmca1/root
cd $HOME
export COLUMNS=150
/mmc/mmca1/.system/bin/bash

root@a910 ~ 11:38:22 &gt; cat .bashrc
export PROMPT_COMMAND='echo -ne &#8220;33]0;root@910:${PWD/#$HOME/~}07&#8221;'
export PS1=&#8221;u@a910 w t &gt; &#8220;
export HISTFILESIZE=100
export HISTSIZE=100
export HISTCONTROL=ignoreboth
export QTDIR=/usr/lib/ezx
export EZX_RES_FONT_PATH=/usr/lib/ezx/lib/fonts
eval &#8220;`dircolors -b`&#8221;
alias ls='ls &#8212;color=auto'
export HOME=/mmc/mmca1/root
export COLUMNS=150
. /etc/init.d/ezxenvr.sh
export
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$QTDIR/lib:/lib:/usr/lib:/usr/lib/ezx/lib:/mmc/mmca1/.system/lib
alias scp='scp -S /mmc/mmca1/.system/bin/ssh -q'
alias sftp='sftp -S /mmc/mmca1/.system/bin/ssh'
alias l='ls -l'
alias ..='cd ..'
alias cp='cp -v'
alias la='ls -la'
alias ll='ls -lA'
~~~

Then, I changed the telnetd and login directory, moving from /mmc/mmca1 to /ezxlocal (11Mb internal A910 storage) so  telnetd is no more dependant of the removable microSD.

2) swapspace
I also set a swap space (  no more
available on motorolafan)
in `/mmc/mmca1/.system/java/CardRegistry:`
~~~
[793bffb3-96f9-d929-bfd7-84e623c514cf]
AniIcon =
AppID = {793bffb3-96f9-d929-bfd7-84e623c514cf}
Args =
Attribute = 4
BigIcon = swapon.png
Daemon = 0
Directory = /mmc/mmca1/.system/swap
Eraseable = 1
Exec = swapon.lin
FixedAllPosition = 0
FixedPosition = 0
GroupID = ezx
IMEI = 35330001*******
Icon = swapon.png
InstalledDate = 2007/6/13
JavaId = -1
MassStorageStatus = 0
MpkgFile =
Name = Swap_on_off.lin
OsVersion = 1.1
Shared = 2
Size = 386K
Type = 2
UserID = ezx
Visible = 1
~~~
root@a910 ~ 12:14:25 &gt; cat /mmc/mmca1/.system/swap/swaponoff.sh
~~~
#!/bin/sh
SWAPDIR=/mmc/mmca1/.system/swap
SWAPFILE=$SWAPDIR/swapfile
if [&#160;! -f $SWAPFILE ]; then
     gzip -c -d  $SWAPFILE
fi
if&#160;! grep $SWAPFILE /proc/swaps; then
     swapon $SWAPFILE
     rm $SWAPDIR/OFF
     touch $SWAPDIR/ON
     my_mid1='/usr/data_resource/ringtone/rt_alert_2.mid'
else
     rm $SWAPDIR/ON
     swapoff $SWAPFILE
     touch $SWAPDIR/OFF
     my_mid1='/usr/data_resource/ringtone/rt_alert_5.mid'
fi
my_alert='/usr/SYSqtapp/phone/alertprocess'
$my_alert -playvol 1 -playfile $my_mid1
~~~
3) Wifi
The WIFI connection tool /usr/SYSqtapp/wsp/wlanapp fails everytime (UMA stuff? Orange or BT limitation? Grrr&#8230;) so the solution is to kill the
process when we are connected and continue with normal dhcp request:
see the awful script `setwifidhcp.lin`:

~~~
#!/bin/sh
/etc/init.d/ezxenvr.sh
export PATH=$PATH:/mmc/mmca1/.system/bin:/mmc/mmca1/.system/sbin
export
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$QTDIR/lib:/lib:/usr/lib:/usr/lib/ezx/lib:/mmc/mmca1/.system/lib
export EZX_RES_FONT_PATH=/usr/lib/ezx/lib/fonts
/usr/SYSqtapp/wsp/wlanapp &gt;/ezxlocal/wlanapp.log 2&gt;&amp;1 &amp;
pid=`find /proc/ -name exe -ls 2&gt; /dev/null|grep wlanapp|cut -d/ -f3`
connected=0
while [ ${connected} -eq 0 ]; do
     sleep 2
     connected=`grep &#8220;Successfully connected to AP&#8221;
/ezxlocal/wlanapp.log |wc -l`
done
kill $pid
kill `cat /ram/tmp/dhcpcd-eth0.pid`
rm /ram/tmp/dhcpcd-eth0.pid
dhcpcd eth0
my_alert='/usr/SYSqtapp/phone/alertprocess'
my_mid1='/ezxlocal/sbin/done.mid'
$my_alert -playvol 1 -playfile $my_mid1
~~~
Now, the problem is the setting of the dnsproxy process:

~~~
cat /etc/dnsproxy.conf
include /ram/tmp/dsmdns.conf
include /ram/tmp/pppdns.conf
~~~
Even when I fix a proper value into `/ram/tmp/dsmdns.conf`, it's not taken
into account.
By mounting a writable `/etc`, I can set a working `/etc/resolv.conf`
<a href="http://www.courville.org/mediawiki/index.php/EZX#Make_.2Fetc_editable_.26_change_root_passwd_.28more_secure_telnet.29" target="_blank">http://www.courville.org/mediawiki/index.php/EZX#Make_.2Fetc_editable_.26_change_root_passwd_.28more_secure_telnet.29</a>

5) `mackconnectivitypack/fake_dsm`

When the wifi link is ok (see iwconfig eth0) I can access internet with
native application
(ping, telnet,&#8230;) but not with web browser, nor native opera nor java opera-mini.

The solution would be to set a fake_dsm route but I did'nt succeed&#8230;
I used the dsm_spy from clovisKKK to adapt the fake_dsm code, as
suggested in the thread
<a href="http://www.motorolafans.com/forums/viewtopic.php?t=9096&amp;postdays=0&amp;postorder=asc&amp;start=0" target="_blank">http://www.motorolafans.com/forums/viewtopic.php?t=9096&amp;postdays=0&amp;postorder=asc&amp;start=0</a>

B******
</blockquote>

As you can easily see, there is still a lot of work to do, and I have to say, I'd prefer to modify the firmware and flash it into the device to get things working without lots of dirty hacks not everybody is able to perform.
