+++
title = "LinBits 30: Weekly Linux Phone news / media roundup (week 4)"
aliases = ["2021/01/31/linbits30-weekly-linux-phone-news-week4.html", "linbits30"]
author = "Peter"
date = "2021-01-31T22:38:00Z"
layout = "post"
[taxonomies]
tags = ["PINE64", "PinePhone", "Purism", "LINMOBapps", "Librem 5", "Manjaro", "Phosh", "postmarketOS", "JingOS", "PineTalk", "SDR"]
categories = ["weekly update"]
+++

_It's sunday. Now what happened since last sunday?_

Installer news, daily Manjaro, KDE CE Unboxings, PineTalk and more. _Commentary in italics._
<!-- more -->

### Software development and releases
* Manjaro [have announced](https://twitter.com/ManjaroLinux/status/1354526309515460609/photo/1) that they are now providing daily images for the PinePhone for Lomiri, Phosh and Plasma Mobile. Get them on [GitHub](https://github.com/manjaro-pinephone/)!
* The [postmarketOS installer is now more capable](https://twitter.com/braam_martijn/status/1354836266085478405), e.g. you can launch it from an SD card and then flash the PinePhones eMMC from there.
* Also, [postmarketOS now supports the Librem 5 Evergreen](https://freeradical.zone/@craftyguy/105632559246231662).
* To combine installers and the Librem 5: [Sebastian Krzyszkowiak](https://twitter.com/dos1/status/1354144544179236873) has submitted a [pull request](https://github.com/dreemurrs-embedded/Jumpdrive/pull/24) that enables JumpDrive support for the Librem 5.
* DanctNIX have [published a script](https://github.com/dreemurrs-embedded/archarm-mobile-fde-installer) that allows installing their Arch Linux ARM with Full Disk Encryption on the PinePhone.


### Worth noting
* [@calebccff](https://twitter.com/calebccff/status/1353465125051760640#m), managed to get Manjaro Linux ARM Lomiri running on OnePlus 6 on mainline Linux. He also made a [postmarketOS installer for the same device](https://github.com/calebccff/pmos-oneplus6).
* [@pojntfx posted a nice thread about postmarketOS running on the Bq Aquaris X5](https://twitter.com/pojntfx/status/1355851049798688769/video/1). _I really need to play with my Aquaris X5 again._

### Worth reading 

* Linux Smartphones: [Pinephone software updates: Manjaro releases daily builds, postmarketOS gets easier to install to internal storage](https://linuxsmartphones.com/pinephone-software-updates-manjaro-releases-daily-builds-postmarketos-gets-easier-to-install-to-internal-storage/). _Nice blog post on two of the main developments this week._
* Purism: [App Showcase: WhatIP](https://puri.sm/posts/app-showcase-whatip/). _A neat tool, that WhatIP! (And BTW: Still waiting for that email.)_
* Drew DeVault: [Use open platforms — or else ](https://drewdevault.com/2021/01/28/Use-open-platforms-or-else.html). _I wish this was less true, but it's not._

### Worth listening
* LinuxLads: [Season 6 - Episode 2: When Piggz Fly](https://linuxlads.com/episodes/season-6-episode-2). _The three Linux Lads are joined by Sailfish OS porter Adam Pigg to talk about mobile Linux._
* PineTalk: [Introduction](https://www.pine64.org/pinetalk/). _I am co-hosting a new Pine64 community podcast with Ezra, who you’ll know as [Elatronion from his PinePhone videos](https://www.youtube.com/channel/UCLN0SPhQo4jAPpTFNsxUnMg). My recommendation for subscribing: Use that secret [Opus feed](https://www.pine64.org/feed/opus/)._


### Worth watching

* TechPills: [How to flash OS images on the Pinephone (SD or eMMC)](https://www.youtube.com/watch?v=7HMkU43P9hw). _Great video by Gabriele!_
* DanctNIX: [Booting Arch Linux ARM on PinePhone with Full Disk Encryption](https://www.youtube.com/watch?v=QZgWUmlvL-Y). _If you want to know what this Full Disk Encryption thing looks like, wonder no more!_
* Linux Lounge: [What's On My Daily Driver PinePhone? (And Rambling About The PinePhone)](https://peertube.co.uk/videos/watch/c6430b12-2b4d-418e-9985-9f371a8e5226). _Interesting video. Now guess what kind of video I might do next?_
* Bhushan Shah: [Plasma Mobile running on Purism Librem 5](https://www.youtube.com/watch?v=7knsqVwfLiY). _So fast!_
* PINE64: [Revisiting PinePhone retro gaming](https://tilvids.com/videos/watch/cb24c868-f73f-416a-ae49-1cd64bd22ce7). _Awesome Retro Gaming video by Lukasz!_
* Privacy & Tech Tips: [RTL-SDR On The Pinephone! Demo, Installation/Hardware](https://odysee.com/@RTP:9/rtl-sdr-on-the-pinephone-demo:3). _What's nerdier than retro gaming? Correct. Software defined radio is!_
* Pizza Loving Nerd: [JingOS: iPadOS Inspired Tablet Linux Distro](https://tilvids.com/videos/watch/4c030ce0-409b-4367-86cd-b9f414eddea5) _Great video! Sadly it seems that JingOS are currently [violating the GPL](https://twitter.com/CarlKDE/status/1355657198437859336). While that's terrible, it's good for me as it means I have a good excuse for not making a video on this otherwise interesting distribution for now._
* R2-Dtech: [Librem 5 - A Phone for "Freedom"?](https://www.youtube.com/watch?v=CckvftCawP0) _A video about the Librem 5, make sure to also check out my video about this; or don't, because most of the Librem 5 footage is taken from my video. He explains Linux Phones quite well though._
* UBports: [Ubuntu Touch Q&A 93](https://www.youtube.com/watch?v=jTyO29bnsyY). _Just Florian (until Marius joins later) with a guest this time, Alan Griffiths, who works on Mir at Canonical. Make sure to catch Alan’s presentation to talking Mir in 2021, including Flutter support. Around minute 48 Florian talks about the PinePhone. Summary: If you want better Ubuntu Touch on the PinePhone, step up and join development, especially for the kernel. Marius Gripsgård explains why Ubuntu Touch can’t do device specific hacks, and adds that Qt 5.12 has landed (in the development channel) though, and the camera viewfinder is now accelerated which I can confirm._

#### KDE CE Unboxing corner
*  Dan Leinir Turthra Jensen: [PinePhone Plasma Convergence Edition Unboxing and First Impressions](https://www.youtube.com/watch?v=SOlcVXvqJi0). _Great unboxing of the KDE Community Edition PinePhone!_
* PewPewPaul: [pinephone kde plasma mobile edition unboxing](https://www.youtube.com/watch?v=0EebvAseqFE), [pinephone kde plasma mobile first boot](https://www.youtube.com/watch?v=dfGum6h1op0) _Two short videos, content as title suggests._ 
*  TalkNTech: [Don't Tell Apple I Have This Phone... Privacy With The Price! (Pine 64 Pinephone, Plasma, Linux) OS!](https://www.youtube.com/watch?v=n-9X4Qe3Y0g). _What a clickbaity tease, 12 Minutes, box not opened. And people say that my videos are long._


### Stuff I did

#### Content

I made one video, it's called "Unboxing the Purism Librem 5 Evergreen", watch it on
  * [PeerTube](https://devtube.dev-wiki.de/videos/watch/66ea3360-2b2a-4e3e-8796-de7ae271ac1b),
  * [LBRY](https://lbry.tv/@linmob:3/unboxing-the-purism-librem-5-evergreen:e),
  * and [YouTube](https://www.youtube.com/watch?v=5WdnPw537nU).
  
I published a two matching [blog](https://linmob.net/2021/01/28/purism-librem5-first-impressions.html) [posts](https://linmob.net/2021/01/30/purism-librem5-visual-impressions.html), where I share my first thoughts and show what the device looks like when photographed. _I will do more Librem 5 content soon, but I want to take my time to form a proper opinion on this device._

#### Random

I also played with [Signal clients unsuccessfully](https://fosstodon.org/@linmob/105645330617876836) and managed to get [Discord running in Anbox](https://fosstodon.org/@linmob/105651169672582130).

#### LINMOBapps

The App List now links this blog and vice versa. That aside, [an addition and some maintenance happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master). Not much happened sadly, as I was too busy with PineTalk and the Librem 5 (and only managed to add things to the tasks list, but _immychan_ thankfully contributed a new game to the games list. Please [do contribute](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/tasks.md)!
