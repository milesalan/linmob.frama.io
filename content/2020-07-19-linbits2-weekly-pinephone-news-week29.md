+++
title = "LinBits 2: Weekly PinePhone news / media roundup (week 29)"
aliases = ["2020/07/19/linbits2-weekly-pinephone-news-week29.html", "linbits2"]
author = "Peter"
date = "2020-07-19T15:00:18Z"
layout = "post"
[taxonomies]
tags = ["Manjaro", "Lomiri", "Bedrock Linux", "UBports", "Ubuntu Touch", "PureOS", "PinePhone", "Librem 5", "PinePhone Convergence Package", "Phosh", "Debian", "Mobian", "Site.js", "Enlightenment", "Tizen", "Game Emulation", "LVGL"]
categories = ["weekly update"]
+++

_It's sunday, it's Linbits time!_

This is some the best stuff that happened around the PinePhone this week. _Commentary in italics._
<!-- more -->

### Software: releases and improvements
* A new build of [ArchLinuxARM for the PinePhone](https://github.com/dreemurrs-embedded/Pine64-Arch/releases/tag/20200715) was released on July 15th, reaching an important milestone (100th commit merged). _More on this at the bottom of this page.
* [PureOS bullseye builds](https://nitter.net/AlexRob12252696/status/1283443658235281408). _[Mobian](https://www.mobian-project.org) has been based on "Bullseye" all the time._
* According to [redditor InfiniteHawk, BedRock Linux can be used with Mobian on the PinePhone](https://www.reddit.com/r/bedrocklinux/comments/hrqsat/bedrock_linux_works_on_pine_phone_with_mobian/). _"Bedrock Linux is a meta Linux distribution which allows users to mix-and-match components from other, typically incompatible distributions."_ This can e.g. be used to [run Blender on the PinePhone](https://www.reddit.com/r/linux/comments/ht3d4k/blender_runs_on_linux_pinephone/). _As different distributions lack different packages you might want or even need, this could prove quite useful. I will likely stick to one distribution using one distribution augmented by flatpaks for now, though. According to InfiniteHawk, `rpmfusion` has quite a number of packages build for use with Fedora ARM, which certainly makes Fedora more interesting._ 
* [Manjaro ARM has Lomiri (formerly known as Unity 8) running](https://nitter.net/fkardame/status/1284270770957570055#m). _See my notes on the UBport Q&A below._ 

### Worth reading
* The obvious first item this week is [Pine64s July Announcement](https://www.pine64.org/2020/07/15/july-updatepmos-ce-pre-orders-and-new-pinephone-version/), published July 15th. PinePhone-related highlights are:
  * PinePhone postmarketOS CE is now on sale,
  * new PinePhone 3GB/32GB Convergence package with a USB-C hub featuring USB A, video out and LAN for just 50 USD more,
  * Pine64 work on getting availability of the USB fix for everybody who does not want to do it on their own,
  * and they have also _"approached a vendor and began exploring our options for a slide-out keyboard akin to those used on the Nokia N900."_
  _The new 3GB RAM/32 GB eMMC edition impressed me so much, that I could not help it and ordered one._
* The [related Hacker News comment thread is interesting, too](https://news.ycombinator.com/item?id=23849202). 
* Not much to read, [just a tweet](https://nitter.net/TechShowMX/status/1283926385946484736). _#nospoilers_
* lupyuen: [Wayland and LVGL on PinePhone with Ubuntu Touch](https://lupyuen.github.io/pinetime-rust-mynewt/articles/wayland) _This is quite technical, but nontheless interesting. Also check out [this tweet](https://nitter.net/MisterTechBlog/status/1282861265547755520) on what's great about Ubuntu Touch._

### Worth watching
* Aral Balkan: [Live Stream: A web site on your phone with Site.js](https://ar.al/2020/07/12/live-stream-a-web-site-on-your-phone-with-site.js/). [According to Pine64](https://nitter.net/thepine64/status/1283190738516348929), you should carefully listen around the 14 Minutes mark in order to know what Pine64 is about. _This just shows how versatile devices that run "real" Linux can be. Also, [site.js](https://www.sitejs.org) and the "Small Web" idea are a lot to my liking._
* [Ubuntu Touch Q&A 80](https://www.youtube.com/watch?v=O2B8FTecVts). _This Q&A is particularly interesting, as the UBports people are joined by some guys from Manjaro, because Lomiri (formerly known as Unity 8) does now run on Manjaro thanks to UBports Marius Gripsgard. Lomiri is without question the most mature mobile Linux shell (Phosh would be second, with Plasma Mobile getting in at a solid third place), so it is great to see it on another distribution that has more and current software packages and is less sandboxed/more tinker friendly than Ubuntu Touch by UBports. Manjaro+Lomiri is one to watch ([photos](https://nitter.net/fkardame/status/1284264730870382594)), as even_ `Click` _is packaged up for Manjaro. Also, the recent Flutter announcement is mentioned._ 
* Дмитрий Куртуков: [pinephone + keyboard + kali linux. Who is your PC now!??](https://www.youtube.com/watch?v=UvsBlW5hvHEsh). _N950 keyboard + PinePhone + Kali Linux = awesome._
* Carsten Haitzler: [PinePhone vs. Z3](https://www.youtube.com/watch?v=j9TkzIKV9gY). _This video compares [Enlightenment](https://www.enlightenment.org/) on X11 on the PinePhone to [Tizen](https://www.tizen.org/) on the Samsung Z3, running the same base libraries. It seems that the binary driver on the Z3 delivers better performance than the open source Lima driver on the Pinephone. In addition to that I think it is worth noting that Enlightenment looks surprisingly good on Mobile; which induced fond memories of [SHR](http://web.archive.org/web/20191126184404/http://www.shr-project.org/trac), that featured an UI based on Enlightenment Foundation Libraries (EFL)._ 
* middygreen: [PureOS on PinePhone - Convergence (sorta)](https://www.youtube.com/watch?v=KT0t4cfQaGo). _Self explanatory, isn't it?_
* sadfwesv: [PinePhone running the latest crust (2020-07-12) capable of 100h standby](https://www.youtube.com/watch?v=K_JuckXcpi4). _PinePhone battery life just keeps getting better._
* sadfwesv: [p-boot seamless display pipeline passover to kernel + some electron fun](https://www.youtube.com/watch?v=Ce9vZj1r_-E). _[p-boot](https://megous.com/git/p-boot/about/) looks like the boot loader one would want when playing with more than one distribution at a time. Impressive work by megi!_
* HackersGame: [Librem 5 Dogwood Game Emulation](https://www.youtube.com/watch?v=ONLpCiTlTQg). _Nice to see some old Nintendo games run on the Librem 5. I wonder how well N64 emulation would work on the PinePhone ..._  
* Purism: [Dogwood What to Expect](https://puri.sm/posts/dogwood-what-to-expect/). _There are a few people out there that will receive the Librem 5 Dogwood batch &mdash; I am in the Evergreen batch &mdash; this what they'll get. [added 07/19/2020, 4:45pm CEST]_ 

### Stuff I did
I have been trying out Manjaro (Phosh) and Huong Tram Linux (aka Arch Linux ARM) and liked the latter better, although both are quite similar adidnd feel faster than Mobian. Receiving phone calls did not work for me &mdash; in the beginning of the week, so they might now. Also, [Maps apps](https://linmob.net/2020/07/18/pinephone-daily-driver-challenge-part1-maps-navigation.html) got a ton of my attention. I am considering setting up another page for the daily driver challenge, listing my criterias and the software that fulfills these (including information on the state of packaging of these softwares across distributions).
