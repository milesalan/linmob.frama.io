+++
title = "CES - A short roundup"
aliases = ["2009/01/15/ces-a-short-roundup"]
author = "peter"
date = "2009-01-15T08:06:00Z"
layout = "post"
[taxonomies]
tags = ["Android", "cebit", "CES", "netbooks", "Palm", "platforms", "Qtopia", "VIA", "webos", "windows_mobile"]
categories = ["events"]
+++
Of course Consumer Electronics Shows are always pretty interesting, and so was the latest CES. A bunch of news, e.g. Microsoft demo'ed Windows 7 among with allowing the download of a the first official beta. But let's forget about Microsoft and have a look at the mobile stuff.

The most interesting device presented at this years CES is, if you ask me, the <a href="http://www.palm.com/us/">Palm</a> Pre and Palm's new WebOS. There were rumors that Palm would prepare a new OS for years (and these rumors were credible as the need for a Palm OS successor was huge). What they released (I'd recommend to watch the <a href="http://www.palm.com/us/products/phones/pre/palm-pre-ces.html">presentation of the device</a>, if you have time and bandwith) looks pretty promising, the Pre is a goodlooking device and a TI OMAP 3 SoC promises that it's a fast one, too. WebOS is, at least as it seems, a browser based OS: Creating Apps in HTML 5, CSS and JavaScript seems weird at first, but when you consider today's web services, it isn't the worst idea after all, as it is confirmed that this isn't the only way to create apps. And don't forget: There is a Linux kernel underneath.
But that's all I want to write about this device, if you want to know more, check out <a href="http://www.engadget.com/2009/01/13/palm-pre-everything-you-ever-wanted-to-know/">Engadget</a>. 

Regarding smartphones there wasn't much I fancy. There is a phone stated to be able to run Android, the &#8220;<a href="http://www.engadget.com/2009/01/13/compulab-unveils-the-exeda-android-winmo-handheld/">exeda</a>&#8221; handheld, a huge device featuring the well known PXA270. Additional to that, Motorola unveiled the <a href="http://www.engadget.com/2009/01/06/motorolas-surf-a3100-headlines-three-pack-of-new-phones/">MOTOSURF A3100</a>, a phone that has been seen on spy shots a long time ago, running Windows Mobile 6.1 which makes it kind of, well, let's say &#8220;not that interesting&#8221; for us. 

Furthermore there were some new mini notebooks presented at the CES, some featuring the new 64bit enabled VIA Nano CPU (e.g. the <a href="http://www.engadget.com/2009/01/14/samsung-publishes-nc20-user-manual-new-images-surface/">Samsung NC20</a>), some featuring the various Atom combinations  people interested in &#8220;mini notebooks&#8221; should already know. Sounding the most interesting to me are the Nano powered and FreeScale i.MX 51 powered ones, especially the <a href="http://www.engadget.com/2009/01/09/pegatron-and-freescale-team-for-low-power-ultra-cheap-netbooks/">FreeScale solution</a>, as it means that these mini notebooks or let's call them mobile companions will run Windows CE in worst case and more likely Linux. This could be (as long as they are delivered with &#8220;good&#8221;, device and user experienced optimized linux distributions) another step to more people using switching to Linux on their desktops, and it could lead to better driver support by hardware manufacturers (thinking of USB TV sticks and such peripherals).

If you want to know even more about this CES, I'd advice you to go over to <a href="http://www.engadget.com/2009/01/12/ces-2009-all-the-stuff-and-more/">engadget once more</a>.

I expect that the Mobile World Congress in about one month will be much more interesting for those mainly interested in handsets and I think that I (equiped with a real digital camera (crazy, huh) will visit this years <a href="http://www.cebit.com">CeBIT</a> at Hannover, so that I can do a little coverage with having seen the stuff myself. 

One last thing, not really CES related: <a href="http://www.linuxdevices.com/news/NS4480114925.html?kc=rss">Nokia/Qt Software adds a LGPL license option to Qt 4.5, a decision which might be of importance for the whole GUI market.</a>
