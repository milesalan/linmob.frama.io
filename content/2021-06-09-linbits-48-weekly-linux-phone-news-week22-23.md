+++
title = "LinBits 48: Weekly Linux Phone news / media roundup (week 22/23)"
date = "2021-06-09T21:10:00Z"
# updated = "2021-06-03T07:54:00Z"
[taxonomies]
tags = ["PINE64", "PinePhone", "LINMOBapps", "Volla", "Librem 5", "Manjaro", "PineTalk", "Fractal-next", "ExpidusOS",]
categories = ["weekly update"]
[extra]
author = "Peter"
# update_note = "Added Purism Librem 5 USA posts that my feed reader did not catch previous to initial publication."
+++

_It's Wednesday. Now what happened since last Wednesday?_

A new p-boot-demo image, new Manjaro releases, GSoC announcements and more!<!-- more --> _Commentary in italics._

### Software releases

* [Manjaro Phosh beta 11](https://forum.manjaro.org/t/manjaro-arm-beta11-with-phosh-pinephone/69068) brings a few updates all around, including better support for Asian languages.
* [Manjaro Plasma Mobile beta 5](https://forum.manjaro.org/t/manjaro-arm-beta5-with-plasma-mobile-pinephone/69318) has also been released.
* Megi's [p-boot-demo image](https://xnux.eu/p-boot-demo/) has seen a new release, delivering 15 distributions on a 5.13 rc4 kernel.

### Worth noting
* Sxmo have set up an awesome new website at [Sxmo.org](https://sxmo.org/). Check it out!

### Worth reading

#### Competition ;-)
* FOSSphones: [Linux Phone News - June 8, 2021](https://fossphones.com/0608-news.html). _Another news roundup from today, yay!_
* LinuxSmartphones: [Test 15 different PinePhone operating systems with Megi’s latest multi-distro demo image](https://linuxsmartphones.com/test-15-different-pinephone-operating-systems-with-megis-latest-multi-distro-demo-image/). _Nice post about Megi's new p-boot-demo image!_

#### PinePhone impressions
* MeanMicio: [Liberating our mobile computing](https://meanmicio.org/2021/06/04/liberating-our-mobile-computing/).
* FOSSingularity: [ExpidusOS Test release 1 PinePhone Review](https://fossingularity.com/2021/06/02/expidusos-v0-1-0-pre-alpha-pinephone-review/). _It's still early days for ExpidusOS._
* FOSSingularity: [Installing Manjaro Plasma Beta 5 on The Pinephone](https://fossingularity.com/2021/06/04/installing-manjaro-plasma-beta-5-on-the-pinephone/).
* FOSSingularity: [Installing Mobian on the Pinephone. A Guide and Review](https://fossingularity.com/2021/06/06/installing-mobian-on-the-pinephone-a-guide-and-review/).

#### Software development
* Jozef Mlich: [Nemomobile fixes round two](https://blog.mlich.cz/2021/06/nemomobile-fixes-round-two/). _Great work and write-up by Jozef!_
* Janet Blackquill: [A Visually Stunning Week in Tok](https://blog.blackquill.cc/a-visually-stunning-week-in-tok/). _I hope that this nicely developing app gets packaged soon somewhere!_
#### GNOME Summer of Code
* Manuel Genovés: [Getting ready for GSoC 2021](https://blogs.gnome.org/manugen/2021/06/06/getting-ready-for-gsoc-2021/). _Manuel will be working on libadwaita, the continuation of libhandy for GTK4._
* Alejandro Domínguez: [Another year in GSoC and Fractal(-next)](https://aledomu.github.io/gnome/another-year-in-gsoc-and-fractal/). 
* Kai A. Hiller: [GSOC 2021: The Beginning](https://blog.kaialexhiller.de/?p=6). _Kai will also work on Fractal._

#### Hardware corner
* FOSSingularity: [A look at the Pine64 Pinephone dock internals](https://fossingularity.com/2021/06/01/a-look-at-the-pine64-pinephone-dock-internals/). _If you've ever wondered what the circuitry of that board looks like._
* LinuxSmartphones: [Volla Phone X will be a rugged smartphone that will likely support Android and Linux](https://linuxsmartphones.com/volla-phone-x-will-be-a-rugged-smartphone-that-will-likely-support-android-and-linux/).
* Purism: [From Fab to Table: Librem 5 USA Supply Chain Security](https://puri.sm/posts/from-fab-to-table-librem-5-usa-supply-chain-security/). 

### Worth listening
* PineTalk [010: Man of Many Apps](https://www.pine64.org/podcast/010-man-of-many-apps/). _Ezra and I pumped out a new episode of this community podcast, where Martijn Braam joins us. Please help out and submit questions and feedback!_

### Worth watching
#### Ubuntu Touch corner
* UBports: [Ubuntu Touch Q&A 101](https://www.youtube.com/watch?v=NHs05IxQqmY). _Lots of news after 4 weeks of silence. The move to 20.04 is progressing, they managed to launch a graphical Lomiri session on the 20.04 base. Many more fixes are landing for OTA 18, which will be quite the bug-fix fest from what it seems. Many new Halium 9 devices have been added to the UBports installer. As a [PinePhone user, you can help them testing the pull request to fix the Online Accounts GUI](https://github.com/ubports/ubuntu-system-settings-online-accounts/pull/17). Also in review: Fixes to MMS._ 
* Glenn Hancock: [Reviewing Ubuntu Touch, Pixel 3 and PinePhone](https://www.youtube.com/watch?v=f_4jAmvaCtY). _Nice comparison, although this seems to be an old release (stable channel?) of Ubuntu Touch on the PinePhone. This does not change the fact that if you just want to run Ubuntu Touch as a daily driver, of the two devices the Pixel 3 is likely the better choice._

#### Software tips and guides
* CWNE88: [Mobian Install on Pinephone](https://www.youtube.com/watch?v=p3xG8F4ymic).
* Privacy & Tech Tips: [Dino (XMPP): Decentralized End To End Encryption Messenger/+ Chatroom Client (Linux/Pinephone Shown)](https://www.youtube.com/watch?v=MyvBmO0tQGc). _Nice vid, although he should have used Dino's ['feature/handy'](https://github.com/dino/dino/tree/feature/handy) branch imho._

#### Unboxing corner
* Nat's Videos: [Librem 5 Unboxing](https://odysee.com/@NatTuck:9/nat-unboxes-librem-5:7).

#### Conference Talks
* GNOME Latam 2021: Martín Abente Lahaye: [GNOME en tu Teléfono](https://www.youtube.com/watch?v=vZetORq74ko). _This is a Spanish video, but the automatic translated subtitles were okay for me. If you are into GNOME development you may [want to read this blog post](https://feborg.es/gnome-latam-2021/) to find more interesting talks to watch._ 


### Stuff I did

#### Content

I only published a silly blog post about [my first year with the PinePhone](https://linmob.net/my-first-year-with-the-pinephone/).

#### Random

* I bought a used [Lenovo Chromebook Duet](https://www.lenovo.com/us/en/laptops/lenovo/student-chromebooks/Lenovo-CT-X636/p/ZZICZCTCT1X) and installed [Cadmium](https://github.com/Maccraft123/Cadmium) on it, getting Debian Sid on the device (I did not need to login with a Google account to get that far, just so you know). After that, I added the [Mobian repo](https://repo.mobian-project.org) and [installed Phosh](https://fosstodon.org/@linmob/106371335524916544). Currently, speakers and standby don't really work, and I did not manage to install Flathub apps limiting me to the sad number of mobile friendly apps packaged in Debian's repos. But, these limitations aside, it's usable and not too bad.

#### LINMOBapps
LINMOBapps got much love this week, either. I have added a few apps: 

* [Pine Pass](https://github.com/mpnordland/pine_pass), yet another GTK password-store.org GUI, and
* [Tangram](https://github.com/sonnyp/Tangram), "a new kind of browser", which does not fit the screen too well, but can still be used to place multiple web apps behind one launcher.

[Read here what (else) happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master). 
Please [do contribute!](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/CONTRIBUTING.md)
