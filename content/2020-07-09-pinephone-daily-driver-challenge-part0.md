+++
title = "PinePhone Daily Driver Challenge - Part 0"
aliases = ["2020/07/09/pinephone-daily-driver-challenge-part0.html"]
author = "Peter"
date = "2020-07-09T16:30:18Z"
layout = "post"
[taxonomies]
tags = ["PinePhone", "Video content", "Daily Driver Challenge", "Mobian", "Camera", "XMPP+OMEMO", "Matrix", "Maps", "Podcast Apps", "necessary apps"]
categories = ["article"]
+++
I shot and [published a video](https://youtube.com/watch?v=0Ju6szQebPk) today, which is &mdash; among other things &mdash; the first part of my "PinePhone as a daily driver" challenge, in which I am currently just trying to solve the most immidiate issues and figure out the most necessary features I would require to use the [PinePhone][https://www.pine64.org/pinephone/] as my primary smartphone.
<!-- more -->
In a [recent post](https://linmob.net/2020/06/26/41-minutes-of-PinePhone-content.html) I made up a list.

> Now: What would I need?
> * access to my Nextcloud instance,
> * a secure (as in encrypted) storage on the PinePhone/SD Card,
> * a way to open/use my KeePass password database,
> * XMPP+OMEMO messaging (already kind of works),
> * encrypted Matrix chats,
> * GPG encrypted email,
> * a way to open text documents that are not PDF (Abiword still has that `--enable-embedded` `./configure` flag),
> * something usable for `org-mode`,
> * an application to view maps and my approximate location.
>
> I would also like
> * navigation,
> * a `quassel`-client,
> * a way to run Android apps (like Threema) if necessary (= working anbox),
> * and a lot more I can't even think of now.

Obviously, this list is nowhere near complete. I would certainly require

* a camera app for quick "foto notes",
* an app for listening to podcasts,
* an app for notetaking,

and a lot more I can't think of now. Many of these are actually almost solved on the PinePhone with [Mobian](https://www.mobian-project.org) right now:

* Gnome Password Safe is a decent KeePass-Compatible password manager,
* the Nextcloud integration via Gnome Online Accounts works, calendar and contacts sync fine,
* XMPP+OMEMO is a given with Chatty, although the messages my brother sends me are quite often not being successfully decrypted,
* Fractal is a decent Matrix client, sadly it lacks encryption,
* Gnome Podcasts seems fine, though it lacks features,
* the new Gnome Camera app is good enough for just capturing a photo of a quick paper note or similar.

The Maps app, however, has issues and needs work, as you can see in my video:
* on [YouTube](https://youtube.com/watch?v=0Ju6szQebPk)
