+++
title = "QML component API's to come together?"
aliases = ["2013/01/20/qml-component-apis-to-come-together"]
author = "peter"
date = "2013-01-20T13:51:00Z"
layout = "post"
[taxonomies]
tags = ["Jolla", "Plama Active", "Link", "QML", "Qt", "Sailfish OS", "ubuntu", "ubuntu for smartphones"]
categories = ["shortform", "software"]
+++
<a href='http://aseigo.blogspot.de/2013/01/qml-component-apis-to-come-together.html'>QML component API's to come together?</a>

_Aaron Seigo has a good point here: Together, Ubuntu for smartphones, Jolla´s Sailfish OS and Plasma Active would have much better chances to make a dent in the universe. Let's hope that this will happen._
