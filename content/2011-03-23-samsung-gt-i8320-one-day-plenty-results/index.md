+++
title = "Samsung GT-i8320 - one day, plenty results"
aliases = ["2011/03/samsung-gt-i8320-one-day-plenty-results.html"]
date = "2011-03-23T09:15:00Z"
[taxonomies]
tags = ["LiMo", "SHR", "Nokia N900", "Samsung GT-i8320"]
categories = ["impressions"]
[extra]
author = "Peter"
+++
Yesterday I finally received the smartphone I planned to buy since february: The Samsung GT-i8320, a TI OMAP 3430, WVGA AMOLED phone which was sold by Vodafone running a LiMo r2 compliant OS. 
<!-- more -->

While there is an Android port on the way (which I didn't try yet), I felt like trying some other open source software that is less popular and thus hadn't been run by anybody on this hardware before (at least I couldn't find traces of such attempts on the web).

{{ gallery() }}

The first software I had to try out was of course <a href="http://shr-project.org/trac">SHR</a>, which stands for &#8220;Stable Hybrid Release&#8221; and is an open source mobile distribution which was created for the Openmoko Neo Freerunner and since has been ported to different, more powerful, newer hardware platforms such as the HTC Dream, the Palm Pre or the Nokia N900. 
And guess what: <a href="http://h1.pargon.nl/wiki/index.php?title=MeeGo">Using instructions and files which were made to run Meego 1.0</a> on the Samsung GT-i8320 plus some original firmware files taken from a LiMo rom, I managed to boot into SHR &mdash; first I had touchscreen issues, but after  deleting /etc/ts.conf and /etc/pointercal.xinput it ran just fine, even though it was noticeable that this very version of SHR isn´t quite optimized for capacative touchscreens.

After this early success (of course SHR isn't really usable on this hardware as of now, as the GT-i8320 modem hardware (QC MSM6290) is not supported by the underlying FreeSmartphone.Org middleware), I tried to run <a href="http://repo.meego.com/MeeGo/builds/1.1.80/1.1.90.7.20110315.10/handset/images/">MeeGo 1.1.90.7</a>, but I wasn't successful &mdash; and as I can't read the last error message (I tried to capture it on with my Palm Pre Plus, but it's unreadable there, too) I am unable to state what exactly is going wrong. The last screenshot shows the bootscreen of <a href="http://www.hackable1.org/">Hackable1</a>/<a href="http://www.defora.org/">Defora OS</a> &mdash; if the SD card used wasn't a slow no name one (having used up all the good ones for SHR and MeeGo) it might have worked &mdash; I might have seen a successful boot up.

Watch my short video: [SHR on Samsung GT-i8320 H1](https://www.youtube.com/watch?v=y0CSJ-hdw0I).

<i>Stay tuned for more.</i>

