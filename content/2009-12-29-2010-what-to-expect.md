+++
title = "2010. What to expect?"
aliases = ["2009/12/29/2010-what-to-expect.html"]
author = "peter"
date = "2009-12-29T17:59:00Z"
layout = "post"
[taxonomies]
tags = ["ARM", "CES", "future", "Intel", "netbooks", "platforms", "VIA"]
categories = ["commentary"]
+++
2010 is approaching pretty fast and it will be another interesting year for mobile devices, we will know that for sure after CES.

Why? First of all, the financial crisis did not really affect the sales of mobile devices, the industry sold lots of smartphones, netbooks and notebooks&mdash;and this will go on, thanks to innovation on both sides, hardware and software.

Let's talk about hardware first&mdash;several devices have been rumored or even announced, and besides this, there are some trends. In terms of computing power, we will see more and more smartphones with Cortex A8/A9 SoCs (Snapdragon (2), OMAP 3/4, Tegra 2), running at clockspeeds up to one gigahertz or even more&mdash;an example device we already know is the Google Nexus One, which will be sold by Google, supported by HTC and use the T-Mobile network&mdash;in the US, at least&mdash;I am not sure, if (or when) it will be available in Europe or Asia as well, but as it is supposed to be launched in early january, we won' t have to wait for too long&mdash;as Google Voice is US only currently, I do not expect it to come to europe too soon.

Besides these fast ARM based SoCs that impress with their speed for applications like web browsing and will be built into Smartphones, PMPs, MIDs, slates/tablets and Netbooks (&#8220;Smartbooks&#8221;),  there will more devices using SoCs that are slower on applications, but have decent video (1080p) capabilities&mdash;like the nVidia Tegra or the Telechips TCC8900, which will sell at cheaper price points than those that deliver more speed&mdash;let's hope that these Tegra1 and TCC8900 devices will vanish in the second half of 2010 (at least in terms of market entrys). Basically I expect devices that are (HD) video focussed, and others that are not (but are faster on other fronts)&mdash;those that do not aim at multimedia will likely run on Intels new Pinetrail platform, which does not allow high resolutions or digital video outputs&mdash;if you want Intel only (there will be video accelerators for Pinetrail by Broadcom and nVidia (Ion2)) and HD Video, you will have to get a Moorestown or CULV (e.g. based on the new Calpella platform) powered device. Actually Pinetrails graphics weakness might help VIA a little bit, maybe we will (finally) see some VX855+Nano (3000) powered netbooks/subnotebooks on western markets.

On the software side there are some big unknowns, especially concerning the ARM space: Will Android be used on slates/tablets, or rather other solutions? Will Jolicloud make its way to netbooks? What about Windows CE and Windows Mobile 7? 

What is likely is that we will see some subsidized solutions offering special services up to netbooks&mdash;we will see netbooks with GPS, and we hear that e.g. ASUS is therefore partnering with RIM and Garmin to offer push e-mail and navigation on their ARM powered netbook devices&mdash;and as &#8220;Pine Trail&#8221; is even more power efficent than the previous &#8220;Diamondville&#8221; platform, we can expect similar things for the Intel powered part of the mobile device market, where Moblin will be finally ready to ship.

_(More after CES.)_
