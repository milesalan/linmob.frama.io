+++
title = "LiMo R4 announced, devices to come?"
aliases = ["2011/03/limo-r4-announced-devices-to-come.html"]
date = "2011-03-06T13:21:00Z"
[taxonomy]
categories = ["software", "commentary"]
tags = ["Android", "bada", "development", "EFL", "Enlightenment", "LiMo", "MeeGo", "openmoko", "platforms", "Samsung", "Samsung GT-i8320"]
[extra]
author = "Peter"
2021_commentary = "Sadly, the architecture diagram image is no longer available."
+++
When LiMo 3 was released, nobody was really excited - it was last year in February, and at that time Intel and Nokia got all the attention for their MeeGo announcement, and besides that, Android was becoming huge at that time. Now, with MeeGo being pretty much dead in the handheld / smartphone form factor, this may change. While there wasn't much reaction on the announcement of  LiMo 4 during MWC this might change as soon as devices will be out (there is not a single LiMo 3 device available in Europe, BTW).
<!-- more -->

When you look at the LiMo platform architecture overview, and you know what this looked like before, you notice that there is a new UI stack: EFL - Enlightenment Foundation Libraries. In fact, this is a contribution by Samsung, who hired Carsten Haitzler, <abbr title="Benevolent Dictator For Life">BDFL</abbr> of this project, a while ago.

At CeBIT 2011 I had the chance to talk to C. Haitzler for about half an hour, and he told me that &#8220;there will be something in the future&#8221; - of course he can't talk about anything, but with Samsung investing in EFL to get a faster UI stack into LiMo, it's likely that there will be Samsung LiMo based device later this year (he also told me that there is no EFL inside Bada), I guess that there will be an announcement in summer. I'll not speculate just quote a few lines he wrote in his blog:

<blockquote>
Samsung is putting real resources behind EFL and using it to make a production-ready OS. The OS not only is Linux based, It uses all the other infrastructure from Linux (DBus, Glibc, Xorg, and much much much more). It is also going to be Open Source (GPL, LGPL etc.) and with open-source upstream gaining contributions back from Samsung.

This is a real effort and not just some research experiment. Stay tuned. Things will only improve from here. If you were hoping for a slew of MeeGo handsets, then maybe you should also keep an eye out for something from Samsung (actual product details not available yet - if it be a tablet, handset or TV or anything else for that matter).
</blockquote>

Many of you will ask: Why LiMo? This question has to be asked, I totally agree. The great thing about LiMo is that it builds on &#8220;real GNU/Linux&#8221; - Android doesn't, it just uses the kernel and some more small parts, but it has its own framebuffer UI stack (no X), its own C library &#8220;bionic&#8221; (no (e)glibc) and so on. Still, open-source people haven't been exactly exited about LiMo - LiMo phones aren't hackable (LiMo2 phones like the Samsung GT-i8320 (sold as Vodafone 360 H1 in Europe) have non writeable rootfs (cramfs) and locked bootloaders, and there were open alternatives like Openmoko. With Openmoko no longer in the smartphone market, and its dated at release hardware aging even more, this issue is partly solved - while contribution and hacking on LiMo still will be difficult with LiMo being a middleware and an IP pool.

Consumers didn't really like LiMo phones, either - LiMo based devices lacked an ecosystem in the past, there were only a few native applications and no free SDK. But these issues are addressable, and with EFL, next generation LiMo based phones will have a blazing fast UI which certainly will be fascinating, and feel more fluid than comparable Android devices. _This might be a comeback._
