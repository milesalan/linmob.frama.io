+++
title = "LinBits 53: Weekly Linux Phone news / media roundup (week 27/28)"
date = "2021-07-14T21:54:00Z"
updated = "2021-07-15T15:15:00Z"
[taxonomies]
tags = ["PINE64", "PinePhone", "Ubuntu Touch", "Phosh", "Maui", "Manjaro", "DanctNIX", "Arch Linux ARM", "postmarketOS", "Plasma Mobile", "E-Reader", "LINMOBapps",]
categories = ["weekly update"]
[extra]
author = "Peter"
update_note = "Added Tiles and mutantC section, linked PeerTube version of Martijn's video."
+++

_It's Wednesday. Now what happened since last Wednesday?_

New sounds for Plasma Mobile, Phosh, Phoc, Ubuntu Touch, Manjaro and DanctNIX see new releases and more!<!-- more --> _Commentary in italics._ 
### Software releases
* [Phoc 0.8.0 has been released](https://social.librem.one/@dos/106557650811135885), bringing a fix for idle inhibition and more. Read the [full release notes for more](https://source.puri.sm/Librem5/phoc/-/releases/v0.8.0). 
* [Phosh 0.12.1 has seen yet another release](https://social.librem.one/@agx/106578916170941330), containing mostly bug and translation fixes. Read the [full release notes for more](https://source.puri.sm/Librem5/phosh/-/releases/v0.12.1).
* [Manjaro Phosh Beta 12](https://botsin.space/@manjarolinux/106555866278226857) has been released.
* Danct12 has released [Arch Linux ARM 2021/07/03](https://github.com/dreemurrs-embedded/Pine64-Arch/releases/tag/20210713), containing Phoc 0.8.0 and more nice new things.
* [Ubuntu Touch OTA 18 is also out!](https://ubports.com/blog/1/post/3764).

### Worth reading
#### Software progress
* NxOS: [Maui Monthly Report 13](https://nxos.org/maui/maui-monthly-report-13/). _Great stuff that's coming along here, a lot to look forward to! Strike and NX Software center sound really exciting._
* Plasma Mobile: [Winners of the KDE and LMMS Plasma Mobile Sound Contest](https://www.plasma-mobile.org/2021/07/08/winners-of-kde-lmms-sound-contest/). _Sounds nice!_
* Purism: [Purism and Linux 5.13](https://puri.sm/posts/purism-and-linux-5-13/). _Mainlining keeps going quite well for the Librem 5!_
* Janet Blackquill: [Approximately Two Pink Weeks In Tok](https://blog.blackquill.cc/approximately-two-pink-weeks-in-tok).

#### GSoC progress
* Claudio Cambria: [An eventful week — Kalendar week 5 (GSoC 2021)](https://claudiocambra.com/2021/07/11/an-eventful-week-kalendar-week-5-gsoc-2021/). 
* Visvesh Subramanian: [A New Look for the Health app](https://visveshs.blogspot.com/2021/07/a-new-look-for-health-app.html).

#### Metadata nerdery
* Philip Withnall: [Add metadata to your app to say what inputs and display sizes it supports](https://tecnocode.co.uk/2021/07/12/add-metadata-to-your-app-to-say-what-inputs-and-display-sizes-it-supports/). _You may find this boring and think: Why this XML stuff? Because it matters. Specifying where an app will fit in metadata is an important step to better app stores for Linux Phones._

#### FOSS reporting
* PineGuild: [Manjaro ARM Beta12 of Phosh for PinePhone](https://pineguild.com/manjaro-arm-beta12-of-phosh-for-pinephone-2/).
* TuxPhones: [Some Kobo e-readers can now run Linux, becoming inexpensive e-ink tablets](https://tuxphones.com/kobo-clara-e-book-ereader-linux-kernel-hack/). _Great post!_

#### Assistive PinePhone 
* Linux Smartphones: [Mycroft, an open source voice assistant works with Linux smartphones like the PinePhone](https://linuxsmartphones.com/mycroft-an-open-source-voice-assistant-works-with-linux-smartphones-like-the-pinephone/). _I have had Mycroft on PinePhone on my To Do list for months. Maybe I'll get to it one day._

#### Ubuntu Touch
* Linux Smartphones: [Ubuntu Touch OTA 18 brings improved performance for older devices, among other updates](https://linuxsmartphones.com/ubuntu-touch-ota-18-brings-improved-performance-for-older-devices-among-other-updates/). _Brad's take on the above Ubuntu Touch announcement._

#### Propaganda
* Purism: [Proud to Be Top Contributor to GTK4](https://puri.sm/posts/proud-to-be-top-contributor-to-gtk4/). _Well, 0.3%. On the other hand, that's more than Canonical or Intel contributed so far._

### Worth watching

#### postmarketOS 
* Wolf fur Programming: [Plasma Mobile Oneplus 6t postmarketos](https://www.youtube.com/watch?v=F5PrS6Ls4A8). _Nice!_

##### eReader extravanganza
* Martijn Braam: [postmarketOS on the Kobo Clara HD](https://spacepub.space/videos/watch/3aac1314-291f-425c-b36a-0f5b6a54e357). _The comments are interesting, mentioning a software called [InkBox](https://www.youtube.com/watch?v=KaBj7acHRHk) for future software._

##### Tiles
* Drew Naylor 2: [RetiledStart demo #4: Landscape All Apps List](https://www.youtube.com/watch?v=FGZ3E5nqb0s). _If you've been missing Windows Phone, this might excite you!_

#### mutantC
* mutantC: [Revisiting mutantC v2, BTW everything works](https://www.youtube.com/watch?v=wh6L0XZP86Y). 


#### Tutorials
* kdlk.de: tech: [PinePhone Essentials: Turning off the Hardware Switches - Camera Turn Off](https://www.youtube.com/watch?v=6hKtGKBD7YE).
* kdlk.de: tech: [PinePhone App Development - Where to start? #01 Rust + GTK](https://www.youtube.com/watch?v=DHpOzgBgf6o). _Really nice!_

#### Grumpy stuff
* Twinntech: [Pinephone daily drive round 2](https://www.youtube.com/watch?v=OLpQ0Hqq5vY). _Admittedly, the title is not entirely fair. He's trying stuff again, and I hope it works. Also, you can see confirmation bias at work._

### Stuff I did

#### Content

Nothing. Well, we recorded another PineTalk, which will be out this weekend. I was going to publish a short post on Phosh's new app drawer feature tonight, but I just couldn't – too tired.

One thing I managed to actually finish, is a [Flatpak manifest so that you can try accelerated video playback in Clapper without tainting your /usr/local](https://framagit.org/1peter10/flatpaks).

#### LINMOBapps
LINMOBapps did not get much love again, but luckily Marco H. contributed one new app:

* [Vakzination](https://invent.kde.org/plasma-mobile/vakzination), a Kirigami app which manages your health certificates like vaccination, test, and recovery certificates.

[Read here what (else) happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master). 
Please [do contribute!](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/CONTRIBUTING.md)
