+++
title = "LinBits 57: Weekly Linux Phone news / media roundup (week 31/32)"
date = "2021-08-18T20:20:00Z"
draft = true
[taxonomies]
tags = ["PinePhone","LINMOBapps", "LinuxPhoneApps",]
categories = ["weekly update"]
[extra]
author = "Peter"
+++

_It's Wednesday. Now what happened since last Wednesday?_

A postmarketOS service pack, Phosh 0.13.0, KDE app ads, Jitsi calling on PinePhone and more!<!-- more --> _Commentary in italics._ 

__Please note:  

### Software releases
* https://github.com/Biktorgj/pinephone_modem_sdk/releases/tag/0.3.0


### Worth noting
* 

### Worth reading

#### Apps
* Gamey: [Podcasts on the Pinephone & Librem5 | Best podcast clients for GNU/Linux phones!](https://open.lbry.com/@gamey:c/podcasts-on-the-pinephone:8?r=D23fWA6ePDwmZpXPfaLBzkDVj5qWRWYW). _Nice roundup!_
* CNX Software: [How to use PinePhone as a mobile hotspot](https://www.cnx-software.com/2021/08/08/how-to-use-pinephone-as-a-mobile-hotspot/)
* Claudio Cambra: [Big fixes, more customisation, many improvements — Kalendar week 10 (GSoC 2021)](https://claudiocambra.com/2021/08/14/big-fixes-more-customisation-many-improvements-kalendar-week-10-gsoc-2021/)
* Sophie Herold: [Get set: Apps for GNOME on its mark](https://blogs.gnome.org/sophieh/2021/08/15/apps-for-gnome-on-its-mark/)
* NxOS: [Maui 2 Release(](https://nxos.org/maui/maui-2-release/). A truly great release.
  * Linux Smartphones: [Maui 2 released (open source, cross-platform app UI framework)](https://linuxsmartphones.com/maui-2-released-open-source-cross-platform-app-ui-framework/)

* PINE64: [Introducing the PineNote](https://www.pine64.org/2021/08/15/introducing-the-pinenote/)
* Linux Smartphones: [Pine64 news roundup: PinePhone Keyboard goes on sale in September, Ubuntu Touch 20.04 is coming, visual voicemail arrives, PineNote E Ink tablet revealed](https://linuxsmartphones.com/pine64-news-roundup-pinephone-keyboard-goes-on-sale-in-september-ubuntu-touch-20-04-is-coming-and-visual-voicemail-arrives/)

#### Jing Pad
* TuxPhones: [JingPad A1 (DVT) review, part 2](https://tuxphones.com/jingpad-a1-linux-tablet-review-part-2/)


https://fosstodon.org/@kop316/106772127260896896
https://blog.mlich.cz/2021/08/nemomobile-in-august-2021/

### Worth watching



### Stuff I did

#### Content

Minor updates

#### Random

* LinuxPhoneApps has made [some more progress](https://github.com/linuxphoneapps/), but not enough to put it online. Given a close family member's recent health emergency I can't set any deadline right now. It's going to be hard enough to keep these updates going.
* I have also played with Waydroid on Arch Mobile a bit, but not really, because I wanted to do so on camera

#### LINMOBapps

These apps were added in the past week, pushing the App count to 309:

* [Breathing](https://github.com/SeaDve/Breathing), a very simple application that guides your breathing pattern,
* [GNOME Screenshot](https://gitlab.gnome.org/GNOME/gnome-screenshot), a small utility that takes a screenshot of the whole desktop; the currently focused window; or an area of the screen,
* [Metronome](https://gitlab.gnome.org/aplazas/metronome) beats the rhythm for you, you simply need to tell it the required time signature and beats per minutes.

[Read here what (else) happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master). 
Please [do contribute!](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/CONTRIBUTING.md)
