+++
title = "GNOME Software 41 will work just fine on mobile"
date = "2021-09-12T15:30:18Z"
[taxonomies]
tags = ["Phosh", "Purism", "Librem 5" ,"PinePhone" ,"GNOME" ,"GNOME Software" ,"Appstream Metadata", "LinuxPhoneApps",]
categories = ["software",]
[extra]
author = "Peter"
# thumbnail = "09-updates.png"
+++
With GNOME 41 being released soon, one important app is now working fine on Mobile. But the new features don't stop there, making it worth a closer look.<!-- more -->

### What is GNOME Software?

GNOME Software is basically the "App Store" for GNOME (and thus Phosh) operating systems. For KDE Users: This is basically the KDE Discover of GNOME. Previously it has been available on most Phosh-running distributions. It can update your distributions, you can use it to install Flatpaks and more.

### What's the current state?

Some distributions just included the not-properly-scaling upstream GNOME Software, sometimes mitigated and made somewhat usable via `scale-to-fit` while other distributions shipped [Purism's patches](https://source.puri.sm/Librem5/pureos-store) to GNOME Software that Purism did for their PureOS store.

### What's changing with GNOME Software 41?

It now works just fine on phones like the PinePhone or the Librem 5, at least that's what I see when I run PureOS Store on my Librem 5 running PureOS Byzantium and my PinePhone running the AUR package `gnome-software-git` on [Arch Linux ARM](https://github.com/dreemurrs-embedded/Pine64-Arch/). Let's just have a few screenshots:

{{ screenshotgallery() }}

As you can see, GNOME Software can now provide a lot more context about apps. If you want to read up on the thoughts that led to this design, make sure to read Tobias Bernard's blog post titled ["Software 41: Context Tiles"](https://blogs.gnome.org/tbernard/2021/09/09/software-41-context-tiles/).

### What remains to be done?

As you can see on my screenshots, even apps that are known to work fine on the mobile form factor don't necessarily show up as running fine. This is an application metadata issue, which is going to have to be fixed by each and every upstream project. Thankfully there's a list at the bottom of the compatibility listing that [links to this page detailing what additional metadata will have to be added to appstream xml files](https://gitlab.gnome.org/GNOME/gnome-software/-/wikis/software-metadata#user-content-how-to-add-missing-hardware-information) – which hopefully will be further augmented, as it's currently just a link to [this blog post](https://tecnocode.co.uk/2021/07/12/add-metadata-to-your-app-to-say-what-inputs-and-display-sizes-it-supports/) we had in LinBits a while ago. Apparently, changes will also have happen over at [Flathub, which currently omits this support data](https://github.com/flathub/flathub/issues/2439). 

If you are an app developer (or someone that wants to help out your favourite app with proper metadata, as it's important but not necessarily fun to add), make sure to also read this [blog post by Tobias Bernard about getting apps ready for GNOME Software 41](https://blogs.gnome.org/tbernard/2021/09/07/ready-for-software-41/).

Assuming the changes at Flathub are going to happen, we can only hope that upstream projects are going to add this metadata – at least this is not [vendor specific custom metadata](https://puri.sm/posts/specify-form-factors-in-your-librem-5-apps/), but part of the [official appstream specification](https://www.freedesktop.org/software/appstream/docs/chap-Metadata.html#tag-requires-recommends).

### Will this obsolete LinuxPhoneApps.org/LINMOBapps?

While I have to admit that due to the complexity of the task, the complications occuring and my general lack of free time to work on these projects I would love to say "yes, this obsoletes my effort and I can rest now", it likely does not. Not every new developer will ever add all the necessary metadata, and knowing the Gaussian curve, some will refuse to accept merge request adding the data. BTW: If you want to help out linuxphoneapps.org, e.g. by coming up with a way to import upstream app metadata, please get in touch!
