+++
title = "Universal: GPE finally runs on my MDA Pro"
aliases = ["2008/03/universal-gpe-finally-runs-on-my-mda-pro"]
author = "peter"
date = "2008-03-28T09:27:00Z"
layout = "post"
[taxonomies]
tags = ["Android", "GPE", "HTC Universal", "NDA Pro", "Openmoko", "OPIE",]
categories = ["hardware", "impressions"]
+++
As there was no other solution to find, I flashed my MDA Pro to  Windows Mobile 5 WWE - to be able to run Linux successfully on it. And it works now!
<!-- more -->

First I tried to run GPE with the <a href="http://www.angstrom-distribution.org/unstable/autobuild/htcuniversal/">newest Angström autobuild image with newest kernel available</a>, but the kernels touchscreen support (and maybe other problems) and the image not starting an Xserver made me unable to gain a fresh experience.
Finally I'm now running the lower one of <a href="http://www.linuxtogo.org/%7Ehtcpxa/htcuniversal/images/x11-GPE/">Goboxlives GPE images for HTC Universal</a>, which works relatively nice (the upper one I tried before seemed to be faster, but hadn't any phone support) since I removed an openmoko-theme (I don't remember the exact name) package, which made the whole UI look a bit weird. openmoko-dialer (of July 2007) is still working, but in Clearlooks Style :)

The phone capabilties are worse than QTopia (which is a great phone software, as I wrote before), it's impossible to receive calls and ending calls is difficult, but you have to take in account, that it is an 2.6.21-hh14 kernel (latest kernel image I could find yet is 2.6.21-hh20) and that this image is quite old (more than half a year). I don't know whether htc-linux project is already working on a 2.6.24 (would be nice, SDIO'd be possible and think of &#8220;Android&#8221;&#8230;) kernel for HTC Universal (website isn't as easy to use as openezx.org), but I think that there will be some newer images some day.

I think I'll try out some more images soon, e.g. Goboxlives' OpenMoko image which is relatively new, and I'll try to get Wifi and GPRS to work, as I didn't manage to that yet.

_Edit:_ I just had a look at that Openmoko Image by Goboxlive. Well, I think I have to write on my view on OpenMoko later, but I first would like to use a newer image&#8230; Phoning with Openmoko seems to work, wifi does not (or I didn&#8217;t find out how it works). The UI is slow, but nice and usage.. well, you &#8216;ve got to get used to it, I suppose.
