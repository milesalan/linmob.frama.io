+++
title = "My first year with the PinePhone. Not a review."
aliases = ["2021/06/09/my-first-year-with-the-pinephone.html"]
date = "2021-06-09T16:10:18Z"
[taxonomies]
tags = ["PinePhone", "PINE64", "PineTalk", "Linux Phones",]
categories = ["impressions", "commentary", "hardware",]
[extra]
author = "Peter"
+++

One year ago, I received my UBports Community Edition PinePhone. I rode my bicycle to a DHL Express facility outside the city, as this was the fastest way to get it. I [immediately recorded and published my unboxing of the device](https://www.youtube.com/watch?v=uhx7XelZuZk) and restarted this very blog. So what did I experience since? What did I think of the PinePhone initially, what do I think about it now?
<!-- more -->
## How it all started

### Early thoughts on the PinePhone and faint-heart Brave Heart

As evidenced by [this early blog post (Nov 2018)](https://linmob.net/one-more-phone-for-gnulinux/), I wasn't really convinced that this would go well. From what I had heard and read about the Pine A64 and PineBook, I wasn't convinced that the AllWinner A64 SoC would do well in a phone. 

Also, as a [Librem 5](https://linmob.net/tags/librem-5/) backer, I feared that this would likely divert attention from the Librem 5 and thus harm its potential of being a successful product, and not do to great on its own (A64), thus making Linux Phones a joke. So, to sum it up, I wasn't too excited.

As time progressed, this changed gradually. I recall watching [Martijn Braam's video on the PinePhone v0.9 developer variant](https://www.youtube.com/watch?v=VyeD1sfQNoM) and thinking: Huh, this looks like a phone! I might want this! The price also made it more appealing, and so I pre-ordered a "Brave Heart"-Edition in late 2019 when it was available.

Sadly, living in Germany, a country with very nitpicky customs officers that don't like missing CE labels or manuals and with all this occurring early into the pandemic, I ended up never received it: It arrived at the local customs (which are somewhere outside the city here), and I read up on how to get an appointment and [how successful other fellow Germans were in getting their PinePhone's into their hands (spoiler: not very)](https://forum.pine64.org/showthread.php?tid=8855&page=6), and decided to ask customs to send the device back to PINE64.

### Receiving UBports Community edition: Excitement and Despair

But I also decided to not give up and order one of the next batch of PinePhones, which was the UBports Community Edition (CE), which as already described above, I was glad to pick up. I honestly wasn't to interested in trying Ubuntu Touch, as it was a known quantity: I had tried it on a Nexus 4 from early 2016 onwards (for a while as a daily driver even), and eventually figured that while it was nice, it wasn't really for me, or rather, it lacked important features, like a workable XMPP client with encryption or GPG encrypted email. I had gone back to LineageOS without Google Apps and mostly F-Droid apps. I had bought this UBports edition to try all these other distributions that were coming up at the time, using Phosh, Plasma Mobile, or something that else that I had never experienced first hand, like LuneOS or Glacier UX of Nemo Mobile.

I was quite excited. The hardware felt nice, it looked like a normal smartphone, and while it would run hot at times and not work well as an actual phone, it felt like these were software issues soon to be fixed.

There were other things that gave me headaches though: Battery life felt abysmal. These were the days before CRUST, and if you would just leave your PinePhone for a while (5 to 7 hours) without turning it off, it's battery would be depleted when you would come back. I also played with the Droid 4 running Maemo Leste during that time, and remember thinking (in late June or early July of 2020): Wow, this Droid 4 is so much closer to being a usable phone - it basically needs dialer and messaging apps and then it's good to go, while this PinePhone ... will it ever run for a full day even? Why did they go with such a small battery given the amounts of power SoC and LTE modem drink away?

### The next community edition

I managed to fight my despair, likely due to CRUST landing and new problems (will the phone wake up fast enough to allow me to answer this call?) but also better battery life becoming a thing. When PINE64 announced that they would make a 3GB/32GB PinePhone I knew that I would have to get another PinePhone. The new hardware revision v1.2a fixed the worst bugs of the UBports CE for this new postmarketOS CE: 
* USB fast charging was fixed,
* and more importantly, connecting a USB-C convergence dock worked now, for connecting USB devices and even external screens to the PinePhone.[^1]
That aside, a phone preinstalled with postmarketOS is plain hilarious, isn't it?

## Fast forward to now

After 3 further community editions PINE64 is now selling a "PinePhone beta edition", preinstalled with Manjaro Plasma Mobile, which features the same hardware (v1.2b) as all the 3 last community editions. Whether it's wise to grant Plasma Mobile the beta moniker at this stage is debatable:[^2] 

It's still lacking features like setting a custom APN (which sadly is something require with my mis-recognized MVNO SIM card)[^3] and does not yet feature apps for arguably essential functions like email, and is in other parts confusing, e.g. Spacebar and Koko are fun names, but not exactly friendly names for new users.

### How did the hardware hold up over time?

My PinePhones overall have been holding up well. But: The back-cover of my UBports CE has been holding up less well: It's slightly broken, first it cracked near the volume rocker, and then later at two other places. Now this isn't really a problem, as PINE64 sell replacement back covers. More severely, likely due to dusty pants pockets, I've got some dust in front of my front cameras, rendering the front camera effectively unusable.

### Do I daily drive the PinePhone?

Not yet.  Some communications apps people dear to my heart use just don't exist for mobile Linux yet. Also, battery life in active use is still not that good, so that with me being an information addict at times, I will quickly run out of battery and also out of patience, when I try to do to much at once and the PinePhone gets really slow. It's close though, and the PinePhone is a daily companion, SIM card included, and is used for lots of tweets and toots, just to prevent people from shouting at me for no good reason.[^4]

My software setup has not changed a lot [since January]: I am still running [Danct12's](https://danct12.github.io/) [Arch Linux ARM](https://github.com/dreemurrs-embedded/Pine64-Arch/releases) from eMMC (with [LUKS encryption](https://github.com/dreemurrs-embedded/archarm-mobile-fde-installer)). Changes in app selection are limited to me now using Nheko and Fluffychat for Matrix instead of Hydrogen.


### So, should you get a PinePhone?

If I would try to count the countless hours I've spent with the PinePhone for this blog and [LINMOBapps](https://linmobapps.frama.io), I would have to say: Don't!

On a more serious note: You really should be a Linux user for this, that either knows a few things (e.g. has built software from source when necessary), or is willing to learn these things (as in: not cry in public chats, but just read up until you can do it), and does not mind a device that's certainly quirky, then do it and get one. You will need these abilities to get MMS working for you, to just name one example. 

If you expect the PinePhone to replace your Android Phone or iPhone without drawbacks or effort on your side, please don't get one. It won't make you happy and only add frustration to your life. This is not the product that is easy for everyone. PINE64 makes hardware for a developer community and if you're not a developer, you should at least enjoy following the process of development and be patient.

## Conclusion

I am quite happy that (despite my initial doubts) I decided to go down the PinePhone rabbit hole. It certainly has changed how I spend huge parts of my free time: I've been writing [48 weekly updates](https://linmob.net/categories/weekly-update/), I've added more than [100 apps to a list and tested them all](https://linmobapps.frama.io/), have made a [quite a few videos](https://linmob.net/videos/) and talked in [multiple](https://nerdzoom.de/podcast/) [podcasts](https://gnulinux.ch/category/podcast) [about](https://radiotux.de/index.php?/archives/8069-RadioTux-Sendung-April-2021.html) this device, including [PineTalk](https://www.pine64.org/pinetalk/), a PINE64 community podcast I co-host. While frustrating at times, it's been fun much more often than this and I think I've learned a lot.

[^1]: I recall that PINE64 thought about offering fixes for these bugs (as in repair services with local partners), but that did not happen - they eventually solved the problem with a discount on v1.2b 3GB PinePhone mainboard.

[^2]: Once yesterdays Plasma 5.22 has properly landed in distributions, I plan to write a review of it where I plan to go into more detail. I hope that the new release will fix a few of the major annoyances that have plagued me, stay tuned!

[^3]: This is likely going to be an issue of the past soon: This [merge request](https://invent.kde.org/plasma-mobile/plasma-settings/-/merge_requests/87) should have finally solved the long-standing issue.

[^4]: _"How dare you tweet about Linux Phones using an iPhone!?"_ and similar feedback is fun and always appreciated. Not. 
