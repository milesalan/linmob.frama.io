+++
title = "N9, N950: Two Nokia MeeGo devices. Thoughts before going to bed."
aliases = ["2011/06/n9-n950-two-nokia-meego-devices.html"]
date = "2011-06-21T23:07:00Z"
[taxonomies]
tags = ["decisions", "developers only", "development", "launchday thoughts", "Nokia N9", "Nokia N950", "opinion"]
categories = ["hardware", "commentary"]
[extra]
author = "Peter"
+++
The N9 was announced in Asia while I was sleeping here in Europe. Now I am about to go to bed again, but as I can't do any serious blogging (videos, images) being online over an EDGE network which feels more like GPRS in terms of speed right now, I want to share some thoughts on the N9 and the N950.
<!-- more --> 
The N950, which I didn't really cover here yet on LINMOB, is the device that we saw on leaked images aeons ago (~ 1 year), a device much looking like the Nokia E7: An 4" HWVGA aluminum slab with a slide out, full 4 row QWERTY keyboard. Don't get overly excited about this device if you aren't a developer, as it will be tough to get one then. Read more on this over at <a href="http://thehandheldblog.com/2011/06/21/nokia-n950-developer-phone/">thehandheldblog.com</a>, and if you are interested in the actual differences between these two new Nokia Handsets, let me point to this post at <a href="http://meegonews.com/2011/06/21/nokia-n9-vs-n950-whats-the-difference/">meegonews.com</a>

For the non Qt / Linux developing rest of us, there will be the keyboard less, polycarbonate N9, which looks different than the existing Nokia phones, it doesn't resemble the N8 e.g.&mdash;I like it &#8230; I think I should stop repeating myself, so nothing on the specs here which I haven't posted yet, they are decent, not breathtaking. 

Pricing and availability. Not much info on that yet. There are, as I noticed earlier today, indicators that the N9 will only sell in a few countries. (-&gt;<a href="http://thehandheldblog.com/2011/06/21/nokia-n9-availiability-india/">TheHandheldBlog</a>) Considering the general excitement about this new Nokia product this would be a very sad thing, but Nokia has (from my view as a mobile linux lover) made tons of sad decisions since Steve Elop came aboard. 

I already linked you to thehandheldblog.com twice, and I will do it a third time in this post, simply because I totally agree to their comment <a href="http://thehandheldblog.com/2011/06/21/with-the-n9-nokia-shows-the-world-its-still-got-it/">&#8220;With The N9, Nokia Shows The World Its Still Got It"</a>.

Last but not least you should watch this video of the presentation (by <a href="http://www.netbooknews.com/28502/nokia-n9-walk-through-with-screen-shots-video/">netbooknews</a>)&mdash;I can't right now (EDGE) :(
