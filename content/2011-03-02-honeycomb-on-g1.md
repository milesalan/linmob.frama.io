+++
title = "Honeycomb on G1"
aliases = ["2011/03/honeycomb-on-g1.html"]
date = "2011-03-02T18:04:00Z"
[taxonomies]
tags = ["CyanogenMod", "htc dream", "htc magic", "modding", "T-Mobile G1", "Yoshi"]
categories = ["shortform"]
[extra]
author = "Peter"
+++
I am&mdash;and I am not ashamed of that&mdash;still using my G1, which I have for a little more than 2 years now. Mine, if you care, is running Gingerbread, to be precise a mod called &#8220;GingerYoshi&#8221; which has some<a href="http://www.cyanogenmod.com/"> cyanogen</a> in it.

* Note, 2021-04-14: The [video](https://www.youtube.com/watch?v=-RKEHfOwnSI) sadly appears to be no longer available.

The one in this video is running Honeycomb, which is ported over by the same team that's behing the GingerYoshi stuff that runs so well on my device. Of course, it doesn't run too great, but it's a sensation that it's running at all on a ARMv6, low memory (ram and flash), non-OpenGL ES 2.0, HVGA device. Eat this, you &#8220;Honeycomb will be dual core only&#8221; lamers!

__VIA:__ <a href="http://www.engadget.com/2011/03/02/htc-magic-t-mobile-g1-gets-honeycomb-port-android-past-and-fu/">engadget</a>

__SOURCE:__ <a href="http://forum.xda-developers.com/showthread.php?t=974087">xda-developers.com</a>
