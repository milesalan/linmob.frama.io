+++
title = "CES 2011: First third hand impressions"
aliases = ["2011/01/05/first-third-hand-impressions.html"]
author = "peter"
date = "2011-01-05T16:10:00Z"
layout = "post"
[taxonomies]
tags = ["ASUS", "ASUS Eee Pad MeMO", "CES2011", "Motorola", "Motorola Atrix 4G", "motorola xoom", "Notion Ink", "Notion Ink Adam", "tegra2", "windows on arm"]
categories = ["events", "impressions"]
+++
_While I could spend my time with something else as well (and I mostly do over the day here in Europe), it's Consumer Electronics Show in Las Vegas, Nevada, USA again. Even though I haven't been that much into tech lately (besides scrolling the daily tech news), I must admit that as always (since I follow mobile computing) I've been pretty exited._

It's CES, and that's great, just like Computex or MWC (the German trade show CeBIT hasn't been too exiting lately, btw) and while it isn't too tough to guess which rumors might become true (even as a bystander, like me&mdash;I would like you to consider me as an attentive bystander), you still always sense something whenever you see a first &#8220;hands on&#8221; or follow a live blogging session from a press conference. There is surprise (rare, but it happens), disappointment, sheer exitement or happiness (because something came out the way you dreamt of it) or fear (you don't like what happens, whatever the trigger is).

I will not bore you with more detailed descriptions, I will just try to name an example for each of the feelings I tried to describe above.

Let's start with __surprise___: When I first saw the ASUS EeePad MeMO I was really surprised (positively) to see a tablet with a stylus from a company like ASUS, which is a hardware company (and adding in a Stylus within a finger touch environment like Android requires additional software to make real sense, from note taking to hand writing recognition). Still, ASUS has proven to be innovative during the last years, and I really hope that others will follow them&mdash;don't forget: it was ASUS who invented netbooks, after all.

Next on: __Disappointment__. 

Well, there wasn't too much disappointment yet, probably because other news have been to overwhelming. Still, there is one product which I followed in the past which will (as I see it now) not live up too its hype, which I've been a little part of. It's the Notion Ink _adam_. Looking at all these Tegra2 tablets getting ready for the market, announced with better screen resolutions and Android Honeycomb (featuring the Android market) I expect this tablet not to be the success it could have been, if only it would have been ready earlier - if NotionInk had managed to hit the market this November with a less perfect, but still promising and relatively bug free outstanding product, NotionInk could have hit it off. Now, still not available in the stores and about to hit within a plethora of similar devices (to the eye of the average customer), considering the amount of thought the makers of _adam_ put into it, inferior solutions featuring the software Google created and Googles Android Market, sounding alltogether less biblical (I never loved these adam, Eden &#8230; names), NotionInk's solution will have a very hard time to perform half decent next to all these things with known brandnames on them. I just hope, that they will survive (and be it as a team, then part of another company - Toshiba seems to need some brilliant engineers, looking at their failure with the Folio 100 (and other, earlier mobile solutions like the TG01), btw ;-) ).

Sheer __exitement / happiness__. 

I like what Motorola did by creating a phone (the Atrix 4G) that will power a subnotebook. This is truely great, even though Motorola sucked at openness (as in hackability) lately, because it is what I dreamt of years ago, when I had my first Linux powered smartphone, the Motorola A780.

__Fear__. Well, I could have just mentioned the NotionInk _adam_ in this section, but I felt just too certain about its failure, so what will I put into this section? 

Yes, it's Microsoft really porting Windows to ARM. It's not Microsoft, I don't expect them to fail, it's that I am afraid of Microsoft killing other, in terms of user experience superior solutions with its momentum. A momentum it has because almost everybody is used to the shit (not talking about software quality here, but about user interface design) they've been delivering for years. I really hoped they'd stick to CE. And to be perfectly honest: It's not just the usability that makes me worry, it' s that I am great fan and supporter (whereever I can) of open source software, which will suffer in one or another way (open 3d graphics drivers is one frontier harder to fight at now). Microsoft really entering the ARM platform (a move which, with Intel (&amp;Nokia) working on Meego) makes perfect sense from all standpoints I can think of (be it technics or business), but still, for the reasons mentioned above I would have loved to see Microsoft failing at doing the obvious thing.

I will write more about this _CES_ if I feel like it, promised.

_Apologies:_ I left out links out of pure laziness, I expect my readers to be able to find the information they need themselves.
