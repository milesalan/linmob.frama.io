+++
title = "Laptop search finished"
aliases = ["2009/11/22/laptop-search-finished.html"]
author = "peter"
date = "2009-11-22T12:23:00Z"
layout = "post"
[taxonomies]
tags = ["HP 6715b", "notebooks"]
categories = ["shortform", "personal"]
+++
Well, this notebook search was so annoying, that I decided to end it today by getting a used notebook on ebay (so cross your fingers please)&mdash;it is an HP 6715b, featuring a non glossy WSXGA+ display (1680x1050 pixel) and, yeah, I'll stay with AMD chips, hoping that the Turion X2&#160;TL-60 will perform well enough for my needs.

Let's hope that this is the cheap and fast solution I have been looking for.
