+++
title = "LinBits 20: Weekly Linux Phone news / media roundup (week 47)"
aliases = ["2020/11/22/linbits20-weekly-linux-phone-news-week47.html", "linbits20"]
author = "Peter"
date = "2020-11-22T20:28:00Z"
layout = "post"
[taxonomies]
tags = ["Librem 5", "Pine64", "PinePhone", "Arch Linux ARM", "DanctNIX Mobile", "KDE", "UBports", "Ubuntu Touch", "Manjaro", "Jumpdrive", "Plasma Mobile", "Purism", "Librem 5", "Unboxing"]
categories = ["weekly update"]
+++

_It's sunday. Now what happened since last sunday?_

Phosh updates, Librem 5 Shipping, PinePhone KDE Community Edition and more. _Commentary in italics._
<!-- more -->

### Software releases and improvements
* [Phosh 0.6.0](https://source.puri.sm/Librem5/phosh/-/releases/v0.6.0) was released last sunday, delivering what I think is a massive improvement to multitasking.
* [Phoc has seen two releases](https://source.puri.sm/Librem5/phoc/-/releases), bumping up the version to 0.5.1, delivering numerous improvements.
* [Squeekboard 1.11.0](https://source.puri.sm/Librem5/squeekboard/-/commit/5aa73347877d1a716d9e668533ae96123d50bf56) is a relatively minor release.
* [Chatty 0.2.0](https://source.puri.sm/Librem5/chatty/-/blob/master/debian/changelog) improves the SMS and chat app quite a bit.


### Worth reading
* PINE64: [November Update: KDE PinePhone CE And A Peek Into The Future](https://www.pine64.org/2020/11/15/november-update-kde-pinephone-ce-and-a-peek-into-the-future/). _I published LinBits way to early last sunday and missed a lot, including this announcement. I already wrote down my thoughts on this in a [previous blog post](https://linmob.net/2020/11/16/pinephone-kde-community-announcement.html)._
  * ZDnet: [PinePhone KDE Linux phone is getting ready for pre-orders](https://www.zdnet.com/article/pinephone-kde-linux-phone-is-getting-ready-for-pre-orders/). _Nice to see more reporting on the PinePhone by outlets focused on general tech._
  * LinuxSmartphones: [PinePhone KDE Community Edition up for preorder in December](https://linuxsmartphones.com/pinephone-kde-community-edition-up-for-preorder-in-december/). _Brad's take, nice as always._
* LinuxSmartphones: [Hacker adds a working fingerprint sensor to the PinePhone](https://linuxsmartphones.com/hacker-adds-a-working-fingerprint-sensor-to-the-pinephone/)
* Purism: [Librem 5 Mass Production Phone Has Begun Shipping](https://puri.sm/posts/librem-5-mass-production-phone-has-begun-shipping/). _This reddit thread cleared up some [questions for me](https://www.reddit.com/r/Purism/comments/jy9axf/when_will_you_ship_librem_5_to_europe/). And if you need evidence that they are really shipping phones out now, see this [thread in the Purism forums](https://forums.puri.sm/t/received-my-librem-5-evergreen/10877)._
  * LinuxSmartphones: [Purism’s Librem 5 Linux smartphone is now shipping](https://linuxsmartphones.com/purisms-librem-5-linux-smartphone-is-now-shipping/). 

### Worth watching
* PINE64: [November Community Update: KDE PinePhone Community Edition & a peek into the future](https://lbry.tv/$/download/november-community-update-kde-pinephone/75535c5fc0b9b38e01a1d7d0fa82348758ac7b8f). _Great work by PizzaLovingNerd._
* r/pinephone: [I'll just leave this here](https://reddit.com/r/pinephone/comments/jxbds1/ill_just_leave_this_here/) _Diablo on the PinePhone._
* Purism: [Librem 5 Visual Walkthrough](https://www.youtube.com/watch?v=cAUNrY_qPCg). _Watch this before starting to use your Librem 5. BTW: It looks really smooth. Also: That phone is a beast, in a good way!_
* Geotech Land: [KDE Plasma Mobile On Pinephone](https://peertube.social/videos/watch/514887ba-568f-446b-ba96-ea29    6977db39). _Nice video._
* IzaicNix: [Pinephone Lomiri with 60hz refresh](https://www.youtube.com/watch?v=ZZdOqEg44Go). _I hope Manjaro (or another distribution) picks up Lomiri. It has so much potential._
* Jaquen hgar: [PinePhone Hands On - Arch Linux](https://www.youtube.com/watch?v=lZ3Gv3W7Dro ). _German language, but maybe still interesting._
* Camden Bruce: [Evolution of Ubuntu Touch for the Pinephone (school project)](https://www.youtube.com/watch?v=zUspXeRa6bQ). _I think I would give this a B+, given that UBports definitely still work on improving PinePhone support. It just takes a long time because doing it properly is hard._
* UBports: [Ubuntu Touch Q&A 89](https://www.youtube.com/watch?v=cQQOdOisPtw) _Highlights: OTA 15 planning changes: Volla Phone support leads to no Qt 5.12 migration in OTA 15, it will be delayed to OTA 16. Around minute 19, they talk about the PinePhone._
* BitBooger: [How to install (flash) Mobian on (to) your PinePhone!](https://www.youtube.com/watch?v=et9IQt6uxkg) _Nice! See also my video below._

#### Manjaro PinePhone Unboxings
* Manjaro Linux: [Developer does an unboxing of the PinePhone Manjaro Community Edition](https://www.youtube.com/watch?v=YdI2s133KMo). 

#### Non-Linux Phone content
* Louis Rossmann: [An important message from Louis Rossmann](https://lbry.tv/@rossmanngroup:a/an-important-message-from-louis-2:6). _Right of ownership and right to repair matter. Not just for users, for independent repair – but also for the planet as a whole._

### Stuff I did

#### Content
* I wrote a blog post on [Pine64's announcement](https://linmob.net/2020/11/16/pinephone-kde-community-announcement.html).
* I streamed for the first time on YouTube. It was fun, and I am going to repeat this once I have worked on my setup. If you want to watch a part of this first stream, [you can do so on YouTube](https://youtu.be/4TghCp4UE8w). Maybe it can help you out with understanding Jumpdrive &mdash; it's unlisted because I had audio issues in the beginning. If you have ideas for topics to discuss on LINMOB live, please get in touch!

#### LINMOBapps
Not much happened here, [just some maintenance](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master).  I have another commit almost ready, and I hope to be able to do the game-split next week, so stay tuned. 

