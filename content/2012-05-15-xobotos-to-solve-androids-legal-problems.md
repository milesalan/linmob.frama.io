+++
title = "XobotOS to solve Android's legal problems?"
aliases = ["2012/05/15/xobotos-to-solve-androids-legal-problems.html"]
author = "peter"
date = "2012-05-15T20:07:00Z"
layout = "post"
[taxonomies]
tags = ["Android", "C", "Mono", "Oracle vs. Google", "XobotOS"]
categories = ["shortform"]
+++
<a href='http://www.theverge.com/2012/5/15/2996144/android-xamarin-xobotos-java-c-sharp'>Android made Java-free thanks to 'XobotOS' project, but what are the legal implications? | The Verge</a>

Just in case you missed the existence of XobotOS, it's "monofied" Android. While it looks like an emergency exit in case of a lost Oracle vs. Google case, it might not be as legally safe as it appears to be, given the concurrence between Microsoft and Google.
