+++
title = "PinePhone Hardware - Six months"
author = "Peter"
date = "2020-06-14T17:42:18Z"
layout = "post"
draft = true
[taxonomies]
tags = ["PinePhone", "Pine64", "impressions"]
categories = ["impressions"]
+++

Six months ago I published my first impressions on the hardware, and I feel that it's time to write and publish a follow-up to that post.

## Hardware

Speaking of the hardware: Not that much changed on that front. Ok, PINE64 fixed some hardware bugs in v1.2a and v1.2b, and I now have a second PinePhone that features 3GB of RAM and a 32GB but slightly slower eMMC. Oh, yeah: My UBports CE back cover does not look so great anymore. 

But, generally, there has been a lot of progress. Let me start with parts where hardware and software are closely working together.

### Camera

Let's start with the camera. In June you could hack ffmpeg to take pictures with back camera. Now, both cameras are supported with [Megapixels](https://sr.ht/~martijnbraam/Megapixels/), which is a decent camera app, offers RAW photography and takes pretty nice pictures. Don't expect this to be on par with your current, recent Smartphone: It's just a 5 megapixel sensor. 

While I am sure that even Megapixels can and will be improved some more in ways I can't even imagine right now, there's one area that definitely needs work: Making the cameras work in the browser, in webcam apps to make video chats a possibility. While you can run Jitsi in the browser, it's audio only for now.

### Safety

In mid-september, PinePhone developer [Megi wrote a blog post about PinePhone safety](https://xnux.eu/log/#017). This post got some attention, e.g. on [Lobste.rs](https://lobste.rs/s/x7m0pq/let_s_talk_about_safety_pinephone) and [Hacker News](https://news.ycombinator.com/item?id=24596248).[^1] While my PinePhone had been getting quite hot every then and now (while charging, mostly correlating with heavy CPU use), I was dismissive of this. My PinePhone never got hotter than some of my other smartphones did. The PinePhone does not have a Galaxy Note 7 problem, and I figured that actual issues would have surfaced with the early (v1.0) developer units, when both hardware and software were even more experimental. 

Megi since went ahead and included mitigations in his kernel in order to improve PinePhone safety. By now, his kernel is used in most distributions: postmarketOS, ArchLinuxARM/DanctNIX mobile, Manjaro and Fedora are all using it, Ubuntu Touch is going to switch to it. Talking about active distributions, only Mobian and the developer focused (really, don't try to use this!) KDE Neon for the PinePhone don't use his kernel. 

While I am glad that this means my PinePhone is now "safe" to use, it has some negative side effects. Charging seems really slow every once in a while, and I have been booting into Mobian on microSD just to charge my phone a bit quicker when I needed to.

### Battery life

Battery life was quite poor in June, but it quickly got better, thanks to [CRUST](https://github.com/crust-firmware/crust), an open-source firmware running on the A64's OpenRISC core.
Unfortunately, by design, CRUST, which puts the SoC/CPU to deep sleep so that it can still be woken up by the Quectel EG25-G modem when necessary, e.g. on an incoming call. So, if you're PinePhone does not wake up to ring before the caller hangs up, you may end up with missed calls without indication of these. This problem has been getting better in the meantime, as this process has been tuned. Also, power-saving improvements to the Modem and also the Realtek RTL8723CS WiFi/BT chip have potential to improve this further. That said, if you don't use your phone all that much, it should last a day now. If you are a heavy phone user, I recommend you get an external battery charger and a secondary battery to carry around with you.

To conclude, I have to say that I wish the PinePhone had a larger battery so that we would not need hacks like CRUST[^2], and I really look forward to [PINE64s keyboard accessory](https://linuxsmartphones.com/swappable-pinephone-back-covers-will-bring-wireless-charging-nfc-and-a-keyboard/), which is going to feature a second battery.  

### Hardware hacks and accessories

There have been quite a few fun projects around the PinePhone, which I mentioned in LinBits whenever I could:
- 



[^1]: Annoyingly, [his follow-up blog post](https://xnux.eu/log/#018) and the fact that his kernel is being used in most of the distributions has not spread as widely, leading to less informed persons bringing this topic up again and again.
[^2]: Purism's Librem 5 Evergreen &mdash; which has a 28nm NXP i.MX8M SoC, opposed to the AllWinner A64's 40nm &mdash; is shipping with a 4500 mAh battery, which is 1.5 the capacity the PinePhone has. One the other hand, I would have hoped to have my Librem 5 which I pre-ordered in October 2017 by now.
