+++
title = "On Tablets - or the DA"
aliases = ["2009/11/06/on-tablets-or-the-da.html"]
author = "Peter"
date = "2009-11-06T13:44:00Z"
layout = "post"
[taxomomies]
tags = ["DA Digital Assistant", "hardware design", "ideas", "platforms", "tablet", "PixelQi", "digital paper", "annotation"]
categories = ["article"]
+++
While I have been focussing on mobile, especially pocketable computing during the last years, always watching what was happening in the wide range from smartphones to subnotebooks, I almost totally ignored tablets.<!-- more -->

Lately tablets have become popular again. While Tablet PCs only attracted a few and convertibles always were too bulky or expensive in comparison to the 'classic laptops for the most of us', we are facing an armada of new tablets. Ok, most of them are still just rumored, and others won't make it to market &mdash; but some will. Apple is said to work on a tablet device, Microsoft might be as well, many Mobile Internet Devices (MIDs) are using a small tablet-y form factors and PixelQi stated that they are working on a tablet, too. Also: CrunchPAD.

**Time for my personal tablet vision!**

As always, what I will try to make up here is inspired by my personal needs. And my personal needs are defined by what I do. So what do I do? I listen to lots of lectures at university, taking notes on blank or dotted paper. Lots of notes. About 50-100 pages per week. As you may imagine, piles of paper clutter my room. And to be frank: I hate that.

I have been trying many things during the last years, like doing my writings on a netbook or using my cellphone microphone to record the lectures. In fact, I went back to paper, as it works best for me &mdash; it is just more convenient to draw and underline important things, to make annotations etc.

But then there are these piles of paper, again. I tought myself how to order them, but it takes time, lots of time. And time is money.

Tablets. Most of them seem to be aimed at internet use, and yes, unless you want to write longform text, and you just like to surf around, they may be quite handy at that task &mdash; but think again: Tablets are relatively thin. Digitizers exist. Wouldn't a tablet and a digitizer make a nice pen and paper replacement? I suppose so.

**Hardware.** 
Well, tablets like I would like to see them, don't have to be more performant than your workstation is. Of course not &mdash; something between the application performance of a high-end smarthone and a CULV powered subnotebook should be just fine (sounds like ARM Cortex A8...)&#160;; battery life (preferably all day (8 hours +)) and size (especially thickness &mdash; not more than 2 centimeters) are more important than performance here. 
_Screen._ Well, I always liked the OLPC XOs' screen technology and as PixelQi even managed to improve on this, it would be the favourite choice. Touch, of course. Preferably multi touch. And a digitizer, to have a pen.
_Screen size?_ Well, I am not sure &mdash; something between 7 (nice to carry) and 12 inch (enough size for drawings). And besides that? _Wireless connectivity_ is a must, USB (OTG) is always nice, (µ)SD is great for storage, a microphone, loudspeaker, integrated microphone/headphone jack, plus a video port for presentations.

**Software.** 
I don't really care about the software (though it should be free software), as long as there is a mode to use it as pen and paper as easy and free of distractions as possible.
Then, of course, this &#8220;paper mode&#8221; should have features like handwriting recognition &mdash; well, this sentence is about to become too long, so I'll just make a quick list:
- archive, calendar-sensitive (e.g. attribute the note to a certain meeting)
- tagging by underlining
- ablility to display PDF (or other documents in the &#8220;background&#8221; (annotation mode)

There are optional features I would like to see, e.g. Text To Speech and Voice Recognition &mdash; to make the lack of a keyboard less of a problem and to make this device a real productive one. Let's not forget collaborative features, of course… This devices would be really useful in every way and a true extension of your brain. 

I would therefore call it <span style="font-weight: bold">the DA</span> (Digital Assistant) ;)

BTW: There is one product announced today that at least comes  close (just considering the hardware here), a [10.4" tablet by MOTO](https://web.archive.org/web/20091108022643/http://www.moto.com/amp/amp_specs.shtml).

