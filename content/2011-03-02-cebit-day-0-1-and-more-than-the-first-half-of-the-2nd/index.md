+++
title = "CeBIT: Day 0,1 and more than the first half of the 2nd day - Round Up"
aliases = ["2011/03/cebit-day-01-and-more-than-first-half.html"]
date = "2011-03-02T16:58:00Z"
[taxonomies]
tags = ["ASUS Eee Pad MeMO", "ASUS Eee Pad Transformer", "CeBIT 2011", "keyboard", "outstanding battery life", "QSD8260", "Rockchip2918", "Samsung Galaxy Tab", "Samsung Galaxy Tab P1000", "tegra2", "Yifang M707"]
categories = ["events", "impressions", "hardware"]
[extra]
author = "Peter"
+++

As usually (at least I believe that it has been like that the past years) CeBIT isn't the place were the great, new revolutionary platforms are being announced. CeBIT is a business centric trade show with tons of non business visitors, that come in because there are tons of tickets available for free. Still CeBIT is interesting, because of its size&mdash;there are always some new devices from smaller manufacturers, or new iterations of products that were announced at CES. Before I will head to Hanover tomorrow, I want to share a link list of the most interesting products of CeBIT spotted by fellow tech bloggers so far.
<!-- more -->
### Yifang M707
Rockchip 2818 based (later: Rockchip 2918 -&gt; ARMv7), 7" tablet with resistive&mdash;I can hear you thinking &#8220;So what's the point with this thing?"&mdash;and here's the answer: Yifang will include it's infrared based handwriting recognition to this most likely rather cheap model, which certainly makes this interesting. Check out the Video by <a href="http://twitter.com/charbax">@charbax </a>on <a href="http://armdevices.net/2011/02/28/yifang-m707-android-tablet-with-infrared-pen-input/">ARMdevices.net</a>


### ASUS EeePad MeMO / MeMIC 
<a href="ASUS_EeePadMeMo_1.jpg" style="clear: right;float: right;margin-bottom: 1em;margin-left: 1em"><img border="0" height="200" src="ASUS_EeePadMeMo_1.jpg" width="196" /></a>
I liked this tablet when ASUS announced it at CES, partly because it is a powerful (Qualcomm QSD8260 dual core ARMv7 Snapdragon SoC inside with support for 1080p @ 30fps and more) 7" tablet, a form factor that's somewhere in between the large 4.3" phone slates and the huge 9.7" / 10.1" tablets and thus interesting, in fact many people out there that are about mobile computing really like the Samsung Galaxy Tab P1000, which is about the same size&mdash;it fits in the pocket of your jacket.The other reason I immediately liked it is the fact that it ships with a capacitive pen and thus is handwriting enabled, we don't know how well this will work out yet, as the software (which is the important thing here, be it &#8220;basic stuff" like palm rejection technology or rather advanced like hand writing recognition) is far from being finalized yet, at CeBIT the MeMO runs with (if the icons aren't ported over) Gingerbread (Android 2.3), it will be released with Honeycomb in June. As the MeMO is a 3G enabled tablet, ASUS thought of an accessory that really sounds like a nice idea, it's the MeMIC, a Bluetooth connected tablet remote and phone. I will try to get my hands on this thing tomorrow, and remain thrilled how well ASUS will manage to create a great experience out of these nice ideas.<br />Check out another Video by <a href="http://twitter.com/charbax">@charbax</a> on <a href="http://armdevices.net/2011/02/28/asus-eee-pad-memo-tablet-and-memic-bluetooth-phone-remote/">ARMdevices.net</a> and the hands on by <a href="http://twitter.com/chippy">@chippy</a> on <a href="http://www.carrypad.com/2011/02/28/eee-pad-memo-bluetooth-handset-hands-on-video/">carrypad.com </a>


### ASUS EeePad Transformer

<a href="ASUS_EeePadTransformer_4.jpg" style="clear: right;float: right;margin-bottom: 1em;margin-left: 1em"><img border="0" height="173" src="ASUS_EeePadTransformer_4.jpg" width="200" /></a>
At CES this was the ASUS tablet that I considered the most boring one compared to the Slider and the MeMO. At CeBIT this device runs Android 3.0 Honeycomb, and what made it so interesting&mdash;now that I mention it&mdash;is that there haven't been to many new devices spotted yet, and a fact that made me listen up: Using this Tegra2 based device with its keyboard dock is, if we can believe ASUS here, going to give you 16 hours of battery life. Build quality is said to be better than Motorola Xoom (<a href="http://twitter.com/#%21/ZaferKilicaslan/status/42980890304970752">German source</a>). That's something, and if this 16 hours figure turns out to be true, this device might replace my 10" ASUS netbook.

Check out another video by <a href="http://twitter.com/charbax">@charbax</a> on <a href="http://armdevices.net/2011/02/28/worlds-first-honeycomb-laptop-at-cebit-2011/">ARMdevices.net</a>

The fourth thing is an __accessory for Galaxy Tab__, that <a href="http://www.carrypad.com/2011/03/01/galaxy-tab-keyboard-case">Carrypad.com</a> tracked down. Besides that (there are some more AMD Fusion based tablets, netbooks and ultra-thins at CeBIT and much more) I didn't find anything that did excite me yet.
