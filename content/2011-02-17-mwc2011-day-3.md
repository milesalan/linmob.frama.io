+++
title = "MWC2011: Day 3"
aliases = ["2011/02/17/mwc2011-day-3.html"]
author = "peter"
date = "2011-02-17T00:01:00Z"
layout = "post"
[taxonomies]
tags = ["Android 2.2 (Froyo)", "MeeGo", "MWC2011", "tegra2", "Tegra3", "WP7"]
categories = ["events"]
+++
_Just a few links, not too exiting&mdash;apparently the major announcements of MWC have happened already._

### Devices:

NEC LifeTouch Note, Android laptop, Japan only,  1GHz nVIDIA Tegra2 dual core, 7&#8221; WVGA touch screen, Android 2.2 (Froyo): <a href="http://www.jkkmobile.com/2011/02/nec-touchnote-7-inch-android-laptop-at.html">JKKmobile</a>, <a href="http://www.crunchgear.com/2011/02/15/lifetouch-note-nec-rolls-out-android-netbook/">CrunchGear</a> (What's really stupid about this thing: This device has a back-facing 2 MP camera&mdash;would have been much more useful if it was front facing (think of video chat))

### Platforms: 

nVIDIA Tegra &#8220;Kal El&#8221; (Tegra3), Quad Core 1,5GHz &#8220;ARM madness&#8221;, said to be about as fast as the (first) Core2Duo&mdash;first products shall be out at the end of this year (<a href="http://www.engadget.com/2011/02/15/nvidia-announces-quad-core-kal-el-soc-promises-it-in-tablets-by/">engadget</a>).<br />If you are interested in other ARM SoC solutions, that are in par with solutions already on the market, head over to <a href="http://armdevices.net/">ARMdevices.net</a>

That's about it, I guess&mdash;Intel AppUp Developer Day took place today and it was all about MeeGo from what I hear, and it was completely full&mdash;so there are still people interested in MeeGo (or they just wanted to grab ExoPC Hardware..)&mdash;it might be more than a dead duck in a swampy pond, e.g. a rising star.

Besides that, it leaked out that <a href="http://jan.wildeboer.net/2011/02/microsoft-absolutely-no-free-software-for-windows-phone-and-xbox-apps/">Microsoft won't allow any GPLv3 (or compatible licenses) licensed software on Windows Phone 7</a>.
