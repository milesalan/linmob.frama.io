+++
title = "2012. Yet another year in mobile Linux!"
aliases = ["2012/01/04/2012-yet-another-year-in-mobile-linux.html"]
author = "peter"
date = "2012-01-04T23:02:00Z"
layout = "post"
[taxonomies]
tags = ["CES", "Ingenic", "JZ4770", "MIPS", "preview", "speculations"]
categories = ["personal"]
+++
2011 was spectacular, but not in a especially good way. Really, while Android is growing like cancer, 2011 was the year which meant twice the death for MeeGo and in a way for webOS (yeah, webOS is going to be opensourced, but still HP's decision to stop pushing more webOS devices has been a killer - in a bad way).

So what to expect from 2011? Surely, Android is getting a lot better, with 4.0 already (and you bet, it's going to evolve in a great way) - and the recent decision to require the availability of the new (stock) 4.0 interface for Android Market access is one that will hopefully help with Android fragmentation.

While CES will be the event that gives us an idea of what exactly will be coming, it's already certain that in terms of silicon ARM Cortex A15 and Intel Medfield devices will certainly be in the top tier. I do believe though, that faster chips and higher res displays aside, not much will change - however we will see better low end devices, made by both, chinese no names and top brands. ARM Cortex A7 and new MIPS32 powered chips by the chinese ultra low cost SoC manufacturer Ingenic will deliver awesome performance on &lt; $ 150  devices - performance as good as what early 2010 devices delivered or even better - so that 1080P playback will be a feature so common that it will likely vanish from product boxes (even today, the Ainol Nova 7 tablet, which is sold for about $100 right now is said to be able to playback 1080p using an Ingenic SoC (XBurst JZ4770 + a likely discrete graphics chip)).

Well, that's it for now. More during and after CES :-)
