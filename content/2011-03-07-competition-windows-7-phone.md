+++
title = "Competition: Windows 7 Phone"
aliases = ["2011/03/competition-windows-7-phone.html"]
date = "2011-03-07T11:55:00Z"
[taxonomies]
tags = ["browser", "Dell Venue Pro", "platforms", "WP7", "CeBIT"]
categories = ["commentary", "impressions"] 
[extra]
author = "Peter"
+++
At CeBIT, I had my second hands on with Windows Phone 7, which is&mdash;while not Linux based&mdash;definitely a competing platform to Linux based smartphone platforms like Android or the upcoming (MeeGo?, the new Samsung one, as LiMo, let's not forget that, isn't a mobile OS, just a middleware/IP pool) or less successful other Linux based smartphone platforms (webOS) out there.
<!-- more -->

Unfortunately, Internet Access was limited to the CeBIT2go page, so I could not test the performance and rendering capabilities of the Internet Explorer based web browser (which would have really interested me the first time, but there was no web access in the store I went hands on for the first time). The Dell Venue Pro, the device I went hands on with, really performed nicely, the overall UI of WP7 is really easy to use, much like the iPhone you immediately know what to do (you don't with Android and all its buttons (the menu button is the problem here, as WP7 has a home and a back button, too), and it's a very fluid UI&mdash;I have to admit that I even like the minimalist design of it.


Comments about the recent Nokia announcement have been so negative because people don't believe in WP7. Of course, it is easy to make fun of this platform: It supports neither copy and paste nor multitasking, and it just runs on one hardware platform&mdash;but just to remind you: Apple's iOS didn't do so until major release 3 respectively 4, and still, people liked it, besides that, Microsoft has already announced that these features will come in this year. Looking at market shares, WP7 doesn't perform too well, either. But you have to look at the age of this platform, it's still very young, and one must not forget that old Windows Mobile apps are not supported&mdash;WP7 is a whole new thing in terms of Apps and such. What I want to say: While I always feel helpless when I go hands on with a Symbian touchscreen smartphone and don't really know what to do (did so with the C7 and the E7, the latter feels extremely great in terms of build quality), WP7 is very straight forward. If I then imagine a Nokia E7 like device running a then even better WP7, I can't find a single reason, why the average consumer shouldn't pick that device up. I don't like Microsoft, and that's why I say: _Don't count them out yet._
