+++
title = "New Netbook (again). First impressions"
aliases = ["2010/02/new-netbook-again-first-impressions.html"]
date = "2010-02-08T15:22:00Z"
[taxonomies]
tags = ["Asus Eee PC 1001p", "first impressions", "Windows XP"]
categories = ["hardware", "impressions",]
[extra]
author = "Peter"
+++
After giving my old netbook (the VIA C7-M powered 7&#8221; Quanta IL-1 based One A120) to my mother so that she can surf the web while being in hospital for cancer treatment, I noticed that I do in fact miss having a netbook. So I got myself a new one, the Atom Pinetrail powered ASUS Eee PC 1001p.

It is a nice netbook, though I still run Windows XP on it, because I don't have time right now to install and customize Linux, I just need this beast to work at university&mdash;preparing examinations isn't fun. What I really like is the fact that it is pretty much non glossy&mdash;the mouse and power buttons are, the screen bezel is and that's it, the case is matte and has a carbon like finish.. well, just take a look at it.

Of course it lacks some features that were nice to have, e.g. a touchscreen would be great, bluetooth is not included (in case you need it) and in current distributions the WLan chip (a new Atheros one) is still unsupported&mdash;I don't know whether the LAN is right now (will edit this if necessary). And than there is one more thing: The left shift key is pretty small, while the right one is just huge&mdash;as I do use the left one more often than the right one, this is an issue for me, but I feel like I am getting used to it, so you most likely will as well. Another keyboard issue: It could be more silent, and it bends a little bit&mdash;the bend should be easy to fix, maybe the _bend fix_ will make the keyboard more silent&#8230;

_That's it for now. More to follow, possibly._

Otherwise you might want to check out the [unboxing video](https://www.youtube.com/watch?v=V1j3h-Io7rY) Balazs from <a href="http://www.ndevil.com">nDevil.com</a> [German] made (IMHO the black one I have is much more beautiful ;-) ).
