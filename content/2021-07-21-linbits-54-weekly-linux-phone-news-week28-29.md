+++
title = "LinBits 54: Weekly Linux Phone news / media roundup (week 28/29)"
date = "2021-07-21T20:38:00Z"
updated = "2021-07-22T12:34:00Z"
[taxonomies]
tags = ["PINE64", "PinePhone", "PineTalk", "PINE64 Community Update", "Sailfish OS", "Ubuntu Touch", "Valve Steam Deck", "LINMOBapps",]
categories = ["weekly update"]
[extra]
author = "Peter"
update_note = "Removed Open Letter upon request of the author."
+++

_It's Wednesday. Now what happened since last Wednesday?_

Plasma Mobile news, GUADEC, Video Recording on PinePhone and more!<!-- more --> _Commentary in italics._ 

### Software releases
* [Chatty 0.3.3](https://source.puri.sm/Librem5/chatty/-/commit/501805ee4e013470c1c9eb17e930f9fe914e3671) was released, featuring a number of new, mostly Matrix-related features. _In my testing on the Librem 5 running PureOS Byzantium however, there's no option for setting up a Matrix account, so I am guessing they want to iterate once more on this feature._ 

### Worth noting
* [Caleb has built Linux 5.14-rc1 for the OnePlus 6, which is awesome on its own. But there is more: It works with zero patches on top of Torvald's master branch, making it magnificient!](https://twitter.com/calebccff/status/1415441138555760642).
* Thanks to Adam Pigg, good things are about to land for Sailfish on the PinePhone: [64bit support and the current, latest Sailfish release](https://twitter.com/adampigg/status/1417818989137436678).
* Want accelerated video playback without compiling? [µPlayer is now available on Flathub](https://flathub.org/apps/details/org.sigxcpu.Livi).
* If you want more UI around your accelerated video playback, maybe this pre-built [flatpak of Clapper](https://github.com/uniformbuffer/com.github.rafostar.Clapper/releases/tag/master_pinephone_3bf636d46bfe2b0d432c762859e4ea00aa3e6412) is something for you. 
* Video recording on the PinePhone: [Someone on reddit made it work!](https://www.reddit.com/r/PINE64official/comments/olyqor/how_i_record_video_on_my_pinephone_in_tolerable/).
* [Emails by Purism](https://fosstodon.org/@linmob/106603224851595225) ;)
* [Maemo Leste have announced that they are working on some anonymity features powered by Tor](https://twitter.com/maemoleste/status/1416425204105064451).

#### Ongoing conference
* GUADEC, the GNOME conference, is taking place until the 25th. I checked [the schedule](https://events.gnome.org/event/9/timetable/#all) found two talks particularily interesting:
    * Bilal Elmoussaoui: [Writing applications using GTK 4 & Rust](https://events.gnome.org/event/9/contributions/194/) and
    * Tobias Bernard: [Adaptive Apps: The Future is Now](https://events.gnome.org/event/9/contributions/202/).

### Worth reading
#### PINE64 Community Update
* PINE64: [July update: community developers portal](https://www.pine64.org/2021/07/15/july-update/). _Looks like the PinePhone hardware accessories are inching closer and closer!_
  * Linux Smartphones: [PinePhone Keyboard will ship with open source firmware (coming soon)](https://linuxsmartphones.com/pinephone-keyboard-will-ship-with-open-source-firmware-coming-soon/) _Brad's take on the hardware keyboard._
  * PineGuild: [Registration for the Pine64 DevZone is now open](https://pineguild.com/registration-for-the-pine64-devzone-is-now-open/).
  
#### Signal compiling
* Linux Smartphones: [Signal desktop app ported to phones running Mobian Linux](https://linuxsmartphones.com/signal-desktop-app-ported-to-phones-running-mobian-linux/). _You can also install Signal desktop easily on Arch thanks to [Privacy Shark](https://privacyshark.zero-credibility.net/#packages), if you want too. Sadly, it's only really usable in landscape mode, and setup without another iOS or Android device running the proper Signal app is not straightforward. I personally thus still prefer [Axolotl](https://axolotl.chat/), but there are more options here, e.g. [gurk](https://github.com/boxdot/gurk-rs) - I recommend checking out the [instructive article on the Mobian Wiki](https://wiki.mobian-project.org/doku.php?id=signal&s[]=signal), and hope to find the time to write a blog post about this topic soon._

#### Open Letters
* _Removed._

#### Software development progress
* Plasma Mobile: [Plasma Mobile 21.07 is Out](https://www.plasma-mobile.org/2021/07/20/plasma-mobile-gear-21-07/). _Great roundup of how things move forward in the Plasma Mobile realm._
* Claudio Cambria: [\*BZZT\* Your 12 o’clock is next on your schedule — Kalendar week 6 (GSoC 2021)](https://claudiocambra.com/2021/07/18/bzzt-your-12-oclock-is-next-on-your-schedule-kalendar-week-6-gsoc-2021/). _More great progress!_ 
* Janet Blackquill: [This Week In Tok: New Features And UI Improvements And Janet Got Tired Of Writing A Cool Title For Every Weekly-ish Post](https://blog.blackquill.cc/this-week-in-tok-new-features-and-ui-improvements-and-janet-got-tired-of). _This Telegram client makes huge steps forward: Mobile Settings! Support for Secret chats!_

#### Peaceful project transitions
* Purism: [How Calls became a part of GNOME](https://puri.sm/posts/how-calls-became-a-part-of-gnome/). _Evangelos Ribeiro Tzaras wrote a nice post here, talking about what I really like about Purism's approach: Build software, together with the community and upstream._

#### Security-based Advertising
* Purism: [Defending Against Spyware Like Pegasus](https://puri.sm/posts/defending-against-spyware-like-pegasus/). _Some valid points here._

### Worth listening
* PineTalk [013: 13th of July](https://www.pine64.org/podcast/013-13th-of-july/). _Ezra and I talked about PINE64's July Community Update, tried our best to answer six community questions, to then (to my surprise both) step down. So if you're reading this and would like to continue a community podcast, step up! It's been a lot of fun to do this, thank you to everybody involved for the opportunity!_

### Worth watching

#### Ubuntu Touch
* UBports: [Ubuntu Touch Q&A 104](https://www.youtube.com/watch?v=JcAc0mOpVrw). _Dalton is joined by Florian this week. They talk about OTA18, 20.04 porting progress, some PinePhone progress Dalton made and improvements to TelePorts. There's also an interesting question revolving around WayDroid (former Anbox-halium) on non-Halium 9 phones._

#### Software progress
* kop316: [Visual Voicemail](https://matrix-client.matrix.org/_matrix/media/r0/download/matrix.org/mGfajTVkiWIeEfaeDlQzZTaF). _Chris, who did great work on getting MMS to work, is now showing of the client part of [his work that enables Visual voicemail](https://gitlab.com/kop316/vvmplayer)._ 

#### Because they can
* UJC TheGuy: [PinePhone video recording](https://www.youtube.com/watch?v=hjW8BBNpxx4). _The Reddit post is linked above._
* noocsharp: [mowc + oasis on the Pinephone](https://www.youtube.com/watch?v=WJaFkxEUWhc). _Ambitious project! If you want to follow the past progress, [check out the corresponding blog posts](https://nihaljere.xyz/words.html)!_

#### Tutorial
*  kdlk.de: tech: [PinePhone Essentials: Change the PinePhone pin/password for login](https://www.youtube.com/watch?v=J-5oSgjBfic). _passwd!_

#### Software reviews
* TechHut: [Manjaro Plasma-Mobile on the Linux PinePhone! (Beta5)](https://odysee.com/@TechHut:1/manjaro-plasma-mobile-on-the-linux:f). _Personal comment: Gosh, I need to promote LINMOBapps more (or get it's replacement going and promote that)!_
* two: [Mobian on the Pinephone FTW](https://odysee.com/@two:66/mobianpinephoneFTW:0). _Loving the sound track. Everything is so much more exciting with a little Bond!_

#### PINE64 Community update
* PINE64: [July Update: Community Developers Portal](https://odysee.com/@PINE64:a/july-update-community-developers-portal:a). _PizzaLovingNerd did a great job once again!_

#### JingOS
* JingOS: [Sending JingPad A1 sample devices to some Youtubers, please wait for 2~4 weeks for their review~](https://www.youtube.com/watch?v=v-i4qfKA5kQ). _Don't worry: I won't be getting one._
* JingOS: [[DEMO] New virtual keyboard for JingOS working with the Terminal app](https://www.youtube.com/watch?v=ru-m2P7jy4U). _While there is no point in even pointing out which OS inspired this keyboard design, it looks very smooth and well implemented!_

#### Current, and somewhat related
* PizzaLovingNerd: [Buy This Instead of a Freedom Phone](https://odysee.com/@pizzalovingnerd:5/buy-this-instead-of-a-freedom-phone:c). _If that laughable, detail-free web site wasn't enough..._
* Gardiner Bryant: [BIGGEST LINUX GAMING NEWS IN YEARS.](https://odysee.com/@TheLinuxGamer:f/steam-deck-1:d) _Yes, this is about the Steam Deck which is a Nintendo Switch like, AMD-powered Gaming handheld that's going to run an Arch Linux based OS featuring a KDE Plasma based user interface. Even as a non-Gamer I must say that I am intrigued._

### Stuff I did

#### Content
* I wrote a blog post, called [Phosh 0.12 and its App Drawer refinement](https://linmob.net/phosh-0-12-app-drawer/). On late Sunday, I added some feedback I received by Purism's developers – so maybe re-visit it if you were an early reader!

#### Random

* I have been playing with current releases on the Ubuntu Touch kernel upgrade channel (which now is on 5.13), and things have definitely improved a lot.
* I have been wondering on making the process of creating this update easier for me and generally better and more collaborative. This [concept](https://blogs.gnome.org/haeckerfelix/2021/07/16/introducing-this-week-in-gnome/) seems intriguing. What do you guys think?



#### LINMOBapps

A few more apps have been added, and I have started to create some open issues to help with what to contribute with - please feel free to add more issues, if you have some: Filing bugs/improvement requests are worthwhile contributions too!

These apps were added in the past week:

* [Obfuscate](https://gitlab.gnome.org/World/obfuscate/), which lets you redact your private information from any image,
* [Gerbil](https://gitlab.com/armen138/gerbil), a little gemini browser, written with mobile linux in mind,
* [Wasp Companion](https://github.com/arteeh/wasp-companion), a Linux companion app for Wasp OS (a smartwatch operating system),
* [µPlayer](https://source.puri.sm/guido.gunther/livi), a minimalistic video player using GTK4 and GStreamer featuring hardware accelerated video playback,
* [Visual Voicemail](https://gitlab.com/kop316/vvmplayer), a frontend for Visual Voicemail.

[Read here what (else) happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master). 
Please [do contribute!](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/CONTRIBUTING.md)
