+++
title = "Qt for Android"
author = "peter"
date = "2011-02-22T09:18:00Z"
layout = "post"
[taxonomies]
tags = ["Android", "Android-Qt", "KDE", "MeeGo", "Nokia", "Qt"]
categories = ["shortform"]
+++
<p>With Nokia dropping its great sounding, not too well delivered platform strategy which was heavily Qt (a software that was called a toolkit years ago and now is more than this) based in favour of Windows Phone 7, there were lots of people asking what would be the future of Qt now. With Intel continuing to support MeeGo and the fact that Qt is a great platform to create software that works natively on Mac OS X, Windows and Linux&mdash;and we shouldn't forget about the K Desktop Environment here&mdash;there is one more indicator that makes a survival of Qt within or without Nokia more likely, no matter what happens to the Qt unit within Microsoft's new puppet: An enthusiast, Bogdan Vatra, announced a Qt port to todays dominating mobile platform, Android. It's called Necessitas Suite.

<a href="http://sourceforge.net/p/necessitas/home/">Necessitas at sourceforge</a>

<a href="http://groups.google.com/group/android-qt/browse_thread/thread/209edef7c5ceec8a?pli=1">Annoucement in the Android-Qt google group</a><br />

__Via:__ <a href="http://mobile.slashdot.org/story/11/02/22/0119247/First-Alpha-of-Qt-For-Android-Released">Slashdot: First alpha of Qt for Android released</a>
