+++
title = "2008. An attempted mobile market summary"
aliases = ["2008/02/2008-a-try-of-a-mobile-market-summary.html"]
date = "2008-02-05T07:49:00Z"
[taxonomies]
tags = ["platforms", "Trolltech", "OpenMoko", "Android", "Azingo", "Qtopia",]
categories = ["software", "commentary",]
[extra]
author = "peter"
2021_commentary = "This analysis is not worth much, but it shows that the market was quite crowded with commercial attempts to build new Linux-based mobile platforms. Today, all of this can be assumed to have been drowned out by Android."
+++

After my angry post about the Software of existing Linux phones, I want to write down my expectations for 2008.
<!-- more -->

It's been really interesting to watch the stream of announcements during the last months, just to give you a few keywords: OpenMoko, Qtopia becomes OpenSource, Android, Azingo&#8230;, Nokia buying Trolltech&#8230;

Quite a lot going on, and we can expect, that there is a new kind of jungle growing like the jungle of Linux distributions on the Desktop&mdash;but as cellphone/smartphone market differs a lot from PC market, it will be really interesting who will be really successful as there are many factors:
Hardware producers, Operators, the OpenSource community&#8230; and, last but not least: consumers.

Why do I put consumers at the back? Well, they are very important for success, as they are &#8220;creating&#8221; the demand for phones, but: I don't know many people that really use their phone for more then some calls and SMS, listening to music and playing some funny Java games.

Do you need a Linux based phone for that? Sad to tell, but if you aren't a kind of geek, you don't&mdash;and I think that is the reason why the two manufacturers I was angry at created there phone stacks as they did: Let's have Linux as a cheap platform that decreases RnD expenses and looks just like other phone interfaces do, is quite adjustable for us, has a nice JavaVM for additional &#8220;software&#8221;.

Somehow in 2007 the market changed a bit in this point, I really think that the iPhone showed people (consumers and developers) that you can do more with a phone than that I described a few lines above (not because the iPhone is that great&mdash;I don't want to talk about it&mdash;but maybe because of Apples' great marketing). Operators like(d) that, I think, because Mobile Internet is a thing they've tried to push for a long time, but besides of businessmen and geeks nobody really used these services.

Let's go back to the beginning of 2007: OpenMoko was already announced, Apple announced the iPhone (and you could hear a &#8220;big bang&#8221;, long flamewars in forums about mobile phones).

Then, in autumn, Motorola announced MotomagX (with a linux SDK(!)), GPhoneAndroid was announced.. and in 2008 this happened: Nokia bought Trolltech (and Motorola announced to switch over to GTK, which is also preferred by LiMo-Foundation, OpenMoko and others (GTK and WebKit appear to be the big players&#8230;)) and there are several announcements of new platforms or phones based on Linux every week, we can be absolutely sure that there will be a bunch of Linux phones in late 2008.

But who will win? Well, it depends on marketing, operators and consumers, and I really think that Android will be quite a success (what doesn't mean that I like it) as there are many powerful companies behind it. Motorola&#8230; well, we can't even be sure whether they will continue to build mobile phones (I can't imagine a cellphone market without them, BTW) and they've got a multiplatform strategy: Linux (Android (maybe on announced new Qualcomm powered phonesm as QC is an OpenHandset member) and LiMo), Symbian UIQ, Windows Mobile and a little bit of there own old proprietary os, some other cheap solutions, exspecially aimed to bring 3G technology to the poorer parts of the world... OpenMoko, I hope they'll be able to gain some market shares big enough to survive, as I really like there open philosophy and as they're completely right, that phones will be pocket computers (in a better way than &#8220;PocketPCs&#8221;).

There will be much innovation in the market, I can imagine an x86-powered Smartphone in 2009, and lots of <del>phones</del> ultra ultra mobile computers with fast data services and big bright screens, which will hopefully have good enough power management to be useful as a mobile device.
