+++
title = "Android, igeekmod R2 status and new plans"
aliases = ["2008/09/24/android-igeekmod-r2-status-and-new-plans.html"]
date = "2008-09-24T09:22:00Z"
[taxonomies]
tags = ["A910", "Android", "hardware", "HTC Universal", "Motorola", "Samsung SGH-i780", "Windows Mobile", "igeekmod"]
categories = ["personal"]
[extra]
author = "Peter"
+++
This blog was really quiet lately, I have to apologize for that. I am still not at home (and without unlimited broadband internet access, which is the reason why I am writing this using my HTC Universal (Windows Mobile 6.1, Opera 9.5 Beta)). 
<!-- more -->
Android is getting ready and the first Android device, the T-Mobile G1 (aka HTC Dream) will be on sale in the US on Tuesday (if I am wrong, sorry, I can't do extensive (and expensive) multi-tabbing on this device) for 199$ and a two year contract. Unforunately it will take some time until the device will appear on sale in the European markets: They say it is due to translation is needed, but the fact that T-Mobile is exclusively selling the iPhone (3G) here in Germany, they don't have this huge need for an internet device is likely not speeding up the process too much ;)

Today the Open Handset Alliance released the Android 1.0 SDK (unfortunately I won't be able to test it right now and they say that Android will be open-sourced later this year.&#8222;

Considering the T-Mobile G1, I have to say that I am not too impressed: 
HTC has better hardware in its product portfolio (e.g. the Touch Pro). The G1 is marketed (and it's no problem to discover that) as an iPhone rival. It has similar specs, but a keyboard (I love Qwertz/qwerty keyboards on small devices) and no 3,5mm headphone jack&mdash;which is not too bad&mdash;but it is always nicer to have one, especially if your phone has a music store on it.

These are my first thoughts regarding the G1, if you want to know more about Android, get yourself the SDK and read some reviews&mdash;I am pretty sure that there will be tons of reviews soon. And if you've got a Motorola EZX cellphone or an other phone on which you can try out Android (I am pretty sure that there some people working on unofficial Android builds for several phones right know).

EZX is the keyword to start talking about A910 igeekmod R2 status: No news at all. I didn't find the time to work on it yet&mdash;and it is not too probable, that I'll find much time to do so, soon.

But I promise to create one more release, with igeekmod R1's speed (the unbranded R58 is dead slow), easy to set up VoIP (might be an ugly hack) and newer SHOWQ binarys&mdash;I can't promise more, as every additional feature will take nore and more time.

As I won't have that much time the next time, I won't blog that much&mdash;and I'll need a good PIM solution&mdash;that just works. I don't like Windows Mobile&mdash;but seems to be a stable and easy to use platform (though it's interface contains huge usability flaws and is inconsistant). But as the Universal has a good size for couch potato surfing, but not to carry it in trousers pockets, I might need a new piece of hardware. 

I had a look at QWERTY smartphones and PocketPCs, and the one I actually liked best (beside HTCs Touch Pro, which is way to expensive for me) is the Samsung SGH-i780, which isn't too small but pretty thin and features good performance and battery life. Some of you may say: uarghs, Windows Mobile (and I can understand that), but I just don't like Symbiam. Palm OS/Garnet is nice but somewhat outdated, Openmoko/QTopia aren't ready (and Neo Freerunner has no hardware keyboard) yet (and aren't as extensible as Windows Mobile is. Maybe there will be a Linux for this Samsung one day, e.g. Android.

That's it for now &#8230;
