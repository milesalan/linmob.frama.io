+++
title = "Comment: Delayed AOSP release of Honeycomb"
aliases = ["2011/03/comment-delayed-aosp-release-of.html"]
date = "2011-03-28T13:42:00Z"
[taxonomies]
tags = ["Android 3.0 (Honeycomb)", "blackberry playbook", "development", "motorola xoom", "OMAP4", "open source", "openness", "qnx", "Samsung Galaxy Tab"]
categories = ["commentary"]
[extra]
author = "Peter"
+++
Recently, Google's <a href="http://www.businessweek.com/technology/content/mar2011/tc20110324_269784.htm">Andy Rubin stated</a> that it would take some more time until they would release the source code of Android Honeycomb. This is not a new phenomenon, it often took Google longer to release the source code than the SDK or the first device running the same Android release. This time it's going to take a lot longer&mdash;but, honestly, this is not surprising.

First of all, one hears of random application / service crashes in Honeycomb, secondly screenshots of Honeycomb on smartphones have indicated, that this Honeycomb platform could have used some more time in the lab&mdash;I believe that the early (in terms of code finalisation) release of Honeycomb was actually a business decision: Get people exited with Honeycomb tablets before the iPad 2 is out must have been the idea, and actually, this seems to have worked out&mdash;while many tech journalists say, that the iPad 2 is the best tablet on the market right now, most of them mention that Honeycomb nonetheless is a strong platform that just needs the right hardware (Samsung's new Galaxy Tabs are going to be great, the Motorola XOOM is often referred to as early, thick and heavy) to be a really strong competitor.

Releasing a software really fast always means that there are some ugly hacks in it, and it makes perfect sense that Google doesn't want to release  source codes that contain ugly hacks and doesn't run well on every hardware platform&mdash;and they can do so due to choosing the Apache license, which isn't as demanding in that sense as the GPL (especially GPL v3) is&mdash;they may be sued to release the GPL licensed software parts though (kernel and a few libraries).

Still, openness is one of the mayor selling points of Android, and so I am very positive, that Google will release the source code of Honeycomb as soon as they have fixed the issues in it&mdash;they could have made Android closed  source in the first place if they had wanted too&mdash;you don't need a Linux kernel to run Dalvik&mdash;a story the Blackberry Playbook tells us (most likely, they might actually run a virtualized Linux kernel&mdash;the TI OMAP 4 SoC is definitely powerful enough to do so).

On the other hand, as someone watching community projects and being in contact with open source people for some time, this is the time to mention again that Android isn't as open as other projects&mdash;new releases are always created behind closed doors, while some bits others changed on the previous release are pulled in, this is not open development. It's just a opening to allow modifications and additions, but then, again, this does it for many and is in a way more than what you are able to do on other platforms that rely on more GPLed code (think of webOS and the no ogg, no flac media player)&mdash;the platforms efforts that are entirely open source are rarely supported / pushed forward by companies and thus remain enthusiast free time projects (<a href="http://gpephone.linuxtogo.org/">G(PE)²</a> is an example here, as it was backed by a unnamed company for a rather short time).

Android is just the most open / free mayor platform out there, but that's something, isn't it?
