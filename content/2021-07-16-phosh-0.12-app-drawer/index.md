+++
title = "Phosh 0.12 and its App Drawer refinement"
date = "2021-07-16T15:30:18Z"
updated = "2021-08-07T07:35:00Z"
[taxonomies]
tags = ["Phosh", "Purism", "Librem 5", "PinePhone",]
categories = ["howto", "software",]
[extra]
author = "Peter"
update_note = "Added reader feedback about sed-based approach."
+++

Phosh 0.12 has a new feature: The app drawer, by default, now only displays mobile friendly apps. Sadly, this requires minor adjustments to .desktop files. While projects catch up with this new reality, here are some instructions that help with adding all the apps you want to that mobile friendly view.<!-- more -->

### The feature

To quote the [release notes](https://source.puri.sm/Librem5/phosh/-/releases/v0.12.0):

<blockquote>
Show adaptive apps in mobile mode and all apps in docked mode based on <code>X-{Purism,Kde}-FormFactor</code>. This
can be toggled via the sm.puri.phosh.PhoshAppFilterModeFlags GSetting.
</blockquote>

To be more verbose, the app launcher on _Phosh_ now has an optional second mode, which only shows mobile compliant apps. But don't listen to me waffling, when there are screenshots:

{{ screenshotgallery() }}

### The problem

Although Purism did [describe the general metadata this bases on quite some time ago in October 2020](https://puri.sm/posts/specify-form-factors-in-your-librem-5-apps/), many mobile compliant apps listed have not implemented the necessary extra metainfo in their AppStream files (which don't matter here) or their .desktop entries (which do matter here.

While going through the apps I have installed, I came up with 16 GTK based apps that are mobile friendly but don't have this feature covered, and the situation is almost hopeless with QtQuick/Kirigami based apps.[^1]

### How to fix apps

The good thing is that you can fix this quickly on your own. All you need to do is add a line to the apps .desktop file, which are usually to be found in `/usr/share/applications/`, `/usr/local/share/applications/` (and `/var/lib/flatpak/exports/share/applications` for Flatpak apps).

To do this, I recommend making a local copy of the .desktop file at hand first in `~/.local/share/applications` and edit this.

All you'll need to do, is add the line
~~~
X-Purism-FormFactor=Workstation;Mobile;
~~~
with your editor of choice. 

You can be quicker by running:
~~~
echo "X-Purism-FormFactor=Workstation;Mobile;" >> yourfile.desktop
~~~

and if you want to be done with this quickly, and are fine with using a shotgun approach, you can just create the following script

~~~
for file in ./*.desktop 
do
    echo "X-Purism-FormFactor=Workstation;Mobile;" >> "$file"
done
~~~

in the folder you have the .desktop files you want to be modified, mark it executable and run it. Done!

#### Reader feedback (added August 7th, 2021)

As reader Jan points out via email, the shutgun approach described above fails for apps that have multiple sections, e.g. a [Desktop action new-window] section like org.gnome.gedit.desktop has. He recommends using the stream editor `sed`as follows instead of my `echo` based approach:

~~~
sed -i '/\[Desktop Entry\]/a X-Purism-FormFactor=Workstation;Mobile;' yourfile.desktop
~~~

This indeed should work with any .desktop file, thanks Jan!

### How to fix KDE apps (July 28th, 2021)

Doing it locally, you can just add the above values - no harm done. But as the KDE folks came up with their [own draft for this a while back, prior to Purism's implementation,](https://lists.freedesktop.org/archives/xdg/2020-October/014405.html), which allows for a larger number of form factors, this would be the option for KDE apps to implement:
~~~
X-KDE-FormFactors=desktop;tablet;handset;mediacenter;
~~~
The relevant value for phones here is "handset", and it's implemented in Phosh so it works as well.

### Conclusions

This is a cool feature, and it can be really useful if you want to use your Linux Phone with Phosh in convergent scenarios every then and now, and thus have some desktop apps clutter up your launcher.
It can also be abused to e.g. hide heavy, slow starting apps you rarely use, but sometimes tap accidentally - all you need to do is set up the local copy of that .desktop file according to your preferences.

Just be creative and have fun, and if you develop a mobile friendly app that does not have [the necessary lines of text](https://puri.sm/posts/specify-form-factors-in-your-librem-5-apps/) added yet, maybe don't wait for my pull or merge request ;-)

### Addendum (July 18th, 2021)

After posting about this blog post, Purism Developers [Guido Günther](https://social.librem.one/@agx/106591923593563166) and [Sebastian Krzyszkowiak](https://twitter.com/dos1/status/1416085146202148864) reached out on Mastodon and Twitter respectively:

* Guido pointed out that the feature can be turned off completely by running `gsettings set sm.puri.phosh app-filter-mode '[]'` and that they have also implemented for the alternative `X-KDE-FormFactor` value (see above) so that KDE apps can implement a specific, non-Purism branded setting.
* Sebastian wrote that AppStream data is going to be much more important long term, as there were screen size attributes upstreamed [into AppStream spec](https://www.freedesktop.org/software/appstream/docs/chap-Metadata.html#tag-requires-recommends). You can track [that work on Purism's GitLab instance](https://source.puri.sm/Librem5/phosh/-/merge_requests/858).



[^1]: I will not do app shaming here, but instead go ahead with merge or pull requests. Ironically, there is (at least) one app that no longer works great on mobile but has implemented the AppStream and desktop-file enhancements.
