+++
title = "linmob weeky roundup (6 / week 12): few news, but at least some"
aliases = ["2009/03/21/linmob-weekly-roundup-6-week-12-few-news-but.html"]
author = "peter"
date = "2009-03-21T19:32:00Z"
layout = "post"
[taxomomies]
tags = ["browser", "fennec", "htc", "iPhone", "LiMo", "openmoko", "Palm", "palm pre", "Samsung"]
categories = ["weekly update"]
+++
<p>Another week has passed by and it is time for another round up of what happened.

Besides Android smartphone speculations and rumors (e.g. there will be a Samsung branded Android running smartphone as soon as Q3, but it will be just a Samsung branded HTC Sapphire / Magic device.. and Samsung Omnia HD might run Android, as well), there was really great news this week.

The <a href="http://antipastohw.blogspot.com/2009/03/using-buttonshield-to-make-open-source.html">OpenSource BlackBerry</a> is great news, don't you think so?

Ok, might be that interesting for everybody. iPhone OS 3.0 isn't really Linux related, but as it is a competitor, it has to be mentionned. You should read <a href="http://www.engadget.com/2009/03/19/mobile-os-shootout-iphone-os-3-0-enters-the-fray/">engadget's mobile OS shootout.</a>

What are further findings of this week?

Well, you should definitely read <a href="http://www.visionmobile.com/blog/2009/03/why-the-limo-foundation-needs-to-go-back-to-the-drawing-board/">Andreas Costantinous opinion about the LiMo platform</a>. I believe that LiMo's idea wasn't the worst we've ever seen (damn it, I still have to write that article about LiMo&#8230;), but now that we have several Linux based platforms like Android, Palm's WebOS and good old Maemo, that don't use the benefits of the LiMo platform, and as LiMo-based ALP and Azingo missed their timelines, they should definitely re-think their approach. The market doesn't wait.

<del>Additionally I would like to link the Openmoko Community Update, but it isn't ready yet&#8230;</del> <a href="http://wiki.openmoko.org/wiki/Community_Updates/March_20%2C_2009">Check out the latest Openmoko Community Updates</a>!

[UPDATE 03/22/2009]

I was a little bit lazy yesterday, so enjoy two mobile internet experience related things that might interest you:
* <a href="http://www.mozilla.org/projects/fennec/1.0b1/releasenotes/">Fennec 1.0 Beta</a> is out, and there is a <a href="http://blog.pavlov.net/2009/03/17/fennec-1-beta-1/">webpage with two nice videos of the soon to come mobile Firefox browser</a>.
* A <a href="http://www.cnetfrance.fr/news/mobilite/htc-magic-android-telephone-google-sfr-39387696.htm">french video</a> (of the SFR HTC Magic) shows that there will be a new zooming mechanism on Android 1.5, circular zooming (via <a href="http://androidsmartphone.de/android-os/android-mit-kreis-zoom-auf-dem-htc-magic/">Android Smartphone</a>).

One more thing: <a href="http://www.koolu.com/">Koolu</a> has some more, newer Android beta images, featuring a new installation process and faster boot-up. But as they aren't officially announced (at least I couldn't find an announcement on koolu.com), I would just recommend you to find them yourselves.. ;)
