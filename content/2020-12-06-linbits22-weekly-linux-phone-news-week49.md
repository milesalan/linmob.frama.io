+++
title = "LinBits 22: Weekly Linux Phone news / media roundup (week 49)"
aliases = ["2020/12/06/linbits22-weekly-linux-phone-news-week49.html", "linbits22"]
author = "Peter"
date = "2020-12-06T19:55:00Z"
layout = "post"
[taxonomies]
tags = ["Pine64", "PinePhone", "Arch Linux ARM", "DanctNIX Mobile", "Mobian", "LUKS", "F2FS", "Manjaro", "Jumpdrive", "Plasma Mobile", "Plasma Desktop", "Gnome", "Phosh", "Purism", "Librem 5", "Mainboard replacement", "Chromium", "Games"]
categories = ["weekly update"]
+++

_It's sunday. Now what happened since last sunday?_

LUKS-encrypted Mobian, DanctNix with F2FS, WiFi driver and modem firmware work by Megi and more. _Commentary in italics._
<!-- more -->

### Software releases

* DanctNIX Mobile/Arch Linux ARM [has had a new release that enables F2FS](https://github.com/dreemurrs-embedded/Pine64-Arch/releases/tag/20201204) on the PinePhone. 
* Mobian [have released](https://fosstodon.org/@mobian/105321244562195316) an [image with an installer](https://images.mobian-project.org/pinephone/installer/) that allows to set up a LUKS-encrypted installation for added security.

### Worth reading

* xnux.eu: [Alternate EG25G userspace project](https://xnux.eu/log/#028). _Megi has been working on the PinePhone's modem firmware._
* xnux.eu: [Fixing fallout from the new WiFi driver](https://xnux.eu/log/#027). _Also, Megi has been working on a new WiFi driver which should save power and (IIRC) fix that annoying more-than-one-AP-issue._
* Boiling Steam: [How-To: Upgrade PinePhone PCB](https://boilingsteam.com/how-to-upgrade-pinephone-pcb/). _If you are going to ugrade your PinePhone's main board, this is going to help._
* Purism: [Avoid “Advertiser ID” with the Librem 5](https://puri.sm/posts/avoid-advertiser-id-with-the-librem-5/). _Another reason for Linux phones._
* PINE64: [Pine Store Community Pricing & Online Retail Stores](https://www.pine64.org/2020/12/02/pine-store-community-pricing-online-retail-stores/). _This is a good idea._
* PINE64: [KDE Community Edition is now available](https://www.pine64.org/2020/12/01/kde-community-edition-is-now-available/) _Rather boring. ;-)_
  * Plasma Mobile: [PinePhone: KDE community edition](https://www.plasma-mobile.org/2020/12/01/pinephone-kde-community-edition.html). _And Manjaro Plasma Mobile it is! Make sure to watch my video linked below._
  * Linux Smartphones: [PinePhone KDE Community Edition is up for pre-order for $150 and up](https://linuxsmartphones.com/pinephone-kde-community-edition-is-up-for-pre-order-for-150-and-up/). _Brad's take on the announcement._

### Worth watching

* Gardiner Bryant: [First impressions of the final Librem 5 Hardware]( https://odysee.com/@TheLinuxGamer:f/gb-evergreen:a). _This is a nice video by Gardiner Bryant, who has also made videos about previous Librem 5 iterations ([Birch](https://www.youtube.com/watch?v=7j7Hlewv9Dg), [Dogwood](https://www.youtube.com/watch?v=C7kDp9b6xFs)). If you want to get more information, maybe watch the [recording of his editing livestream](https://www.youtube.com/watch?v=J2ahX2RCDX4)_.
* Elatronion: [PinePhone KDE Plasma Mobile - The Android Replacement‽](https://lbry.tv/@Elatronion:a/pinephone-kde:0). _Nice video on KDE Neon, which brings me to my nitpick: Please always mention which distribution you are reviewing, including which exact image you used._
* PizzaLovingNerd: [PinePhone OSes: Manjaro Lomiri](https://tilvids.com/videos/watch/fcbc8de7-b4c2-498f-beb7-9942438604b0). _Great video. I have to disagree with his comments on the Ubuntu Touch design choices, they are good reasons for them. If Libertine worked and once Ubuntu Touch rebases on 20.04 (which is likely going to take until mid-2021), they wouldn't be an issue, imho._
* PanzerSajt: [Mobian with Gnome Desktop 3 on PinePhone](https://www.youtube.com/watch?v=7hi67pQivr4). _Nice! Instructions would be helpful though. I'd recommend checking the [Mobian Wiki](https://wiki.mobian-project.org/doku.php?id=desktopenvironments#gnome-shell) for now._
* Privacy & Tech Tips: [Flashing Mobian Linux To Pinephone With Full Disk Encryption Setup](https://www.youtube.com/watch?v=vjSASi7IbIU). _Great guide on how to use the new installer image referenced above._
#### Live stream corner
* Trustno1: [Flash an OS directly to PINEPHONE](https://www.youtube.com/watch?v=FvRcyZLCaq4). _This might help you with using JumpDrive. (See also: [LINMOBlive #0](https://youtu.be/4TghCp4UE8w).)_
* UBports: [Ubuntu Touch Q&A 90](https://www.youtube.com/watch?v=FD67UPVFouM). _OTA 15 news, news about a new effort by Dylan van Assche called [Convergence Components](https://gitlab.com/dylanvanassche/convergence-components) to make developing for all the mobile Linux platforms at once possible, Dalton talks about his efforts to bring Linux 5.10 to Ubuntu Touch on the PinePhone and more._

### Stuff I did

#### Content
I published two videos this week:
* PinePhone: Manjaro Plasma Mobile (soon on the KDE Community Edition): [PeerTube](https://devtube.dev-wiki.de/videos/watch/5c73aab1-cb41-4354-81fe-a56a36324806), [LBRY](https://lbry.tv/@linmob:3/pinephone-manjaro-plasma-mobile-soon-on:d), [YouTube](https://www.youtube.com/watch?v=eJ8V0lRxKgM);
* PinePhone: Plasma Desktop and the Convergence Dock: [PeerTube](https://devtube.dev-wiki.de/videos/watch/d96f2590-31f7-4928-abbc-79e3526d877c), [LBRY](https://lbry.tv/@linmob:3/pinephone-plasma-desktop-and-the:2), [YouTube](https://www.youtube.com/watch?v=f2Mmd3gRaTs);
On monday, I streamed live for the second time, unfortunately screwing up audio again. The fixed recording is on [PeerTube](https://devtube.dev-wiki.de/videos/watch/ff0b9166-e57f-419d-9bc4-95c39a896814). Also, I published two edited best-of videos to highlight the (IMHO) most interesting segments of that stream: 
* Best of LIMMOB Live #1: postmarketOS GNOME on the PinePhone: [PeerTube](https://devtube.dev-wiki.de/videos/watch/8ffb56cb-31bb-4998-925c-d8cfb6f7b0ae), [LBRY](https://lbry.tv/@linmob:3/best-of-limmob-live-1-postmarketos-gnome:0), [YouTube](https://www.youtube.com/watch?v=1TZm-ZPlToE); 
* Best of LIMMOB Live #1: Maemo Leste on the PinePhone: [PeerTube](https://devtube.dev-wiki.de/videos/watch/aec95d0e-b8cf-4091-ba8e-4f2d487c7a97), [LBRY](https://lbry.tv/@linmob:3/best-of-limmob-live-1-maemo-leste-on-the:a), [YouTube](https://www.youtube.com/watch?v=xN5HTNcWyL0).


#### LINMOBapps
The game-split has finally happened, and I am considering a "web app list" next.  Also I  [managed to add some apps](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master).
If you use web apps on your PinePhone, Librem 5 or postmarketOS phone, please tell me about it!

#### Random  Stuff
* [Chromium can now run on the PinePhone without blurriness](https://nitter.net/linmobblog/status/1335614750626115585#m). 
