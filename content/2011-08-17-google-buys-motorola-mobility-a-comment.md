+++
title = "Google Buys Motorola Mobility - A Comment"
aliases = ["2011/08/google-buys-motorola-mobility-comment.html"]
date = "2011-08-17T12:03:00Z"
[taxonomies]
tags = ["Android", "google", "Motorola"]
categories = ["hardware", "commentary"]
[extra]
author = "Peter"
2021_commentary = "Google would not hold on to Motorola for too long and sold it on to Lenovo in 2014. Interestingly, Google later bought roughly half of HTC's design and research talent in 2017 for US$1.1 billion."
+++
_It's been a while since my last post here (over a month in fact), and this post won't be that great - apologies for that._

Almost everybody shared their feelings and thoughts on the fact that Google announced it's willingness to buy Motorola Mobility for USD 12,5 billion and I have to share my 2 cents on that, too.
<!-- more -->
I think that this deal makes perfect sense. Motorola Mobility hasn't been doing too well, and they have been  Android only in the smartphone markets - despite that, they are a US company, just as Google, so integration (I doubt that the brand &#8220;Motorola&#8221; will vanish) won't be that problematic. While many users certainly hope for Google to change Motorolas software team as people haven't been all that happy with Motoblur and Motorola 's slow Android updates (if any), this is not why this is happening. It should be a well known fact that Android has many enemies out there, partly due to its relatively open nature, partly due to its blazing success. Patent fights are going on, and the thing is: Google doesn't have that many patents, as it's not that long in the mobile OS business. Motorola on the other side has loads of patents as it has always been busy to patent things, in fact, as Motorola Mobility CEO Sanjay Jha recently uttered, Motorola was even planing to use their patent portfolio to make some money, e.g. by sueing other Android device makers.

This exactly is the reason why the other Android device manufacturers almost like the fact, that Google is aquiring Motorola Mobility, as it protects them. Google, on the other hand, will, while certainly interested in a profitable and flourishing Motorola Mobility, not use their new hardware division to get other companies like LG, HTC or Samsung out of the market; as they are interested in maximising Androids market share.
