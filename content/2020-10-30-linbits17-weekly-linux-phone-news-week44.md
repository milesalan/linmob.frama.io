+++
title = "LinBits 17: Weekly Linux Phone news / media roundup (week 44)"
aliases = ["2020/10/30/linbits17-weekly-linux-phone-news-week44.html", "linbits17"]
author = "Peter"
date = "2020-10-30T22:39:00Z"
layout = "post"
[taxonomies]
tags = ["Librem 5", "Pine64", "PinePhone", "PineCone", "PineCom", "Arch Linux ARM", "Nemo Mobile", "Manjaro", "postmarketOS", "Linux", "Ubuntu Touch", "QWERTY", "hardware keyboards", "Fxtec Pro1", "Cosmo Communicator", "Browsers", "p-boot", "LINMOBapps", "SailfishOS"]
categories = ["weekly update"]
+++

_It's ~~sunday~~ __friday__. Now what happened since last sunday?_

Phosh 0.5.0, 60 Hz on the PinePhone, PineCone WiFi/BT LE Modules, QWERTY Linux awesomeness and more. _Commentary in italics._
<!-- more -->

### Software releases and improvements
* Phosh 0.5.0 [has been announced](https://social.librem.one/@agx/105111676277464675) by Guido Günther. It adds more keybindings and a very nice docked mode which makes convergence a lot more useful. And there is a lot more in the full [release notes](https://source.puri.sm/Librem5/phosh/-/releases/v0.5.0).
* 60 Hz kernel: Megi's kernel has [been](https://github.com/megous/linux/commit/e8be2e4affb906af7137437ac62fb3912609c11e) [patched](https://github.com/megous/linux/commit/29533cb13f24e26e7ddbc044597a3020437352e2) to properly drive the display at 60Hz. It makes quite the difference, especially scrolling feels much more smooth now. _The patched kernel has already landed in Arch and Manjaro, and postmarketOS should have it soon. It's one of these changes that improve the situation so much that you wonder "How could I ever deal with this phone before that patch?" when you go back to a not yet updated OS._
* The Nokia 8110 4G GPL source has been finally [released](https://reddit.com/r/KaiOS/comments/jjonp9/nokia_8110_4g_gpl_source_code/).

### Hardware announcements

* PineCone: PINE64: [Nutcracker Challenge: Blob-Free WiFi & BLE](https://www.pine64.org/2020/10/28/nutcracker-challenge-blob-free-wifi-ble/). _This is an nice idea, I really hope that it is going to be successful and wonder whether PineCone could become the WiFi module of PineCom, which would be making that new handheld device a fully blob-free one._

### Worth reading

* Gamey: [Pinephone: Best Web Browsers ArchLinux ARM Phosh](https://lbry.tv/@gamey:c/Best-Pinephone-Web-Browsers:0). _My favourite browser on the PinePhone is still Angelfish, although the new 60Hz patched kernel seems to have helped Epiphany performance quite a bit._
* Purism / Kyle Rankin: [The General Purpose Computer In Your Pocket](https://puri.sm/posts/the-general-purpose-computer-in-your-pocket/). _Another Rankin' rambling._
* xnux.eu log/Megi: [New p-boot features – reboot to FEL/eMMC, 60FPS fix](https://xnux.eu/log/#023). _Megi's blog post on the kernel improvement mentioned above plus an announcement of a new p-boot feature I am actually interested in. I really hope that Megi's next multi-boot image includes Nemo Mobile, too._
* Linux Smartphones: [F(x)tec Pro1-X smartphone runs Ubuntu Touch or LineageOS, has a physical keyboard (crowdfunding)](https://linuxsmartphones.com/fxtec-pro1-x-smartphone-runs-ubuntu-touch-or-lineageos-has-a-physical-keyboard-crowdfunding/). _Quite pricey for a Snapdragon 835 phone, but manufacturing small scale devices with QWERTY is more expensive than you would think. Also, this is one of the less risky crowdfunders, as the hardware basically exists and is proven to work. I managed to hold off as I have [more than enough QWERTY devices](https://fosstodon.org/@linmob/105108613796535501) already._
* Linux Smartphones: [Cosmo Communicator’s Linux OS gains new cover screen features](https://linuxsmartphones.com/cosmo-communicators-linux-os-gains-new-cover-screen-features/). _As Brad reports, users of this device have not been happy with the Linux support previously. Stay tuned!_

### Worth watching

* Planet Computer: [Cosmo Communicator New Update Oct ‘20 - Linux on Cosmo](https://vimeo.com/472188188). _Quite interesting: Looks like they are using the Ubuntu Touch Phone and Messages apps on a Plasma Desktop._
* HomebrewXS: [Arch Linux on the PinePhone](https://www.youtube.com/watch?v=kw3FFz7o-MU). _A short walkthrough through Arch Linux ARM/DanctNIX Mobile._
* HomebrewXS: [Anbox on the PinePhone](https://www.youtube.com/watch?v=ahitAWmrqzU). _Anbox works even better now than it did in my last video._
* HomebrewXS: [Meet Mobian on the PinePhone](https://www.youtube.com/watch?v=Zz6oXIzpURI). _The same for Mobian._
* Dark1 Linux, Tech, Gaming: [The BEST OS for the PinePhone is....](https://lbry.tv/@dark1LTG:3/the-best-os-for-the-pinephone-is:0). _I can't agree with his conclusion, there are just too many bugs. His favourite OS is pretty good on other devices (and I contribute money to its development), but it's just not there on the PinePhone. I agree that the Open Store is pretty great though._
* FOSDEM: [Extending the lifetime of smartphones with Replicant, a fully free Android distribution](https://www.youtube.com/watch?v=VuOeyrIcpJE). _This is quite old, but newly published and interesting._

### Stuff I did

#### Content [^1]
* I finally [made a video](https://devtube.dev-wiki.de/videos/watch/08b7d4e6-f736-47cc-a25e-c05be5c2165b) on [Nemo Mobile](http://nemomobile.net/). I am not too happy with it, but felt that I needed to get it done despite an empty battery. It's a great project, I like many of their design choices and it is fully open source. I hope they manage to transition over to Fedora soon, so that more fully baked PinePhone [^1]images can help Nemo Mobile gather the positive attention they deserve.
* I shared a few photos on social media, showing [convergence](https://fosstodon.org/@linmob/105119199285800351), [more convergence](https://nitter.net/linmobblog/status/1321912149967556608#m), and my [QWERTY collection](https://nitter.net/linmobblog/status/1321184247089156096). Also, a [QWERTY miscommunication](https://nitter.net/linmobblog/status/1321469305964400641#m) occured.
* All my videos are now on PeerTube. Just click the link in the footer to get to my channel.

#### LINMOBapps
Not much happened here, [just a few additions and some maintenance](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master). Last weeks [video asking for help](https://devtube.dev-wiki.de/videos/watch/76e3029b-4f1f-4db5-a70e-7a1009062154) was not successful; apparently people who care about games are not into maintaining lists, who could have known? 

#### and more ...
* I built [KeePit](https://github.com/DannyGB/KeePassTouch) (a Ubuntu Touch KeePass app) for ARM64 today and that went flawlessly; only my database does not seem to be compatible, but that is a common problem among KeePass clients, I had a similar issue with [ownKeepass](https://github.com/jobe-m/ownkeepass) on Sailfish OS (I really need to clean up my database and then save it in the most secure while compatible format). Building clicks is really, really easy; if you want to try it too, head over to [clickable-ut.dev](https://clickable-ut.dev/en/latest/). 

[^1]: This weeks LinBits is published early for personal reasons.
