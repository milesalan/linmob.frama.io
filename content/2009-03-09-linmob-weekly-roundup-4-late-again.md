+++
title = "linmob weekly roundup (4): late again."
aliases = ["2009/03/09/linmob-weekly-roundup-4-late-again.html"]
author = "peter"
date = "2009-03-09T08:32:00Z"
layout = "post"
[taxonomies]
tags = ["3D7K", "Android", "GTA03", "Nokia", "openmoko", "Qt Extended",]
categories = ["weekly update"]
+++

I have to apologize, but yesterday wasn't a writing day for me, so this roundup is a little bit late, again.

While my trip to CeBIT was particularily interesting (though I will have to prepare better the next time), I didn't notice many interesting news last week, e.g. I can't remember seeing an interesting new Android phone (though the <a href="http://wiki.openmoko.org/wiki/3D7K">Openmoko 3D7K</a> (was called GTA03 before) might turn out to be a rather open Android phone, as it seems to be a fact that it will have a capacitive touchscreen)...

What might be interesting for those interested in Android, is a video which shows the Android Cupcake Branch (Android 1.5), which will run on the HTC Magic, running on the Openmoko Freerunner. Remembering that the FR is based on an older microarchitecture than the main Android branch and that the Cupcake you see is a kind of a backport to less efficient microarchitecture and that there is (at least AFAIK) no video hardware acceleration (due to the lack of a good Glamo driver) it runs pretty well.

Interested? Check out the <a href="http://www.newlc.com/en/freerunner-mobile-which-support-android-cupcake">video over at newlc.com</a>!

As mentioned before, there might be a nice offering in the range of devices that I am kind of addicted to: Small devices (PDA to MID range) with a full QWERTY-keyboard, the <a href="http://road.de/">ROAD GmbH handyPC S101</a>, which was in fact announced in 2006 and is featuring rather outdated hardware (PXA270, only EDGE), but as some bloggers found it somewhere at CeBIT, and shot <a href="http://www.umpcportal.com/2009/03/road-s101-handypc">two videos</a>, one <a href="http://www.umpcportal.com/2009/03/road-s101-handypc-running-android">running Android</a>, it might find it's way to market some day, joining devices like the Nokia Communicators.

Back to the software side of life. Maemo 5 SDK alpha was released, featuring OMAP 3 support which is a clear hint that the next Nokia Internet Tablets (which might get phone capabilities, too) will have a more powerful SoC and might be nice for beagleboard-people, too.

Nokia owned Qt Software released Qt 4.5 under LGPL and announced that it would discontinue Qt Extended (aka Qtopia)(and that Qt Jambi will be discontinued after 4&mdash;maybe the community will work on this Qt parts. But the end of Qt Extended may be not bad at all, as some parts of it shall find its way into main Qt branch.If you are interested in Qt development, head over to <a href="http://www.linuxdevices.com/news/NS4824952673.html?kc=rss">LinuxDevices.com</a>.

_That's it for now, hope to see you again next saturday ;)_
