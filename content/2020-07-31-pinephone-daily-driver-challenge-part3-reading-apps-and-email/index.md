+++
title = "PinePhone DDC pt. 3: Reading Apps and Email"
aliases = ["2020/07/31/pinephone-daily-driver-challenge-part3-reading-apps-and-email.html"]
author = "Peter"
date = "2020-07-31T21:53:18Z"
layout = "post"
[taxonomies]
tags = ["PinePhone", "Video content", "Daily Driver Challenge", "postmarketOS", "Arch Linux ARM", "Huong Tram Linux", "NewsFlashGTK", "Email", "Geary", "Evolution (App)", "Read-it-Later", "Wallabag", "RSS", "Miniflux", "Foliate", "Ebooks", "Flatpak", "Flathub", "Scaling", "Phosh"]
categories = ["videos", "howto",]
+++
I have decided to change my approach: Writing the post before shooting the third "PinePhone as a daily driver" video. This week is going to be about Flatpak, Reading apps and the intricacies of running (non-)Flatpak Qt and GTK apps on the respective "other side". Part 2: Reading apps and Email.
<!-- more -->

__Caveat:__ _For the past week I have been struggling with receiving calls on Phosh/ModemManager based distributions, approximately since the massive improvements in battery life due to new Crust firmware. I found another user suffering from this issue, but assuming my tweets are being read among PinePhone users, it does not seem to be a widespread problem. I am continuing with this series despite of these issues, but be aware that core functionality might be currently not available._

### Reading Apps
I know, reading apps may not be the most pressing issue for a phone. But given the size of the PinePhones display, it can be quite good as a device that serves up text for the broadband interface of our human brains, the eyes.

#### RSS apps

_Ever since I had that sidebar RSS plugin in Firefox way back when, I've been an RSS junkie._  
I was very upset when Google sunset Google Reader[^1], but kept reading RSS feeds and eventually settled on [Miniflux](https://miniflux.app) as a self-hosted solution. For a long time I did not find a Miniflux-compatible desktop RSS reader, but I became aware of one: It's called [News Flash](https://gitlab.com/news-flash/news_flash_gtk), is available on [Flathub](https://flathub.org/apps/details/com.gitlab.newsflash) (also in Fedora and it can be built from the [AUR](https://aur.archlinux.org/packages/newsflash/)) and supports the following services:
* feedbin
* feedly
* Fever 
* Miniflux
* Local RSS

News Flash does not scale perfectly on the PinePhone in Phosh, but a `scale-to-fit NewsFlashGTK on` makes it fit in really well. It just works, although switching from article to article has been implemented via keyboard shortcuts only currently. While you can just use the virtual keyboard for that, it blocks quite a large area that could be used for displaying text. Still, I use it this way, because let's face it: Not every item of news is that interesting, and then there is always a feed or two that only deliver an excerpt anyway, forcing you into a browser to read the entirety of the article anyway.


#### Read it Later clients
Speaking of going through a ton of news items: Every now and then you will come across an article that is both interesting and too long to read in that situation. That's one of my uses of [Wallabag](https://wallabag.org/en), a self-hostable Read-it-later service (more popular, proprietary services include Instapaper and Pocket). Now, if you use Wallabag too, check out [Read It Later](https://gitlab.gnome.org/World/read-it-later/), an app written in Rust that is fully libhandy enabled and works perfectly, even though it just has the basic features (e.g. it lacks tags).
Read it Later is available on [Flathub](https://flathub.org/apps/details/com.belmoussaoui.ReadItLater) and in the [AUR](https://aur.archlinux.org/packages/?O=0&SeB=nd&K=read-it-later&outdated=&SB=n&SO=a&PP=50&do_Search=Go).


#### eBooks
I have to admit that I rarely read an ebook on my phone, but I recall doing that before I had a dedicated device. On the PinePhone with Phosh, Foliate as a modern GTK based ebook reader is the obvious choice, and guess what: After a `scale-to-fit com.github.johnfactotum.Foliate on` it even works quite well. Even if you don't have any ebooks yet, Foliate has currently three integrated catalogs that help you at obtaining (DRM free) ebooks. Most of these are in the public domain, but that does not make them less interesting, as I have found that having books that are lengthy and heavy if they were printed on paper actually are easier to read when you can take them with you to many places. 
And thinking of a future in which the PinePhone can serve as a daily driver, don't you picture yourselves standing in a cue, reading Tolstoi?
Foliates catalogue can be crashy, but the reader has been comparatively stable in my not too extensive testing and renders ebooks very well. 
Foliate is available in [Flathub](https://www.flathub.org/apps/details/com.github.johnfactotum.Foliate), in Arch Linux ARM and Fedora.


### Special: Email

Getting an Email client to run and scale properly was hard on the Phosh based distributions for a long time. [Geary](https://wiki.gnome.org/Apps/Geary), being the modern GNOME app it is, seemed to be the obvious choice for a long time. I eventually read that [Evolution](https://wiki.gnome.org/Apps/Evolution/) would work ok after adjusting it a bit, and that on postmarketOS, and given some adjustments it can be quite usable after a `scale-to-fit evolution on`, as long as you only use it with one mail account and don't have to many subfolders. You just have to disable all the sidebars and set the mail preview part to show up below the list of received mails. As Evolution supports GPG, you can use it for encrypted mail, too &mdash; I did not get around to trying this yet.
Evolution is packaged in every Phosh based distribution I tried so far &mdash; and it is available on [Flathub](https://flathub.org/apps/details/org.gnome.Evolution).

However, on the evening of writing this, I tried Mobian again, and guess what: Geary has been optimized and scales properly now. Why I could not setup a GMail account, a regular IMAP account of mine worked just fine. I am sure that other distributions will follow eventually.
Geary is packaged in every distribution with Phosh I tried, too. It also is available on [Flathub](https://flathub.org/apps/details/org.gnome.Geary).

### Screenshots

#### NewsFlash

##### NewsFlash: Wrong scaling
![NewsFlash: Wrong scaling](newsflash_wrong_scale.png)
##### NewsFlash: After scale-to-fit
![NewsFlash: After scale-to-fit](newsflash_scale-to-fit.png)
##### NewsFlash: Sign in
![NewsFlash: Sign in](newsflash_signin.png)
##### NewsFlash: List of posts
![NewsFlash: List of posts](newsflash_postlist.png)
##### NewsFlash: Post view (feed with excerpts only)
![NewsFlash: Post view (feed with excerpts only)](newsflash_post_excerpt.png)
##### NewsFlash: Post view (full feed)
![NewsFlash: Post view (full feed)](newsflash_post_full.png)

#### Read It Later

##### Read It Later: Start/sign up screen
![Read It Later: Start/sign up screen](readitlater_start_signup.png)
##### Read It Later: Loading...
![Read It Later: Loading...](readitlater_loading.png)
##### Read It Later: List of posts
![Read It Later: List of posts](readitlater_postlist.png)
##### Read It Later: Post view
![Read It Later: Post view](readitlater_post.png)
##### Read It Later: Post, Menu options
![Read It Later: Post, Menu options](readitlater_post_menu.png)
##### Read It Later: Setting screen
![Read It Later: Setting screen](readitlater_setting.png)
##### Read It Later: Dark Mode
![Read It Later: Dark Mode](readitlater_darkmode.png)

#### Foliate

##### Foliate: Start screen
![Foliate: Start screen](foliate_start.png)
##### Foliate: Catalogue
![Foliate: Catalogue](foliate_catalogue.png)
##### Foliate: Catalogue: Single Book View
![Foliate: Catalogue: Single Book View](foliate_catalogue_single_book.png)
##### Foliate: Reading Mode: Book Title page
![Foliate: Reading Mode: Book Title page](foliate_book_title.png)
##### Foliate: Reading Mode: Book Chapter
![Foliate: Reading Mode: Book Chapter](foliate_book_chapter1.png)

#### Geary

##### Geary: Add account
![Geary: Add account](geary_add_account.png)
##### Geary: List view
![Geary: List view](geary_mail_list.png)
##### Geary: Mail view
![Geary: Mail view](geary_mail.png)
#### Evolution
##### Evolution: View Settings 1
![Evolution: View Settings 1](evolution_setting1.png)
##### Evolution: View Settings 2
![Evolution: View Settings 2](evolution_setting2.png)
##### Evolution: Mail View
![Evolution: Mail View](evolution_mailview.png)
