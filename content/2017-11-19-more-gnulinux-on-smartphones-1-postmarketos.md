+++
title = "More GNU/Linux on Smartphones: 1. postmarketOS"
aliases = ["2017/11/19/more-gnulinux-on-smartphones-1-postmarketos.html"]
author = "Peter"
date = "2017-11-19T08:58:28Z"
layout = "post"
[taxonomies]
tags = ["alpine linux", "distributions", "linux", "mobile linux distributions", "postmarketOS"]
categories = ["article"]
+++

_While the Crowdfunder for the Librem 5 successfully finished, I want to shed
some light on possible alternatives to it for people that want to run
GNU/Linux on Smartphone hardware. First
up: [postmarketOS](https://postmarketos.org/)_ <!-- more -->

[postmarketOS](https://postmarketos.org/) (short pmOS) is a relatively new
project. A little more than two months ago the projects founder ollieparanoid
published a [blog post](https://postmarketos.org/blog/2017/09/03/100-days-of-
postmarketos/) titled: 100 days of postmarketOS, listing pretty amazing
accomplishments in these first 100 days.

**Target devices:**  
Differing from other attemps at getting a non-Android-Linux[^1] onto smartphones, pmOS aims to target multiple devices with small device specific
parts and a set of software that then is interchangeable between multiple
devices. It is based on [Alpine Linux](https://www.alpinelinux.org/), a
distrOnly recentlyibution designed for "power users who appreciate security,
simplicity and resource efficiency". Currently, [17 devices are
supported](https://wiki.postmarketos.org/wiki/Supported_devices) (not counting
QEMU targets) and in various states of support. The best supported devices
currently is the good old Nokia N900, as it pmOS can stand on the shoulders of
giants here with regard to mainline Linux support.

**Approach:**  
There is not one interface, due to a modular approach of pmOS and in order to
support devices that are less capable. Currently, [supported
interfaces](https://wiki.postmarketos.org/wiki/Category:Interface) (more a
"will be" than an "are" statement as far as I could gather) include _good old
Hildon_ (known from Maemo) and Plasma Mobile.

So, to boil it down: Device agnostic, UI agnostic, open and friendly.


[^1]: postmarketOS is technically not a GNU/Linux OS, as it is built around `musl` and `busybox`
