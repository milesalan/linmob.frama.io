+++
title = "Looking for a new laptop"
aliases = ["2009/10/21/looking-for-a-new-laptop"]
author = "peter"
date = "2009-10-21T12:27:00Z"
layout = "post"
[taxonomies]
tags = ["16:9", "Apple", "glossyness", "hardware design", "laptop",]
categories = ["hardware", "personal",]
+++
Though I found an ebay seller, that offers the SmartV5 MID, I am afraid I cannot grab one for an early review, as I think a new laptop is more urgent&mdash;my current one, a HP Compaq nx6325 is over three years old now and it was a low end offer back then (at least mine with its Sempron CPU).

Before I had a look at the notebook market, I made a list of what I would like to have. At first I want a non glossy non 16:9 display, preferably high resolution (being fed up with XGA), and would prefer a non glossy casing as well&mdash;I hate to look at fingerprints all the time. Besides this, before we talk about the insides, I would like a well built laptop, looking rather classic, something alike Thinkpads  or older MacBook Pros. The insides&mdash;dual core, preferably a recent chipset, graphic performance is nothing I really care about (I rather care about good loudspeakers as I am used to my HPs Altec Lansing Speakers), so integrated graphics would be fine, loads of ram and a big enough hard drive (200GB at least), and that machine should be able to last more than 3 hours on battery and not cost a fortune.

Well, turns out that the insides aren't the problem at all&mdash;e.g. there is a variant of the Lenovo Thinkpad SL500 that would fit my needs almost perfectly, though it is said to be less sturdy than the more expensive Thinkpad offers&mdash;but then, it turns out that it does not run Linux that well&mdash;the ACPI implementation appears to be rather bad and some other issues&mdash;as Linux is my primary OS it is a no go.

Then there are some Acer Travelmates, but actually I find them all ugly besides the Timeline series&mdash;and as I have to look at that device almost every single day for about 2 years, ugly devices are a no go. The Timeline ones are nice, but they don't contain a DVD drive besides the 15,6 inch one (I admit that I rarely need such a drive, but I need it often enough that no external drive would be annoying (especially in use at home, as it is one more thing you are supposed to have on or near to your desk))&mdash;which has something i want to avoid, as this one has a none centered keyboard due to additional number keys i never even use on desktop keyboards, then 1,4GHz dual core does not sound that fast (and my next notebook is about to last for years on my desk) and a 1366 x 768 pixel resolution at 15,6 inch is just disappointing.

Then there is the brand name which is printed on my current laptop, HP&mdash;I could of course go on using them, maybe again an AMD offering (though Intel is quite ahead).  Their new HP ProBook 6545b will be available in Germany soon, but.. the keyboard. Really, I don't want those number keys, rather a better sound system or something like that. Besides I would have to go with 1600x900 pixel resolution, which won't be that affordable.

After all, the offerings on the market are that disappointing, that I have started to think of buying a early 2008 MacBook Pro&mdash;I always loved their design, even back when there where Powerbooks&mdash;and now I am watching ebay every day. After all, they have an ok screen resolution, are non glossy and certainly well built. And running 3 operating systems would be nice, wouldn't it?
