+++
title = "Playing with Swmo"
date = "2021-10-05T21:53:18Z"
[taxonomies]
tags = ["PinePhone","Wayland","Sxmo","Swmo",]
categories = ["commentary", "software",]
[extra]
author = "Peter"
+++

Of all the UI's people use on their Linux Phones, Sxmo is one of the few that has its origins on the PinePhone. But it uses X.org, which like it or not, is legacy technology. Recently there's been work of bringing the Sxmo experience over to Wayland, and these are my notes after trying it for a few hours.

<!-- more -->

### What does Sxmo experience mean?

If we check out [Sxmo.org](https://sxmo.org), Sxmo's nice website, we learn that Sxmo stands for Simple X Mobile and is providing a "simple menu-driven interface", using [suckless](https://suckless.org/) tools. But what does that mean? Well, you get a tiling window manager and some of the tools that people use along side them. If you want to see how it developed, I suggest you to watch both of my [videos on Sxmo](https://linmob.net/videos#sxmo) in chronological order and then [the official introductory video](https://diode.zone/w/8MtkgWkkmyH946hwTGLAjE).

For me, Sxmo has been really hard to use at first &mdash; initially it just was an UI build on button presses, using the power, volume up/down keys of the PinePhone. It got better with gestures, but given that Sxmo ran everything at 1x scaling (-> tiny UI elements) and that I to this day struggle with casually putting Sxmo to sleep with a locked screen and waking it up again, I never stuck to it.

### How is Swmo different?

_Please keep in mind that Swmo has not been released officially, so parts of the following may be subject to change!_

Obviously, dwm and dmenu have been replaced with sway and bemenu. The virtual keyboard, [SVKBD](https://git.sr.ht/~mil/sxmo-svkbd), has been replaced with [wvkbd](https://git.sr.ht/~proycon/wvkbd). [simple terminal](https://st.suckless.org/ is replaced with [foot](https://codeberg.org/dnkl/foot). Now what does this mean? Well, mainly that there's no Xwayland at work at all times, so this is a nice and pure wayland experience, which is visually similar to current Sxmo &mdash; I might make a video demo.

### How to try Swmo out?

I installed postmarketOS Edge with Sxmo via pmbootstrap, and then `apk add git` and cloned the [sxmo-build](https://git.sr.ht/~mil/sxmo-build) repo and just ran `sxmo_build_all.sh --wayland`. A reboot and I was all of a sudden in a very similar looking, but different, Wayland based world.  

### How do I like it?

With Sway seemingly being set to 2x scaling, graphical apps look a lot better and work as they would on Plasma Mobile or Phosh. The keyboard is quite nice, the gestures are the same, and I think I could get used to them if I force myself for a long enough interval of time. Speed wise, it feels similarly fast. Memory usage appears to be a bit higher than on Sxmo, but it's still definitely lower than it is on with Phosh or Plasma Mobile. With two foot windows, a vis editor, htop and two tabs in Firefox open it was around 660 MB for me - that was connected to an external display though, which might contribute to RAM use here.

#### Convergence

With all the difficulties that have carried over from Sxmo in normal use[^1], this brings me to something Swmo excels at. While Sxmo was almost great at working connecting to a larger display, as it's tiling window manager almost would have been great at that, hampered by not having a mouse cursor[^2]. Sway-based Swmo does not share this issue. This is what [it looks like](https://fosstodon.org/@linmob/107049945578329447). Plug in your pointing device, and a mouse cursor will show. Also, this being Wayland, the external screen can be scaled at 1x while the phones' screen remains at 2x.For me, as a long time i3/sway user, this is a really excellent experience. To me, this is easily the best "use the PinePhone as a computer" experience I have had so far, better than Plasma Mobile and Phosh.[^3] Virtual workspaces and tiled windows are great for productivity, and having bemenu gives you a great app launcher on Super+D, leading to low resource, productive working environment once you have the most important shortcuts memorized. 

#### Future: The keyboard case thingy

Aside from connecting a "normal" keyboard to it, I am quite positive that Swmo will be a pleasant experience with PINE64's Keybaord Accessory, once that is going to be finally available, which is important, as many [phone optimized apps](https://linuxphoneapps.org) don't run too well in landscape per-se and Phosh's top and bottom bars make the letterbox even *letterboxier*.

### Conclusion

I will certainly keep this microSDXC install around and follow how Swmo develops, as it's just too good at what it is good at. Who knows, maybe I will get used to the bits that make my use of Swmo tedious now. Looking back at how it Sxmo started, the current state of both, Sxmo and Swmo represents huge progress in terms of usability. There's also quite some movement in the space of software that is at least close to the suckless philosophy, I recently stumbled upon [visurf](https://sr.ht/~sircmpwn/visurf/) and [Mepo](https://sr.ht/~mil/Mepo/), which are both something to look forward to.

[^1]: The me problem of me being unable to reliably putting the phone into a sleep state, with screen lock, as it breaks my over-a-decade habit of just pressing the power button for this.

[^2]: At least without further tinkering, out of the box. To be fair, I never investigated that issue more than just trying and finding out there was no mouse cursor.

[^3]: In fact, I've written this post in [Vis](https://sr.ht/~martanne/vis/) on Swmo on the PinePhone.