+++
title = "Reviving linmob!"
author = "peter"
date = "2011-01-23T12:24:00Z"
layout = "post"
[taxonomies]
tags = ["announcement", "blogging",]
categories = ["internal"]
+++
This place has been silent for about 10 months&mdash;but now it is being revived. It's not that I didn't blog about tech, I did (at my personal blog brimborium.net) but that didn't work out to well, I was really lazy over there.

A few days ago I suddenly remembered this old blog and started with repairing it's layout, which had been broken for quite a long time&mdash;and then decided that it would be fun to start &#8220;blogging&#8221; again, and as I've still been publishing little stupid stories at another place, I realized that the &#8220;blogging&#8221; I had in mind was tech-blogging. So linmob had to be revived, because this has been the place where I published quite a some tech related things&mdash;just for fun.

I am not yet sure how this will turn out, it's possible that I will stop writing here in a year or half a year&mdash;I will try to do a good job and thus follow an edititorial plan, which might kill the fun&mdash;but doing something without a concept just doesn't feel good to me.

First of all there will be some crossposts from brimborium&mdash;I call it filling the gap.

_More on the plans I have may follow later on._
