+++
title = "GNU/Linux on Tablets: Hardware"
date = "2021-06-26T14:30:18Z"
updated = "2021-06-27T11:00:00Z"
[taxonomies]
tags = ["PineTab", "Linux Tablets", "Ubuntu Touch", "JingOS", "JingPad", "postmarketOS", "Mobian", "SailfishOS",]
categories = ["commentary", "hardware",]
[extra]
author = "Peter"
update_note = "Added SailfishOS section, links to RasPad and CutiePi and the Community port of Ubuntu Touch to the Lenovo Tab M10 FHD Plus based on Twitter feedback I had not seen before publication."
+++

Since it's relaunch last year, this blog has been mostly focused on phones. Tablets, however, are a thing too, and of course GNU/Linux can be run on tablets (and I've been doing so myself in the past). This is the first post of a series that mainly tries to narrow down the criteria of potential hardware, which doesn’t go without touching software as the different distributions support different hardware. 

With tablets, we face many of the same problems that we have been having with phones: You can't just install Linux on any tablet, and, what about the software? 
<!-- more -->
## Current "Linux first" tablets

Aside from a variety of efforts that build tablets around [the Raspberry Pi single board computer](https://raspad.com) or (in a saner approach) [compute module](https://cutiepi.io/), there have been two Linux first tablets that drew some attention lately:

* The PINE64 PineTab, basically a PinePhone without cellular connectivity featuring a 10", 1280*800px screen, which is currently out of stock (due to component shortage induced price hikes), and
* the JingPad, a chinese tablet that's obviously inspired by the iPad Pro in software and hardware. Sadly, it's based on a SoC that features a [PowerVR GPU](https://en.wikipedia.org/wiki/PowerVR) and thus is highly unlikely to ever run a mainline kernel (and if it will, it will require a userspace blob for the GPU). Also, Jingling develop a (at least in its current beginnings) KDE technologies custom UI, which is open source, but follows a "let's throw that code over the wall when we have a release" known from Android.[^1] If you can live with these drawbacks and can afford to spend 599 USD, get one on [Indiegogo](https://www.indiegogo.com/projects/jingpad-world-s-first-consumer-level-linux-tablet#/)

Given that both of these Linux first tablets have their drawbacks, let's rather discuss post-market Linux installs on tablets.

## Installing (GNU/)Linux on Tablets on your own

With tablets becoming popular and widely available following Apples iPad, Android Honeycomb and Windows 8, there's a lot of used but still "good enough" hardware out there that you can hack to run Linux. 

But: This does not mean it's always easy to do so, as with ARM-based hardware you'll likely need a device specific kernel (and there's always hardware that might need specific patches).

So let's have a look at the hardware that's supported by established/larger Linux for touchscreens/mobile first distributions:

### Ubuntu Touch

Ubuntu Touch list six tablets as supported on [their devices page](https://devices.ubuntu-touch.io). These devices include the 
* bq Aquaris M10 F(HD), a device sold with Ubuntu Touch back when it was still governed by Canonical, 
* the Nexus 7 2013 (LTE), 
* the Sony Xperia Z4 Tablet (a Snapdragon powered 810 tablet, which seems to be still surprisingly expensive when bought used),
* and, as the one device running a non-vendor mainline kernel, the (currently unavailable) PINE64 PineTab.
You could of course also try [to port Ubuntu Touch to your tablet device](https://docs.ubports.com/en/latest/porting/), which is something people do:
E.g. there's a community port for the [Lenovo Tab M10 FHD Plus](https://gitlab.com/ubports/community-ports/android9/lenovo-tab-m10-fhd-plus).

So if Ubuntu Touch is your jam, these are devices that you might want to get. And if you think it is not, watch [this video](https://www.youtube.com/watch?v=k5Lfj51h_0M) that shows Ubuntu Touch on the Lenovo Tab M10 FHD Plus and tell me again that you don't think Ubuntu Touch is great on tablets. 

### JingOS

Even if you don’t buy a JingPad, [JingOS](https://en.jingos.com) can be run on x86 tablets. While the selection of officially supported devices is quite short with

* Surface Pro 6,
* Huawei MateBook 14 and
* eve V,

it’s definitely worth a try on your x86 based Tablet or convertible.

### SailfishOS

Remember the [Jolla Tablet](https://jolla-devices.com/jolla-tablet/), which almost sunk the Jolla boat? While that was the opposite of a success story, SailfishOS has been [ported by the community](https://gitlab.com/sailfishos-porters-ci) to a number of tablets. One of these porting efforts is [SailfishOS x86](https://sailfish-x86.yeheng.org/), which supports a limited number of devices, but should run on everything supported by Ubuntu 20.04.


### Mobian

[Mobian](https://www.mobian-project.org) support two tablets officially:
* the aforementioned PINE64 PineTab and
* the [Microsoft Surface Pro 3](https://wiki.mobian-project.org/doku.php?id=install-x86), a Intel Haswell-based tablet.

### postmarketOS

postmarketOS aim to provide an operating system that's portable a wide variety of devices. While the main focus of postmarketOS is on phones, their [Devices page](https://wiki.postmarketos.org/wiki/Devices) lists a few tablets. Two tablets are listed as "Community devices", meaning that they are reasonable well supported:
* the [PINE64 PineTab](https://wiki.postmarketos.org/wiki/PINE64_PineTab_(pine64-pinetab)) (again!),
* and the [ASUS MeMO Pad 7 (asus-me176c)](https://wiki.postmarketos.org/wiki/ASUS_MeMO_Pad_7_(asus-me176c)).

There are many more tablets in the "Testing" list, which may be of interest if you have such a tablet in a drawer somewhere or know someone who has.

## "Um, I have hardware, but it's not listed anywhere. What can I do?"

Given that this list only contained a very limited number of devices up until now, you may ask: I have a used tablet here, I want to put GNU/Linux on it, how can I do so?

### Let's presume you have a device with an Intel CPU
I know that feeling, so let me start by telling you about my experiences with Intel Atom BayTrail powered devices. Let's start with BayTrail and Atom. The Intel Atom line of processors was introduced by Intel to power the then popular "Netbooks", tiny, low powered, affordable notebooks. Netbooks made quite the splash, as computing devices in their rough size class (UMPCs or sub-notebooks) had previously been aimed at business customeers only and thus been prohibitively expensive.

Eventually, Netbooks faded when Tablets took over. Intel also tried to get their Atom CPUs into smartphones, which wasn't the success they had hoped for. All that said, what really matters with regard to this story, is that you need to check which kind of Atom CPU your old device has, and which kind of GPU that device features: One that's been designed by Intel and thus likely has good support in Linux, or a "PowerVR" one that's been designed by Imagination Technologies. Unless you really like to suffer, if it's the latter, I recommend you to just give up.

BayTrail CPUs, fortunately, feature an Intel-designed GPU. Sadly, this did not mean that running Linux on a BayTrail tablet is straight-forward and easy. At first, Intel [did not really bother to get decent BayTrail support](https://www.phoronix.com/scan.php?page=news_item&px=Intel-Linux-Bay-Trail-Fail) into Linux, which thankfully has since changed.

While that sounds easy, it does not mean it's straight forward. There are more issues to work around. And, surprisingly, as the postmarketOS wiki page for the [ASUS MeMO Pad 7](https://wiki.postmarketos.org/wiki/ASUS_MeMO_Pad_7_(asus-me176c)) with its work-around to flash a new, custom, unlocked bootloader exemplifies, it's often easier to get proper Linux running on devices shipped with Windows instead of Linux orginally, altough you may run into [that 32bit UEFI problem](https://rk.edu.pl/en/how-cool-lenovo-flex-10-netbook-and-how-install-linux-32-bit-uefi-system/#8).

I personally have had two Baytrail-powered devices, 

* an ASUS VivoTab 8 Note (M80TA), which originally shipped with Windows 8, but I since managed to get Ubuntu 18.04 installed on, before I handed it down to my brother,
* and a Tolino Tab 8, which is still stuck on Android 4.4 due to a locked bootloader.[^2]

What I am trying to say: Many Intel-(or AMD-)powered tablets can run Linux, but it's not as easy as installing Linux on your typical notebook or desktop. With x86 Android devices, locked bootloaders are an obstactle to be aware of.

### ARM-powered tablets and convertibles

With ARM, the situation is somewhat similar: Locked bootloaders are a mayor obstacle here, too. But that aside, the same problems that prevent us from installing GNU/Linux without fancy layers like Halium on smartphones: Very few ARM SoCs have decent enough mainline support. 

But, depending on what you expect from a tablet, and a tablet being a more casual device, it may be more acceptable to live with a few shortcomings: So if you mainly want to browse the web and read PDFs, unsupported speakers may be acceptable. 

So, if your ARM tablet has an unlockable bootloader, look out for small projects or blog posts where some has managed to get Linux running on it.

I really don't know how many tablets fall into this or the previous category. Hardware [shipped with Chrome OS](https://www.chromium.org/chromium-os/developer-information-for-chrome-os-devices)[^3] is definitely a likely candidate, as the coreboot-based Chrome OS bootloaders are easily unlocked, leading to a 'scare screen' at each boot, which often can be 'solved' by replacing default bootloader by another Coreboot based bootloader.

## Conclusion

While potentially every device can be coerced to run Linux, the reality of Linux on Tablets requires choosing hardware wisely, and often at least intermediate "Linux skills" to get going. So choose wisely, and don't expect things to be great out of the box. 

The software landscape, which was questionable years ago, has definitely improved thanks to mature projects like Ubuntu Touch (which sadly only supports a handful devices), community efforts using Phosh or Plasma Mobile and commercially-backed newcomers like JingOS &mdash; and let's not forget that both KDE and GNOME are perfectly viable (with a few tweaks maybe) on > 9" devices. _But more on that in a later blog post._


[^1]: This is problematic, as large commits without individual commit messages make code changes poorly documented, which make it hard for the upstream KDE projects to participate and upstream potential improvements added by the JingOS developers.

[^2]: By now I am so desperate that I am considering to just try the bootloader developed for the ASUS me176c. What can possibly go wrong? (A lot, but then the screen is already cracked!) Also, locked bootloaders (at least beyond the manufacturers willingness to supply updates) should be illegal.

[^3]: Look for "Chromeblet" and "Convertible". I have one of each with the Lenovo Duet and the ASUS Chromebook C101PA, which I will write about in detail in future posts.
