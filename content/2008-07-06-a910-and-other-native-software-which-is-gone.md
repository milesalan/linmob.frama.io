+++
title = "A910 and other native software which is gone"
aliases = ["2008/07/a910-and-other-native-software-which-is.html"]
date = "2008-07-06T09:48:00Z"
[taxonomies]
tags = ["A910", "business", "IM", "Motorola", "platforms", "skype"]
categories = ["projects", "software",]
[extra]
author = "Peter"
2021_commentary = "Sadly, nobody ever got in touch."
+++
Some days ago, I had to learn, that all these old news posts, which say that Motorola releases A910 as a smartphone weren't wrong. 
<!-- more -->

According to my source Motorola was developing some interesting applications for it, like Skype and a file-sharing tool (which made its way into RAZR² V8 according to my source). Unfortunately, all these apps (and firmwares, we'd really need an *.SHX flash, to repair all these broken A910s out there) seem to be gone. 

But it is funny to see that there weren't only plans. The plans were executed and after they had developed all this stuff, they decided that it was no more fitting their product strategy. This might be due to Motorola's multi-platform strategy (Linux/Java (EZX/JUIX/MotoMAGX), Symbian, Windows Mobile, Symphony/P2K), or just to the fact, that A910 didn't meet their quality requirements for a business phone (I could understand that, as there are phones that are made of better materials than A910 is).

I am just guessing here, but if there is one thing we don't have to guess: A lot things at Motorola went wrong these days.


__Added 7/8/2008:__
Of course there was additional software planned and developed, e.g. a instant messanger (just extract iconres.ezx and you'll find lots of icons related to that), which might be excluded (as the upper software) due to these entities which we call network operators - somehow operators are users' enemy in additional communications software (which they can't charge as much for as for as they do for SMS).

If there is anybody out there reading this, owning a sweet Martinique (A910) or E2 which has any of this software (if you know somebody who has that it'd be great, too) please be so kind and extract the relevant software codegroups using <a href="http://www.e2mod.com/content/view/78/28/">this tool</a> (this backup doesn't include any personal data). Or just contact me by writing a little comment. Thanks!
