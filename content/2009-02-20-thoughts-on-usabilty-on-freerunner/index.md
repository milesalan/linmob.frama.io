+++
title = "Thoughts on usabilty on FreeRunner"
aliases = ["2009/02/20/thoughts-on-usability-on-freerunner.html"]
date = "2009-02-20T15:06:00Z"
[taxonomies]
tags = ["Debian", "development", "free software", "FSO", "gta02", "Neo Freerunner", "paroli", "style", "toolkits", "usability",]
categories = ["software", "commentary"]
[extra]
author = "Peter"
+++
_Today I installed Debian onto my Neo FreeRunner, to be precise the relatively new, Debian based community distribution <a href="http://wiki.openmoko.org/wiki/Fyp">FYP</a>, which basically combines FSO/Zhone with LXDE. As it is a very new distribution, this post can't really critisize it, it's just about writing up some things I noticed and some ideas regarding usability on the Neo Freerunner&mdash;rather regardless of the distributions. FYP is just the drop of water inspiring to write this, the first two paragraphs will be Debian related, so you can probably skip them if you're in a hurry._

<!-- more -->
First of all, it is great to have an easy way to setup Debian on the FreeRunner, as I said before, Debian armel has great and vast repos, compared to most other FreeRunner distributions, which make it easy to try out many applications on the FreeRunner without having to compile them oneselves.

<img style="margin: 0pt 0pt 10px 10px;float: right;cursor: pointer;width: 210px;height: 280px" src="1.jpg" alt="Debian on FreeRunner" border="0" />

But you have to keep in mind, that Debian isn't focussed on mobile devices, so additional repositorys with software aimed at mobile devices are a necessity in my opinion. This is an observation I made while using <a href="http://linmob.blogspot.com/2008/04/htc-on-debian-with-x.html">Debian on the MDA Pro/HTC Universal</a> some time ago, and I didn't change my mind on that, examples I remember are the <a href="http://www.qoheleth.uklinux.net/blog/?p=137">embedded version of Abiword</a> or tools like xmonobut helping you with right clicks, which would be nice to have in such an additional repository, but which partially could lead to confusion in regular Debian.

But let's come to real usability issues, anyway. In terms of usability there are two kinds of distributions on FreeRunner, those who aim to be finger-friendly, and those that just want to have basic features finger friendly, but require a stylus for the rest - FYP is one of the latter. I think that both usage ways have their point, I like finger friendly efforts a lot, but I like to use software I know from my desktop as well (BTW: I would almost always use a stylus, as long it is a small and handy one - as I hate finger prints, wear,tear and dirt on the display - even think about getting a capacitive stylus for G1 - nontheless: finger friendly is stylus friendly too, as you don't have to be that exact), at least as long as their is no finger friendly alternative.

An usability issue, which is rather an aesthetic issue, I see on distributions like SHR is that they mix different toolkits, like ETK and GTK+ - the applications look different, and while Enlightenment related apps are close to be too beautiful in terms of performance, GTK+ apps often look dated or sometimes even bad at all. I think it was Midori on SHR unstable (I know, just a framework demo, not aiming to be a real distribution, but nontheless used by some as one), which looked really bad, using the nice old orange OM2007.2 icon theme, but a plain grey GTK+ theme (I am sorry that I didn't check, which one) - but the OM2007.2 GTK theme wouldn't have resulted in a much more appealing UI anyway.

In my opinion these optical issues should be fixed by an &#8220;optical initiative&#8221;, which rolls out a set of themes, which makes GTK+, ETK etc. apps look more alike, while being fast and not eye-candy in a way which messes up performance, as performance has to be a priority and anyway: I personally adore simplicity. This means: Don't use too power hungry GTK+ engines (e.g. I was really surprised to see FYP using Clearlooks&#8230; imho not a good idea) to match up with Enlightenment effects, rather remove some &#8220;bling&#8221; of Enlightenment.

<img style="margin: 0pt 0pt 10px 10px;float: right;cursor: pointer;width: 210px;height: 280px" src="2.jpg" alt="FreeRunner running OM" border="0" />

I believe the style &#8220;<a href="http://www.hackable1.org/wiki/Screenshots">hackable:1</a>&#8221; inherited from OM2007.2, the ASU look and the style of  new and upcoming <a href="http://www.paroli-project.org/">&#8220;Paroli&#8221;</a> are good examples of what I mean.

Additionally I would propose to implement a simple way to enable the user to switch between GTK-iconsets and themes (or rather two configurations of one theme) like a button among illumes applications (or even better as a panel plugin), as there are basically two kinds of applications using the GTK+-Toolkit on OpenMoko devices: Those written for the Neo FreeRunner and familiar embedded devices (OM2007.2, again) and those written for the desktop but being lightweight enough to run on embedded devices like the FR (btw, it would be nice to have a probably xmonobut like easy way to perform right mouseclicks, too) .

I strongly believe that this could lead to a better usability or even workflow ;-)  (and so to more popularity for the free platform of OpenMoko and the community) and would be happy if anybody would pick this ideas up soon - being pretty sure that I am not the only one worried about usability and aesthetics.
