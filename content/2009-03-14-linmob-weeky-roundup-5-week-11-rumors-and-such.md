+++
title = "linmob weeky roundup (5 / week 11): rumors and such"
aliases = ["2009/03/14/linmob-weekly-roundup-5-week-11-rumors-and-such.html"]
author = "peter"
date = "2009-03-14T22:47:00Z"
layout = "post"
[taxonomies]
tags = ["3D7K", "browser", "cupcake", "GTA03", "Palm", "palm pre", "T-Mobile G1", "VIA"]
categories = ["weekly update"]
+++

Another week has almost passed by, and as usual (looking at the last week) not that much has been happening&mdash;at least concerning really pocketable devices that run Linux.

First of all there have been <a href="http://www.engadgetmobile.com/2009/03/11/cupcake-android-update-coming-to-g1-in-april/">several</a> rumors about Android 1.5 coming to the G1 in april (at almost the same time the HTC Magic will find its ways to your favourite seller (as far as you live in Europe)&mdash;even if this would not happen official, it was very unlikely that there would be no Android 1.5 (Cupcake) for the G1 at all.<br />(In fact there are some builds to try out on various web pages, e.g. <a href="http://andblogs.net/images/snapshots/">there</a>&mdash;they lack the Google and closed source apps, but otherwise they aren´t bad at all.)

Over to Openmoko: No, still no GTA03/3D7K spyshots or something like that, but I strongly welcome Risto H. Kurppa's efforts to start a <a href="http://wiki.openmoko.org/wiki/Browser_review">browser comparison on the wiki</a> which he announced in the <a href="http://lists.openmoko.org/pipermail/community/2009-March/043895.html">community mailing list</a>&mdash;browsers are one of the real weak points of the Openmoko platform right now&mdash;besides the browser on Android and possibly the browser of Qt Extended 4.4.x I haven't tried yet (nor seen it on video), there is no satisfying solution (<a href="http://scap.linuxtogo.org/files/e4340718ef28604518012670269d0063.png">I have seen a promising screenshot on scap.linuxtogo.org some time ago</a>, but nobody seems to know what it shows). And now, that telephony is at least close to be called reliable, it does not sound to absurd to work on that issue.

Well, and as far as I didn´t miss too much, that´s all what I would consider interesting, as long as you are not interested in things like special Android offers or known devices appearing in various contrys under different brandings.

Ok, there is another <a href="http://jkontherun.com/2009/03/12/palm-pre-webcast-tidbit-browser-nearly-4x-faster/">Palm Pre webcast</a>: The browser appears to be amazingly fast...

And then for those interested in netbooks or MIDs/UMPCs: <a href="http://www.linuxdevices.com/news/NS4078675371.html">VIA announced a new HD capable chipset, the VX855</a>, which should be a fine companion to C7-M/Nano, eating even less Watts. But as there is no information on pricing and availability, this appears to be a Q4 thing&mdash;at best.

Oh, and I nearly forgot the best: OLPC XO2 news: It might be ARM based, which wasn´t unlikely as there is the strong aim to have XO2 to draw even less power than XO and AMD announced to discontinue their Geode processors.<br />There might be a certain point that makes you bang your head against the next wall, but nontheless I will link the <a href="http://www.pcworld.com/article/161112/olpc_set_to_dump_x86_for_arm_chips_in_xo2.html">PCWorld article</a>... Enjoy it ;)

_Hope to see you again here soon ... ;):_
