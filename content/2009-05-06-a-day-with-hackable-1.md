+++
title = "A day with hackable:1"
aliases = ["2009/05/06/a-day-with-hackable-1.html"]
author = "peter"
date = "2009-05-06T21:14:00Z"
layout = "post"
[taxonomies]
tags = ["bearstech", "Debian", "distributions", "hackable:1", "openmoko", "missing video",]
categories = ["software", "commentary"]
+++
I've been quite silent recently, due to being fed up with all this electronics and lots of other things to do (job + studies).

So, after a while, my FreeRunner's battery was pretty discharged, when I wanted to use it again, as I was planning to try out rev4 of hackable:1 and OM2009 testing. Though this is about hackable:1, let me write one sentence regarding OM2009: It's boring. Phone and nothing else, no calendar, nothing&mdash;not even a launcher for other applications (you can use it with illume, btw, but you will have to edit some configuration files).

And now, let's go on to hackable:1, which is some kind of a mixture of Debian and OM2007.2&mdash;&#8220;a Gnome Mobile initiative&#8221;. After partially dissapointing experience with rev3 (or was it a testing version?), which had a really unusable matchbox-keyboard (nearly impossible to get 1 letter and not 3 per press) but wasn't that bad besides this, good enough to give it another try.
Now, there is a new version out, which I had to test after having had a look at it on YouTube.

__Help needed to find this video__

The keyboard has become better, though it doesn't match Android 1.5 (&#8220;Cupcake&#8221;) keyboard, and you might prefer illume's keyboards <a href="http://scap.linuxtogo.org/files/a0b7812b479d5cb7c9732db9e84a0a2c.png">(exspecially the one I saw on scap, big buttons and unpredictive</a>), even thumb typing in landscape mode (autorotate, yeah!) wasn't horrible. The idea to start the keyboard via AUX is simple and you get used to it within minutes&mdash;not perfect, but a good solution. What I really liked as well is the new browser (woosh!), which will hopefully get somemore features&mdash;but it is a good start, it is fast and simple&mdash;if they manage to keep it that fast while adding some features (tabs, kinetic scrolling, zoom buttons) it would be very nice to use as a basic browser&mdash;I believe that it's important for the speed to be able to disable Javascript (and maybe SSL in the future) to keep it that speedy.

Now let's get to the things, that should become better: I am a fan of setting applications that give you the opportunity to change any setting you want to change&mdash;the way you can toggle (press AUX long) phone, WiFi, GPS and so on is great (USB &amp; autorotate toggle need a status message and I was unable to find a button for immidiate suspend) is great, but it would be nice to have a full featured settings app within the applications to set up GPRS and all the stuff you can imagine. Then hackable:1 needs an easy WiFi network chooser&mdash;WiFi-radar is included but lacks a shortcut, so that you have to start it using the terminal (still better than nothing). And the terminal.. Why on earth didn't they include vala-terminal, which uses GTK as well, but has nice features? 
Then there is abiword included, what I like a lot&mdash;if it wasn't the stock abiword, it would be much nicer to have the <a href="http://tthef.net/blog/?p=137">&#8220;embedded&#8221; version</a> of it (as it was more usable, exspecially in landscape mode with keyboard..). 

_Conclusion_

Anyway, hackable:1 is on a good way&mdash;I can't tell how stable the phone is (hackable:1 isn't FSO based) yet, and there are some flaws, but thinking of the whole concept (homescreen...) it is certainly one of the better (or even best) distributions for the Openmoko FreeRunner.
