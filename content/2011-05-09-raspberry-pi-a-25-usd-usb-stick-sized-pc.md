+++
title = "Raspberry Pi: A 25 USD USB Stick sized PC"
aliases = ["2011/05/09/raspberry-pi-a-25-usb-usb-stick-sized-pc.html"]
author = "peter"
date = "2011-05-09T14:19:00Z"
layout = "post"
[taxonomies]
tags = ["ARM11", "education", "Raspberry Pi", "ubuntu"]
categories = ["hardware"]
+++
When I stumbled upon <a href="http://www.raspberrypi.org/">this</a> three days ago, I felt like I absolutely had to blog this. Let's have a look at the provisional specs first:

* 700MHz ARM11
* 128MB of SDRAM
* OpenGL ES 2.0
* 1080p30&#160;H.264 high-profile decoder
* Composite and HDMI video output
* USB 2.0
* SD/MMC/SDIO memory card slot
* General-purpose I/O
* Open software (Ubuntu, Iceweasel, KOffice, Python)

Let's talk about the SoC first. While a 700MHz ARM11 SoC isn't what you'd call fast, it's good enough for basic tasks. I think that a likely SoC for this thing is the Telechips TCC8902, which is a popular SoC for low cost tablets and supports 1080p thanks to an included ARM Mali 200 GPU&mdash;it would fit. But then again, this isn't a finalized device yet and there are tons of ARM-based SoCs out there, and as ARM11 ones aren't exactly high end, many of them remain pretty much unknown.

As I said: The SoC is ok. What's not convincing is the amount of RAM; 128MB is barely enough for todays smartphone OS like Android&mdash;Ubuntu won't be much fun, you will be forced into using swap which will certainly cause some wear out on your memory card and slow the overall system down.

Nontheless, after <a href="http://www.youtube.com/watch?v=pQ7N4rycsy4&amp;feature=player_embedded">having listened to the interview with the man behind this foundation </a>(Raspberry Pi foundation), David Braben, which is not yet founded afaik&mdash;this seems to be a nice idea, as the relative scarceness of memory is even great for the purpose it's meant for: It's about eduction, educating kids about computing.
