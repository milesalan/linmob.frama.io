+++
title = "The N9 exceeds in everything it sets out to"
aliases = ["2011/10/23/the-n9-exceeds-in-everything-it-sets-out-to.html"]
author = "peter"
date = "2011-10-23T11:52:00Z"
layout = "post"
[taxonomies]
tags = ["n9", "N900", "nokia n9", "Nokia N900", "peakmobility", "Quote", "quote", "review"]
categories = ["shortform"]
+++
<blockquote>The N9 exceeds in everything it sets out to accomplish and even beats the competition in their own game, so you can&#8217;t really say its a bad device. What&#8217;s important to realize here is, that the N9 is in no way a successor of the N900, even if its initial efforts might&#8217;ve been to that direction.</blockquote>

<div class='attribution'><p><a href="http://twitter.com/#!/creip">Chris</a> for Peak Mobility in <a href="http://peakmob.blogspot.com/2011/10/from-n900-to-n9-review.html">his review of the N9</a>.</p>

<p>This review is an especially interesting one because it delivers a comparison to the N900, as the reviewer is a N900 enthusiast.</p></div>
