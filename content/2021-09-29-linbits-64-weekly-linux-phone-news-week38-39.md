+++
title = "LinBits 64: Weekly Linux Phone news / media roundup (week 38/39)"
date = "2021-09-29T21:35:00Z"
draft = false
[taxonomies]
tags = ["PinePhone","LINMOBapps","LinuxPhoneApps","Android","Reddit","Plasma Mobile","Phosh","Ubuntu Touch","Sailfish OS","Maemo Leste","postmarketOS"]
categories = ["weekly update"]
[extra]
author = "Peter"
+++

_This week in mobile Linux:_ Android kernels adopting a more mainline approach, Waydroid running on SailfishOS and Ubuntu Touch, libcamera, how to cross compile for Plasma Mobile (this time with a tool that should help a lot), a GTK app to browse LINMOBapps and more! <!-- more --> _Commentary in italics._ 

### Software releases
* [Manjaro Phosh beta 16 for the PinePhone](https://twitter.com/ManjaroLinux/status/1443263810589102080) has been released, delivering a few things early: MMS support, splash screens for Phosh &mdash; features that at least partly have not even been merged into their upstream projects. _While it's nice to be have these things early, please don't bug upstream with bug reports concerning these unmerged features._
* Gtkeddit is now [Headlines](https://gitlab.com/caveman250/Headlines)!

### Worth noting
* [LINMOBappsBrowser](https://github.com/Speiburger/LINMOBappsBrowser) is a new project that's building a more performant, native GTK way to browse apps.csv on your Linux Phone! _It's early days, but I love what Speiburger has built so far!_
* Maemo Leste have made progress [around email](https://twitter.com/maemoleste/status/1443180939144740868)!
* Popcorn Computers (who are making the [Pocket P.C. hacker terminal](https://pocket.popcorncomputer.com/) are  [collaborating with postmarketOS](https://twitter.com/tuxphones/status/1443270015441113091).
* Waydroid of the week: [Sailfish OS on PinePhone now with networking](https://twitter.com/adampigg/status/1442379954386255873#m).


### Worth Reading
#### Hardware
* Liliputing: [GPD Pocket 3 handheld computer will be a convertible tablet with stylus support (and a modular feature)](https://liliputing.com/2021/09/gpd-pocket-3-handheld-computer-will-be-a-convertible-tablet-with-stylus-support-and-a-modular-feature.html). _Nice hardware!_
* TuxPhones: [Cadmium is a Linux distribution to liberate ARM Chromebooks](https://tuxphones.com/cadmium-linux-for-chromebooks/). _Cadmium is seriously nice, only kernel upgrades aren't fun (yet)!_

#### Software
##### Software progress
* Claudio Cambra: [Turbocharging Kalendar — Kalendar devlog 15](https://claudiocambra.com/2021/09/26/turbocharging-kalendar-kalendar-devlog-15/). 
* Alberto Mardegan: [MiTubo 0.3 brings basic RSS support](http://mardy.it/blog/2021/09/mitubo-03-brings-basic-rss-support.html). _MiTubo is not just for Ubuntu Touch, hint hint._
* Phoronix: [Libcamera Maturing Well As Open-Source Camera Stack](https://www.phoronix.com/scan.php?page=news_item&px=libcamera-2021). _I look forward to libcamera support landing for the Librem 5!_
* Jolla Blog: [What’s up with Sandboxing?](https://blog.jolla.com/whats-up-with-sandboxing/). _These plans sound promising!_
* UBports Blog: [Ubuntu Touch OTA-19 Release](https://ubports.com/blog/ubports-news-1/post/ubuntu-touch-ota-19-release-3779). _I missed this last week, do read it!_
* This Week in GNOME: [#11 Forty-one!](https://thisweek.gnome.org/posts/2021/09/twig-11/). _Some nice, new apps!_

##### Mainline all the things
* Phoronix: [Google Finally Shifting To "Upstream First" Linux Kernel Approach For Android Features](https://www.phoronix.com/scan.php?page=news_item&px=Android-Linux-Upstream-First). _Now this is going to take a while to really make an impact, but I am glad to see this happen anyway. Thanks to PizzaLovingNerd for making sure that I would not miss this!_
* TuxPhones: [The "10 Year Smartphone" initiative aims for a sustainable mobile ecosystem](https://tuxphones.com/10-year-smartphone-initiative-right-to-repair-lifecycle/). _This feels really important, although the idea [is not exactly a new one](https://ollieparanoid.github.io/post/postmarketOS/). To be fair: This is a different, political approach._

##### Distro future
* Christian F.K. Schaller: [Fedora Workstation: Our Vision for Linux Desktop](https://blogs.gnome.org/uraeus/2021/09/24/fedora-workstation-our-vision-for-linux-desktop/). _Now this is not about phones per se, but what's attractive about Silverblue or Kinoite would be great for phones, too._

##### Development news
* Gunibert: [GNOME Builder 41 Highlights](https://www.gunibert.de/posts/gnome_builder_41/). _Look at this!_

##### Cross Compiling 
* Han Young: [Cross Compile to PinePhone Part Three](https://www.hanyoung.uk/blog/cross-compile-to-pinephone-part-three/). _Awesome!_

### Worth Watching

#### Phosh
* Geotechland: [Manjaro Phosh - Pinephone BETA 15](https://tilvids.com/w/613e1cc2-fd9d-477a-b0a0-ebfae26668d4). _Great video! Sadly I doubt too much of an app startup time improvement is going to happen for big apps like Firefox. But, with e.g. with the [splash screen](https://gitlab.gnome.org/World/Phosh/phosh/-/merge_requests/886) feature, perceived app load time is going to be reduced. It's landed in Manjaro beta 16, BTW._

#### Plasma Mobile 
* Nat Tuck: [A Look at Plasma Mobile on the PinePhone](https://odysee.com/@NatTuck:9/nat-quick-look-pinephone-plasma:7). _Great in depth video review!_

#### GNOME
* mzeinali: [PostmarketOS with Gnome desktop on Pine phone](https://odysee.com/@mzeinali:c/PinePhone:a). _Seems to work well!_

#### Fun projects
* Martijn Braam: [Running Octoprint on the PinePhone with camera](https://spacepub.space/w/xpkUVzdVBvGLka8yfZg2k5). _If the PinePhone does not work for you as a phone (or you have to many PinePhones), and you like 3D printing, here's a use case for you!_

#### Tutorials
* (RTP) Privacy & Tech Tips: [SSH Passwordless Logins & Avoid SSH MITM Attacks](https://www.youtube.com/watch?v=CZ8BjLjl7EA). _Great howto by RTP!_

#### Ubuntu Touch corner
* UBports: [Ubuntu Touch Q&A 109](https://www.youtube.com/watch?v=nca_5_k4cH8). _Dalton is back and joined by Alfred and Marius to discuss OTA 19, current challenges around Miracast and Miroil. Also: A new stable release for the PinePhone is going to land this week, yay!_
* anino207: [Early Preview of Waydroid on Ubuntu Touch (Pixel 3a)](https://www.youtube.com/watch?v=dyOgI8PRt9w). _Waydroid is just soo smooth, even for gaming!_
* Wolf Fur Programming: [Ubuntu Touch Oneplus 6](https://www.youtube.com/watch?v=jFvwAaMC17U). 

#### Sailfish OS
* Adam Pigg: [Pinephone /home encryption using SailfishOS](https://www.youtube.com/watch?v=CZ8BjLjl7EA). _I missed this last week, but it's just to nice!_

#### Shorts
* 5ilver: [Pinephone mms via matrix](https://imgur.com/a/FhAdJby). _If you want to know how to do this, ask on [Reddit](https://www.reddit.com/r/PinePhoneOfficial/comments/pxoou1/video_of_mms_images_working_on_mobian_via_matrix/). I guess that this uses [MMMPuppet](https://gitlab.com/untidylamp/mmmpuppet), but I am not sure._
* Sailfish official: [xz2 sailfish os 4.2.0.21](https://www.youtube.com/watch?v=B5aGZPo-AuE). _Don't believe that channel name!_

### Stuff I did

#### LINMOBapps

This week I've been tackling the ["overhaul" milestone](https://framagit.org/linmobapps/linmobapps.frama.io/-/milestones/1), a bit more, 4 of 7 issues could be closed.
Also, a few apps were added, and more are to follow shortly:
* [Run Free!](https://gitlab.com/greenbeast/run-free), a running log app built for Phosh with some basic analysis of your running schedule, submitted by its creator &mdash; thank you very much again, Hank! 
* [Plano](https://github.com/DiegoIvanME/plano-rewritten), a cartesian plane calculator that uses GTK4 and libadwaita
* [GNOME Software](https://gitlab.gnome.org/GNOME/gnome-software),
* [GNOME Calculator](https://gitlab.gnome.org/GNOME/gnome-calculator) and
* [MiTubo](https://gitlab.com/mardy/mitubo), a native (QML) video player for YouTube videos.

Because of a few apps that were archived the week before (don't worry, they are going to show up on LinuxPhoneApps too), the total app count of apps.csv is now at 314.

[Read here what (else) happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master) on LINMOBapps this week. And please [do contribute!](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/CONTRIBUTING.md)

#### Linux Phone Apps

Not much happened here, aside from thinking about how to implement a few things. I am hoping to make some progress with this project on the coming saturday.
