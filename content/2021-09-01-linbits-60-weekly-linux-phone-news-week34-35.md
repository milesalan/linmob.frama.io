+++
title = "LinBits 60: Weekly Linux Phone news / media roundup (week 34/35)"
date = "2021-09-01T21:32:00Z"
draft = false
[taxonomies]
tags = ["PinePhone","LINMOBapps","Giara","Plasma Mobile","Manjaro","pineroot","Phosh","Ubuntu Touch","Sailfish OS","Nitrokey",]
categories = ["weekly update"]
[extra]
author = "Peter"
+++

_It's Wednesday again! Now what happened since last Wednesday?_

Phosh 0.13.1, Plasma Mobile Gear 21.08, Sailfish OS 4.2.0, a new RC of Ubuntu Touch for PinePhone and more!<!-- more --> _Commentary in italics._ 

### Software releases
* [Phosh 0.13.1 has been released](https://social.librem.one/@agx/106850175569486154), delivering a very welcome new feature: A button to close all notification at once. There's more though, so make sure to read the [release notes](https://gitlab.gnome.org/World/Phosh/phosh/-/tags/v0.13.1).
* [Sailfish OS 4.2.0 "Verla](https://forum.sailfishos.org/t/release-notes-verla-4-2-0/7092) has been released to Early Access Subscribers. _Plenty of little fixes and improvements, but that's always welcome, too, right? Also: Do watch Leszek's video linked below._
* Plasma Mobile Gear 21.08 was released, [changelogs are available](https://www.plasma-mobile.org/info/plasma-mobile-gear-21-08/).
* [Manjaro have released another Plasma Mobile beta release (No. 6).](https://forum.manjaro.org/t/manjaro-arm-beta6-with-plasma-mobile-pinephone/80241). _Nice!_
* [pineroot, the buildroot environment for PinePhone, is now featuring the freshly released Linux kernel 5.14.](https://twitter.com/pineroot1/status/1432682637504753666)

### Worth noting
* Nitrokey, who make Authentication hardware, but also sell secure laptops and a Nextcloud box, [have announced that they are working on a Phone](https://social.nitrokey.com/@nitrokey/106840138400937766). _Exciting! Now we don't know what this will be – after all it could be just some secure Android thingy –, but I am definitely excitedly awaiting their full product announcement!_
* [The Mobian Wiki has some new info that might be handy for anyone looking to use Bitwarden on a Linux phone](https://wiki.mobian-project.org/doku.php?id=bitwarden#how-to-install).

### Worth reading
#### Software progress 
* Claudio Cambria: [Checking events and ticking todos on your Pinephone — Kalendar devlog 12](https://claudiocambra.com/2021/08/28/checking-events-and-ticking-todos-on-your-pinephone-kalendar-devlog-12/). _Awesome!_
* Janet Blackquill: [A Hoppy Week In Tok](https://blog.blackquill.cc/a-hoppy-week-in-tok?utm_source=atom_feed). _It's getting better and better!_
* Plasma Mobile: [Plasma Mobile Gear 21.08 is Out](https://www.plasma-mobile.org/2021/08/31/plasma-mobile-gear-21-08/).  _Nice roundup! For even more details [read the changelog](https://www.plasma-mobile.org/info/plasma-mobile-gear-21-08/)._

#### Software progress reporting
* PineGuild: [Manjaro developers announced the release of the sixth beta of the Manjaro ARM with Plasma Mobile.](https://pineguild.com/manjaro-arm-beta6-with-plasma-mobile/)
* Linux Smartphones: [Phosh v0.13.1 released (and you can finally dismiss all notifications at once)](https://linuxsmartphones.com/phosh-v0-13-1-released-and-you-can-finally-dismiss-all-notifications-at-once/). _Finally indeed._
* UbuntuHandbook: [Giara 1.0 Released – Reddit App for Linux Desktop & Smart Phone](https://ubuntuhandbook.org/index.php/2021/08/giara-1-0-released-reddit-app-for-linux/). _I had been using a pre-release build of GTK4/libadwaita-based Giara for a while and it's quite the improvement! Great job!_

#### Software development
* Juan Pablo: [Milonga in Flathub!](https://blogs.gnome.org/xjuan/2021/08/25/milonga-in-flathub/) _Cambalache is a new tool for the creation of user interfaces for Gtk and the GNOME desktop environment._

#### Software Catalogues
* Sophie's Blog: [“apps.gnome.org” is Online!](https://blogs.gnome.org/sophieh/2021/08/26/apps-gnome-org-is-online/) _I wish I could bring up LinuxPhoneApps this quick!_

#### Good business
* Linux Smartphones: [After ten years, Sailfish OS maker Jolla is finally profitable](https://linuxsmartphones.com/after-ten-years-sailfish-os-maker-jolla-is-finally-profitable/). _Great to see Jolla doing okay!_

#### Competition
* FOSSphones: [Linux Phone News - August 31, 2021](https://fossphones.com/0831-news.html). _Nice write-up!_

#### Linux handhelds
* Liliputing: [MutantC v4 is a DIY, hackable handheld PC powered by a Raspberry Pi](https://liliputing.com/2021/08/mutantc-v4-is-a-diy-hackable-handheld-pc-powered-by-a-raspberry-pi.html). 


### Worth listening
* postmarketOS Podcast: [#8 INTERVIEW: Caleb Connolly (of OnePlus 6 / SDM845 Mainlining Fame)](https://cast.postmarketos.org/episode/08-Interview-Caleb-Connolly-SDM-845-Mainlining/). _Great episode! I hope to play with a OnePlus 6 soon._


### Worth Watching

#### Good Unboxing
* Nat Tuck:  [PinePhone Unboxing, Comparison, First Boot](https://www.youtube.com/watch?v=U-YN5UbzNHk) _This is much more than just an unboxing, but also a comparison with the Librem 5. Make sure to read the [related blog post](https://nattuck.fogcloud.org/2021/08/29/pinephone-unboxing/)._

#### Kalendar
* Claudio Cambria: [Kalendar alpha running on Pinephone (devlog 12)](https://tube.kockatoo.org/w/0328c630-82e4-4feb-85d6-a16ac14c2cb6).

#### Shorts
* NOT A FBI Honey pot: [how pinephone user's flex.....](https://www.youtube.com/watch?v=ce09rFzukxI). 
* NOT A FBI Honey pot: [WHEN someordinarygamers{mutahar} finds out the pinephone can manage Virtual machines.....](https://www.youtube.com/watch?v=bWCFEg3o3gU).

#### Development workflow
* king of hacks roise: [Pinephone development workflow](https://www.youtube.com/watch?v=zxCcAasXalI). _Nice vid! Will definitely watch it again if I ever find the time to do more than just minor busywork! BTW: [The app being worked on is this one](https://github.com/Jeremiah-Roise/PineCast) (LINMOBapps MR/LinuxPhoneApps PR to add it would be super welcome! )._ 

#### Ubuntu Touch 
* UBports: [Ubuntu Touch Q&A 107](https://www.youtube.com/watch?v=R-HanW1NIrA). _Alfred, Dalton, Florian and Marius talk about new apps, keyboard news, a new PinePhone release canddidate image that needs testing, OTA 19 work and more!_

#### Sailfish OS
* Leszek Lesner: [SailfishOS 4.2 - Whats new!?](https://www.youtube.com/watch?v=w2VAha1i2cg). _Great overview!_

#### GPL enforcement fun
* Naomi 'SexyCyborg' Wu: [Getting GPLv2 Compliance From A Chinese Company- In Person!](https://www.youtube.com/watch?v=Vj04MKykmnQ). _This is the full video teased last week._

### Stuff I did

Nothing. They call it a vacation. You're basically not allowed to do anything you normally do. It's crazy!

#### LINMOBapps

[Read here what (else) happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master), as I did some maintenance on other app listings. As with LinuxPhoneApps.org, progress with LINMOBapps will likely be even slower in the coming week, but I will happily accept Merge Requests! So please [do contribute!](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/CONTRIBUTING.md)
