+++
title = "GTA04: Speed up your FreeRunner / Neo1973"
aliases = ["2011/01/gta04-speed-up-your-freerunner-neo1973.html"]
date = "2011-01-25T23:43:00Z"
[taxonomies]
tags = ["beagle board", "development", "Golden Delicious Computers", "gta04", "omap3", "omap3530", "openess", "openmoko", "openness", "option gtm601w"]
categories = ["hardware"]
[extra]
author = "Peter"
+++

After having decided to reactivate linmob (rebranded to LINMOB) I asked myself what would be a good first article, and there could be only one answer: Write about something that isn't sprayed on the walls of every other tech blog / tech news site, and it would be great if that would fit to past linmob articles.
<!-- more --> 
<div style="float:right;margin:10px 0 10px 10px;"><img src="gta04.gif" /><br /><small><center>GTA04 (Image by Golden Delicious Computer)</center></small></div> 
Luckily I then remembered the GTA04 project (<a href="http://www.gta04.org/">gta04.org</a>) which is building replacement boards for Openmoko's FreeRunner / Neo1973 phones. The company behind this thing is well known in the german Openmoko community&mdash;Golden Delicious Computers / handheld-linux.com&mdash;they were not only official Openmoko distributor, but they later offered Buzz- and Bass-fixes.

So, what will we find on these boards?

The main two chips may not be top notch in 2011 (they were top notch in 2009) but definitely a lot better than the dusty chips in the ARMv4/ARM9 GPRS Freerunner:

* <a href="http://focus.ti.com/dsp/docs/dspcontent.tsp?contentId=53403&amp;DCMP=OMAP_Feb27_2008&amp;HQS=Other+OT+omap35x">TI OMAP3&#160;3530</a> 600MHz/720MHz ARMv7 / Cortex A8 SoC (almost similar to the OMAP 3430 found in the Palm Pre(Plus), Motorola Droid / Milestone, Nokia N900) with integrated PowerVR SGX graphics* <a href="http://mobile.engadget.com/2011/01/05/options-refreshed-gtm601-gtm609-module-is-the-smallest-with-v/">Option GTM601W UMTS module</a>

In addition to that you'll find a fascinating set of features, which is on par with todays solutions:

* Wi2Wi WLAN/Bluetooth module + antenna
* GPS module + antenna switch
* LIS302 (accelerometer)
* LSM303 (compass and accelerometer)
* ITG3200 (gyroscope)
* BMP085 (barometric altimeter)
* Si4721 (FM transceiver)

What remains yet unknown is the amount of RAM and Flash memory&mdash;I hope it will be were it is supposed to be in 2011&mdash;it should be 512MB Ram and at least the same amount of ROM/Flash memory. It should be possible to add in a 1.3 MPixel camera.

What remains is the same 2.8 inch 285dpi VGA screen with resistive touchscreen and the same case&mdash;to things that make this upgrade board a lot less interesting for the average user.

But let's rather talk about the advantages, which is openess. In times where companies like Motorola or Sony Ericsson lock down the bootloaders of their smartphones which makes hacking them (even though they run Android and thus a Linux kernel (looking way back at Motorola EZX/MAGX phones you understand that this is not an Android issue&mdash;a Linux kernel does not imply that your phone is hackable), a phone with an open bootloader, embraced by the Open Source community can be a strong solution.

By the way: The GTA04 root is the <a href="http://linmob.blogspot.com/2010/02/replacement-board-for-openmoko.html">Openmoko Beagle Hybrid</a> and there are plans for even more advanced devices&mdash;of course development costs money&mdash;I doubt this will go on without a certain amount of sales.

__For further information__ head over to the already mentioned <a href="http://gta04.org/">gta04.org</a> website, to the <a href="http://wiki.openmoko.org/wiki/GTA04">GTA04 article of the Openmoko.org wiki</a>, to <a href="http://www.handheld-linux.com/wiki.php?page=GTA04-Early-Adopter">handheld-linux.com</a> or check out the <a href="http://lists.openmoko.org/mailman/listinfo/community">Openmoko Community Mailinglist</a>. If you speak german (or can live with Google translation) make sure to have a look at <a href="http://freeyourphone.de/">freeyourphone.de</a>.
