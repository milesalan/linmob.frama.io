+++
title = "LinBits 62: Weekly Linux Phone news / media roundup (week 36/37)"
date = "2021-09-15T21:07:00Z"
updated = "2021-09-16T07:05:00Z"
draft = false
[taxonomies]
tags = ["PinePhone","LINMOBapps","LinuxPhoneApps",]
categories = ["weekly update"]
[extra]
author = "Peter"
update_note = "Added Link to Waydroid being available in Manjaro."
+++

_It's Wednesday again! Now what happened since last Wednesday?_

Megapixels with more contrast, easy installable Waydroid on DanctNIX and postmarketOS, Librem 5 pricing increases and more!<!-- more --> _Commentary in italics._ 

### Software releases
* [Megapixels 1.3.0 has been released](https://invent.kde.org/tfella/keepassk/-/raw/master/org.kde.keepassk.appdata.xml), bringing changes to the processing leading to different colors and more contrast. _I personally like the change a lot and think the resulting images look better and closer to reality, but I have also seen people not liking it._
* [Clapper 0.4.0](https://github.com/Rafostar/clapper/releases/tag/0.4.0) has been released. It now uses libadwaita and has new adaptive settings menu among many other improvements.
* [Danct12's Arch Linux ARM distro has seen a new release](https://github.com/dreemurrs-embedded/Pine64-Arch/releases/tag/20210908), bringing it up to date. _Since then, the megi's 5.14 kernel has landed and there also have been updates to uboot which you should apply (with uboot there's an additional, manual dd step outlined during the update) for better battery life._
* Manjaro have [announced and released Phosh Beta15 of Manjaro for the PinePhone](https://forum.manjaro.org/t/82660).
* postmarketOS [have released v21.06 Service Pack 2](https://postmarketos.org/blog/2021/09/13/v21.06.2-release/) bringing some new-ish software to users of their stable release.

### Worth noting
* If you want to try Waydroid (Anbox, but better), [it's now in the @danctnix repo](https://fosstodon.org/@xaviers/106936888809418637), and you can also [test it on postmarketOS](https://mastodon.fam-ribbers.com/@bart/106937368289238752) and [Manjaro](https://aspiechattr.me/@ManjaroLinuxBot/106937691079748930).

### Worth reading

#### Software progress
* Claudio Cambria: [Tag filtering, better navigation and lots of little UI tweaks — Kalendar devlog 14](https://claudiocambra.com/2021/09/11/tag-filtering-better-navigation-and-lots-of-little-ui-tweaks-kalendar-devlog-14/). _It gets better and better!_
* Alexander Mikhaylenko: [Cleaning up header bars](https://blogs.gnome.org/alexm/2021/09/10/cleaning-up-header-bars/). _I am not a fan of flat design, so I am not too convinced that this will be a great change, but admittedly using software is different to looking at mockups of software._
* Tobias Bernard: [Software 41: Context Tiles](https://blogs.gnome.org/tbernard/2021/09/09/software-41-context-tiles/). _More information, displayed in glancable and optionally more detailed form is a good thing._
* KDE: [KDE Ships Frameworks 5.86.0](https://kde.org/announcements/frameworks/5/5.86.0/).
* Jozef Mlich: [Nemomobile in September/2021](https://blog.mlich.cz/2021/09/nemomobile-in-september-2021/). _Nice progress!_

#### Sad developments
* Liliputing: [ Purism Librem 5 Linux smartphone is getting another (big) price hike](https://liliputing.com/2021/09/purism-librem-5-linux-smartphone-is-getting-another-big-price-hike.html). _I have a comment on this below under "Random"._
* TuxPhones: [Purism Librem 5 sees another (steep) price increase for 2022](https://tuxphones.com/purism-librem-5-2021-2022-price-increase/).

#### Reviews
* Aaron's Blog: [Review of the PinePhone (postmarketOS 3GB version)](https://www.ahoneycutt.me/blog/pinephone-review/). _Nice review!_

#### Needs title#
* TuxPhones: [Linux kernel needs more phones and tablets, says developer](https://tuxphones.com/linux-kernel-needs-arm-phones-tablets-mainline/). _Amen!_
* Gamey: [Plasma Mobile apps in Phosh](https://gamey.tech/posts/plasma-mobile-apps-in-phosh/). _Great how to! I guess I should link this post [here](https://linmob.net/pinephone-daily-driver-challenge-part4-crossing-desktop-environment-boudaries/)._

#### Qt 6 development
* Qt Dev Loop: [Introduction to the QML CMake API](https://www.qt.io/blog/introduction-to-the-qml-cmake-api).

#### PINE64 Community Update
* PINE64: [September update: Hurdles and Successes](https://www.pine64.org/2021/09/15/september-update-hurdles-and-successes/). _Noteworthy progress on PinePhone accessories and I am happy to see PineTalk coming back!_

### Worth listening

* Linux Unplugged: [Episode 423 What Makes a Linux User?](https://linuxunplugged.com/423).

### Worth Watching

#### PINE64 Community Update
* PINE64: [September Update: Hurdles and Successes](https://odysee.com/@PINE64:a/september-update-hurdles-and-successes:e). _Great video as always!_

#### Ubuntu Touch Q&A
* UBports: [Ubuntu Touch Q&A 108](https://www.youtube.com/watch?v=AS5BlUpqT1s). _It's totally worth watching it, but I somehow lost my notes, so I can't give much info here._

#### Librem 5 corner
* Gardiner Bryant: [The 5 things holding back the Librem 5](https://odysee.com/@TheLinuxGamer:f/librem5-final:1). _One of these issues is actually critical: Battery life! Also, make sure to look for [this video on YouTube](https://www.youtube.com/watch?v=E939fioA5bE) to read the title is has over there._

#### Distro walktroughs
* jesica: [Manjaro on PinePhone](https://www.youtube.com/watch?v=T6hlLcIQpAI). 

#### PinePhone impressions
* My Linux Diary: [My impressions of the PinePhone](https://odysee.com/@MyLinuxDiary:4/pinephoneimpressions:5).
* NOT A FBI Honey pot: [8am pinephone rant that no one asked for.......](https://www.youtube.com/watch?v=j1sD12w8Xeo). _CW: NSFW._

#### Sailfish OS Corner
* Jolla-Devices: [Sailfish OS 4.2 is out: how to request the "Verla" update manually](https://www.youtube.com/watch?v=zcTOHv1g3so). _Nice how to!_
* Puffercat: [Sailfish OS - Only daily driver suitable mobile linux?](https://www.youtube.com/watch?v=VuMCvLuhbVU).

#### Waydroid
* DanctNIX: [Subway Surfers running under Waydroid on PinePhone](https://www.youtube.com/watch?v=bG0uAQqeqW4). _Nice!_

#### Shorts 
* NOT A FBI Honey pot: [https://www.youtube.com/watch?v=32AWsci4YPY](https://www.youtube.com/watch?v=32AWsci4YPY).

### Stuff I did

#### Content

* I wrote a [blog post](https://linmob.net/gnome-software-41-will-be-fine-on-mobile/) about the new features of GNOME Software 41.

#### Random
* I wanted to write about the recent [Purism Email](https://forums.puri.sm/t/email-from-purism-thank-you-purism/14654), but did not manage to find the time and patience to write something meaningful that's not a rant. While I am glad that Purism are able to recommence shipping regular Librem 5s, the announced pricing increases are (while likely necessary for business reasons, as delays are costly) essentially removing the phone from meaningful further adoption.

#### LINMOBapps

The following app was added in the past week, pushing the app count to 313:

* [White Noise Mobian](https://gitlab.com/greenbeast/white_noise_mobian), a White Noise app for Mobian and other distributions.

[Read here what (else) happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master) on LINMOBapps this week. And please [do contribute!](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/CONTRIBUTING.md)

#### Linux Phone Apps

Because of a very busy week White Noise Mobian is not yet listed on [the new place](https://alpha.linuxphoneapps.org). But at least the app listings have [now reached](https://github.com/linuxphoneapps/linuxphoneapps.org/issues/1) "feature parity" and the [app overview page](https://alpha.linuxphoneapps.org/apps) has more information on individual apps. 
