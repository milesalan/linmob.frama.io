+++
title = "LinBits 38: Weekly Linux Phone news / media roundup (week 12)"
aliases = ["2021/03/28/linbits38-weekly-linux-phone-news-week12.html", "linbits38"]
author = "Peter"
date = "2021-03-28T21:45:00Z"
layout = "post"
[taxonomies]
tags = ["PINE64", "PinePhone", "Purism", "Librem 5", "LINMOBapps", "Fedora", "Arch Linux ARM", "DanctNIX", "Nemo Mobile", "PineTalk"]
categories = ["weekly update"]
+++

_It's sunday. Now what happened since last sunday?_

New DanctNIX and Fedora image releases, GNOME 40 on the PinePhone, PureOS Byzantium and more.  _Commentary in italics._
<!-- more -->


### Software development and releases

* A new [Fedora image for the PinePhone](https://github.com/nikhiljha/pp-fedora-sdsetup/releases/tag/0.5.0) has been released. _
* Arch Linux ARM have [released a newer image](https://github.com/dreemurrs-embedded/Pine64-Arch/releases/tag/20210328) so that you're initial `sudo pacman -Syyuu` does not take that long.
* neochapay has been working on new [Nemo Mobile images based on Manjaro](https://twitter.com/neochapay/status/1374029467526500355).


### Worth noting
* [Manjaro Linux now offers all locales on Phosh without compiling them on the phone](https://twitter.com/ManjaroLinux/status/1375101138823970820/photo/1).
* The Librem 5 back camera [enablement has made furry much progress](https://www.reddit.com/r/Purism/comments/maddj1/l5s_back_camera_now_taking_photos_at_4208x3120/).

### Worth reading 

* Martijn Braam: [Do you really want Linux Phones?](https://blog.brixit.nl/do-you-really-want-linux-phones/). _This is a really important post by Martijn Braam, and he has a point. If you read one article this week, please make it this one and read it carefully, especially the parts about the kernel and the community. Also check out the related [hacker news thread](https://news.ycombinator.com/item?id=26574731)._
* Tobias Bernhard: [Drawing GNOME App Mockups](https://blogs.gnome.org/tbernard/2021/03/22/drawing-mockups/). _If you want to create a GNOME app, this is likely of interest to you._
* Purism: [Sneak Peek of the Next PureOS Release on the Librem 5](https://puri.sm/posts/sneak-peek-of-the-next-pureos-release-on-the-librem-5/). _This is a really interesting post by Purism. Sadly, it does not give any hints at when Byzantium will be the default on the Librem 5. Judging from the screenshot, which does not show the PureOS store yet, I'd guess that might still take more than just a few weeks._
  * LinuxSmartphones: [Disk encryption, GPS, new apps and settings coming to the Librem 5 with PureOS](https://linuxsmartphones.com/disk-encryption-gps-new-apps-and-settings-coming-to-the-librem-5-with-pureos/). _Brad's take._
* Spaetzblog: [Pure-maps on Mobian/PureOS](https://sspaeth.de/2021/03/pure-maps-on-mobian-pureos/). _Packaging chronicles. It's also packaged in postmarketOS for everyone who really dislikes Flatpak for its use of disk space or from reading FUD._
* Stealthgun's blog: [Gentoo on a PinePhone: Making it a usable phone ](https://stealthgun.tweakblogs.net/blog/19368/gentoo-on-a-pinephone-making-it-a-usable-phone). _Sounds like fun._
* Purism: [Librem 5 and Librem 5 USA: What are the Differences?](https://puri.sm/posts/librem-5-and-librem-5-usa-what-are-the-differences/). _I still don't get why non-US subjects should get a Librem 5 USA._
* Linux Smartphones: [Roundup: Librem 5, Precursor, Fedora, Gentoo, Nemo Mobile, and GNOME 40](https://linuxsmartphones.com/roundup-librem-5-precursor-fedora-gentoo-and-nemo-mobile/). _Nice collection._



### Worth listening
* PineTalk 005: [Marching on](https://www.pine64.org/2021/03/26/005-marching-on/). _Ezra and I managed to get another one out of the door. Hope you like it, and please, ENGAGE!_
* [Linux Action News 181](https://linuxactionnews.com/181). _Dalton Durst joins Chris and Wes to talk about Ubuntu Touch OTA 16._
* FOSS North Pod #34: [Lukasz Erecinski from Pine64](https://foss-north.se/pod/episodes.html#ep34). _Nice podcast &mdash; and well done, Lukasz!_

### Worth watching

* Martijn Braam: [Gnome 40 on the PinePhone](https://www.youtube.com/watch?v=w2owj4549_s). _Great little demo of postmarketOS GNOME 40 on the PinePhone._
* E. Matt Armstrong: [PinePhone JumpDrive](https://www.youtube.com/watch?v=QXB1b-NW81o). _Yet another JumpDrive tutorial._
* Luke Bryan: [Installing Repeater-Start Amateur Radio Repeater App on Librem 5 Phone](https://www.youtube.com/watch?v=vtjg_FVOXD0). _That's an app I did not know off yet._
* Hex DSL: [I got a pine-phone. Not at all like a pine-cone (First impressions)](https://www.youtube.com/watch?v=O1OgomVCiLo). _Nice vid._
* Sk4zZi0uS: [More PinePhone Things that work now that didn't before 2021 03 25](https://www.youtube.com/watch?v=zCDOuO1cXWc), [Gushing about the PinePhone for 30 mins - 2021-03-23](https://www.youtube.com/watch?v=1pyE3t2iflM). _Hm, glad things work better for me._
* The Linux Experiment: [Is PLASMA as good on MOBILE as on the desktop? - KDE Plasma Mobile review](https://tilvids.com/videos/watch/ec68c0c7-02a4-4fa2-a958-4e3d2b9d0084). _I wish I could agree with his conclusion, but: Getting great cameras on newer hardware will not be easy, as great phone cameras are at least 80% percent software, and also, Plasma Mobile does not have an email app yet, so .. yeah._
* UBports: [Ubuntu Touch Q&A 97](https://www.youtube.com/watch?v=xbDz3PrpFcI). _Another update: NFC as a new feature; a work around for USB resets that make the modem disappear on the PinePhone, Focal Fossa (20.04) progress and more._ 

### Stuff I did

#### Content

None this week. I tried to and recorded for a few minutes, but it turned out: I would have needed more time to produce something meaningful about Ubuntu Touch and postmarketOS Plasma Mobile on the Librem 5 needs more time to get good. Don't expect videos next week; but I might be able to churn out a blog post. That aside, I'll be on holidays and try to spend a lot of time away from keyboard and off social media.

#### Random
I played with an unofficial Fedora 34 build for the PinePhone. After setting a password, I was able to launch GNOME 40, which ran surprisingly well and (given a few tweaks and maybe a little more powerful hardware) feels closer than 3.38 was for phone use. The same Fedora image comes with Phosh too, and is ok with that.

#### LINMOBapps

I added four apps to LINMOBapps. [See here what (else) happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master). Please [do contribute](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/tasks.md)! 


 

