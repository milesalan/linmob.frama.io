+++
title = "PinePhone Setup: More on Scaling in Phosh"
aliases = ["2021/02/13/pinephone-setup-scaling-in-phosh.html"]
author = "Peter"
date = "2021-02-13T18:45:00Z"
updated = "2021-11-07T15:50:00Z"
layout = "post"
[taxonomies]
tags = ["Purism", "Librem 5", "PinePhone", "Phosh", "Scaling", "yad", "wlr-randr"]
categories = ["howto"]
[extra]
update_note = "Updated to reflect Phosh 0.14.0/Phoc 0.9.0 changes to scale-to-fit app_id's."
+++

_This post also applies to basically any phone that runs Phosh, be it one of the many postmarketOS devices, or &mdash; naturally &mdash; the Purism Librem 5._

<!-- more -->

Following up to my blog post detailing my setup, I was asked how exactly I scale web apps like Element to somewhat work in Firefox on the PinePhone. As the same applies to other web apps (e.g. Discord, more on that in a future blog post), I figured I should make a dedicated blog post detailing how I do that, as simply executing `ctrl`+`-` does not cut it.

### Scaling, generally (Normal apps)

First, let's reiterate how we can affect scaling generally in the GNOME on Mobile environment comprised of Phoc (compositor), Phosh (shell) and Squeekboard (virtual keyboard). 

As not every app that might be useful does scale nicely out-of-the-box; meaning parts of the window will be off the screen and thus unreadable and untouchable.

There's a general "shotgun approach", that applies scaling to every app: 

`$ gsettings set sm.puri.phoc scale-to-fit true`

I recommend a more nuanced approach as the global method can have negative side effects on optimized apps. This approach enables the forced scaling on a per-app-basis. To do that we need to have `scale-to-fit`, and a script, which I usually name `get-app-id.sh` and put in `~/.local/bin` (which is in my PATH).[^1]

~~~~~
#!/bin/bash
WAYLAND_DEBUG=1 $1 |& grep 'xdg_toplevel@[0-9]\+\.set_app_id'
~~~~~

Just copy these two lines into a text file, e.g. named `get-app-id.sh`, on your PinePhone and make it executable by running `$ chmod +x get-app-id.sh`.

Let's say, you want to use KeePassXC, which does not scale well, but becomes somewhat usable by running `scale-to-fit`. But you won't get there by running `$ scale-to-fit keepassxc on`. You can now run `$ ./get-app-id.sh keepassxc` and _KeePassXC_ will start up. Go back to your terminal and you will find a line of text similar to:

~~~~~~
[4049990.198] -> xdg_toplevel@21.set_app_id("org.keepassxc.KeePassXC")
~~~~~~

The part between the "" is what you are looking for. Just run `$ scale-to-fit org.keepassxc.KeePassXC` in the terminal and the next time you start KeePassXC, it should be usable &mdash; or at least fit the screen.

The same works for Flatpak apps, as I've described in [a previous blog post](https://linmob.net/2020/07/27/pinephone-daily-driver-challenge-part2-flatpak-and-scaling-in-phosh.html#manual-scaling-in-phosh). 

#### Phosh 0.14.0/Phoc 0.9.0 and later

As of Phosh 0.14.0/Phoc 0.9.0[^2] and later, the script above is no longer necessary for most apps &mdash; you can just use the proper App ID that [LinuxPhoneApps.org](https://linuxphoneapps.org) lists (e.g. `org.gnome.Evolution` for Evolution or `org.gnome.calendar` for Calendar) instead of the output the script generates. 

### Scaling web apps/Changing phoc scaling

For web apps, this method does not work. We need to take another route, that scales every UI element down, and thus makes the interface work on the smaller phone screen. By default, on PinePhone and Librem 5, the UI scaling is set to 2. 

In a the blog post "[Easy Librem 5 App Development: Scale the Screen](https://puri.sm/posts/easy-librem-5-app-development-scale-the-screen/), Purism's Kyle Rankin describes a way to do this kind of scaling nicely via a graphical user interface. This "scale_screen" script is now part of a PureOS [package called l5-goodies](https://source.puri.sm/librem5-apps/librem5-goodies).

Dependencies for this script are [wlr-randr](https://github.com/emersion/wlr-randr), which is to Phoc (and other [wlroots](https://github.com/swaywm/wlroots)-based compositors) what _xrandr_ is to Xorg, and [yad](https://sourceforge.net/projects/yad-dialog/) (short for  "yet another dialog"), a tool for creating graphical dialogs from shell scripts. They should be packaged in your distribution of choice.

To use it, I just put [l5-scale-the-screen](https://source.puri.sm/librem5-apps/librem5-goodies/-/blob/master/l5-scale-the-screen) in `~/.local/bin` and [l5-scale-the-screen.desktop](https://source.puri.sm/librem5-apps/librem5-goodies/-/blob/master/l5-scale-the-screen.desktop) in `~/.local/share/applications`; again, these need to be in your PATH, which they likely are not by default.[^1]

With this, you can now easily make web apps like Element or Discord usable, the same applies to a few desktop apps. Just adjust the scaling until it fits your needs. 
For general help on web apps with Firefox, read my [blog post about my PinePhone setup](https://linmob.net/2021/01/09/my-setup-with-danctnix-archlinuxarm.html) and watch my video "Firefox Profiles and Site Specific Browsers on the PinePhone" on [PeerTube](https://devtube.dev-wiki.de/videos/watch/165ca7c9-aa35-4b07-867f-74064542c9fd), [Odysee](https://odysee.com/@linmob:3/firefox-profiles-and-site-specific:c) or [YouTube](https://www.youtube.com/watch?v=h7aLE8jBOPc).

[^1]: To see what's in your PATH and to learn how to add directories to your path, check out this [HowToGeek.com](https://www.howtogeek.com/658904/how-to-add-a-directory-to-your-path-in-linux/) article. Please note that you will also need the file as executable, which is done with `chmod +x FILENAME`.

[^2]: See the [Phoc 0.9.0 changelog](https://gitlab.gnome.org/World/Phosh/phoc/-/releases#v0.9.0).
