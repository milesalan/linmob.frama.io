+++
title = "Warning: This place might become droidy..."
aliases = ["2009/02/08/warning-this-place-might-become-droidy.html"]
author = "peter"
comments = true
date = "2009-02-08T20:25:00Z"
layout = "post"
[taxonomies]
tags = ["A910", "A910i", "Android", "Debian", "software", "T-Mobile G1"]
categories = ["personal", "shortform"]
+++
Though there is no way to root the german T-Mobile G1/HTC Dream yet, I decided to get one. This can mean that this place will become pretty androided soon, I am thinking of software reviews and such.

But for those that already start crying for the good old times: After having seen Debian for the rooted US/UK-G1s I had to think about something like that for the good old A910s. No more news about that, and until the last week of february I will have absolutely _NO_ time to work on that front. So just keep cool and watch out.
