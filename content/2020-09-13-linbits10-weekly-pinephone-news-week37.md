+++
title = "LinBits 10: Weekly PinePhone news / media roundup (week 37)"
aliases = ["2020/09/13/linbits10-weekly-pinephone-news-week37.html", "linbits10"]
author = "Peter"
date = "2020-09-13T12:45:00Z"
layout = "post"
[taxonomies]
tags = ["Pine64", "PinePhone", "Librem 5", "Arch Linux ARM", "Quickddit", "MMS", "libhandy", "gaming", "Anbox", "App Lists", "Camera", "Nemo Mobile", "Manjaro", "Lomiri", "Phosh", "postmarketOS", "Megapixels"]
categories = ["weekly update"]
+++

_It's sunday. Now what happened since last sunday?_

libhandy 1.0, MMS, a new camera app, new Manjaro images and much more. _Commentary in italics._
<!-- more -->

#### Software releases and improvements
* [libhandy 1.0.0 has been released](https://mamot.fr/@KekunPlazas/104829402183664361). 
* [libhandy-rs 0.7, targetting libhandy 1.0, has also been released](https://mstdn.fr/@bilelmoussaoui/104831255152605311). _libhandy-rs are the Rust-bindings for libhandy._
* Alexander Mikhaylenko has been working on libhandy-fying [Epiphany/GNOME Web](https://fosstodon.org/web/statuses/104818952896282994) and [Nautilus](https://fosstodon.org/web/statuses/104831587918208725).
* [vgmms](https://git.sr.ht/~anteater/vgmms) is an SMS and MMS client build atop ofono and MMSd with VGTK and Rust. _The news item here is __MMS__, which apparently is still being used in some parts of planet earth._
* Martijn Braam has worked on his camera app, [Megapixels](https://git.sr.ht/~martijnbraam/megapixels). It can be installed on postmarketOS from [his repository](http://repo.brixit.nl). _I tested it, and it works &mdash; it even felt a bit nicer than Gnome Camera/Pinhole. Please keep in mind that you do need a patched kernel for the front camera currently._
* [Calindori](https://invent.kde.org/plasma-mobile/calindori), the calendar app for Plasma Mobile, has been released in its [first "stable" version 1.2](https://dimitris.cc/kde/2020/09/11/Calindori_1_2.html).
* [Nemo Mobile](http://nemomobile.net/) are [making](https://nitter.net/neochapay/status/1304818781970141186) [progess](https://nitter.net/neochapay/status/1305035405004353536) with their move to a Fedora base.
* [Manjaro](https://manjaro.org/) have published further [PinePhone images](https://osdn.net/projects/manjaro-arm/storage/pinephone/), including a [third alpha release of their Phosh image today](https://osdn.net/projects/manjaro-arm/storage/pinephone/phosh/alpha3-20200913/). The Lomiri situation has already improved quite a bit since their first published image.


### Other hardware
* The [YARH.IO MKI](http://yarh.io/yarh-io-mki.html), a "fully hackable and custamizable Raspberry Pi based handheld, running Raspberry Pi OS and supporting all other Operating Systems available for Raspberry Pi" has been announced. _It is a little 'meh', but then I don't think it's aiming at the same niche the PinePhone does. If you want a handheld with ports, and love the Raspberry Pi platform, this  might be for you._


### Worth reading

* Liliputing: [Mobian Linux is now available for the PineTab (as well as the PinePhone)](https://liliputing.com/2020/09/mobian-linux-is-now-available-for-the-pinetab-as-well-as-the-pinephone.html). _Mobian on the PineTab!_
* eighty-twenty: [Squeak-on-a-cellphone update: touchscreen working!](https://eighty-twenty.org/2020/09/09/squeak-postmarketos-touchscreen).  
* Purism: [Your Phone Is Your Castle](https://puri.sm/posts/your-phone-is-your-castle/). _Purism CSO Kyle Rankin rambles once more about phone security and privacy and why it matters. It's worth a read, but please consider that [there are voices](https://nitter.net/DanielMicay/status/1304205042724098050) that don't agree that Purism's security story is all that convincing because of issues like non-upgradable firmware._
* HackersGame: [Robocalls: Fight Bots with Bots](https://www.hackers-game.com/2020/09/08/robocalls-fight-bots-with-bots/). _This is a great idea._

### Worth watching
* Purism: [Librem 5 Emulators and Controllers](https://youtu.be/yYdJBqm_hKE). _One for the gamers._
* Roboron: [Trying Simutrans on a Pinephone](https://youtu.be/Da1xMKhJBek). _Another one! (I always loved playing simutrans. The main reason I stopped was that would just eat hours without me even noticing.)_
* Howto: Digital Privacy & Infosec: [Pinephone Linux Smartphone: Sailfish OS Demo](https://youtu.be/Ar70x44jfTU). _A short demo of Sailfish OS. Unfortunately, shortly after this video was made, support for the modem and all it adds was enabled about a day later._
* Howto: Digital Privacy & Infosec: [SXMO (Simple X Mobile) On Pinephone Uses Only 329mb RAM!](https://www.youtube.com/watch?v=pBeJLVShRwg). 
* Martijn Braam: [In depth tutorial: inserting an SD card in a PinePhone](https://www.youtube.com/watch?v=9S_8twLnYFA). _24 seconds. The first thing he puts into the device is called a "microSIM card". It is for telephony._
* Martijn Braam: [Quick overview of application development on the PinePhone](https://www.youtube.com/watch?v=mZWD6a703T0)
* @maeda_: [make sound on #PinePhone #Clojure #Overtone #emacs_pgtk #lispymode #Squeekboard](https://nitter.net/maeda_/status/1304067353139798018/video/1). _This uses Jack, btw._
* UBports: [Ubuntu Touch Q&A 84](https://www.youtube.com/watch?v=OMKXZQGSUJc). _Topics include the PineTab, OTA 13 (which is for the other devices, not the PinePhone), Qt 5.12, why Marius Gripsgard likes working with Manjaro and more._
* jacky.wtf: [Talking all things PinePhone and open source hardware/software! !kde !blm](https://www.twitch.tv/videos/738432719). _A twitch steam, a first in this list!_



### What did I do?

I  played with a few things:
* I uploaded another video showing Anbox, this time on Arch Linux ARM on sunday evening,
* and tried [a few more Android apps later](https://fosstodon.org/web/statuses/104825657455498444) (Slide would run fine later this week),
* I tried the 20200909 Manjaro Lomiri image and was quite impressed with their progress,
* I [liked Megis p-boot image](https://fosstodon.org/@linmob/104837139841987743),
* I tried to build [Quickddit](https://github.com/accumulator/Quickddit/), a reddit client for Ubuntu Touch, on Arch Linux ARM and [made it almost work](https://fosstodon.org/web/statuses/104847063738455383) (I did not manage to login into my Reddit account. I also looked into building other software created for Ubuntu Touch, namely [KeePit](https://github.com/DannyGB/KeePassTouch), [uNav](https://github.com/costales/unav) and [Dekko](https://gitlab.com/dekkan/dekko), but these either failed for reasons I could not figure out or have a different build process and require more dependencies. Especially Dekko seems likely to be a tough nut to build. Just check the "clickable.json" files in the project repos to get an idea how the software is actually built.

I also spent some time on web development. This blog is going to move to different hosting _(suggestions still welcome)_ in the next two weeks and will switch from Jekyll to [Hugo](https://gohugo.io). With that switch, the [LINMOBapps app list](https://linmobapps.frama.io), which now lists 165 apps, will have it's item in the navbar. The App Directory is also still in its planning phase, and I hope to apply the learnings of moving this website over to Hugo to the process.    
