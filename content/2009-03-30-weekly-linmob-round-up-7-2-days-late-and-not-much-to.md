+++
title = "Weekly linmob round up (7): 2 days late and not much to tell, again"
aliases = ["2009/03/30/weekly-linmob-round-up-7-2-days-late-and-not-much-to-tell-again.html","2009/03/30/weekly-linmob-round-up-7-2-days-late-and-not-much-to.html"]
author = "peter"
date = "2009-03-30T17:03:00Z"
layout = "post"
tags = ["2.6.29", "a1210", "a810", "beagle board", "E28", "Motorola", "openmoko", "pandora", "qt embedded",]
categories = ["weekly update"]
+++
First a short rip off from &#8220;linmob&#8221; on identi.ca:

* <a href="http://is.gd/pzEy">A small device, BeagleBoard based, with a that has a projector and a laser keyboard. Interesting.</a>
* <a href="http://is.gd/phd4">If this ever becomes really useable, OpenMoko users will be able to show off their devices again.</a>
* <a href="http://is.gd/oUSv">Motorola A1210 for China: TI OMAP 850, runs &#8220;homegrown Linux&#8221;.</a> Bet it's made by E28&#160;<a href="http://is.gd/oUT2">(?)</a>, like the A810.
* <a href="http://tinyurl.com/cvwa9o">Linux 2.6.29 is out, runs fine on my notebook :-) ... </a>
* <a href="http://tinyurl.com/crd8dt%20">OpenSource gaming handheld &#8220;Pandora&#8221; apparently being ready soon&#8230;</a>

Well, that's what I believe to be the most interesting stuff. 

_And as I don't have time for the boring stuff now, so see you on next saturday._
