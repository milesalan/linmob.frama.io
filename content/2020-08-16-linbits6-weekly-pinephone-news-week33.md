+++
title = "LinBits 6: Weekly PinePhone news / media roundup (week 33)"
aliases = ["2020/08/16/linbits6-weekly-pinephone-news-week33.html", "linbits6"]
author = "Peter"
date = "2020-08-16T12:07:18Z"
layout = "post"
[taxonomies]
tags = ["real life testing", "PinePhone", "Ubuntu Touch", "UBports", "Anbox", "Arch Linux ARM", "Manjaro", "Maemo Leste", "Hardware mods", "PINE64 Community Update", "PINE64", "Reviews", "Dino", "XMPP", "OMEMO"]
categories = ["weekly update"]
+++

_It's sunday. I need to stop tinkering and make it Linbits time!_

This is some the best stuff that happened in the Linux on Mobile and PinePhone space this week. _Commentary in italics._
<!-- more -->

### Software: releases and improvements

* [GNUHealth, a social health softwre, is apparently shaping up nicely on the PinePhone.](https://nitter.net/meanmicio/status/1294666367816347650)
* [mobile-firefox-config](https://gitlab.com/postmarketOS/mobile-config-firefox). _postmarketOS published a set of tweaks for the Firefox browser that make it more usable on the PinePhone. BTW: My favourite browser is Angelfish, unfortunately it is not available everywhere._
* [Arch Linux ARM/Huong Tram Linux saw a new release](https://github.com/dreemurrs-embedded/Pine64-Arch/releases/tag/20200814), enabling the Firefox tweaks above, shipping with `phosh-renice`, a [set of scripts](https://github.com/KeironO/phosh-renice) that prioritises certain processes originating from the [Mobian](https://www.mobian-project.org) community, and other improvements. Danct12 also packaged `anbox`, too.
* Manjaro released a new Plasma Mobile alpha after 4 months, which you can [watch in action](https://invidious.13ad.de/watch?v=Wn7bliZrHJ8) or [download](https://osdn.net/projects/manjaro-arm/storage/pinephone/plasma-mobile/alpha6/). Also: [More Lomiri teasing](https://nitter.net/fkardame/status/1293681887572107264).
* Maemo Leste [have shared](https://nitter.net/maemoleste/status/1294721617227128832) that they have made progress porting Qt5 to their distribution.

### Hardware fun

* silver has built a [folding PinePhone case](https://fedimaker.space/@silver/104665479131040331). _I really like this!_ 

### Worth reading

* Pine64: [August update: getting into a rhythm and not slowing down](https://www.pine64.org/2020/08/15/august-update-getting-into-a-rhythm-and-not-slowing-down/). _Pine64's monthly update is always worth a read, especially when they include a video by yours truly. Kidding aside, it really is._
* mcol.xyz: [a full day test-driving ubuntu touch](https://mcol.xyz/2020/08/a-full-day-test-driving-ubuntu-touch-on-the-pinephone.html). _This is a good write up about using the PinePhone with UBports in a real world scenario._
* AndroidPolice: [This smartphone has physical kill switches for its cameras, microphone, data, Bluetooth, and Wi-Fi](https://www.androidpolice.com/2020/08/15/this-smartphone-has-physical-kill-switches-for-its-cameras-microphone-data-bluetooth-and-wi-fi/). _While I get the kill switch excitement with the Librem 5, I haven't used the kill switches on the PinePhone yet, as removing the back plate and the tiny switches ... no. Just no._
* AndroidPolice: [The Linux-based PinePhone is the most interesting smartphone I've tried in years](https://www.androidpolice.com/2020/08/13/the-linux-based-pinephone-is-the-most-interesting-smartphone-ive-tried-in-years/). _It's rare that mainstream phone news outlets pick up on products like the PinePhone, and it is even rarer that they do so in such a nice way._ 
* Dino: [Setting out for calls and conferences](https://dino.im/blog/2020/08/setting-out-for-calls-and-conferences/). _Dino, which is an XMPP client with excellent support for OMEMO-encryption, have announced that they will add support for calls and conferences. I put this into this segment as Dino is not libhandy-enabled yet and does not run particularily well on the PinePhone, but with support for internet native calls it becomes a lot more interesting &mdash; and a lot can happen in a year._

### Worth watching

* UBports: [Ubuntu Touch Q&A 82](https://invidious.13ad.de/watch?v=e-NIgbHgSRc). _This is an interesting one for people interested in the PinePhone and PineTab. If you lack the time to watch all of it: Around Minute 0:15 VoLTE, after that QtWebEngine; 8:35: PineTab; 13:00: Initial Camera support for the PinePhone ([toot](https://fosstodon.org/@linmob/104698819001147326)); 14:15 also the Bluetooth toggle has been fixed; 20:15: Lomiri on Manjaro._
* matrixdotorg: [Open Tech Will Save Us Meetup #5](https://invidious.13ad.de/watch?v=tJ8tthkVAOQ). _Starting around minute [32:10](https://youtu.be/tJ8tthkVAOQ?t=1930), Matthew Petry, a Pine64 volunteer, talks a bit about the running Synapse (Matrix-server) on a RockPro-Cluster, but also about Pine64 in general and thus about the PinePhone, too._ 
* Дмитрий Куртуков: [Look on the PINEPHONE keyboard](https://invidious.13ad.de/watch?v=MMTlu3De-Eg). _We've seen previous iterations of this particular "attach a Nokia N950 keyboard to the PinePhone"-keyboard before. This new video focuses just on the keyboard. Well done!_
* PizzaLovingNerd: [PinePhone OSes: PureOS](https://invidious.13ad.de/watch?v=4bjXxeu8oqk). _The PureOS port is definitely interesting, and PizzaLovingNerd did a good job with this video._
* Martijn Braam: [mobile-config-firefox on the PinePhone](https://invidious.13ad.de/watch?v=Co6qnlw4hgE). _As mentioned above, the postmarketOS community has worked hard to improve desktop Firefox on the PinePhone. Here is Martijn Braam, postmarketOS developer, giving you an idea of these improvements &mdash; you'll need to watch carefully to see them, but in real life they do make quite the difference._
* Сергей Чуплыгин: [Volume button work on pinephone](https://invidious.13ad.de/watch?v=9NGp5VRbWLc). _Three seconds of Sailfish OS with working volume buttons. Great to see progress on Sailfish OS!_
* Leszek Lesner: [AirDrop Alternative KDEConnect - Android, Linux, Windows, SailfishOS, Plasma Mobile](https://invidious.13ad.de/watch?v=EKvcNuHYulg). _This is not about the PinePhone, but with KDE Connect being a really helpful piece of software, I figured to include it._
* Nick Elsmore: [Simple Retroarch Test (daveshah port)](https://invidious.13ad.de/watch?v=Ki7ucNhWk_w). _This is a special treat, as it shows the long-coming DragonBox Pyra handheld running Retroarch._ 

### Stuff I did

* I replaced my old Mobian install with their f2fs-image. While others apparently have run into issues with this image, it has been working fine for me for the past four days. Only difference is that it feels a lot faster. _BTW: Make sure to get decent, name brand microSDHC/microSDXC cards. It makes a difference._ Also I contributed [instructions on using Pantalaimon to the Mobian Wiki](https://wiki.mobian-project.org/doku.php?id=fractal), though I went back to using it with out the CPU limiter, as that would not work for me.
* I asked for a topic to make a video on on Fosstodon and Twitter, and just like in the comments to my [Qt Apps on Phosh-video](https://invidious.13ad.de/watch?v=Wn7bliZrHJ8), Anbox was hugely popular. Therefore I had to finally wrap my head around it, and [published a video](https://invidious.13ad.de/watch?v=v06KUrfs69k) and [wrote an article](https://linmob.net/2020/08/15/anbox-on-the-pinephone.html).
* I tried Manjaros new Plasma Mobile image on video. It's nice, but certainly not ready for daily use yet. I will have another look at the KDE Neon images next week, to see how well these do work.
* I decided that I will start adding an Application List to this blog, starting from the [MGLApps](https://mglapps.frama.io/) list, adding more information and hopefully even tiny screenshots to the list on an app by app basis. _Pull requests welcome!_ 
