+++
title = "MWC2011: Day 1"
aliases = ["2011/02/mwc2011-day-1.html"]
date = "2011-02-14T23:25:00Z"
[taxonomies]
tags = ["Alien Dalvik", "Android", "Android 3.0 (Honeycomb)", "Gnome", "Intel Atom N450", "LiMo", "medfield", "MeeGo", "MeeGo tablet UX", "MWC2011", "Myriad", "OMAP4", "webos", "WeTab", "WP7"]
categories = ["events"]
[extra]
author = "Peter"
+++
### Platforms (software):

* Intel show off MeeGo tablet UX on ExoPC/WeTab platform (Atom N450): mobile bloggers not too impressed: Lot of potentital, but not much progress on UX side since Computex (probably because Intel and Nokia focused on the &#8220;below UX&#8221; parts of the platform first): Impressions on <a href="http://www.engadget.com/2011/02/14/intel-shows-off-more-of-its-meego-tablet-ui-still-needs-lots-of/">Engadget</a>, <a href="http://www.carrypad.com/2011/02/14/intel-launches-meego-tablet-user-experience/">Carrypad</a>, <a href="http://blog.laptopmag.com/intel-shows-off-meego-tablet-ux-complete-with-panel-based-home-screen">Laptopmag</a>, @chippy's audioboo <a href="http://boo.fm/b277783">1</a> <a href="http://boo.fm/b277788">2</a>
* Intel furthermore stated that there will be a Medfield phone this year&mdash;the device they showed ran Android, though: <a href="http://www.carrypad.com/2011/02/14/intel-shows-medfield-phone-running-android-at-mwc/">Carrypad</a>
* <a href="http://armdevices.net/2011/02/14/honeycomb-will-work-on-cheaper-tablets/">@charbax (ARMDevices.net) was told by an unnamed source that Android 3.0 (Honeycomb) will also run on cheap tablets (ARM11)</a>. It remains to be confirmed, but due to Androids open nature this sounds highly likely and it is nice to see that those initial &#8220;Honeycomb will only work on Dual Core ARMv7&#8221; bullshit rumors are what they are: Bullshit. Furthermore, this makes cheaper tablets like the SmartQ N7 a lot more interesting.
*  Alien Dalvik by Myriad apparently running quite nicely on MeeGo: <a href="http://boo.fm/b277957">@chippy's audioboo</a> Isn't going to be available for end users (due to its rather low level nature and monetization, I assume), but I am sure that once it shipped on a device or leaks out people will try to integrate it into webOS or LiMo, too.
* LiMo foundation announce the 4th release of their platform, featuring multi touch and 3D effects, while going on to build on GNOME software&mdash;first devices to be out in summer: <a href="http://www.limofoundation.org/en/Press-Releases/limo-foundation-unveils-limo-4.html">press release</a> 
* Windows Phone 7&#160;2011 future: CDMA, Copy and paste in March (<a href="http://www.engadget.com/2011/02/14/windows-phone-7-update-with-copy-and-paste-cdma-support-coming/">engadget</a>); IE9, multi-tasking, twitter integration later this year (<a href="http://www.engadget.com/2011/02/14/microsoft-shows-off-windows-phone-7s-future-with-multitasking/">engadget</a>)&mdash;multi-tasking UI looks surprisingly like webOS's cards, well, Microsoft have always been good at reimplementing the ideas others had before, haven't they? Adding the recent Nokia partnership to this, counting out this platform would be overhasty.

### (Noteworthy) Devices:

* LG Optimus 3D, OMAP 4, 4,3&#8221; glasses free 3D smartphone: <a href="http://armdevices.net/2011/02/14/lg-optimus-3d/">ARMdevices.net</a>
* Samsung Galaxy Tab 10.1, nVidia Tegra 2 based Google experience tablet: <a href="http://armdevices.net/2011/02/14/honeycomb-on-the-samsung-galaxy-tab-10-1/">ARMdevices.net</a>
* Samsung Galaxy S2, 4.3&#8221; WVGA, 1GHz dual-core Samsung  Exynos Cortex A9 with ARM Mali 400 graphics: <a href="http://armdevices.net/2011/02/14/samsung-galaxy-s2/">ARMdevices.net</a>
 
 _More tomorrow._
