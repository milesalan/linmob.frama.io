+++
title = "QtMoko on the GTA04"
aliases = ["2011/10/24/qtmoko-on-the-gta04.html","2011/10/24/radek-polak-shot-a-nice-video-of-his-distribution.html"]
author = "peter"
date = "2011-10-24T19:55:00Z"
layout = "post"
[taxonomies]
tags = ["community", "Debian", "distributions", "Freerunner", "gta02", "gta04", "openmoko", "Openmoko Freerunner", "QtMoko", "Qtopia"]
categories = ["shortform"]
+++
Radek Polak shot a [nice video of his distribution QtMoko](https://www.youtube.com/watch?v=PgZzxmHzWtg), which is essentially a Debian based distribution based on Qt Extended (Qtopia), originally developed for the Openmoko Freerunner, running on the <a href="http://www.gta04.org">GTA04</a>.

Source: <a href="http://lists.openmoko.org/pipermail/community/2011-October/065580.html">Openmoko Community Mailing Lists</a>
