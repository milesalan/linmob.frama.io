+++
title = "LinBits 63: Weekly Linux Phone news / media roundup (week 37/38)"
date = "2021-09-22T20:45:00Z"
draft = false
[taxonomies]
tags = ["PinePhone","LINMOBapps","LinuxPhoneApps",]
categories = ["weekly update"]
[extra]
author = "Peter"
+++

A rather dull seven days have gone by: GNOME (Calls) 41 has been released, join Jolla's 10th birthday party, Waydroid in OpenSUSE, how to cross compile for Plasma Mobile development, two new podcast episodes worth listening to (with chapter markers on Linux Phones), and a video shortage?!<!-- more --> _Commentary in italics._ 

### Software releases
* [GNOME Calls 41](https://gitlab.gnome.org/GNOME/calls/-/tags/41.0) has been released. The major new feature is a UI to manage SIP accounts and the ability to place and receive SIP calls from the dial pad. Read the [full release notes](https://gitlab.gnome.org/GNOME/calls/-/blob/897a394833fc27b66df19bec28403730efd95876/NEWS) for more details. _This is in line with GNOME 41's release, which might bring some progress regarding upstream mobile compatibility. Stay tuned for more on that topic._
* [Chatty 0.4.0](https://source.puri.sm/Librem5/chatty/-/blob/fbb56619a0787971895a682dbfbde03fe474226f/debian/changelog) has also been released, delivering quite a few changes. _Note that this release does not deliver MMS or Matrix functionality just yet, but with all the underlying changes, things are definitely moving forward._

### Worth noting
* If you still have not tried Waydroid yet, [it's now packaged for OpenSUSE, too](https://twitter.com/hadrianweb/status/1440697394031648768).
* Jolla, the company behind SailfishOS [is going to turn 10 years old soon](https://twitter.com/JollaHQ/status/1440644219056443393)! If you want to celebrate with Jolla in Berlin or Finnland, [register here](https://www.eventbrite.co.uk/e/jolla-10-years-celebration-in-berlin-october-14-tickets-174993539387)!
* If you're building Kasts, my favourite Podcast client for Linux Phones from git master now, [you'll end up with support for chapter markers](https://fosstodon.org/@mogwai/106971638499103100)! _Awesome!_
* If you're running postmarketOS Edge on your PinePhone with Phosh, note this as it [affects your phone calls](https://postmarketos.org/edge/2021/09/19/pinephone-profile-switching/).

### Worth reading

#### Hardware news
* PineGuild: [Extension cases for PinePhone are (almost) ready](https://pineguild.com/extension-cases-for-pinephone-are-almost-ready/).
* FX Technology: [September Pro1-X update](https://www.indiegogo.com/projects/pro1-x-smartphone-functionality-choice-control#/updates/all). _This is not just a regular progress report, they have hired an in-house engineer for Linux and mainline support, which is awesome news!_


#### Software progress
* Claudio Cambria: [New tag management capabilities, usability improvements, and a bunch of fixes — Kalendar devlog 15](https://claudiocambra.com/2021/09/19/new-tag-management-capabilities-usability-improvements-and-a-bunch-of-fixes-kalendar-devlog-15/). _Just awesome!_
* MauiKit.org: [Maui Report - 15](https://mauikit.org/blog/maui-report-15/). _I like what's coming here. I am especially looking forward to Strike and Sol moving to a stable release!_ 
* Marcus Lundblad: [Maps and GNOME 41](http://ml4711.blogspot.com/2021/09/maps-and-gnome-41.html). _Quite some progress for 41. Unfortunately, the issues with the navigation mode on mobile devices is still present._
* Peter Hutterer: [An Xorg release without Xwayland](https://who-t.blogspot.com/2021/09/an-xorg-release-without-xwayland.html). _While this is not too relevant for our mobile devices (although Sxmo is working on a Sway-based Swmo, which somewhat fits this), it's worth reading if you depend on Xorg heavily still._

#### Fuchsia?
* Camden Bruce: [Fuchsia on the PinePhone part 1— the concept](https://medium.com/@camden.o.b/fuchsia-on-the-pinephone-part-1-the-concept-f09b60970fa8). _Nice write-up on the prospect of bringing Fuchsia to the PinePhone!_

#### Tutorials
* Han Young: [Cross Compile to PinePhone Part One](https://www.hanyoung.uk/blog/cross-compile-to-pinephone-part-one/), [Cross Compile to PinePhone Part Two](https://www.hanyoung.uk/blog/cross-compile-to-pinephone-part-two/). _Great how to, looking forward to Part Three! Let's see if I'll follow it soon, or instead stick to my "uh, I can just do this on the phone" madness._
* Gamey: [Plasma Mobile apps in Phosh](https://gamey.tech/posts/plasma-mobile-apps-in-phosh/). _This was in last weeks LinBits, but since there's one more helpful environment variable added since, it's in here again._

#### Qt 6 development
* Qt Dev Loop: [QML Modules in Qt 6.2](https://www.qt.io/blog/qml-modules-in-qt-6.2). _If you want to get your hands wet with Qt 6 QML apps, this should be an interesting read._


### Worth listening
* postmarketOS: [#9 PBP, RP64, ModemManager, Phosh customization, Principles](https://cast.postmarketos.org/episode/09-PBP-RP64-ModemManager-Phosh-Principles/). _Great show! Make sure to stay on for the end: Principles may sound boring, but they matter a lot._
* PineTalk: [S2E01: PineTalk News - Season 2 Introductions](https://www.pine64.org/2021/09/17/s2e01-pinetalk-news-season-2-introductions/). _Glad to see PineTalk make a come back, as two separate shows in one feed created by two teams! While I am usually not a fan of Seasons in Podcasting, it makes sense here, and it's a great listen - although, minor nitpick, more shownoty shownotes whould be great! I am very much looking forward to how this goes on._

### Worth Watching

#### Caleb at Linaro
* LinaroOrg: [Linaro and postmarketOS on the OnePlus 6 Demo Video](https://www.youtube.com/watch?v=QA82_jyeLtE). _Great demo!_

#### Kubernetes Corner
* Kubernetes Community Days U,K: [pinephone, pocket & pub ready k8s lab environments for the time-poor CKx student Ewan Nisbet 1.1.6](https://www.youtube.com/watch?v=vS_DG894u5c). _One more thing you can do with Linux Phones. Given the amount of devices I have here (more than I can use at once), I really should try replicating this._

#### Sad, sarcastic stuff
* Twinntech: [pinephone battery Amazing](https://www.youtube.com/watch?v=48o_o5Vp4wc). _Something is seriously wrong with his setup there. This sounds way worse than it used to be before CRUST landed. Quality sarcasm though._

#### Shorts of postmarketOS
* NOT A FBI Honey pot: [ok, I went suckles {SXMO,} #pine64 #pinephone #linux](https://www.youtube.com/watch?v=UACIxfYq-5g). _postmarketOS installer, everybody!_
* Jakob: [Waydroid on OP6T running PostmarketOS](https://www.youtube.com/watch?v=1kPheJQP4Wg). _This is Waydroid on GNOME on postmarketOS. Well done!_
* Baonks 81: [Samsung Galaxy 3G SM-A500H/DS run postmarketOS kernel 5.13.0](https://www.youtube.com/watch?v=zhkdnFGy29c). 
* Baonks 81: [Samsung Galaxy 3G SM-A500H/DS run Phosh Firefox aquarium webgl sample](https://www.youtube.com/watch?v=Yo9FFAENrGs). _Nice!_

#### GNOME 41
* The Linux Experiment: [GNOME 41 - The first step towards a GNOME Platform?](https://www.youtube.com/watch?v=qCeVAqj97DQ). _Not too mobile specific, but still somehow relevant._

### Stuff I did

#### LINMOBapps

This week I've been finally tackling the ["overhaul" milestone](https://framagit.org/linmobapps/linmobapps.frama.io/-/milestones/1), and also added two new columns that should hopefully help enticing contributions to LINMOBapps/LinuxPhoneApps going forward.
All that aside, one new app, contributed by it's developer, Hank Greenburg, was added:

* [Elephant Remember](https://gitlab.com/greenbeast/elephant_remember), an app that's basically a work around for GNOME Calendar for Mobian and potentially other distributions. Thank you very much, Hank!

Because of a few apps being archived (don't worry, they are going to show up on LinuxPhoneApps too), the total app count of apps.csv is now at 309.

[Read here what (else) happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master) on LINMOBapps this week. And please [do contribute!](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/CONTRIBUTING.md)

#### Linux Phone Apps

We now [have a roadmap](https://github.com/linuxphoneapps/linuxphoneapps.org#whats-the-roadmap), although I am not so sure of "the current template is likely to not change too much" while writing these lines. I think I am going to have to look into metainfo.xml/appdata.xml conversions to TOML first (at least for a bit), and consider what to make of the result to make sure that this is going to be actually true. Not to do step 2 before step 2, but I think that before 1.3 (csv->toml) is approached, there should at least be an idea how things are going to work out. After all, reducing the load of having to monitor listed apps is one key goal of this whole endeavour.
