+++
title = "3 x TI OMAP 3 powered devices to be reviewed here soon"
aliases = ["2011/03/14/3-x-ti-omap-3-powered-devices-to-be.html"]
date = "2011-03-14T16:54:00Z"
[taxonomies]
categories = ["personal", "hardware"]
tags = ["Android 2.1 (Eclair)", "HVGA", "LiMo", "QVGA", "webOS 2.1.0", "WVGA"]
[extra]
author = "Peter"
+++
Right now there are three devices on their way to me, and oddly enough, they all run on the same, dated SoC. Besides that, the three are pretty different: One runs Android (2.1), one webOS (2.1.0) and the third one a LiMo compliant operating system. Display resolutions differ, too: QVGA, HVGA and WVGA, while display sizes vary from 2.8&#8221; to 3.5&#8221;. 
<!-- more -->
While there are tons of posts on my &#8220;to be done&#8221; list that I can write without a single new device, I felt like I would need some new hardware in my hands as the stuff I have is half dead&mdash;at CeBIT my G1's hardware keyboard broke, and my Pre (minus) has a broken headset jack and will be picked up for repair soon.

Wasting some money on low cost devices feels awkward&mdash;but well, for the money I spent on these three devices I could have bought one cheap (non dual-core) super phone, that's why I chose this option.
