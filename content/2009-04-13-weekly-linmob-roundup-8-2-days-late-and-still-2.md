+++
title = "Weekly linmob roundup (8): 2 days late and still 2 short"
aliases = ["2009/04/13/weekly-linmob-roundup-8-2-days-late-and-still-2-short.html", "2009/04/13/weekly-linmob-roundup-8-2-days-late-and-still-2.html"]
author = "peter"
date = "2009-04-13T15:41:00Z"
layout = "post"
[taxonomies]
tags = ["beagle board", "eten m800", "Openmoko", "SHR",]
categories = ["weekly update"]
+++
Well, feels like nothing happened. Ok, I didn't do much this week, needed some vacation, worked on my bike and so on&mdash;but hey: There wasn't much this week. Really.

That's my only dent: <a href="https://aseigo.blogspot.com/2009/04/plasma-on-netbooks.html"> KDE4.4 for netbooks</a>

Now let's check the feedreader whether there was more ...

For those who understand german, <a href="http://">I wrote something</a> about <a href="http://fl0rian.wordpress.com/2009/03/29/a-smart-beaglebrick/">that BeagleBrick Florian Boor built</a>. Then there were several Android rumors, but as long as it isn't at least about hands on something new, I won't cover it. 

<a href="http://blog.shr-project.org/2009/04/hebrew-arabic-and-general-rtl-support.html">SHR has RTL support, e.g. for hebrew and arabic</a> and it runs on the <a href="http://blog.shr-project.org/2009/04/shr-is-running-on-eten-m800.html">Eten M800</a>.<a href="http://laforge.gnumonks.org/weblog/2009/04/07#20090407-gta03_cancelled">And finally, Harald &#8220;LaForge&#8221; Welte shared some thoughts on Openmoko Inc.</a> I am afraid that's it.
