+++
title = "Saying hello again"
aliases = ["2009/06/22/saying-hello-again.html"]
author = "peter"
date = "2009-06-22T10:40:00Z"
layout = "post"
[taxonomies]
tags = ["Android", "OpenMoko", "life", "buzzfix", "palm pre", "T-Mobile G1", "vpnc"]
categories = ["personal"]
+++
I was quite silent the last months, due to focussing on other things in life then linux on mobile devices. I didn't cover the Computex at all, just due to the fact I was just bored of all Android MIDs and netbooks or smartbooks as ARM wants to call the ones that run on their SoCs (in fact ARM just designs the cores, the chips are... and so on, but I hope you know that, anyway) and prefered lying in the sun, drinking lots of beer, writing sad or weird poems from time to time and reading books by american novelists like <a href="http://en.wikipedia.org/wiki/Jack_Kerouac">Kerouac</a> instead. Reading those books about a life of delirious journeys, partys and hitch hiking, I really thought &#8220;fuck those great devices I have, they were so useless on trips like that as their batterys don't last longer than two days in my use.&#8221; And I still try to figure out a way to be able to use my G1 on the first episode of my journey through europe (by foot), as I can only afford it sleeping outside sometimes. Guess I need some kind of solar charger.. as I don't want to use a paper notebook only because I suck at handwriting on the go&mdash;with the G1 I can talk and write and walk and have a beer in the other hand (tho&#8217; that makes writing harder).

On the other hand, my FreeRunner was all that time &#8220;soon off to buzz fix&#8221;, right now it still is (it arrived at <a href="http://www.goldelico.com/Home.html">Golden Delicious</a> on June 13th) and I have the feeling that the recent lay offs at openmoko really affected the community in a bad way, it does not look that vital anymore, think of [FreeSmartphone.Org](http://www.freesmartphone.org), no more releases since the good guys were, let's use honest words, fired.

And Android on G1 ... well it ain't that interesting, just a bunch of Android ROMs (Hero, Rogers, Ion.. )&mdash;that are all the same and aren't the same time&mdash;over at XDA Devs and the <a href="http://forum.xda-developers.com/showthread.php?p=3738632#post3738632">VPNC port to Android</a> doesn't seem to become a comfortable app any time soon, at least right now I don't believe in it&mdash;and it is the app I await most. 

Meanwhile the Palm Pre is on sale in the US, if it wasn't CDMA and if I had money for it and didn´t feel like &#8220;better save it for my trip&#8221;, I would get it immidiately, I don't love its glossy looks that much, but among the <a href="http://opensource.palm.com/packages.html">released sources</a> there is, guess what, vpnc and it is said to be hackable as hell, <a href="http://laforge.gnumonks.org/weblog/linux/mobile/">Harald Welte kind of likes it</a>&mdash;besides that it is faster than all the ARM Hardware I have here right now. 

But stop dreaming, Peter, otherwise you'll be worried that you stuck on your G1 which isn't that bad after all&#8230;.

That's it guys, for now. <a href="http://identi.ca/linmob">Follow my dents</a> or <a href="http://twitter.com/EtlamRetep">follow me on twitter</a>, were i still drop a line about this topic from time to time&#8230;. 

(Besides: I guess I won't be at <a href="http://www.linuxtag.org/2009/">LinuxTag</a> though I planned to be there, because I will lie in the sun in a delirious way, my life is so fucked up right now already, and now there is this <a href="http://www.kieler-woche.de/">Kieler Woche</a>&mdash;it'll kill (or kiel) me even more.)
