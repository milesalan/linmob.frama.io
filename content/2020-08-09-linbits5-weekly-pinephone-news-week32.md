+++
title = "LinBits 5: Weekly PinePhone news / media roundup (week 32)"
aliases = ["2020/08/09/linbits5-weekly-pinephone-news-week32.html", "linbits5"]
author = "Peter"
date = "2020-08-09T11:15:18Z"
layout = "post"
[taxonomies]
tags = ["Manjaro", "Arch Linux ARM", "Mobian", "libhandy", "Phosh", "Purism", "PinePhone", "Lomiri", "Raspberry Pi", "Raspberry Pi Zero", "DIY hardware"]
categories = ["weekly update"]
+++

_It's sunday. Time for a short rounding up of the past week._

This is some the best stuff that happened around the PinePhone this week. _Commentary in italics._
<!-- more -->
### Software: releases and improvements
* [libhandy 0.90 released](https://mamot.fr/@KekunPlazas/104648142113069390). _This is a bigger deal than the version might suggest: The API is now stable._
* [phosh 0.4.3 released](https://social.librem.one/@agx/104624591847644767). _Just a small release, but every release is progress._
* [megi has released a modem power management driver](https://xnux.eu/devices/feature/modem-pp.html). _While this effort is apparently unlikely to be upstreamed in this form, it is an important step for better battery life on the PinePhone._ 
* [Manjaro ARM Alpha2 with Phosh](https://forum.manjaro.org/t/manjaro-arm-alpha2-with-phosh-pinephone/157249). _Moving forward, getting better bit by bit. Also: [More Lomiri Teasing](https://nitter.net/fkardame/status/1291736063682252800)._
* [Arch Linux ARM](https://github.com/dreemurrs-embedded/Pine64-Arch/releases/tag/20200805). _The big feature for me in this one is ["Fix incoming calls not showing."](https://nitter.net/linmobblog/status/1290987799135563776)_

### Worth reading
* Purism: [Librem 5 June 2020 Software Development Update](https://puri.sm/posts/librem-5-june-2020-software-development-update/). _These updates are always late, but still interesting._
* amosbbatto: [The strategic advantages of Phosh for mobile Linux](https://amosbbatto.wordpress.com/2020/08/05/advantages-of-phosh/). _This post is long, but definitely worth reading. I think that the many PinePhone distributions and all the GNU/Linux software that works with Phosh show how great Purism's approach is working already._ 		

### Worth watching
* anino207: [Gearboy Color (GBC Emulator) on the PinePhone with Ubuntu Touch](https://invidious.13ad.de/watch?v=FM2RawI_iE0). _One for the gamers!_
* GeoTech Land: [Manjaro Phosh Impressions on Pinephone](https://invidious.13ad.de//watch?v=3pj-htOwGjM). _A quick look at the current state of Manjaro on the PinePhone._
* N-O-D-E: [Trying to build the ultimate Raspberry Pi computer (Zero Terminal V3)](https://invidious.13ad.de/watch?v=wiJqUWfR90I). _While a Raspberry Pi Zero Pocket computer sounds way less interesting to me now that the PinePhone is available, the keyboard build is quite interesting._
* Purism: [Librem 5 Web Apps](https://invidious.13ad.de/watch?v=rHO43kh6zN8).

### Stuff I did
I was on holiday and took it easy. I played around with some more software on Arch Linux ARM, which now ̣&mdash; with incoming calls working &mdash; is definitely my favourite distribution. Also, I contributed a bit to the Mobian wiki and played with an Arch Linux ARM chroot on UBports but could not figure out Wayland support in the chroot.
