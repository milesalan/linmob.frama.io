+++
title = "IDEA: DocumentViewer for A910 / Rokr E2"
aliases = ["2008/04/idea-documentviewer-for-a910-rokr-e2.html"]
date = "2008-04-20T19:19:00Z"
[taxonomies]
tags = ["A910", "A910i", "development", "ROKR E2", "modding", "ideas"]
categories = ["projects", "shortform",]
[extra]
author = "Peter"
+++

As these EZX phones have no document viewer, I thought about an easy workaround to get one.

Maybe you know all these *2html (pdf2html, doc2html, xlstohtml) tools? Got it?

Indeed. I am thinking about writing a script, which starts these tools and then opens the documents, which were converted to HTML files with the preinstalled Opera Browser.
