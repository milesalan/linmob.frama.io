+++
title = "A910 firmware: Number of features to integrate is proportional to number of problems"
aliases = ["2008/04/a910-firmware-number-of-features-to.html"]
date = "2008-04-22T12:52:00Z"
[taxonomies]
tags = ["A910", "A910i", "brwdaemon", "development", "modding", "opera", "platforms", "vpnc"]
categories = ["projects"]
[extra]
author = "Peter"
+++
Sometime ago I wrote about cross-compiling VPNC&mdash;it wasn't too hard. But it does not seem to work as it cannot use the WiFi connection&mdash;like wget, if I start it from my terminal.
<!-- more -->
Apparently, if I can believe the WiFi-icon, there is some power-saving mechanism, which kills the connection every time when there is no transfer&mdash;applications which are allowed to transfer data are the browserdaemon, the VoIP application, Opera and Java apps.

This means that I'll have to find out, how to get browserdaemon (I think that this might be the way Opera and Java use) to set up a connection (at it's best permanently) or use a script (like tried before A910i was announced) to achieve a permanent WiFi connection.

And all this due to my university. Bad stuff. (BTW: I do not understand, why they do not have a web interface for mobile devices&#8230;). I'll send them a mail ;)

After all it isn't that bad. If I want to create a document viewer, I will have to find a way to start opera manually anyway. As always I will almost go mad during that work, and hope for free software on A910&mdash;this MontaVista/Trolltech/Motorola software is just horrible as there is no documentation (I think, if I am wrong PLEASE correct me / I will check motodev today).
