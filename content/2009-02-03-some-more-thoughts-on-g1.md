+++
title = "Some more thoughts on G1"
aliases = ["2009/02/03/some-more-thoughts-on-g1"]
author = "peter"
date = "2009-02-03T07:38:00Z"
layout = "post"
[taxonomies]
tags = ["Android", "first impressions", "T-Mobile G1", "thoughts"]
categories = ["hardware", "impressions",]
+++
Yesterday the T-Mobile G1 (HTC Dream) was finally released in Germany (and lots of other continental european countries) and of course I had to get my hands on it as soon as possible&mdash;so i did today. 

Before doing so, I thought about how this device would feel in landscape mode, i was wondering how this keyboard would feel as there is this &#8220;big thing with buttons&#8221; on the left side of it, which is in fact quite unusual comparing the G1 to other QTWERTY keyboard equiped smartphones. I wondered whether I would like the camera at all, how the touchscreen would feel, how the device itself would feel, what about fingerprints and all this. 

So I got up early in the morning, just to have a look at the device before university, because I feared that I couldn&#8217;t think of anything else after i discovered the day before that the G1 is available from about 300 euros without a plan&mdash;which is way lower than i expected, and that it is available with an unattractive plan for (considering not to use that plan at all) for about 270 euro&mdash;even less money, still considering to get a brand new device (i am not generally against the G1&#8217;s look, I am not one of those that say it looks old fashioned or bad&mdash;it is a little bit bulky and not that glossy, but if there is one thing i disliked on my Samsung SGH-i780 besides Windows Mobile, it was its glossyness.

The device I got hold of first, was a white one (I won't get a white one, anyway ;) ), which seems to have a different surface than the black one&mdash;it is rather glossy. First impressions were quite positive, the browser is really OK (not as great as the iPhones one, but still great) the touchscreen is really very reactive (you could say capacitive ;-) )&mdash;I thought that maybe panning down the top bar (those familiar to Android will know what I mean) would be difficult&mdash;it isn't at all. Works really as it should. Unfortunately, the device was charging, so it was hard to get a good impression of the keyboard. But the impression i got is quite positive anyway&mdash;the &#8220;big thing with buttons&#8221; gives you a feeling of safety, i didn't fear dropping it&mdash;even typing one handed (which is in fact difficult, anyway. What kind of dissapointed me (I guess you have to get used to it) was the trackball, which I couldn't really use for navigation&mdash;using the touchscreen was a lot faster and more intuitive&mdash;I tend to believe that a touchpad (like on the Samsung SGH-i780) would have been a better solution.. but, as I said: I guess this just needs time to get adjusted to.

Later today I went to a big electronics store here in Kiel, and hey, they really had a G1 in black&mdash;and it wasn't recharged. So I got a better impression of the handling, exspecially of the keyboard, and yes: It's ok for me, even if the &#8220;big thing with buttons&#8221; disturbs a little bit&mdash;my thumbs are apparently big enough. First of all: The black one has different haptics, as it isn't glossy. I have to say that I really prefer the black one, it just feels good. The browsing experience was the same, and I just started the Android Market and installed Opera Mini&mdash;which is the browser I'd use when using GPRS as I don't use the net the much on the go that I'd need a data plan (I pay 24Ct/MB) (at least I don't need a data plan until now, might change with the G1&#8230; ;-) ). Well, these are my first impressions.

I think I'll get that device as soon as there is a hack to root it.
