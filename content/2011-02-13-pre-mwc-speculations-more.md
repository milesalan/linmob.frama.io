+++
title = "Pre-MWC speculations & more"
aliases = ["2011/02/13/pre-mwc-speculations-more.html"]
author = "peter"
date = "2011-02-13T19:43:00Z"
layout = "post"
[taxonomies]
tags = ["Android", "ARMv7", "MeeGo", "MWC2011", "Nokiasoft", "smartphones", "webos", "speculation"]
categories = ["events", "shortform"]
+++
What we'll see at this years MWC won't be too surprising. Top level smartphones will feature dual core ARMv7 SoCs with fullHD capable graphics; WVGA will be replaced with higher resolutions in these devices (qHD or &#8220;Retina&#8221;(DVGA)-displays, 1GB of Ram will acompnie these.

The software side might be more interesting, I hope for some MeeGo smartphones that will actually make it to the market - even though this has become a lot less likely with Nokia's WP7 announcement. Regarding webOS, the only news that might come up are carrier partnership announcements, and Android? Second Gingerbread release with some Honeycomb bits pulled in? Possible.

Personal I am not too interested in all these new devices, as I don't have the dough to buy any of these now - and besides that I am much more interested in getting a tablet device to replace my netbook.

Nontheless I will buy another smartphone very soon - and this will be exiting. (Maybe it's on my Android ports list... Feel free to guess.)
