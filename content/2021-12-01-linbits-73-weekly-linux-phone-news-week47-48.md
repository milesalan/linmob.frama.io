+++
title = "LinBits 73: Weekly Linux Phone news / media roundup (week 47/48)"
date = "2021-12-01T21:57:00Z"
draft = false
[taxonomies]
tags = ["PinePhone", "PinePhone Pro","DanctNIX", "Manjaro", "Fluffychat","Nemo Mobile",  "JingPad", "LINMOBapps"]
categories = ["weekly update"]
[extra]
author = "Peter"
+++
Seven days in Linux Phones went by, this happened: libhandy 1.5.0, Fluffychat 1.0, NemoMobile running on two more devices, postmarketOS adds support for another Snapdragon 845-based phone, LoRa backplate fun and more!
<!-- more --> _Commentary in italics._ 

### Software releases
* libhandy 1.5.0 [has been released](https://gitlab.gnome.org/GNOME/libhandy/-/commit/8fa8306a79215fc6ebf2483145da98bf9b2495ab), introducing support for managing color schemes. fixes to HdyPreferencesPage and HdyTabbar among other fixes.
* Fluffychat [1.0 is out](https://gitlab.com/famedly/fluffychat/-/tags)!

### Distribution releases
* Danct12 has [put out another build of his Arch Linux ARM-based distribution](https://github.com/dreemurrs-embedded/Pine64-Arch/releases/tag/20211129) for PinePhone, PineTab and PinePhone Pro, upgrading to a rc-kernel for all devices, as that has PinePhone Pro support. 
* Manjaro have [released Manjaro Phosh beta 19](https://github.com/manjaro-pinephone/phosh/releases/tag/beta19), their first beta release to support the PinePhone Pro.

### Worth noting
* NemoMobile has been shown running on the [Volla Phone](https://twitter.com/neochapay/status/1465320659756761090) and the [Google Pixel 3a](https://twitter.com/erikinkinen/status/1465054553951330308).
* [First benchmarks of the upcoming Rockchip RK3588 SoC have been posted to r/linuxhardware](https://old.reddit.com/r/linuxhardware/comments/r60lcp/rk3588_is_3_times_faster_than_the_raspberry_pi_4/). _This looks really promising. Power consumption should be fine, because this is going to be produced in an 8nm process. In all fairness, it’s worth noting that the Raspberry Pi 4 performance figures may be lower due to it running a 32bit build of Android._
* The [Shiftphones SHIFT6mq has gained some postmarketOS support](https://fosstodon.org/web/@calebccff/107367859670499698)!

### Worth reading

#### Software progress
* This Week in GNOME: [#20 Colorful Characters](https://thisweek.gnome.org/posts/2021/11/twig-20/).
* Volker Krause: [October/November in KDE Itinerary](https://www.volkerkrause.eu/2021/11/27/kde-itinerary-october-november-2021.html). _Great progress!_
* Claudio Cambra: [Kalendar v0.3.0 out soon, with improved stability, efficiency, accessibility… and a Windows version?? – Kalendar devlog 23](https://claudiocambra.com/2021/11/28/kalendar-v0-3-0-out-soon-with-improved-stability-efficiency-accessibility-and-a-windows-version-kalendar-devlog-23/).
* TSDgeo: [Okular PDF digital signature improvements coming "soon" thanks to NLnet](https://tsdgeos.blogspot.com/2021/12/okular-digital-signature-improvements.html). _Okular is awesome. I really look forward to digital signature support coming to Okular-mobile!_ 
* GabMus: [Block ads in WebKitGtk](https://gabmus.org/posts/block_ads_in_webkitgtk/). _I know that WebKitGtk does not have too many fans, but in a world where almost everything is Chromium and Mozilla dwindling while overshadowing their good work with colorful experiments, this browser engine deserves more love than it usually gets IMHO._ 

#### Hardware
* Liliputing: [Raspberry Pi brings an HP 95LX palmtop PC back to life](https://liliputing.com/2021/11/raspberry-pi-brings-an-hp-95lx-palmtop-pc-back-to-life.html). _Retrofitting old hardware is quite difficult, so this definitely deserves a massive thumbs-up!_
* Martijn Braam for TuxPhones: [A look at Popcorn Computer’s new Pocket P.C.](https://tuxphones.com/popcorn-computer-pocket-pc-linux-pda-first-look/). _AllWinner A64 paired with a 1080p screen. Please read this if you always wondered why the PinePhone only has a 720p screen._

#### PinePhone Mainline progress?
* Martijn Braam: [Running the kernel.org tree on the PinePhone](https://blog.brixit.nl/running-the-kernel-org-tree-on-the-pinephone/). _This is rough, but not really surprising. The PinePhone runs Megi-line Linux, not Mainline Linux. [Please note: This remark is in no way meant as a criticism of Megi's amazing work!]_

#### PinePhone Pro 
* MakeUseOf: [Is the PinePhone Pro the Linux Phone Daily Driver We've Been Waiting For?](https://www.makeuseof.com/pinephone-pro-linux-phone/). _It sure won’t be initially. These things take time, and it’s super important to emphasize that you can’t look at experimental, community driven Linux Smartphones like you would at your regular smartphone that ships with a mature OS._

#### postmarketOS Praise
* Drew DeVault: [PostmarketOS Revolutionizes Smartphone Hacking](https://drewdevault.com/2021/11/26/postmarketos.html).

#### Lora Fun
* JF's dev blog: [Flashing the LoRa backplate for the PinePhone](https://codingfield.com/blog/2021-11/flash-the-lora-pinephone-backplate/).
* JF's dev blog: [A driver for the LoRa backplate for the PinePhone](https://codingfield.com/blog/2021-11/a-driver-for-the-pinephone-lora-backplate/). 

#### GloDroid
* Liliputing: [Glodroid brings Android 12 to the PinePhone (sort of)](https://liliputing.com/2021/11/glodroid-brings-android-12-to-the-pinephone-sort-of.html).

#### Discussions
* GhettoComputers: [Ask HN: Know any non Librem5 PinePhone for Mobile Linux? Legacy devices too!](https://news.ycombinator.com/item?id=29373106). _While part of this is hard to read and may annoy you, there is some productive discussion of devices._

### Worth watching

#### Librem 5 Comparison
* The Conscious Resistance: [Privacy Phone Review: Librem 5 vs. Above.Phone](https://odysee.com/@theconsciousresistance:7/privacyphonereview:b). _Arguably, the camera can work now. It’s still in the process of enablement. That aside, most the criticism seems valid._

#### PinePhone LoRa fun
* jf: [Receiving LoRa messages on the Pinephone](https://video.codingfield.com/videos/watch/6d713488-5469-442c-a40e-961daa3d9636). _Nice!_

#### Criticism
* Twinntech: [Pinephone is a flashlight again](https://www.youtube.com/watch?v=zVr2T3tIZyI). _Sadly, non-working GNOME Software is stuff that I would not even notice, given that I just don't use this. The problem he has there sounds like it might require dropping to the terminal, something that's intimidating to Linux noobs._

#### JingPad
* Massimo Antonio Carofano: [JingPad A1 - Boot to Recovery (Fastboot, Bootloader, Factory reset..)](https://www.youtube.com/watch?v=_20xhIItpQY).
* ShortCircuit: [Is Linux The ANSWER??? - JingPad A1 Tablet](https://www.youtube.com/watch?v=P-14-qlKyHA). _Nice video. Please note though, that this is not KDE Plasma Mobile, it's just based on that tech specs._

#### Shorts
* NOT A FBI Honey pot: [pinephone running waydroid](https://www.youtube.com/watch?v=Ebm8d-w9pTc).

### Stuff I did

#### Content
Nothing.

#### LINMOBapps and Linux Phone Apps
This week I added 
* [Shipments](https://git.sr.ht/~martijnbraam/shipments), a python library and a GTK3 mobile/desktop application for tracking multiple shipments, and
* [Add Hours and Minutes](https://github.com/Akaflieg-Freiburg/addhoursandminutes), a calculator adds times given in hours and minutes. 

More is going to follow this week. Also, stay tuned for an announcement regarding this project soon-ish.

[Read here what (else) happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master) on LINMOBapps this week. And please [do contribute!](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/CONTRIBUTING.md)
