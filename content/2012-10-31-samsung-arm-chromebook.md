+++
title = "A Samsung ARM Chromebook"
aliases = ["2012/10/31/google-has-made-the-chromebook-more-affordable"]
author = "peter"
date = "2012-10-31T12:13:00Z"
layout = "post"
[taxonomies]
tags = ["ARM Cortex A15", "chromebook", "google", "metal", "Video", "Samsung"]
categories = ["shortform", "hardware",]
+++

Google has made the Chromebook more affordable recently, making it the first mass market ARM Cortex A15 device. I will try to get one (and explain why later), [but here is a really great commercial](https://www.youtube.com/watch?v=HfSHoNFCUpY
).
