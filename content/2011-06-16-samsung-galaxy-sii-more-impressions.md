+++
title = "Samsung Galaxy SII - More Impressions"
aliases = ["2011/06/16/samsung-galaxy-sii-more-impressions.html"]
author = "peter"
comments = true
date = "2011-06-16T15:55:00Z"
layout = "post"
[taxonomies]
tags = ["first impressions", "impressions", "Samsung Galaxy SII", "photo-todo", "video"]
categories = ["shortform", "videos"]
+++
This time it's a video, because I didn't feel like writing a long story. More detailed impressions will follow as soon as I have got ideas or questions to answer. If you want to see what I find noteworthy, feel free to check out the <a href="https://picasaweb.google.com/106783261023883900993/SamsungGalaxyS2?authkey=Gv1sRgCIb0pPX6tNi80gE&amp;feat=directlink">latest screen shots on Picasa</a> - I will try to submit some daily.

LINMOBnet on YouTube: [Samsung Galaxy S2 - More Impressions - Part 1](https://www.youtube.com/watch?v=utyOU2qq538), [Part 2](https://www.youtube.com/watch?v=Sdg3wDfIK_o)

