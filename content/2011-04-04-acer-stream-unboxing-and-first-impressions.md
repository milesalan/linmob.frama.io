+++
title = "Acer Stream - Unboxing and first impressions"
aliases = ["2011/04/acer-stream-unboxing-and-first.html"]
date = "2011-04-04T21:39:00Z"
[taxonomies]
categories = ["impressions","videos"]
tags = ["Acer Stream", "Android", "Android 2.1 (Eclair)", "facts", "first impressions", "Unboxing"]
[extra]
author = "Peter"
+++
So I bought myself an Acer Stream, a Qualcomm Snapdragon QSD8250 powered device with an 3.7&#8221; WVGA AMOLED screen&mdash;sounds similar to the HTC Desire and Nexus One&mdash;and well, how shall I put this, it is, but it is cheaper&mdash;I was able to buy it for about 220 Euro incl. shipping.

This low pricing comes as no surprise if you look at facts like the number of forum posts about the Acer Stream&mdash;it didn't sell that well, most likely a lot worse than Acers previous device, the Acer Liquid. This maybe happened due to the design of the very device, which is unlike its predecessor not sleek and almost a classic&mdash;but different, not ugly but.. well&mdash;i don't know how to put it. What comes to your mind when you look at it is that it is pretty huge compared to devices that feature a similar screen&mdash;it's about 4mm wider than the Nexus One and weighs 140 grams&mdash;which is 10 grams more than the Google phone&mdash;but that's it. The overall hardware feels good, but I can't tell whether the touchscreens surface is made of glass or plastic&mdash;I think it's plastic like the rest of the hardware.

LINMOBnet on YouTube: [Acer Stream - Unboxing](https://www.youtube.com/watch?v=uS251c1B18c)

The unit comes with Android 2.1 (Eclair) preinstalled (2.2 (Froyo) Update is available at acer.com/mobile) , with a special interface developed by Acer. Some of you will think &#8220;noooo&#8221; now, but it isn't too bad and if you really don't like it, you can disable it in Settings -&gt; Applications -&gt; User interface. The result is something pretty similar to &#8220;vanilla Android&#8221;, there are a few differences, though: The Dialer and Contacts App are Acer made, the Music Player and eMail Camera App aren't the AOSP ones, either.

<i>More soon.</i>
