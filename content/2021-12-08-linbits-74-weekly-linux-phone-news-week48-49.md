+++
title = "LinBits 74: Weekly Linux Phone news / media roundup (week 48/49)"
date = "2021-12-08T21:32:00Z"
draft = false
[taxonomies]
tags = ["PinePhone", "PinePhone Pro","Phosh","Plasma Mobile","Sxmo","Malware","Nemo Mobile","Waydroid","JingPad", "LINMOBapps"]
categories = ["weekly update"]
[extra]
author = "Peter"
+++
_This week in Mobile Linux:_ A huge Plasma Mobile Gear release, a minor Phosh update, an overhaul to mobile-config-firefox, PinePhone malware, hope for mobile Signal Desktop lovers and more!
<!-- more --> _Commentary in italics._ 

### Software releases
* mobile-config-firefox, the attempt to get a desktop browser to work acceptably well on a smartphone screen, [has seen a new pre-release](https://gitlab.com/postmarketOS/mobile-config-firefox/-/tags/3.0.0_rc1). It supports Firefox >= 91 properly and contains the following changes: The navigation bar is being moved to the bottom, uBlock origin is being installed by default, about:reader is now adaptive and lots for further UI fixes. Check [ollieparanoid’s thread for visual impressions](https://fosstodon.org/web/@ollieparanoid/107394745970284867).
* Phosh 0.14.1 has been released, delivering improvements for lock screen call handling (avatars and DTMF) and convergence (improving docked mode when screen of, a run command prompt (Alt+F2)). Go read the [full release notes](https://gitlab.gnome.org/World/Phosh/phosh/-/releases#v0.14.1) for more!
* Plasma Mobile Gear 21.12 has been released, updating large number of core apps. Make sure to read the release announcement linked [below](#software-progress)!

### Worth noting
* Signal Desktop might be more usable on Linux Phones [now that it has a collapsible side panel](https://teddit.net/r/PinePhoneOfficial/comments/r7w19l/so_full_version_of_signal_now_has_collapsible/).
* It looks like Sxmo might be getting a [swipe-typing keyboard](https://social.librem.one/@zachdecook/107384429641135497) soon!
* Malware is now a thing on the PinePhone (well, sort of): On December 5th, an IRC user posted what was supposed to be a snake game on the PinePhone chat, offering it as a package for Arch/Manjaro. Sadly, it turned out to contain malware that used a known [vulnerability to brick the Quectel EG25-G modem](https://nns.ee/blog/2021/04/03/modem-rce.html). Root permissions, as you may have guessed, where obtained during package install, and the actual malicious component was the obfuscated post-install script. RTP’s video [below](#security) has more info on this. _So, reminder: Don’t install random packages offered by random IRC users you don’t know. Also, kudos to PINE64 for catching this fast and making it public (well, on Discord only afaik, but still)._

### Worth reading
#### Software progress
* Plasma Mobile: [Plasma Mobile Gear 21.12 is Out](https://plasma-mobile.org/2021/12/07/plasma-mobile-gear-21-12/). _Four months of hard work condensed into one post absolutely worth reading – even if you’re not a Plasma lover._
* Jozef Mlich: [Nemomobile in December/2021](https://blog.mlich.cz/2021/12/nemomobile-in-december-2021/). _Great strides forward!_
* This Week in GNOME: [#21 Software Cleanup](https://thisweek.gnome.org/posts/2021/12/twig-21/). _Nice developments!_

#### Software progress at the library level
* Benjamin Otte: [sizable news](https://blog.gtk.org/2021/12/03/sizable-news/). _GTK 4.6 news._
* mclasen: [Pango updates](https://blogs.gnome.org/mclasen/2021/12/03/pango-updates-4/). _Yes, this is not mobile specific, but foundations matter._

#### Non-Linux Software Progress
* Phoronix: [Genode OS 21.11 Now Has Working Intel Gen9+ Graphics, Better PinePhone Support](https://www.phoronix.com/scan.php?page=news_item&px=Genode-OS-21.11). _I wish I had the time to try this!_

#### Linux Phone Buyers Guide
* TuxPhones: [The best Linux phones you can buy right now](https://tuxphones.com/best-linux-phones-buying-guide-2022/). _Really good post! (I’ve been meaning to write something similar, but didn’t get to it – now I don’t have to!)_

#### Shipments
* Liliputing: [PinePhone Pro and PineNote now shipping to developers](https://liliputing.com/2021/12/pinephone-pro-and-pinenote-now-shipping-to-developers.html). _By now, many of these devices have arrived!_

#### Metadata progress
* Matthias Klump: [New things in AppStream 0.15](https://blog.tenstral.net/2021/12/new-things-in-appstream-0-15.html)

### Worth watching

#### Ubuntu Touch
* UBports: 	[Virtual machines? On my Ubuntu Touch? \| Ubuntu Touch Q&A 113](https://www.youtube.com/watch?v=xVmucF8aVok). _Alfred developed a new app, OTA 21 is going to happen, Teleports (Telegram client) problems due to Telegram rushing things, notch support, 20.04 progress, Ayatana indicators landing, core development release roadmap. They also discuss community questions._

#### Retiled launcher progress
* Drew Naylor 2: [RetiledStart demo #5: Pinning, Unpinning, and Resizing Tiles](https://www.youtube.com/watch?v=lvbaCgOvsik). _Nice!_

#### Security
* RTP: [Pinephone News: Snake "Game" Malware Shared!](https://tube.tchncs.de/w/tunn31t5p5R1gZirLMq2TW). _Great video! Minor nitpick: I don't think you really need to read every bit of code before you install it. Don't install random packages, and if you use random PKGBUILD scripts with Arch (or Manjaro), make sure to check those before installing - it should be doable!_

#### Waydroid
* NOT A FBI Honey pot: [a pinephone rant……](https://www.youtube.com/watch?v=yg3SJsypM8M). _Not a rant._

#### GloDroid
* Powen: [Current setup of pinephone android 12](https://twitter.com/powen00hsiao/status/1466740447376965634).

#### JingPad
* ETA PRIME: [JingPad A1 First look World's FIRST Consumer-level ARM-based Linux Tablet](https://www.youtube.com/watch?v=A2NEkRtLOzc). _It's not actually running KDE Plasma though, and it resembles iOS more than Android. That aside, good video!_

### Missed your story?
If we missed an important bit of news, please get in touch. If you want to make sure we don't miss your app release, distro update, blog post or video, just ping us on social media or send me an email! _It's very much appreciated!_

### Stuff I did

#### Content
Nothing, again. I did not manage to write my "Abandoning the Zola approach to LinuxPhoneApps" post. _Maybe I'll manage to do so in the coming week._

#### LINMOBapps and Linux Phone Apps
This week I added 
* [Audio Sharing](https://gitlab.gnome.org/World/AudioSharing), which allows to automatically share the current audio playback in the form of an RTSP stream that can then be played back by other devices, and
* [loop](https://gitlab.gnome.org/danigm/loop), a simple audio loop machine application to create music.

More is going to follow this week.

[Read here what (else) happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master) on LINMOBapps this week. And please [do contribute!](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/CONTRIBUTING.md)
