+++
title = "My Linux Phone Setup (November 2021)"
date = "2021-11-06T20:32:18Z"
updated = "2021-11-12T14:31:00Z"
draft = false
[taxonomies]
tags = ["Pine64", "PinePhone", "Arch Linux ARM", "DanctNIX Mobile", "Phosh", "Mobian", "Plasma Mobile", "libhandy", "setup", "Software choice", "Web Apps",]
categories = ["howto", "software"]
[extra]
author = "Peter"
update_note = "Added a link to the eMMC hardware mod; added footnote to elaborate on the compilation of Rust apps; added note to Axolotl no longer being blurry and postmarketOS Wiki link rregarding the eMMC mod."
+++

I wrote up what I did then use in [January](https://linmob.net/my-setup-with-danctnix-archlinuxarm/). Back then I thought I would update this at the latest after six months, but … well, let's update it now. Back then I only wrote about the PinePhone, and as it's the Linux Phone I use most regularly (and I likely would not use different apps on another device), I'll limit my post to my PinePhone's DanctNIX install again.
<!-- more -->
## What changed?

When I started to write this post, I thought that my setup did not change that much – writing it made me realize this is not true, and a lot of changes have accumulated. But that perception also was not entirely wrong: If I had not had a brief stint with postmarketOS Edge, it would have even still been the very same DanctNIX install, since it just does not break despite me trying hard with all the software I try for LinuxPhoneApps.org. I stopped using Flatpaks, as it had somehow started to use more than 7 Gigabytes on my phone. This did not bother me too much until I required more space to try out [WayDroid](#waydroid), but then I had to do something and removing Flatpaks was the easiest option.

### GNOME Web as default web browser (and as web app runner)

Let's start with a big change, as browsers are apps I think we all use a lot: I made __[GNOME Web](https://apps.gnome.org/app/org.gnome.Epiphany/)__ (epiphany) my default browser: It's still slower than Firefox (which I still have installed), but it got faster enough with the past releases[^1] so that using it is bearable, mainly because it's faster at starting up, which is a welcome benefit. Since I try to mostly use light-weight websites on my PinePhone if possible, it's good enough. Another nice benefit of GNOME Web is gesture navigation to go back and forth. 

Also, with Firefox dropping the SSB feature, my Matrix client __[Hydrogen](https://hydrogen.element.io)__ is now running as GNOME Web web app ([more on that below](#matrix-hydrogen)).


_The following is the mostly the old post, but ordered into categories._

### Secondary browsers
#### Angelfish
> * __Angelfish__ (plasma-angelfish, AUR): The Plasma Mobile browser that I use a lot.

I still have [Angelfish](https://apps.kde.org/angelfish/) installed, but I use it less often these days. It progressed nicely though, its adblocker is great, and on Plasma Mobile it can be used to turn websites into web apps.

#### Firefox
Regarding Firefox, you may remember [my adjusted configuration of mobile-config-firefox](https://gitlab.com/1peter10/mobile-config-firefox/-/tree/proton-tab-hack), which I still use. Also, I am using two plugins in Firefox:
* [uBlock origin](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/) (duh!), and
* [Privacy Redirect](https://addons.mozilla.org/en-US/firefox/addon/privacy-redirect/), to avoid slowly loading, JavaScript-heavy social media websites and get to the content faster.

### Calendaring, tasks and email

#### Calindori
> * __Calindori__ (calindori-git (AUR)): Plasma Mobile calendar. I am going to [set it up to sync](https://dimitris.cc/kde/2020/12/30/Online_Calendars.html), but I didn't not get around to that yet.

I never got around to setting up sync, and these days Calindori is [looking for a new maintainer](https://invent.kde.org/plasma-mobile/calindori/-/issues/18).

#### Now: GNOME Calendar (adaptive)
I switched to the [adaptive downstream](https://source.puri.sm/Librem5/debs/gnome-calendar) build of GNOME Calendar, as that integrates seamlessly with Phosh and GNOME Online Accounts. To install it, I used [Manjaro's PKGBUILD](https://gitlab.manjaro.org/manjaro-arm/packages/community/phosh/gnome-calendar-mobile) &mdash; these days it's also packaged in DanctNIX (gnome-calendar-mobile). Now, this adaptation build of GNOME Calendar is not perfect, e.g. it lacks a day view (just like upstream GNOME Calendar does, where GNOME Shell somewhat delivers a day view), which is something you'd really want on a phone. Also, it does not fit the screen perfectly without `scale-to-fit org.gnome.calendar on` yet. We'll get there, and being able to sync calendars with my Nextcloud is great! 

#### Other option: Kalendar
If I knew how to integrate my Nextcloud calendar set up for it (namely which settings app I have to install) or if I ran Plasma Mobile (which I will certainly give another shot once Modem Manager lands), I would likely use [Kalendar](https://invent.kde.org/pim/kalendar) instead, as it has that neat day view. I have it installed (AUR: kalendar-git) and try to monitor its progression.

#### Evolution
> * __Evolution__ (evolution): Mail client. This is not adjusted for the mobile form factor at all, but once a few elements are hidden, it works ok. I mainly use it, because I had serious bad luck with Geary, which sometimes would not fetch my mails.
 
While Geary has since improved and Geary upstream works great on phones since GNOME 40, I am still using Evolution with scale-to-fit. I just like to have an option for  plain text email and GPG.

#### GNOME To Do
> * __To Do__ (gnome-to-do-mobile, Manjaro package): This is the mobile-friendly downstream of Gnome To Do which basically is a simple To Do manager that integrates nicely with Nextcloud. I had to steal Manjaro's package here, I would have prefered to just steal their PKGBUILD but was unable to find it on their GitLab.

I still use GNOME To Do, but switched to the regular package, and made it work with `scale-to-fit org.gnome.Todo on`. 

### Image viewing

#### Image Viewer (eog)
> * __Image Viewer__ (eog): This really should be preinstalled.

#### Now: Image Roll
I am no longer using `eog`, but switched to [Image Roll](https://github.com/weclaw1/image-roll) (AUR: image-roll) instead, as it just has a few more features that are nice to have. Also, it is written in Rust.[^2]

### Video
#### mpv
> * __mpv__ (mpv): Always good to have this one around.

Still true. Admittedly, I don't really watch video on my PinePhone (or any other phone), which is something that might change once I have the keyboard dock (to have better battery life) or after GStreamer 1.20 lands, which will help with hardware accelerated video playback.

#### Purism Stream
Another app I used while it worked was [Purism's work-in-progress Stream app](https://source.puri.sm/todd/Stream), which is a YouTube app that makes use of invidious instances to preserve user privacy. Currently, it seems to have issues in finding a suitable instance, which sounds fixable – but sadly not for me.

### Music streaming
#### YouTube audio: YouPlay
>* __YouPlay__ (not packaged): A simple YouTube music downloader. I rarely use it, but it was fun to try to write a stupid local installer. 

I did not reinstall [YouPlay](https://codeberg.org/ralfhersel/youplay), since I don't listen to music often and when I do, YouTube is not typically the place I go for music. If you are looking for a YouTube app for Music, I'd still recommend it or [AudioTube](https://invent.kde.org/plasma-mobile/audiotube).

#### New: Spot
__[Spot](https://github.com/xou816/spot)__ (AUR: spot-client) is an excellent app for Spotify Premium subscribers, built in GTK4 and libadwaita. I know, it doesn't make sense to subscribe given I rarely listen to music, but since I am currently subscribed to that service, I use Spot.

### File sync
#### Nextcloud Client
> * __Nextcloud__ (nextcloud-client): Works terribly (scale-to-fit com.nextcloud.desktopclient.nextcloud on helps with initial setup), but I had to start using this as Notes (gnome-notes) would take ages to sync my Notes folder because it does not seem to cache locally. The good thing is: It only has to be setup once.

Still using this for an always reliable file option sync for whenever WebDAV is not working because of bad connectivity.

### Note taking
#### Notorious
> * __Notorious__ (notorious-git(AUR)): Replaced Gnome Notes as my editor when I switched over to the above sync solution. It's fine, but I might try ThiefMD (AUR: thiefmd) as well soon.

Same here, in conjunction with Nextcloud Client this is an important tool for collecting items I might otherwise forget for LinBits or LINMOBapps.

### Passwords and authentication
#### GNOME Password Safe
> * __Password Safe__ (gnome-passwordsafe): Client to use KeePass databases. While it was dreadfully slow with my database earlier, this improved lately, so that I now longer need to use KeePassXC on my phone.

I am still happy with Password Safe. I would like it to be faster, but it works well enough.

#### OTPClient
For OTP, I have added [OTPClient](https://github.com/paolostivanin/OTPClient) (AUR: otpclient), which works well and is a decent and seemingly often overlooked app in the OTP space. Given Numberstations new import and export feature, I might switch to Numberstation eventually though for its integration with Megapixels.

### Messaging

#### Multi protocol (Pidgin)
>* __Pidgin__ (pidgin): This is a piece of really old GTK2-software, but as its _libpurple_ is what Chatty is building upon, I decided to add it to my setup. For it to run, Chatty has to be killed, which is why I had to add a script and a launcher to easily do this. Pidgin is interesting as it supports many services such as Discord, Signal, Threema etc. via plugin. I'll report back on this once I've set up some of these services.

Sadly, I never got to a point to share a success story - while I got Discord and Signal to a somewhat workable state, it did not work well enough: Receiving messages and finding chats was hit and miss. Therefore, I no longer have Pidgin installed.

#### Matrix: Hydrogen

While I was using [Hydrogen](https://hydrogen.element.io) (in Firefox) in January and I am using it now again, this has not been the case for the entire time. I have dabbled with __[Nheko](https://nheko-reborn.github.io/)__, which is pretty great since their 0.8.2 release, and __[Fractal Next](https://gitlab.gnome.org/GNOME/fractal/-/tree/fractal-next)__, the GTK4/libadwaita future of Fractal (which is not really ready yet), which is perfectly alright for unreleased software.
There's only one reason I switched away from Nheko and went back to Hydrogen: Nheko is only available on the AUR, and requires a (time-intensive) rebuild whenever one of its dependencies gets upgraded, which is happening quite often on Arch Linux ARM.

#### New: Axolotl (Signal client)

Instead of dabbling with Pidgin to obtain a working Signal client, I ended up installing [Axolotl](https://github.com/nanu-c/axolotl) (AUR: axolotl-bin). Installing it is easy enough from the AUR, which is recycling the [excellent Debian package work done by Arno Nühm](https://github.com/nuehm-arno/axolotl-mobian-package). <del>Sadly, anything Electron still comes with some XWayland blurriness by default,</del>While Axolotl from 1.0.7 going forward is no longer blurry (thanks Ferenc for your [great work](https://github.com/nanu-c/axolotl/pull/619) and telling me about it!), I opted to go with a GNOME Web based setup to interface with it. To do so I downloaded the [axolotl-server.desktop](https://github.com/nuehm-arno/axolotl-mobian-package/blob/main/axolotl-server.desktop) file from Arno's repo and put it into `/etc/xdg/autostart/`, opened `127.0.0.1:9080` in GNOME Web and tapped "Install Site as Web Application". This way, Axolotl is always running, and I don't need to toggle Squeekboard in order to type something. Also, once I use the phone, I'll get a notification if I have new Signal messages.[^3]

#### New: XMPP: Dino (feature/handy branch)

For XMPP I am relying on the [adaptive/mobile-friendly libhandy-enabled branch of Dino](https://github.com/dino/dino/tree/feature/handy), which I also use on my desktop. The main reason for this is OMEMO encryption and interoperability with my other clients - while Chatty can support XMPP and also OMEMO, it did not play well with my Dino install on desktop, so that I did not see messages I wrote in Chatty on Dino and vice versa.[^4]

#### New: KDeltaChat (DeltaChat clients)

[KDeltaChat](https://git.sr.ht/~link2xt/kdeltachat) (kdeltachat-git) is a Kirigami client for DeltaChat, the email-based messenger, so that you don't have to use the Electron-based DeltaChat desktop app (which also works on mobile). I recommend giving it a try, I'm at delta@linmob.net.

#### Telegram Desktop
> * __Telegram desktop__ (telegram-desktop):  I am not a massive fan of this service, but there are activities in my life that basically require using this. While I also have Telegram set up in Chatty (and thus Pidgin, which is way more useful as an interface than Chatty currently is), I sometimes start this app to browse Telegram. With a [modified .desktop file](https://framagit.org/linmobapps/linmobapps.frama.io/-/raw/master/desktop-files/fixes/telegramdesktop.desktop) it works just fine.

#### Now: Telegrand

I am no longer running Telegram desktop, but use a git-build of [Telegrand](https://github.com/melix99/telegrand) (a GTK4/libadwaita telegram client; AUR: telegrand-git), which I would only recommend if you don't use Telegram a lot for now.

### News and Podcasts
#### NewsFlash
> * __NewsFlash__ (newsflash-git (AUR)): I use this to follow the news, as I am an RSS addict since I was first told about RSS. 

I removed NewsFlash (I just did not use it a lot and only realized I still had it installed during updates) and just use [Miniflux](https://miniflux.app)'s web interface in GNOME Web these days. This does not mean that NewsFlash is not a great app, it definitely is - and you can get it from the Arch Linux ARM community repo these days, the package name is `newsflash`.

#### GNOME Podcasts
>* __Podcasts__ (gnome-podcasts): I recommend importing an OPML file you exported from your previous Podcast client for this app, as you would have to hunt for feed URLs (which for some podcasts is unacceptably hard) otherwise.

#### Now: Kasts
These days I am using __[Kasts](https://invent.kde.org/plasma-mobile/kasts)__ (AUR: kasts-git) instead, as it has tons of neat features, including Podcast discovery via podcastindex.org or chapter markers. Also, it manages to inhibit PinePhone suspend, so that you can actually listen to full episodes. IMHO, Kasts is one of the best apps for the PinePhone, period.

### Social Media

#### Twitter: Cawbird
> * __Cawbird__ (cawbird-git (AUR)): Twitter client. I switched to git when the 1.3.x release appeared.

I am now running the normal package from the community repository (currently 1.4.2), and it's still great and quite got a bit smoother.

#### Reddit: Quickddit
>* __Quickddit__ (quickddit (AUR)): Reddit client, originally developed for Ubuntu Touch. It's quite nice, and I just prefer it's wider feature set to Giara, which would be more native on Phosh.

#### Now: Headlines (previously Gtkeddit)
No longer installed. For a while I went with [Giara](https://giara.gabmus.org/), these days I am using __[Headlines](https://gitlab.com/caveman250/Headlines)__, a GTK4/libadwaita Reddit client written in C++, which does not match the feature set, but is smoother enough to give up on Multireddits.

#### Mastodon: Tootle
>* __Tootle__ (tootle-git (AUR)): Mobile friendly Mastodon client. It's not perfect, but good enough for some light Mastodon interactions.

I am still using Tootle. For a while I was using the 2.0-alpha1 GTK4/libadwaita variant of it, but since then libadwaita development has outrun Tootle. Also, with Arch shipping Vala 0.54.3 currently, it's generally difficult to build Tootle on Arch Linux ARM.

I also use [Pinafore.social](https://pinafore.social) as a GNOME Web app from time to time, as it allows to differentiate between all notifications and mentions. 

### Utilities
#### Backups
> * __Backups__ (deja-dup): To restore my backup and to backup my current setup. An alternative to this would be "Pika Backup", which is based on borg instead of duplicity.

I am still running deja-dup, having upgraded it to the deja-dup-git AUR package a while ago, which uses GTK4 and libadwaita and fits the screen perfectly. Once the release happens (likely around the time GNOME 42 gets release, but I am just guessing here), I'll add it to [LinuxPhoneApps](https://linuxphoneapps.org).

#### GNOME Weather
>* __Weather__ (gnome-weather-git (AUR)): The mobile patches that have been available in Gnome Weather for PureOS and Mobian in a while seem to be making it upstream. The current version has issues when you install.

Still using Weather (now `gnome-weather` from the extra repo).

#### New: Birdie 

__[Birdie](https://github.com/Dejvino/birdie)__ is a simple alarm clock.

#### New: GNOME Disks
__[Disks](https://gitlab.gnome.org/GNOME/gnome-disk-utility/)__ (gnome-disk-utility) is fully mobile friendly. I rarely use it, but I keep it installed for these rare use-cases.

#### New: GNOME Maps
__[GNOME Maps](https://apps.gnome.org/app/org.gnome.Maps/)__ (gnome-maps) is one of these apps, that's partially there – if I required navigation, it would not work well for me, as that part does not fit mobile screens well. I am just using it to check were I am if I ever get lost or to plan my routes manually, and for that it's more than adequate.

#### New: KDE Itinerary

__[KDE Itinerary](https://apps.kde.org/itinerary/)__ (itinerary) is a digital travel assistant supporting many features, including importing bookings and tickets. I used to it to travel by train in Germany successfully. It's great.

#### New: KTrip

__[KTrip](https://apps.kde.org/ktrip/)__ is the matching travel companion app, that allows you to find journeys between specified locations. I used the [Manjaro PKGBUILD](https://gitlab.manjaro.org/manjaro-arm/packages/community/plamo-gear/ktrip) to install it.

#### New: PinePhone Compass
__[PinePhone Compass](https://gitlab.com/lgtrombetta/pinephone-compass)__ is just a compass app. I mostly use it for fun.

#### New: Phosh Antispam and Visual Voicemail

__[Phosh Antispam](https://gitlab.com/kop316/phosh-antispam)__ and __[Visual Voicemail](https://gitlab.com/kop316/vvmplayer)__ are two great utilities developed by _kop316_ that do just what their name suggests. I have them mainly installed to check them out, as my cellular carrier does not even support Visual Voicemail. For Antispam I would like to have a Blacklist, as I am to lazy to reformat all phone numbers in my contacts to make sure white listing works - spam calls are way less of a problem in Germany and are limited to a handful of reoccuring numbers in my case.

#### New: Siglo (InfiniTime client)
__[Siglo](https://github.com/alexr4535/siglo)__ (siglo) is an app I only use when something went wrong, or, to be more precise, when I managed to not charge my PineTime in too long, so that it turned off. Connecting it with Siglo sets the time, which is quite an important feature on a watch, right?


### No longer installed and not replaced
#### SIP and more: Jami
> * __Jami__ (jami-gnome): [Jami](https://jami.net/) is an interesting service (you may know it under its previous name GNU Ring); unfortunately I don't know anyone who uses it. It can also be a SIP app, altough calls did not work out-of-the-box for me (and I did not have the time to tinker yet).

I removed Jami after GNOME Calls shipped working SIP support.

#### Games: SuperTuxKart
> * __SuperTuxKart__ (supertuxkart): Once [`~/.config/supertuxkart/config-0.10/`](https://framagit.org/linmobapps/linmobapps.frama.io/-/raw/master/config/supertuxkart/config.xml) and the [launcher](https://framagit.org/linmobapps/linmobapps.frama.io/-/raw/master/desktop-files/fixes/supertuxkart.desktop) are adjusted, the game works ok, at least with the less graphically challenging maps. I think it's just running on the CPU and not using the GPU, but I am not sure about that. Also, the proximity sensor interferes with it, which is annoying, but there seems to be a [simple way to fix this](https://forum.pine64.org/showthread.php?tid=11168&pid=77588#pid77588). 

No longer installed. Sorry, I just don't game.


#### Display tweaks
>Also, I installed:
> * wlr-randr and yad to use [the script in this old blog post by Purism](https://puri.sm/posts/easy-librem-5-app-development-scale-the-screen/). The script is quite helpful, and I look forward to doing more with yad in the future. Also, tor and tor-socks are installed, but that will follow later.

This script is no longer necessary, as the GNOME Control Center app (settings) now has a similar feature.

### WayDroid

As mentioned above, I have [WayDroid](https://waydro.id/) (waydroid) installed. I use it in single window mode - when I use it, which is quite rare. Aside from its startup time, WayDroid is quite fast once it's running. Sadly, it's also good at depleting the battery.
Here's what I installed on WayDroid:
- FDroid,
- Aurora Store (a front-end for Google Play),
- Discord (I wish there Web app was somewhat responsive).

I rarely use WayDroid though. Native apps are what works better, and what's necessary for an attractive platform.[^5]

## Further modifications

Let's keep this short:

* I am running [Biktorgj's Modem firmware](https://github.com/Biktorgj/pinephone_modem_sdk),
* this [is my /etc/environment](https://github.com/1peter10/linuxphone-tweaks/blob/main/etc/environment), 
* and I use [this](https://git.kescher.at/jeremy.kescher/kescher-archpp/src/branch/testing/eg25-autorestart) to make the modem more reliable. 

I also replaced `gstreamer` with `gstreamer-git`.

## What's keeping me from using the PinePhone as a daily driver?

Aside from occasional reliability issues, and a few non-essential proprietary services (but that might even work with [microG](https://microg.org/) on WayDroid), I might miss[^6], it's mostly three things:

* Battery life, and
* Speed,[^7]
* Camera.

While general PinePhone battery life is okay, screen on time is just not long enough for my use - charging throughout the day would be necessary, but is not always possible. I hope that the PinePhone Pro is going to be better with regards to battery life, but even if it does not, it will at least most likely fix my other issues. The PinePhone Keyboard accessory shows that an external battery case is possible, so a fix is nigh, if not available yet.

To conclude: _We're getting there!_


[^1]: GNOME Web 40.3 powered by WebKitGTK 2.34.1 is what I am currently using.

[^2]: _Please note:_ I stopped compiling Rust apps (like image-roll, telegrand, fractal-next, spot-client or gnome-podcasts and libraries like libdeltachat on the PinePhone and instead compile them on my [ARM Chromebook](https://archlinuxarm.org/platforms/armv8/rockchip/asus-chromebook-flip-c101pa), which also runs Arch Linux ARM. The same goes for everything Electron (mainly for disk size reasons), or large libraries like [tdlib](https://github.com/tdlib/td). If you want to know how to setup the Arch User Repository (AUR) and ZRAM (which is also necessary on my Chromebook), see [this old post of mine](https://linmob.net/pinephone-building-plasma-mobile-apps-from-the-aur/).

[^3]: Tapping the notification does not open the Web App, sadly - but that's okay. If you are depending on push notifications: Those are a hard challenge (just look at Google-free AOSP ROMs) and are unlikely to happen soon. That said, if you are as much as an phone addict as I am, you're looking at that screen multiple times per hour anyway, so do you really need push notifications?

[^4]: If you want to run Dino, you can use [my PKGBUILD script](https://framagit.org/linmobapps/pkgbuilds/-/tree/main/dino-mobile-git) to install it.

[^5]: Make no mistake, I am very happy that Waydroid is a thing - for phones and also the Linux desktop. I contributed to their fundraiser, and you should too.

[^6]: I acknowledge I am privileged here since I don't need MMS or banking apps to work, which many other people do need.

[^7]: With regard to speed: PinePhone feels faster and smoother than it did in January, and in theory I could attempt the [eMMC hardware modification](https://fosstodon.org/@PINE64/107233869420315079) by symmetrist ([documented on the postmarketOS wiki](https://wiki.postmarketos.org/wiki/PINE64_PinePhone_(pine64-pinephone)#eMMC)) that's being discussed currently.
