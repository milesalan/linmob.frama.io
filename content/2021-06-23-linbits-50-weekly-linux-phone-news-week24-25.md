+++
title = "LinBits 50: Weekly Linux Phone news / media roundup (week 24/25)"
date = "2021-06-23T20:55:00Z"
[taxonomies]
tags = ["PINE64", "PinePhone", "LINMOBapps", "PineTalk", "Ubuntu Touch", "postmarketOS", "Nemo Mobile"]
categories = ["weekly update"]
[extra]
author = "Peter"
+++

_It's Wednesday. Now what happened since last Wednesday?_

A new NemoMobile image, 300 booting devices and a backup tool for postmarketOS and more!<!-- more --> _Commentary in italics._

### Worth noting

* postmarketOS has [reached 300 booting devices](https://twitter.com/pmOS_devices/status/1407277205051301888).
* The Mobian Wiki has a great page on [Japanese IME](https://wiki.mobian-project.org/doku.php?id=japanese-ime) now.

### Worth reading

#### Software development
* Jozef Mlich: [Own Nemomobile image](https://blog.mlich.cz/2021/06/own-nemomobile-image/). _I really find Jozef's progress reports quite inspiring._
* Claudio Cambria: [Week 2 on Kalendar (GSoC 2021)](https://claudiocambra.com/2021/06/20/week-2-on-kalendar-gsoc-2021/). _Nice progress! I'll need try that "Add event" dialogue on a phone, though._
* Visvesh's blog: [An Update on my GSoC project](https://visveshs.blogspot.com/2021/06/an-update-on-my-gsoc-project.html). _Visvesh is working on [Health](https://gitlab.gnome.org/Cogitri/Health)._
* Linux Smartphones: [Purism shows off new features coming to PureOS for the Librem 5 smartphone (camera software, wireless toggles, screen rotation and more)](https://linuxsmartphones.com/purism-shows-off-new-features-coming-to-pureos-for-the-librem-5-smartphone-camera-software-wireless-toggles-screen-rotation-and-more/). _This is Brad's take on the Purism blog post we had in the last installment._

#### Linux Phone Deatchmatch ;-)
* ThatGeoGuy: [Librem 5 Evergreen vs. Pinephone (Part 2 of ???)](https://thatgeoguy.ca/blog/2021/06/22/Librem-5-Evergreen-vs-Pinephone/). _An interesting second part, check out the [HN comment thread, too](https://news.ycombinator.com/item?id=27605712)._

#### PinePhone keyboard corner
* xnux.eu/log: [Pinephone keyboard's firmware I2C interface](https://xnux.eu/log/#041). _Great post by Megi!_


### Worth listening
* PineTalk [011: Lounging with Linux Lounge](https://www.pine64.org/podcast/011-lounging-with-linux-lounge/). _Ezra interviews Linux Lounge, a Linux-centric content creator, and we discuss the PINE64 June Community Update. Also we answer a question regarding "suitable beginner software projects", which might be interesting._

### Worth watching

#### Ubuntu Touch avenue
* UBports: [Ubuntu Touch Q&A 102](https://www.youtube.com/watch?v=IGQ2iuZ9oPw). _It's Dalton and Alfred again, talking about Ubuntu Touch progress. Also, Dalton tells a story about the nightmares he endured on Android that led him to end up on and stick with Ubuntu Touch._
* Low Orbit Flux: [PinePhone – Linux Smart Phone Ubuntu Touch Install Test FINAL](https://www.youtube.com/watch?v=-DeOgkLbdYo), _Long, but with music._
* anino207: [Ubuntu Touch Tablet Experience - Lomiri](https://www.youtube.com/watch?v=k5Lfj51h_0M). _Awesome explainer of Ubuntu Touch on tablets!_
* CyberPunked: [Ubuntu Touch - OnePlus 5 / Cheeseburger (2021-06-18)](https://www.youtube.com/watch?v=gb7KadUIj50).

#### App corner
* Martijn Braam: [postmarketOS Backups 0.2.0](https://www.youtube.com/watch?v=4neYnrLgTy8). _This app works with apk, the Alpine package manager, so it's really focussed on postmarketOS, but it's still awesome to see some new developments in this area._
* Geotechland: [Foliate Linux Ebook Viewer Review](https://odysee.com/@geotechland:6/foliate-linux-ebook-viewer-review:a). _Included because it also works on the PinePhone._

#### Tutorials
* kdlk.de: tech: [PinePhone Essentials: Install a PinePhone OS on a SD-Card](https://www.youtube.com/watch?v=wgg8yA_avqo). _The umpteenth video on using Balena Etcher! Just in case anyone cares: You could also use [GNOME Disks](https://wiki.gnome.org/Apps/Disks) or [USBimager](https://bztsrc.gitlab.io/usbimager/) for this._

#### Development streams
* Caleb Connolly: [postmarketOS and Linux Mainline on the OnePlus 6: Debugging kernel driver probing for 5.13](https://www.youtube.com/watch?v=ds4pwC9NsQg),

#### Foreign Languages
* Alexander Stetyukha: [Pinephone смартфон для любителей GNU/Linux (обзор)](https://www.youtube.com/watch?v=wIG9MXKZj-0). _This video is a demo of Megi's latest p-boot-demo image and also of GloDroid and the automatically translated subtitles are fun to read._

#### Grumpy stuff
* Twinntech: [Pinephone the dreaded MMS stuck -no text incoming how to work around the issue.](https://www.youtube.com/watch?v=m-GTjMKVZaE)

### Stuff I did

#### Content

None, sorry. Busy times :( I have started writing something though, so there's hope for some content later this week.


#### LINMOBapps
LINMOBapps did not get much love this week, either. I have added a few apps: 

* [Gnote](https://gitlab.gnome.org/GNOME/gnote), a Tomboy inspired notes app, that scales well (reviewed release 40.1), only the settings menu does not fit the screen properly. But: Opening notes is tedious and requires the use of the enter key on the software keyboard,
* [postmarketOS Backup](https://gitlab.com/postmarketOS/postmarketos-backup), a backup application that integrates with apk to quickly backup and restore your system state in postmarketOS and
* [Pinephone Compass](https://gitlab.com/lgtrombetta/pinephone-compass), a proof of concept of a simple compass app for the Pine64 Pinephone.

[Read here what (else) happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master). 
Please [do contribute!](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/CONTRIBUTING.md)
