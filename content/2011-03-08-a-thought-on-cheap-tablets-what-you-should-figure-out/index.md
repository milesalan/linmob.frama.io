+++
title = "A thought about cheap tablets - what you should figure out before buying"
aliases = ["2011/03/a-thought-on-cheap-tablets-what-you.html"]
date = "2011-03-08T12:24:00Z"
[taxonomies]
categories = ["hardware", "commentary"]
tags = ["advice", "Android tablets", "pierre cardin /shenzhen"]
[extra]
author = "Peter"
+++
I am still about to get a tablet, and every time a new cheap tablet pops up, I am considering it because I like to make a bargain. Of course, a cheaper tablet always lacks some features when compared to a more expensive one - be it the brand or more. 
<!-- more -->
If you are considering cheap tablets, you are apparently not too much about brands, and if you are willing to import such a device, you shouldn't care about warranty. Often there are some more features that aren't quite as good as with the more expensive alternative: These are <b>CPU / GPU</b> performance, amount of <b>RAM</b>, <b>weight</b>, <b>touch</b> screen quality, <b>3G</b>, <b>battery</b> life - if the, say, china tablet not made/sold by a top brand at a drastically lower price, you have to expect one or more of the named features being inferior

<table cellpadding="0" cellspacing="0" class="tr-caption-container" style="float: right;margin-left: 1em;text-align: right"><tbody><tr><td style="text-align: center"><img src="CIMG0058.jpg"  /></td></tr><tr><td class="tr-caption" style="text-align: center">Pierre Cardin PC1018&#160;9.7&#8221; Tegra2 tablet</td></tr></tbody></table>

Independent of your use case, you shouldn't go for an Android tablet that has specs inferior to: 500+MHz ARM11 CPU, 256MB Ram, WiFi, Android 2.1 - as battery life is usually not on the average spec sheet (or mentioned for fictional usage cases), and weight varies depending on the form factor, I don't give you numbers here. Of course, tablets may still work for you, if you just want to use them as a digital picture frame or an e-reader - but still, it won't be fun to use them as build quality will be most likely crap, too.

If you want to use your tablet for the web, you might need Flash for a full web experience. If you want to have a good Flash experience, with Flash 10.1 for Android, you will need a tablet that has an ARMv7 (ARM Cortex A8, ARM Cortex A9) SoC and runs Android 2.2 (Froyo) - and if you want to play safe and have a snazzy device, 512MB RAM are better for you (a device like <a href="http://armdevices.net/2011/03/02/kinstone-95-7-arm-cortex-a9-amlogi-single-core-tablet/">this one</a> shows that this doesn't mean you'll have to pay a fortune).

But then, again, great specs don't make a great tablet user experience yet. At the Pierre Cardin booth at CeBIT I was told that their Froyo running 9.7&#8221; Tegra 2 tablet has a battery life of 3-4 hours - which is way inferior to what brands like Samsung, Motorola, LG or Apple offer. You may argue, that Froyo isn't optimized for Tegra 2 devices - and you are damn right, Honeycomb is Tegra 2 optimized and may increase battery life, but still: You can't tell when you'll get Honeycomb on your cheap Tegra 2 (or in general dual core ARM) device - though unfortunately you can't be sure of fast Android updates on your top brand (tablet) device, anyway. 

_Still, it all depends on your use case. Most of us don't really know what their use case of a new device class is. So get a cheap device or a more expensive one that you can return to find this out - and then make your decision._
