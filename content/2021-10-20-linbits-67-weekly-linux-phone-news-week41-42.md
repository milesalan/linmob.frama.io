+++
title = "LinBits 67: Weekly Linux Phone news / media roundup featuring PinePhone Pro (week 41/42)"
date = "2021-10-20T22:00:00Z"
draft = false
[taxonomies]
tags = ["PinePhone", "PinePhone Pro", "Librem 5", "Manjaro Phosh", "PINE64 Community Update", "Maui",]
categories = ["weekly update"]
[extra]
author = "Peter"
+++

_Summarizing the past week in Linux on Mobile:_ 
KDE turns 25 and ships Plasma 5.23, Jolla turns ten and announces a Sailfish X discount, the Maui project reports great improvements, Manjaro release the 17th beta of their Phosh flavour for the PinePhone. Oh, and the PinePhone Pro happened! <!-- more --> _Commentary in italics._ 

## PinePhone Pro Special

In order to make the news of the past two weeks news more digestible, this weeks weekly update is split into two parts: This first part, that focuses on the PinePhone Pro and is in many ways a follow-up to my [Initial thoughts post](https://linmob.net/thoughts-on-the-pinephone-pro/), and a "regular weekly update", minus the volume of PinePhone Pro content. _Read it now_ or _[skip to the regular update!](#standard-update)_


### Reddit (r/linux) AMA about the PinePhone Pro

Lukasz Erecinski, PINE64's community manager, [did an  AMA on r/Linux](https://reddit.net/r/linux/comments/qald3w/pinephone_pro_was_announced_last_week_ama/). Since it's quite long, here's what I found particularily interesting:
  * Regarding the closed source TF-A blob that makes suspend work, Lukasz replied that PINE64 are talking about this and a few other things to RockChip, the SoC vendor.
  * PINE64 don't have plans for 5G (which would be something for a future device) support yet, same for dual SIM. Regarding band support, which may be disadvantageous in some regions, Lukasz explains that the EG25-G (the G is for "global") was chosen in order to only have to make one SKU.
  * They also don't plan for a future RISC V Phone. _I guess the first step would be a reasonably powerful RISC V single board computer._
  * Regarding redesigned, external kill switches: Also no plans.
  * PINE64 also does not have pans for a "flagship device". The aim is to provide devices that can run a full Linux stack with open drivers, which is not possible with flagship SoCs. 
  * Estimates on battery life are not yet possible, as developer units with the RK3399S are only shipping next week (current pre-production devices are using the non-binned, not voltage locked RK3399).
There's also this interesting bit on the regular PinePhone (which is of course staying around): 
  * After the Community Editions, PINE64 actually made money on the PinePhone which was funneled into creating the PinePhone Pro.
_You might also want to search for "Daily Driver", the term I've learned to dread the most._


### Real life video of a PinePhone Pro prototype
* Martijn Braam: [The PinePhone Pro](https://spacepub.space/w/7BAJhAaBKwfhMuUVaMWGAd) _This is the one video you must watch. No spoilers!_ (Also on [YT](https://www.youtube.com/watch?v=pCxDcMdr_fo).)


### Destination Linux 248: Interview with Lukasz Erecinski
This is a great interview with Lukasz, that explains a lot. Did you know that PINE64 make up 8% of Rockchips' business? Now you know, and there's more - just watch it.
* Destination Linux: [248: PinePhone Pro: Exclusive Interview with Pine64](https://youtu.be/xAi3OT2_Ah8?t=840).

### PinePhone Pro podcasting highlight
* PineTalk [S02E02: The big PP](https://www.pine64.org/2021/10/16/s2e02-pinetalk-the-big-pp/). _Great discussion about the PinePhone Pro!_
* [make sure to also check out the other podcasts under "Worth Listening", as they all talk about the PPP too!](#worth-listening)

### Video Reflections on the Announcement                          
* Brodie Robertson: [Pinephone Pro: Mobile Linux To The Next Level](https://www.youtube.com/watch?v=iseIxRgLhdM). _Great video. If he were to ask me, I'd recommend the Pro even for messing around. It's likely going to be just more fun._
* Geotechland: [Pinephone Pro - The Year Of The Linux Smartphone?](https://tilvids.com/w/7cfb08f1-077d-4766-9eb0-dacb8f0407e8)
* Linux Lounge: [How Powerful Will The PinePhone Pro Be?](https://tilvids.com/w/uct4ord1QswnQSDWB54yC6)
* Niccolò Ve: [PINEPHONE PRO ANNOUNCED! Specs and Features!](https://tube.kockatoo.org/w/69dd4dd8-65d5-4d34-ab68-3b200ed477cf)
* TechHut: [PinePhone Pro - Mobile Linux is getting AWESOME](https://www.youtube.com/watch?v=ekGCPb-Jlko).
* The Linux Experiment: [PinePhone Pro - Do Linux Phones have their flagship?](https://www.youtube.com/watch?v=Ar8JdKj5Yv8)
* Techlore: [PinePhone Pro is HERE — Is it a worthy upgrade from iOS & Android!?](https://www.youtube.com/watch?v=Ky1IvvRHIpw)
* Twinntech: [pinephone pro released](https://www.youtube.com/watch?v=BQ2lnJTLup4). _I don't really know why I include this here._

### Technical details
* xnux.eu log: [Pinephone Pro – A Quick Review](https://xnux.eu/log/#047). _Great post by Megi. Please note that the voltage measurements refer to a pre-production PinePhone Pro with RK3399, not RK3399S._

### PinePhone Pro Kernel Progress
* xnux.eu log: [Pinephone Pro – support merged into my kernel tree](https://xnux.eu/log/#048). _There are some issues, but honestly, this is not looking too bad._

### Tech Blogs
* TuxPhones: [PinePhone Pro released: specs, pricing and very first impressions](https://tuxphones.com/pinephone-pro-linux-phone-specs-pricing/).
* Liliputing: [PinePhone Pro is a faster Linux Smartphone for $399](https://liliputing.com/2021/10/pinephone-pro-is-a-faster-linux-smartphone-for-399.html).
* PineGuild: [PinePhonePro with Rockchip RK3399 is coming](https://pineguild.com/pinephonepro-with-rockchip-rk3399-is-coming/).
* Phoronix: [PinePhone Pro Announced As New Linux Smartphone](https://www.phoronix.com/scan.php?page=news_item&px=PinePhone-Pro).

## Standard update

### Software releases

* Manjaro ARM Beta 17 with [Phosh for the PinePhone has been released](https://forum.manjaro.org/t/manjaro-arm-beta-17-with-phosh-pinephone/86898). Notable changes  include the addition of an adaptive build of GNOME Calendar, Phosh Antispam and Numberstation. _I'll need to check this one out, for GNOME Calendar alone._

### Worth noting
* Midstall Software, the developers of Expidus OS, have [shared a 6 months plan from now to March of 2022](https://twitter.com/MidstallSW/status/1448444424313192454). They are also going to release another pre-alpha test image soon.
* KDE turns 25 and have released a [25th Anniversary Edition](https://kde.org/announcements/plasma/5/5.23.0/). _Congratulations!_

### Worth reading
#### PINE64 Community Update
* PINE64: [October Update: Introducing the PinePhone Pro](https://www.pine64.org/2021/10/15/october-update-introducing-the-pinephone-pro/). _Aside from the big news (see above) I am glad to see the Keyboard Accessory landing soon!_

#### Development progress
* project insanity: [postmarketOS development status 2021](https://blog.project-insanity.org/2021/10/11/postmarketos-development-status-2021/). _I missed this for last weeks LinBits, but it's interesting, containing progress on a number of postmarketOS ports._
* This Week in GNOME: [#14 Well-Rounded](https://thisweek.gnome.org/posts/2021/10/twig-14/). _More movement towards GTK4 and LibAdwaita!_
* Mauikit.org: [Maui Report 16](https://mauikit.org/blog/maui-report-16/). _Bug fixes (5x faster app startup) and features... Nice!_

#### Life hacks
* Dorota Czaplejewicz for Purism: [Speak to me!](https://puri.sm/posts/speak-to-me/). _More stuff you can do with your Linux Phone! ;)_
* xnux.eu log: [How to adapt multi-boot image to your needs](https://xnux.eu/log/#046). _A nice guide, that should help with p-boot in general._

#### Changing the World
* OpenSUSE news: [Hospital to run GNU Health, openSUSE](https://news.opensuse.org/2021/10/20/hospital-to-run-gnu-health-openSUSE/). _Great to see FOSS help to make a difference!_

#### Free Software Funding
* Kyle Rankin for Purism: [How Purism Funds Free Software](https://puri.sm/posts/how-purism-funds-free-software/). _I must say that I like how they are doing things._

#### Jolla's birthday
* Sami Pienimäki on blog.jolla.com: [Happy Birthday Jolla!](https://blog.jolla.com/happy-birthday-jolla/) _Congratulations again! Looks like 2022 is going to be a great year for Jolla and SailfishOS!_

#### PinePhone Impressions
* Colin Wren: [My first PinePhone and diving into mobile Linux](https://colinwren.medium.com/my-first-pinephone-and-diving-into-mobile-linux-e67fa9fbe8b4). _Quite the journey!_
* Camden Bruce: [The ultimate one year review: daily driving the PinePhone](https://medium.com/@camden.o.b/the-ultimate-one-year-review-daily-driving-the-pinephone-25bc41a05533). _Really helpful post, do read it!_


### Worth listening
* PEBKAC: [S01E02 Mid Month Catchup!](https://pebkac.show/2021/10/16/pebkac-s01e02-mid-month-catchup/). _In a way, this podcast is an audio edition of what I do right here, right now. Give it a listen!_
* DLN Xtend: [79: Shut Up and Take My Money](https://dlnxtend.com/79). _Great discussion of the PinePhone and the PinePhone Pro!_

### Maybe worth watching
* Jolla: [Jolla 10 years celebration](https://www.facebook.com/jollaofficial/videos/1141033772970113/) **Warning**: This link goes to Facebook and if you do as much as try to skip to the actual start of the video, it requires you to log in. _I became impatient after 4 minutes, therefore I watched this partially in Tor Browser afterwards before running out of time. It's worth it - if you can tolerate Facebook. I hope this is going to be uploaded somewhere else eventually._

### Worth watching

#### PINE64 Community Update
* PINE64: [October Update: Introducing the PinePhone Pro](https://www.youtube.com/watch?v=RwtKpQBIDqw). _Great video, PizzaLovingNerd!_

#### Tutorials
* Avisando: [PinePhone - 5 Tips and Tricks for Manjaro Phosh](https://www.youtube.com/watch?v=n01a4K19VXU). _Nice one! Don't do that battery change though._

#### Shorts
* OneInterWeb: [The PinePhone Rocks!](https://www.youtube.com/watch?v=bQ_deuiuGUU)
* cemaxecuter: [DragonPhone w/ SDR++ (PINEPHONE)](https://www.youtube.com/watch?v=bTfbn8u3s9A). _Remember when Ezra and I discussed an FM radio question on PineTalk?_
* U STARK: [Ubuntu touch Waydroid App test 1](https://www.youtube.com/watch?v=Kq-EBott9zI). 

### Stuff I did

#### Content

I've written one [blog post about my "Initial thoughts" regarding the PinePhone Pro announcement](https://linmob.net/thoughts-on-the-pinephone-pro/). _This required some changes to Minimola I still need to push to GitHub._

#### Random

* I've finally played with two QWERTY phones that do run Linux (well, Debian Stretch, but I suppose that still counts). More on that soon, I hope!
* [3000](https://twitter.com/linmobblog/status/1450585571001241601)! 

#### LINMOBapps

Activity has picked up again, I've added one app:
* [LibreLingo Mobile](https://codeberg.org/dimkard/librelingo-mobile/), a language learning application based on LibreLingo. _I added it immidiately as I think that [Libre Lingo](https://librelingo.app/about) is an extremely cool project!

Looking at the overall progress of the project, I have to thank _hamblinggreen_ for doing amazing work and adding repology names, which is a tedious task for sure, but is going to be really useful for the LinuxPhoneApps transition and might be nice for other projects like [LINMOBappsBrowser](https://github.com/Speiburger/LINMOBappsBrowser! 

[Read here what (else) happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master) on LINMOBapps this week. And please [do contribute!](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/CONTRIBUTING.md)

#### Linux Phone Apps

No visible progress, sorry! I have decided to make sure that the site can run without me pushing buttons - meaning, hosting is supposed to change to [sourcehut pages](https://srht.site/) (or if I can't pull that off, GitHub Pages). 

Next on the priority list is getting work done that better explains the next steps, getting mailing lists started and cobbling something together that makes it easy to bring the current apps over &mdash; I realized that wondering to much about whether the current approach will work just leads to stagnation. _Also: If I don't get this done this year, I'll likely never get it done._
