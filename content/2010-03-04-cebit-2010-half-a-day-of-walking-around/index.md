+++
title = "CeBIT 2010: Half a day of walking around"
aliases = ["2010/03/cebit-2010-half-day-of-walking-around.html"]
date = "2010-03-04T11:15:00Z"
[taxonomies]
tags = ["cebit", "CEBiT2010", "Classmate", "netbooks", "UMPCs"]
categories = ["hardware", "events"]
[extra]
author = "peter"
+++
I had to go to CeBIT this year again, without really knowing why, probably because it's just on the way from Kiel to my parents location. Well, anyway, I was there and was pretty lazy besides walking around, I had my G1 and my camera with me and didn't take more pictures than 11&mdash;because it was pretty boring and because taking pictures has become an unusual event to me during the last two years. 
<!-- more -->
{{ gallery() }}

So why was it boring? Few new stuff on the mobile devices front, close to zero announcements of new devices at CeBIT (apparently CES, MWC and Computex are the place to announce new stuff today)&mdash;and CeBIT 2010 is definitely smaller than 2009 was. But still, it was pretty crowded, and I when I went to HTC and Sony Ericsson, which both had (still have) pretty small (almost disappointingly small) booths, i was already to tired to wait to get my hands on their new devices. 

The most fun part of CeBIT for me was looking at all these Shenzhen devices&mdash;it is really impressive how many electronic companys are located there. 

One of the few places where I actually stopped was the uSmart booth&mdash;as they were announcing the worlds smallest PC, which it most likely isn't, but still, it is even smaller than the UMid mbooks while offering the same screen size (<a href="http://armdevices.net/2010/03/03/usmart-sun-power-technology-limited-windows-xp-intel-atom-4-8-laptoptablet-form-factor/">check out a video of the device at ARMDevices.net</a>). 

In fact I have to admit that I don't see a use for a device this small running a real PC OS&mdash;and the 2 hours of battery life they promise are pretty much disappointing as well. I like the new Intel Classmate PC (Gen4) a lot better&mdash;it seems to be a really nice device, not only for kids&mdash;if the price tag will be not too much higher than other "netvertibles", this could (depending on distribution) be even a best seller. 

<a href="http://www.youtube.com/watch?v=4HJOcAubkHE">Check out the video Sascha from Netbooknews</a> produced, it's fun to watch&mdash;imagine that device in black&mdash;wouldn't that be cool?

Other cool devices unfortunately didn't make their way to CeBIT, e.g. no Notion Ink Adam, in fact I didn't see a single Tegra II Device&mdash;but I have to admit that I didn't really look too carefully. 

Of course there were some more new netbooks / convertible netbooks, but they were not only leaked before&mdash;most netbooks are pretty boring as they are all pretty similar. The most interesting standard netbooks at CeBIT are the new EeePCs which are wrapped in aluminum which is awesome. And, of course Acers new nVidia ION &#8220;2&#8221; netbook, the 10&#8221; sized 532G&mdash;but as Acer's stand is located in the resellers area, you will have to be exhibitor or press to check that one out (I was at CeBIT as &#8220;press&#8221; and didn't (because of being horribly tired), so feel free to insult me).
