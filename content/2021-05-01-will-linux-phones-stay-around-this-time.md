+++
title = "Will Linux Phones stay around this time?"
aliases = ["2021/05/01/will-linux-phones-stay-around-this-time.html"]
author = "Peter"
date = "2021-05-01T16:00:00Z"
updated = "2021-05-03T10:45:00Z"
layout = "post"
[taxonomies]
tags = ["history", "future", "Community", "Linux phones",]
categories = ["commentary",]
[extra]
update_note = "Added 'Discussions'."
+++

_While working through the old posts in order to adjust the front matter (and sometimes more), I thought: Will this last? Or will this go south soon, like it did around with previous projects again? A recent [reddit post](https://old.reddit.com/r/pinephone/comments/myt08t/another_dead_end_or_finally_a_proper_linux_phone/) reinforced this line of thought, so I decided to write down my thoughts on the issue._ 
<!-- more -->

### The Past

There are multiple ways to look at this. But let's start by making a short list of main reasons of failure per effort, starting with the companies involved:

* Openmoko: Financial issues,
* Nokia (Maemo/Meego): Change in corporate strategy (new CEO),
* HP (webOS): Change in corporate strategy (new CEO),
* Mozilla (FirefoxOS): Change in "Corporate" strategy (shifting focus to IOT),
* Canonical (Ubuntu Touch): Change in Corporate strategy (shifting focus to cloud and profitability).

This is certainly too simple, and I do acknowledge that the actual story is much more complex in every case. Also, it's tough to compare the current players, PINE64 and Purism (and the large variety of community projects), to companies of the size of Nokia, HP or even Mozilla or Canonical. The comparison to [Openmoko](https://en.wikipedia.de/Openmoko) is most likely the one that works best. 

#### Openmoko, specifically

Openmoko started in 2006 as a project in a [larger company](https://en.wikipedia.org/wiki/First_International_Computer) and was then spun-out. Their hardware was slightly dated for the time and thus aging badly (ARMv4, GPRS;  Linux also ran on many devices originally running Windows Mobile at the time), and weaker than they had hoped. They developed their OS in the open but on their own (developers were employed), and had to redo it three times.[^1] They [could not afford to make their third phone](https://linmob.net/openmoko-plan-b-and-sean-moss-pultzs-talk-from/), which would have fixed many issues (3G, competitive SoC, ...) for [financial](https://en.wikipedia.org/wiki/Financial_crisis_of_2007%E2%80%932008) reasons, and opted to make the [WikiReader](https://en.wikipedia.org/wiki/WikiReader) instead and then [slowly sizzled away](https://linmob.net/openmoko-announces-its-4th-product-shiftd/) as a company.

The 2006 to 2009 time frame was a rough time to get started on the smartphone market: Both Apple and Google (as part of the [Open Handset Alliance](http://www.openhandsetalliance.com/)) entered a thus very dynamic market at the time. 3G technology made mobile data actually worthwhile, the ARMv7 architecture added processing power and capacitive touch screens changed user interfaces forever.

### Today

When we compare that to now, we have a quite different situation: The year over year improvement in smartphone technology is arguably decreasing and the market can be assumed saturated. 

Unlike Openmoko, both PINE64 and Purism also produce other consumer electronics products that they sell, which leads to a more diversified business: They don't depend on their phones only.

#### Purism

Purism do develop their own software in house, they are taking a clever, minimalist, community-enabling approach which is working quite well. Taking as much as possible from and working with upstream projects and developing the little things they need to do:

* [libhandy](https://source.puri.sm/Librem5/libhandy/) (as a library to make GTK apps mobile friendly and convergent, now a [GNOME project](https://gitlab.gnome.org/GNOME/libhandy/)),
* a [mobile shell](https://source.puri.sm/Librem5/phosh/), [virtual keyboard](https://source.puri.sm/Librem5/squeekboard)] and [call](https://source.puri.sm/Librem5/calls) and [chat](https://source.puri.sm/Librem5/chatty) apps,
* hardware enablement of their Librem 5 device.

Sadly, they aren't as good at shipping phones: They recently have announced [further delays and price increses](https://puri.sm/posts/the-ball-and-supply-chain/).[^2]

#### PINE64

Fortunately, PINE64 has been better at shipping their PinePhone. Openmoko supposedly had 10,000 phones shipped in 2009, PINE64 have already shipped more than 3 times that amount. This is important, as it means that there's a relatively large community of both developers and users that actually have physical hardware to develop and use [Linux phone apps](https://linmobapps.frama.io).

Community is also where the software development with PINE64 lives, they produce devices for community software. This led to increased activity at community projects like [Plasma Mobile](https://www.plasma-mobile.org) (previously [Plasma Active](https://en.wikipedia.org/wiki/KDE_Plasma_4#Plasma_Active)) and many new little projects, like Sxmo and countless others. 

### Does this mean we are "safe"?

We can never be sure, but the situation is a lot better. There's momentum! We have hardware, and I have heard rumors of multiple upcoming Linux Phones for 2021, although I am not sure how many of these efforts will actually make it given the current component shortage.

Even if we were to remain limited in hardware to PinePhone and Librem 5, [postmarketOS](https://www.postmarketos.org) and other efforts like [Droidian](https://droidian.org/) are bringing all that newer Linux Phone software goodness to more devices.

Let's also not forget that projects like [Ubuntu Touch](https://ubuntu-touch.io/) (picked up by [UBports](https://ubports.com/) when Canonical had dropped it) and Jolla's [Sailfish OS](https://sailfishos.org/) have been churning on all the time and seem to keep going.

What's needed though is that there's a good collaboration and care. Don't [burn](https://blog.brixit.nl/do-you-really-want-linux-phones/) the developers out! Contribute in code, documentation, translation or just help other users whenever you can, if not be patient and try to get informed by watching videos or reading! 
Remember: Screaming on social media is for toddlers! Offer feedback, but do it _constructively_.

_We can only be safe if we build and grow this together!_

### Addendum 2021-05-03: Discussions

This post almost went viral, check out or join the discussions on:
* [Mastodon](https://fosstodon.org/@linmob/106160821358230907),
* [Lemmy](https://lemmy.ml/post/62435),
* [Twitter](https://twitter.com/nixcraft/status/1388801589909999620),
* [Hacker News](https://news.ycombinator.com/item?id=27010556),
* [Reddit](https://old.reddit.com/r/linux/comments/n2ubdc/will_linux_phones_stay_around_this_time/).


[^1]: For more Openmoko history, read [these](https://www.vanille.de/blog/openmoko-10-years-after-mickeys-story/) [posts](http://laforge.gnumonks.org/blog/20170709-10years_openmoko/) by former Openmoko engineers.
 
[^2]: With this further delay I feel even less confident recommending pre-ordering a Librem 5. You may get it for less money now (then later), and when you get it in 2022, further hardware enablement and general software development will hopefully have made the device fully viable as a daily driver&mdash;but it's risky, as the component shortage is going to be affecting all of their sources of income. You can also help Purism's effort by contributing to their [Fund-Your-App challenge](https://puri.sm/fund-your-app/).
