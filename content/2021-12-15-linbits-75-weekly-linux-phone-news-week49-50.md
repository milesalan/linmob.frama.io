+++
title = "LinBits 75: Weekly Linux Phone news / media roundup (week 49/50)"
date = "2021-12-15T21:32:00Z"
draft = false
[taxonomies]
tags = ["PinePhone", "PinePhone Pro","PINE64 Community Update","Phosh","Plasma Mobile","Nemo Mobile","Meltemi","Purism","Framebufferphone","LINMOBapps"]
categories = ["weekly update"]
[extra]
author = "Peter"
+++

_7 Days in Linux Phones:_ PinePhone Pro Explorer Edition will be up for order soon, distro releases and Calls and Chatty developments, the Framebufferphone project, Meltemi (one Nokia Linux project that never saw the light of day) on camera and more!

<!-- more --> _Commentary in italics._ 
### Software releases
#### Apps
* GNOME Calls 42.alpha.0 has been [announced](https://fosstodon.org/web/@devrtz@mastodon.online/107450465206852666) and [released](https://gitlab.gnome.org/GNOME/calls/-/tags/42.alpha.0).
* Chatty 0.5.0-beta4 has been released, [delivering more MMS improvements and fixes](https://source.puri.sm/Librem5/chatty/-/commit/3f9e1af45d1262a7b8d115d103c0e83fb327b6ac). [Beta 3](https://source.puri.sm/Librem5/chatty/-/commit/e1502ae56f0a769cad0b71d67944c6ac984b4dbc) earlier this past week delivered Matrix and chat dialog fixes. 
* [Megapixels 1.4.3 is a bug fix release](https://gitlab.com/postmarketOS/megapixels/-/commit/c0e76da4b7715e311f0480b121aeb89837e19562) for our favourite Camera app.

#### Distributions
* [Manjaro Phosh Beta 20 has been released](https://twitter.com/ManjaroLinux/status/1471060340905582592), delivering Chatty 0.5-beta3 and other current software.
* Manjaro [Plasma Mobile Beta 9](https://github.com/manjaro-pinephone/plasma-mobile/releases/tag/beta9) has also been released. _There's no changelog, but from trying it briefly it ships all the goodness included in Plasma Mobile Gear 21.12._
* Arch Linux ARM (Danct12) has seen a [new release on December 12th](https://github.com/dreemurrs-embedded/Pine64-Arch/releases/tag/20211212), delivering Kernel, crust, Plasma Mobile Gear 21.12 and many further updates. 

### Worth noting:
* If you’re in the UK, and want to get Sailfish X on your smartphone, [Jolla has now got you covered again post-Brexit](https://blog.jolla.com/congrats-uk/).
* If mainline Linux Smartphones are you’re thing: [Martijn Braam build an awesome, simple webpage for you to look the amount of mainline deviations up](https://mainline.space).
* Alternatively, there's also [https://not.mainline.space/](https://not.mainline.space/). _It's for fun!_
* If Sxmo (with X or Wayland) still feels to bloated for you, check out ~mil's new project [Framebufferphone](https://sr.ht/~mil/framebufferphone/). It's early days, but definitely an interesting endeavour!
* Always wanted to wrist mount your (Pine)Phone? [Go for it](https://www.thingiverse.com/thing:5159672), or watch a [video demo](https://imgur.com/vuXYHdm) first! _Great work by 5ilver!_

### Worth reading

#### PINE64 Community Update
* PINE64: [December update: a year in review](https://www.pine64.org/2021/12/15/december-update-a-year-in-review/). _A truly big update at the end of the year. Sticking to phones, the PinePhone Explorer Edition is going to be up for orders in a few weeks time, and so is the keyboard after some last fixes. The other back covers are also coming along, so Q1/2022 is going to be filled with accessory releases. But there's more, so do read it!_
  * Liliputing: [$ 399 PinePhone Pro Explorer Edition Linux Smartphone will go on sale within weeks](https://liliputing.com/2021/12/399-pinephone-pro-explorer-edition-linux-smartphone-will-go-on-sale-within-weeks.html). _Brad's take._

#### Software progress
* KDE blog: [KDE Gear 21.12](https://kde.org/announcements/gear/21.12.0/). _Convergence, right? Most of these apps are desktop apps, but some work on Mobile too._
* This Week in GNOME: [#22 Spring Time…?](https://thisweek.gnome.org/posts/2021/12/twig-22/). _The same caveat applies here. Great that NewsFlash is being ported to libadwaita and GTK4!_
* danigm: [Loop: A simple music application](http://danigm.net/loop.html). _I added it to LinuxPhoneApps.org last week, and now it's on Flathub!_
#### Purism Development 
* Martin Kepplinger for Purism: [Purism and Linux 5.16](https://puri.sm/posts/purism-and-linux-5-16/). _Great stuff coming!_
* Amos B. Batto: [Amount of code developed by Purism for the Librem 5 phone](https://amosbbatto.wordpress.com/2021/12/15/amount-code-librem5/). _A quarter million lines of code. Impressive._

#### PinePhone vs. Librem 5
* Amos B. Batto: [Comparing the Librem 5 USA and PinePhone Beta](https://amosbbatto.wordpress.com/2021/12/10/comparing-l5-and-pp/). _Just a warning: Firefox’s reading mode claims reading this takes 88 to 112 minutes, Miniflux claims it's only 66 minutes, so be sure to have some time on your hands. That said, there's a lot of good info in there. There are a few caveats I have to apply though: Mobian (it appears weird that Amos is still using Bullseye here, BTW!), while a great project with a really helpful wiki, is on the slow side of PinePhone distributions – while this does not make a significant difference in benchmarks, e.g. postmarketOS or Arch IMHO just feel faster._

#### Mobile shell comparison
* /u/The01player: [Small review of a few PinePhone UIs (Phosh, Sxmo, KDE)](https://old.reddit.com/r/PINE64official/comments/rewgqw/small_review_of_a_few_pinephone_uis_phosh_sxmo_kde/).

#### Fun projects
* Bacardi55: [Send sxmo notifications to a matrix room](https://bacardi55.io/2021/12/11/send-sxmo-notifications-to-a-matrix-room/). _Why, you ask? Why not?_
* Liliputing: [PineBoy 3000 wrist mount turns a PinePhone into a Pip-Boy](https://liliputing.com/2021/12/pineboy-3000-wrist-mount-turns-a-pinephone-into-a-pip-boy.html). 

#### Install tutorial
* Nemo Mobile: [Nemomobile on Volla Phone](https://nemomobile.net/pages/nemomobile-on-volla-phone/). _Nice install tutorial!_

#### For (aspiring) developers
* rabbiticTranslator: [Long KFluff – compiling C++/Qt with g++/clang++, QMake, CMake, Kate, KDevelop, QtCreator](https://rabbitictranslator.com/wordpress/index.php/2021/12/12/long-kfluff-compiling-with-stuff/). 

#### Pinephone Keyboard – p-boot landscape mode
* xnux.eu log: [Pinephone Keyboard – p-boot landscape mode](https://xnux.eu/log/#057). _It's coming along! Make sure to watch the video below!_

#### <del>Cereal</del> Serial Access
* Purism: [Avoid Boot Loops, Try Librem 5 Serial](https://puri.sm/posts/avoid-boot-loops-try-librem-5-serial/). _Let's just hope you won't need this._

#### Blunders
* Bhushan Shah: [TLP has uhttps://puri.sm/posts/avoid-boot-loops-try-librem-5-serial/nintended side effects on Mobile devices](https://blog.bshah.in/2021/12/12/tlp-unintended-side-effects/). _Wow, that’s bad. Luckily [postmarketOS is not affected](https://mastodon.fam-ribbers.com/@bart/107432738162751052)._

#### History lesson
* Liliputing: [Nokia Kataya and Ion Mini 2 get the hands-on treatment (never-released Nokia phones)](https://liliputing.com/2021/12/nokia-kataya-and-ion-mini-2-get-the-hands-on-treatment-neer-released-nokia-phones.html). _Seeing Meltemi is quite thrilling._

#### Current Desktop Things
* Nate Graham: [What desktop Linux needs to succeed in the mainstream](https://pointieststick.com/2021/12/09/what-desktop-linux-needs-to-succeed-in-the-mainstream/). _Yes, this is not about mobile Linux per se, but do read it anyways._ 

### Worth watching

#### Tutorials
* Purism: [Avoid Boot Loops, Try Librem 5 Serial](https://www.youtube.com/watch?v=3YA-XUalo_I). _I like this explainer, but I don’t understand posting this from a marketing perspective, as it makes it sound like it's a real possibility that you’ll have to solder on your up to 2000 USD phone to fix boot issues._
* PINE64: [How to use serial console on the PinePhone (Pro)](https://www.youtube.com/watch?v=f5R_hKgDFk8). _Turns out: The device built for the community to provide software does better here._
* Avisando: [PinePhone - How to install (some) Android applications on Manjaro Phosh (Waydroid)](https://www.youtube.com/watch?v=tUv9lOGaydw).
* Lawnmover Repairs And Stuff: [Getting the PinePhone set up and ready to use!](https://www.youtube.com/watch?v=T_32j49NMWY) _Great channel name!_

#### PINE64 Community Update
* PINE64: [December update: a year in review](https://www.youtube.com/watch?v=Ftk7Wm3-_ug). _Great job on this video summary once again, PizzaLovingNerd!_

#### New hardware meeting old software
* Danct12: [Arch Linux ARM with LXDE on PinePhone Pro](https://www.youtube.com/watch?v=BWk2Z2HjT7U). _Reminds me a lot of the now long-abandoned AVMulitPhone OS._

#### Hardware fun
* Liliputing: [PineBoy 3000 wrist mount turns a PinePhone into a Pip-Boy](https://liliputing.com/2021/12/pineboy-3000-wrist-mount-turns-a-pinephone-into-a-pip-boy.html).

#### Keyboard fun
* sadfwesv: [Linux/p-boot tuning for Pinephone Keyboard (Final Version!?)](https://www.youtube.com/watch?v=ZaciUkMrXiY)

#### PinePhone reviews
* Root BSD: [The Pinephone is ready! Well, sort of..](https://www.youtube.com/watch?v=OoyuT4Pom2Q).
* Idcrafter: [Pine64 PinePhone Review](https://www.youtube.com/watch?v=cZv9nx4dMXU). _This video is in German, but you will get some good look at some new Plasma Mobile developments, as this shows off current Manjaro Plasma Mobile Dev._

#### PinePhone (Pro) gaming
* CalcProgrammer1: [PinePhone vs. PinePhone Pro running Star Wars Jedi Academy with OpenJK](https://www.youtube.com/watch?v=T406a8EQ5T0).

#### Conference Talks
* OpenAlt: [Jozef Mlich: Nemomobile](https://www.youtube.com/watch?v=6t7yV83rKI4). _You may have watched this when it was streamed. If you haven't, go watch it now :)_

#### Sailfish + Waydroid
* Sailfish official: [Xz2c sailfish os 4.2.0.21 + waydroid](https://www.youtube.com/watch?v=61YM66oebLI).


#### Shorts
* Zvost: [UBUNTU TOUCH ASUS ZMPM1 #Cusrom #Oprek](https://www.youtube.com/watch?v=lanIcOGzAOY).
* Сергей Чуплыгин: [let's indicators fixed](https://www.youtube.com/watch?v=ZktfWGE2gZs). _Nemo Mobile!_

# Missed your story?
If we missed an important bit of news, please get in touch. If you want to make sure we don't miss your app release, distro update, blog post or video, just ping us on social media or send me an email! _It's very much appreciated!_

### Stuff I did

#### Content
Nothing, again. I did not manage to write my "Abandoning the Zola approach to LinuxPhoneApps" post. _Maybe I'll manage to do so in the coming week._

#### LINMOBapps and Linux Phone Apps
This week I only added 
* [Millipixels](https://source.puri.sm/Librem5/megapixels), a camera application for the Librem 5.

Soon, with holidays and all, I hope to increase my activity again.

[Read here what (else) happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master) on LINMOBapps this week. And please [do contribute!](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/CONTRIBUTING.md)
