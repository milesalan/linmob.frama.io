+++
title = "On HP webOS and the HP Veer"
aliases = ["2011/05/on-hp-webos-and-the-hp-veer.html"]
date = "2011-05-14T15:46:00Z"
layout = "post"
[taxonomies]
tags = ["competition", "HP Pre3", "HP Veer", "webOS", "openness", "palm pre", "Palm Pre Plus"]
categories = ["hardware", "commentary"]
[extra]
author = "Peter"
+++

As the HP Veer is about to be launched in the US (May 15th on AT&amp;T), <a href="http://www.precentral.net/review-ATT-hp-veer-4g">reviews</a> <a href="http://www.webosroundup.com/2011/05/hp-veer-4g-small-phone-big-review/">are</a> being published. Some are more positive than others, naturally, and i am not surprised that the reviews of the webOS focussed sites write about this new little kid in town a lot more positive than those that review everything. The Veer is special, and I felt irritated when it was announced. I felt like: Hell no, why did they turn the Pixi into a slider (the hinge is said to be great, firm and sturdy, though), and even worse, why did they keep the Pixi's subpar display? I couldn't really understand it and I can't now and so can't some others.
<!-- more -->
<a href="http://thisismynext.com/2011/05/09/veer-4g-review/">The one review I really agree with is the one that Joshua Topolsky, former editor-in-chief at engadget, now at thisismynext.com, did</a>. Mr Topolsky is not a webOS hater, he's been using the Pre2 for quite some time and told everybody that he liked it, but he is not a fanboi and honestly, there could have been more progress with webOS since HP bought Palm.

The worst part about todays webOS is the really bad Email application. I am not talking about Synergy, I am talking about the UI of the email application in webOS, which really offers the options one would like to see. To delete multiple messages (or, say, to mark some as read) is virtually impossible - you have to delete one, and then the next. There are a few patches in Preware for the Mail app, but patching is not for everybody and these patches don't make the Application a good one, they are much rather quick hacks to make the thing less painful. The only thing that became notably better with webOS 2.1.0 is that you can opt out deleting mails by a simple swipe - on 1.* I once lost an important mail without noticing it because of this stupid behaviour, so this &#8220;just swipe and it's gone&#8221; thing should have never been done in the first place.

Besides that, loading in webOS often takes longer than it should. Sometimes this is due to performance issues in webOS, sometimes its due to a slow network connection. But you can't really distinguish it in many apps, so in many users minds its webOS that's blamed, with its huge, slowly spinning wheel.

Another example is the startup webOS. Takes forever. Android Gingerbread is a lot quicker, even on inferior hardware.

I really think that HP should work on optimization or start something like a HP webOS enthusiast program, which receives updates quick and early and which allows people to really change things. Open up some more stuff, make your development process a little more open, allow people to port over newer, more optimized kernel versions to webOS, make it easy to change the used version of gstreamer to allow for support of *.ogg and *.flac files, enable people to try out newer WebKit / V8 versions or something - I am pretty sure that there are tons of people out there, that would totally love to help you to make webOS faster if you open it up a little bit more. 

Move, HP. Don't listen to your webOS fanbois, listen to those, who know your opponents. The main advantage of your webOS on smartphones is the great card based multitasking. But the others are getting closer. Android won't suck at multi tasking usability forever, even Windows Phone 7 will get something nice and we don't know how great actual MeeGo devices will be (BTW, MeeGo is quick at startup, maybe, HP, run Luna atop of MeeGo? Google builds Chrome OS on a MeeGo basis, so why shouldn't HP do that?)

Back to the Veer, the little Qualcomm MSM7230 powered powerhouse. I won't buy one. A small screen with an awkward resolution and not too great colors is not for me, and BTW, i consider the Pre/Pre Plus/Pre² to be a small smartphone already. A Palm Pre³, with a better Mail application&#8230; that might get me tempted.
