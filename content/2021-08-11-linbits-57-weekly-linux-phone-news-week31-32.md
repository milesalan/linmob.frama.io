+++
title = "LinBits 57: Weekly Linux Phone news / media roundup (week 31/32)"
date = "2021-08-11T20:20:00Z"
[taxonomies]
tags = ["PinePhone","LINMOBapps", "LinuxPhoneApps",]
categories = ["weekly update"]
[extra]
author = "Peter"
+++

_It's Wednesday. Now what happened since last Wednesday?_

A postmarketOS service pack, Phosh 0.13.0, KDE app ads, Jitsi calling on PinePhone and more!<!-- more --> _Commentary in italics._ 

### Software releases
* [postmarketOS 21.06 Service pack 1 has been released](https://postmarketos.org/blog/2021/08/04/v21.06.1-release/). _While the changes sound minor, they contain important quality of life improvements._
* [Phosh 0.13.0 brings improved call handling when shell is locked, lockscreen notifications, highcontrast theme support and more](https://social.librem.one/@agx/106731099570048542). Read the [full release notes](https://gitlab.gnome.org/World/Phosh/phosh/-/tags/v0.13.0) for all the changes made. 

### Worth noting
* [Siglo is now available a Flathub](https://flathub.org/apps/details/com.github.alexr4535.siglo), which should help InfiniTime users across distributions.

### Worth reading

#### Distro progress
* Mobile NixOS: [July 2021 round-up](https://mobile.nixos.org/news/2021-08-03-july-2021-round-up.html).
* Mobian Blog: [To Bullseye and beyond!](https://blog.mobian-project.org/posts/2021/08/11/bullseye-release/). _I am so glad when Mobian is finally no longer frozen!_

#### Software advancements
* Purism: [Librem 5 support in mainline Linux](https://puri.sm/posts/librem-5-support-in-mainline-linux/). _Nice progress!_
* Claudio Cambra: [Making Kalendar pretty — Kalendar week 9 (GSoC 2021)](https://claudiocambra.com/2021/08/08/making-kalendar-pretty-kalendar-week-9-gsoc-2021/). _Kalender is really shaping up nicely! If you want to, and run an Arch-based distro, you can try it fresh from the AUR - the package name is _`kalendar-git`_._

#### PinePhone keyboard news
* xnux.eu: [Wrapping up Pinephone keyboard firmware development](https://xnux.eu/log/#044). _Another great report by Megi!_

#### News roundups
* Linux Smartphones: [Linux smartphone news roundup: Waydroid, postmarketOS, Phosh, and MauKit updates](https://linuxsmartphones.com/linux-smartphone-news-roundup-waydroid-postmarketos-phosh-and-maukit-updates/).
* Linux Smartphones: [News roundup: PinePhone keyboard, mainline Linux kernel, and Waydroid](https://linuxsmartphones.com/news-roundup-pinephone-keyboard-mainline-linux-kernel-and-waydroid/).
* This Week in GNOME: [#4 Building...](https://thisweek.gnome.org/posts/2021/08/twig-4/). _Make sure to read the parts about Libadwaita and Portfolio - at least!_

#### JingOS 
* TuxPhones: [ JingPad A1 (DVT) review, part 1 ](https://tuxphones.com/jingos-linux-tablet-review-part-1/). _Great review, going into detail on performance and the non-mainline kernel._

#### Apps
* Gamey: [PINEPHONE: 5 more amazing Apps!](https://odysee.com/@gamey:c/5-best-pinephone-apps-2:9). _A few really nice apps that you might not know yet!_

#### Ideas and Projects
* Simon Sáfár: [Emacs OS Mobile](https://simonsafar.com/2021/emacs_mobile/). _Nice idea. I once [saw something similar](https://fosstodon.org/@ssafar/106713414736046806), and there's been prior art._
* Danct12: [How to overclock (or underclock) the PinePhone CPU](https://danct12.github.io/PinePhone-CPU-overclocking/).
* Marius Welt: [Carola – Personal Voice Assistent](https://marius.bloggt-in-braunschweig.de/2021/08/06/carola-personal-voice-assistent/). _Warning: This post is in German, and is about a personal voice assistant that only understands German for now - so get your favourite web translation tool ready._

#### PinePhone usage report
* Will Webberley: [Pinephone update: the first few weeks](https://wilw.dev/blog/2021/08/09/pinephone-first-few-weeks/). _Keep it up!_


#### For Developers
* Janet Blackquill: [Asynchronous QtQuick UIs and their implementation: The Toolbox](https://blog.blackquill.cc/asynchronous-qtquick-uis-and-their-implementation-the-toolbox).
* jwestman.net: [Finishing GNOME Builder's Mobile Device Support](https://www.jwestman.net/2021/08/06/finishing-gnome-builder-mobile-support.html). _This is quite cool!_
* jwestman.net: [Tutorial: Mobile App Development with GNOME Builder](https://www.jwestman.net/2021/08/06/tutorial-mobile-dev-with-builder.html). 

#### Getting involved
* Tobias Bernard: [Community Power Part 5: First Steps](https://blogs.gnome.org/tbernard/2021/08/06/community-power-5/). _Remember: If you want your favourite FOSS projects to improve, get involved!_

#### Stuff only I care about because I am struggling with LinuxPhoneApps.org
* Sophie's Blog: [An “Apps for GNOME” website](https://blogs.gnome.org/sophieh/2021/08/05/apps-for-gnome/). _Looks very nice, runs fine, but I did not grok how it works yet._
* Aleix Pol: [KDE is All About the Apps, an Akademy 2021 recap](https://www.proli.net/2021/08/07/kde-is-all-about-the-apps-an-akademy-2021-recap/).

### Worth watching

#### PinePhone Keyboard
* Martijn Braam: [PinePhone keyboard prototype #3, flashing and demo](https://spacepub.space/videos/watch/0e2ce735-2299-4e04-9323-da34440d34e4)

#### PinePhone Software progress
* pink pantsu: [Jitsi meet video call with Gstreamer](https://www.youtube.com/watch?v=AlmOvYJzIoI) _This is this weeks_ __highlight.__ _The code is [here](https://github.com/linh-nh/gstreamer-jitsi-meet), and there's a [Reddit post](https://teddit.net/r/pinephone/comments/p1hskg/i_made_a_jitsi_meet_call_with_pinephone/) that explains a little more. While the author seems slightly disappointed with the amount of CPU use, compared to [previous attempts](https://marius.bloggt-in-braunschweig.de/2021/03/04/pinephone-breakthrou-on-fedora-pinephones/) at getting video chatting working on the PinePhone, this is a massive improvement._ 
* 蕭博文powen: [progress of Odysseyra1n with pinera1n using manjaro](https://www.youtube.com/watch?v=0EkEFZlKQUE). _If you want to jailbreak an iPhone..._
* KDE: [KDE Gear ⚙️ 21.08: An ad for Elisa, KDE's simple and fancy music player](https://tube.kockatoo.org/w/c6nzTCjcm9S9jvpbAJhMcD). _Fun ad!_

#### JingPad Preview
* LearnLinuxTV: [The JingPad A1 - Hands-on Preview!](https://www.youtube.com/watch?v=NbD8KBFLxjY). _Nice review!_
* TuxPhones: [JingPad A1 (Developer Edition) Unboxing ](https://www.youtube.com/watch?v=AWHaXXqiOAs). 

#### Ubuntu Touch Corner
* Digital Wandering: [Choosing Between Ubuntu Touch and Windows Phone in 2021](https://www.youtube.com/watch?v=_JMNU_bZYww). 
* TeckMonster: [Ubuntu Touch 3D Benchmark - Pixel 3a VS Volla Phone VS Nexus 5 VS Nexus 7](https://www.youtube.com/watch?v=wBNTR1WW0Tk)

### Stuff I did

#### Content

Sadly none this week, just a minor addition to the [Phosh App Drawer](https://linmob.net/phosh-0-12-app-drawer/#reader-feedback-added-august-7th-2021) post.

#### Random

* LinuxPhoneApps has made [some more progress](https://github.com/linuxphoneapps/), but not enough to put it online. Given a close family member's recent health emergency I can't set any deadline right now. It's going to be hard enough to keep these updates going.
* I have also played with Waydroid on Arch Mobile a bit, but not really, because I wanted to do so on camera

#### LINMOBapps

These apps were added in the past week, pushing the App count to 309:

* [Breathing](https://github.com/SeaDve/Breathing), a very simple application that guides your breathing pattern,
* [GNOME Screenshot](https://gitlab.gnome.org/GNOME/gnome-screenshot), a small utility that takes a screenshot of the whole desktop; the currently focused window; or an area of the screen,
* [Metronome](https://gitlab.gnome.org/aplazas/metronome) beats the rhythm for you, you simply need to tell it the required time signature and beats per minutes.

[Read here what (else) happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master). 
Please [do contribute!](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/CONTRIBUTING.md)
