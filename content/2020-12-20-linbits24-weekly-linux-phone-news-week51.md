+++
title = "LinBits 24: Weekly Linux Phone news / media roundup (week 51)"
aliases = ["2020/12/20/linbits24-weekly-linux-phone-news-week51.html", "linbits24"]
author = "Peter"
date = "2020-12-20T22:12:00Z"
layout = "post"
[taxonomies]
tags = ["Pine64", "PinePhone", "Arch Linux ARM", "DanctNIX Mobile", "Manjaro", "Sxmo", "Mobian", "PineEye", "Phosh", "UBports", "Ubuntu Touch"]
categories = ["weekly update"]
+++

_It's sunday. Now what happened since last sunday?_

PINE64's last Update in 2020, GTK 4.0, Sxmo 1.2.0, Plasma Mobile news and more. _Commentary in italics._
<!-- more -->

### Software releases and announcements

* [Phosh 0.7.1 has been announced](https://social.librem.one/@agx/105401991250603196), a minor release containing a few bug fixes – [full release notes](https://source.puri.sm/Librem5/phosh/-/releases/v0.7.1).
* [Sxmo 1.2.0](https://lists.sr.ht/~mil/sxmo-announce/%3C20201214135928.ewj5bcbj53sn6pv4%40worker.anaproy.lxd%3E) has been released. _Make sure to watch the video linked below._
* [OpenSUSE/slem.os has seen another release](https://nitter.net/hadrianweb/status/1339327183182057475), featuring Phosh, Phoc, new GNOME 3.38 apps and fixes to the Modem.
* Mobian: [Status update 2020-12-15](https://forum.pine64.org/showthread.php?tid=12511). _a-wai, head of the Mobian project, wrote a nice update here._
* [Manjaro Phosh Beta 4](https://forum.manjaro.org/t/manjaro-arm-beta4-with-phosh-pinephone/43299) has been announced, featuring Megi's new 5.10 kernel and many other updates. _Stay tuned for my video on this new Manjaro release._

### Worth reading

* PINE64: [December Update: The Longest One Yet](https://www.pine64.org/2020/12/15/december-update-the-longest-one-yet/). _This is even longer than usual, as it also contains a "2020: Year in review section". Maybe just watch the video (linked below) and read Brad's take._
  * LinuxSmartphones: [PinePhone keyboard and wireless charging add-ons are on the way](https://linuxsmartphones.com/pinephone-keyboard-and-wireless-charging-add-ons-are-on-the-way/). _Brad's take._
* GTK Development Blog: [GTK 4.0](https://blog.gtk.org/2020/12/16/gtk-4-0/). _This is nice to see. There are already plenty new mobile optimized apps in the works based on GTK 4._
* Plasma Mobile: [Plasma Mobile is finishing 2020 strong with new updates](https://www.plasma-mobile.org/2020/12/16/plasma-mobile-update-november-december.html). _Nice update. That 5.21 release is going to be quite something!_
  * 9to5 Linux: [KDE’s Plasma Mobile UI Is Now Faster and Less Resource-Intensive](https://9to5linux.com/kdes-plasma-mobile-ui-is-now-faster-and-less-resource-intensive). _9to5 Linux's take._
  * FOSS2go: [December news from the world of Plasma Mobile](https://foss2go.com/december-news-from-the-world-of-plasma-mobile/). _FOSS2go's take._
  * LinuxSmartphones: [Plasma Mobile picks up performance, theme, and app improvements](https://linuxsmartphones.com/plasma-mobile-picks-up-performance-theme-and-app-improvements/).  _Brad's take._
* Plasma Mobile: [Plasma Mobile: Technical debt and moving forward](https://www.plasma-mobile.org/2020/12/14/plasma-mobile-technical-debt.html)
  * FOSS2go: [Plasma Mobile is dropping support for Halium devices](https://foss2go.com/plasma-mobile-is-dropping-support-for-halium-devices/). _FOSS2go's take._
  * LinuxSmartphones: [KDE Plasma Mobile ends Halium support, shifts focus to phones running mainline Linux kernel](https://linuxsmartphones.com/kde-plasma-mobile-ends-halium-support-shifts-focus-to-phones-running-mainline-linux-kernel-en/). _Brad's take._
* UBports: [Ubuntu Touch OTA-15 Release](https://ubports.com/blog/ubport-blogs-news-1/post/ubuntu-touch-ota-15-release-3736). _Remember: OTA 15 is for all devices but the PinePhone._
  * FOSS2go: [OTA-15 for Ubuntu Touch is coming: what’s new?](https://foss2go.com/ota-15-for-ubuntu-touch-is-coming-whats-new/). _FOSS2go's take._
  * LinuxSmartphones: [Ubuntu Touch OTA-15 brings bug fixes and support for more phones](https://linuxsmartphones.com/ubuntu-touch-ota-15-brings-bug-fixes-and-support-for-more-phones/). _Brad's take._
* FOSS2go: [Kernel update available on Ubuntu Touch in Pinephone.](https://foss2go.com/kernel-update-available-on-ubuntu-touch-in-pinephone/). _5.10. Nice and promising. Remember, major improvements are likely to land as side products of the transition to Qt 5.12._ 
* LinuxSmartphones: [Manjaro, Mobian, and OpenSUSE release updated OS images for the PinePhone](https://linuxsmartphones.com/manjaro-mobian-and-opensuse-release-updated-os-images-for-the-pinephone/). _Nice release summary!_
* LinuxSmartphones: [Sxmo 1.2.0 adds support for gesture controls](https://linuxsmartphones.com/sxmo-1-2-0-adds-support-for-gesture-controls/). _Brad's take on the new Sxmo release._
* Gamey: [Matrix on the Pinephone & Librem5 #2](https://lbry.tv/@gamey:c/Matrix-Pinephone-Librem5:b). _Great write-up! Please remember that [Hydrogen](https://hydrogen.element.io) is another great option, working nicely in Firefox, Chromium and Epiphany/Gnome Web._

### Worth listening

* postmarketOS podcast:  [#1: History](https://cast.postmarketos.org/episode/2020-12/). _Even if you don't usually listen to podcasts, to listen to this one. It's really worth it._

### Worth watching

* Arbitrary Tech: [Pinephone Usability Review](https://www.youtube.com/watch?v=5m3T-jIanGE). _Despite the title, this video focuses on Sailfish OS and Mobian and does not take other distributions into account._
* PINE64: [December Update: the Longest One Yet](https://lbry.tv/@PINE64:a/december-update-the-longest-one-yet:d). _Great video by PizzaLovingNerd!_
* Gardiner Bryant: [HEAD TO HEAD: The Librem 5 vs. PinePhone Manjaro Edition](https://lbry.tv/@TheLinuxGamer:f/librem5vpinephone:c). _This is something many people have been waiting for: A head to head comparison of Librem 5 and PinePhone. Surprisingly, the Librem 5 is not ahead everywhere, but honestly, I would have likely decided to shoot again while [editing](https://www.youtube.com/watch?v=f5ijK91Rlc0). Still, with all these caveats, it's worth watching._
* Gardiner Bryant: ["Are Linux Smartphones about to KILL Android?" RE: Mrwhosetheboss](https://www.youtube.com/watch?v=6qa6hBRZJjA). _A rare RE: video that might actually be worth watching._
* proycon: [SXMO 1.2.0: A minimalist UI for Linux phones like the pinephone](https://www.youtube.com/watch?v=pfWKYZDtm1k). _Nice video that also demos the new gestures in Sxmo that I had shown off [way back when in my video](https://www.youtube.com/watch?v=c0-JatWCDuo)._
* Pedro Francisco: [Sailfish en Pinephone](https://video.hardlimit.com/videos/watch/9671d6de-05ae-48f6-be3e-fbe81a1734e2). _Nice._
*  Privacy & Tech Tips: [Pinephone: Linux Security Options + Current Setup & More](https://www.youtube.com/watch?v=aYlLc1OXvLs). _If you care about additional security and privacy on your PinePhone, you really should watch this video._

#### Manjaro Corner

* everydaypine: [PinePhone with Manjaro Arm Beta 4 (Phosh)](https://lbry.tv/@everydaypine:a/PinePhone-with-Manjaro-Arm-Beta-4-(Phosh):f). _Nice video!_
* Linux Lounge: [PinePhone Manjaro CE Unboxing & First Impressions](https://lbry.tv/@LinuxLounge:b/pinephone-manjaro-ce-unboxing-first:9). _YAPUV!_
* Linux Lounge: [Testing Calls On The PinePhone Manjaro CE](https://lbry.tv/@LinuxLounge:b/testing-calls-on-the-pinephone-manjaro:5). _Calls. They aren't easy to demo, which is why I hate doing it. From what I've experienced on Arch Linux ARM and heard about Mobian, the situation is less grim than this video makes it sound._ 

#### Ubuntu Touch Corner
* Elatronion: [PinePhone UBPorts Ubuntu Touch Lomiri: The Most Developed Mobile Linux OS](https://lbry.tv/@Elatronion:a/-PinePhone-UBPorts-Ubuntu-Touch-Lomiri--The-Most-Developed-Mobile-Linux-OS:8). _Great video on Ubuntu Touch!_
* UBports: [Ubuntu Touch Q&A 91](https://www.youtube.com/watch?v=RWmXy24X-H4). _It's their last Q&A for the year, so it's a 2020 in review session. You've been warned._

### Stuff I did

#### Content

I made two videos about PureOS on the PinePhone:
* a short walkthrough through it that got totally sidetracked by suddenly getting into gaming: [YouTube](https://www.youtube.com/watch?v=oT9XUkui4zs), [PeerTube](https://devtube.dev-wiki.de/videos/watch/278f6f88-cfc8-405d-9efb-fbbaef8fef50);
* and one just about SuperTuxKart ([PeerTube exclusive](https://devtube.dev-wiki.de/videos/watch/b7cc9c6a-f16d-442e-97b3-a686129f2662)).

Also, I procrastinated on a blog post that I am hopefully going to publish next week.

#### LINMOBapps

Not much happened here, just [a few additions and some maintenance](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master). Please [do contribute](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/tasks.md)!

