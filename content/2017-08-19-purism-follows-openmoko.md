+++
title = "Purism follows Openmoko"
aliases = ["2017/08/19/purism-follows-openmoko.html"]
author = "Peter"
date = "2017-08-19T06:48:24Z"
layout = "post"
[taxonomies]
tags = ["coreboot", "free software", "GNU/Linux", "openmoko", "opensource", "Purism", "smartphones", "history"]
categories = ["article"]
+++
<!-- more -->
![Openmoko with latest QtMoko]({{ site.baseurl
}}/assets/openmoko-e1503132421510.jpg)

**The 10 Year Neo1973 Anniversary Aftermath**

Recently my feed reader delivered the news to me that ten years had passed since the release of the first Openmoko phone, the Neo Freerunner. For this occasion, two people, [Michael Lauer](https://www.vanille.de/openmoko-10-years-after-mickeys-story/) and [Harald Welte](http://laforge.gnumonks.org/blog/20170709-10years_openmoko/), then involved into the project have written special blog posts.

**Looking Back at the Situation and Flaws**

It was interesting to look back and realize, that not only the Neo 1973 had been announced before the the announcement of the Apple iPhone, but had been to market at roughly the same time. Android followed later with an US launch in the late summer of 2008. As _connaisseurs_ will certainly know, the Neo 1973 (named after the year the first mobile phone call had been placed) would not remain Openmokos only smartphone, but be followed up by an improved, iterative device (which was produced in way larger numbers) called the Neo Freerunner. 
Unfortunately, both smartphones were (even in their time) limited in hardware. At a time, where capacitive touch screens at sizes of 3 inches+ were the _shit_ , Openmoko had kept a 2.8inch resistive touchscreen atop a VGA display (which was high res at that time, with the original iPhone and also the HTC Dream featuring HVGA displays (480x320)), which unfortunately for "finger touching"[^1] did not sit flush. So the display was good, but hard to use, the CPU of choice were from Samsungs S3C24xx family (ARMv4 architecture, ARM9 single core) clocked at 233 and 400 MHz respectively. Connectivity features included WLan b/g, Bluetooth 2.0 and GPRS, whith GPRS also being dated already in a time where phones at least had EDGE (OG iPhone) or 3G technology (Windows also early Android phones). On top that, as one more nail in the coffin the graphics accelerator of the Neo Freerunner turned out to be a graphics decelerator, as it could accelerate anything at the then high VGA resolution.
Also, the software stack did not convince at first try and required rearchitecturing, which as far I remember even happened twice.

**Reasons for Failure**

Aside from the shortcomings of the device, the 2007/2008 financial crisis did not help the company either, as it was a little subsidy of First International Computers, a taiwanese OEM/ODM, and the main other players in the smartphone market were Apple, Google (+ established hardware partners like HTC, Samsung, Motorola and Sony), Apple and Microsoft (+ hardware partners, see Android; which was early and failed due to market forces). 
Due to financial troubles, Openmoko had to realize, that the successor to GTA01 (Neo 1973) and GTA02 (Neo Freerunner), the GTA03, which was supposed to be powered by more beefy hardware set to fix the flaws of the earlier devices, had to be cancelled (Mickeys blog post has a photo of a prototype of the device which I did not see back then). While some went on to use their GTA02s, this ended the idea of a niche with potential for mass market phone to run free software plus maybe a few blobs, that most importantly run on a SoC which is not controlled by the modem.[^2]
(I will just shortly mention the [GTA04](http://projects.goldelico.com/p/gta04-main/) and [Neo900](https://neo900.org/) project here, which &mdash; while great &mdash; certainly do not have mass appeal at all and still feature single core SoCs).

**Purism!**

**Who is Purism?**

Now, [Purism](https://puri.sm/)! Purism is also a startup, and they have been delivering some Linux hardware already with their Librem 13 and 15 laptops, which they launched using crowd funding campaigns (which, by the way, were not a thing in the Openmoko days). For now, their smallest offering is the Librem 11 tablet/convertible. The Librem 13 v1 laptop supports Coreboot despite being an Intel Skylake (6th gen i*), an opensource BIOS/UEFI-replacement, which is important as it shows that Purism are (despite early mixed reviews of their first laptops) not only trying what they can to deliver on their promises but also have a proven track record.

**Smartphone: Librem 5**

Last fall, Purism had put out a survey to figure out what features their phone, which is set to run the new mobile version of their PureOS (Gnome desktop, Debian based Linux distribution, so not Android!), should have. Unfortunately I missed that survey.[^3] Now, job offerings on their website give us some details, aside from the fact that the phone will likely have a 5 inch screen: It will be using a FreeScale i.MX 6 SoC[^4]. This is a sane choice, as this means, they chose hardware that should work with the software they want to run without hacks like libhybris[^5] and should hopefully be available in the necessary quantities. This would have been one of the problems with the chip I would have had in mind for GNU/Linux phones, the Broadcom BCM2837[^6].

**But Ubuntu Phone failed recently, now why should Purism pull it off?**

Now many people argue (e.g. on [last weeks episode of the Linux Unplugged podcast](http://www.jupiterbroadcasting.com/117251/lilo-and-slackware-lup-209/)), that even Canonical did not manage to pull off a successful phone OS. That's true. But keep in mind: Purism's approach seems to be very different. In terms of the software stack, they seem to have decided to go work atop the foundations that are also included in the Linux desktop stack, and not to reinvent the wheel on their own terms as Ubuntu did.[^7] Surely, on can argue: 
Why are they going the Gnome way ([Source](https://puri.sm/job/pureos-phone-developer-ui/)), why are they not taking Plasma Mobile? (And I have no answer here.)
More importantly, as mentioned before, Purism is building their own phone hardware and not simply taking an already ready smartphone design and then run some GNU/Linux userland on an Android kernel, and thus hopefully will be able to deliver kernel updates in time and such things that are necessary to attract the folks that were interested in Openmoko: Privacy and security concious people, die hard free software people and so on, a crowd that never (should have) cared much about Ubuntu Touch. 
Will they succeed? Maybe. If they manage to get the hardware right (as in mostly bug free) and they manage to communicate, that they are different for a reason; and also manage to attract developers of free software, then they will have good chance. And for the less technically inclined users, they mostly have to explain why certain apps aren't available, what to use instead and have the basic functionality (calls, texts, e-mail and a decent web browser) mostly ready at delivery.

_I wish them the best of luck and will start saving money in order to support them. May they be more successful than Openmoko!_

[^1]: Yes, there were these things they called styli, remember?
[^2]: Which due to regulations must run proprietary firmware.
[^3]: Or at least I cannot remember taking it.
[^4]: See [job offering](https://puri.sm/job/pureos-phone-develop((See [job offering](https://puri.sm/job/pureos-phone-developer-os/). This actually inspired me to use this title, as this is not a high end, state of the art chip (it has up to 4 ARM Cortex A9 cores), but much rather one that is very open, has open documentation and offers accelerated graphics for Xserver (and _Wayland_, I hope), which almost no other SoC does.
[^5]: libhybris is the way Jolla and Ubuntu Phone use to get their GNU/Linux userland software to work on Android phones.
[^6]: Which is well known thanks to being used in the Raspberry Pi3. The BCM2837 has more problems than just supply, firstly it supporting only 1 GB Ram, secondly the proprietary boot process of the Raspberry Pi &mdash; really, the one point it has going for it, is that it can run GNU/Linux GUI at decent, accelerated speeds.
 [^7]: This is not intended to be hate on Canonicals Mir, which I consider a not too bad idea and in which am interested in seeing were it will end up.
