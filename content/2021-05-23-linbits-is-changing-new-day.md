+++
title = "LinBits is changing: New day, ..."
aliases = ["2021/05/23/linbits-is-changing-new-day.html"]
date = "2021-05-23T17:39:00Z"
[taxonomies]
tags = ["announcement"]
categories = ["internal"]
[extra]
author = "Peter"
+++

_It's sunday. Now what happened since last sunday?_ That has been the tag line for the "weekly update" for quite a while. It won't be for next LinBits.

<!-- more --> 

I have been thinking about changes to "LinBits" for the past months, as I was not really happy with many of my updates content. They often not only felt hastily put together, they actually were a race against a ticking clock.

### Why the old timing had its problems

The weekend is my main period of social interaction, which leads to delays and sometimes even conflict which do make the update worse. I knew: I should move it, but Friday and Saturday are not an option. Monday or Tuesday? Nope, I record a podcasts in the evening on these days, alternating by week. Thursday is when I usually record videos, if I manage to do so. _(I managed this week, feel free to check [them](https://linmob.net/videos/#PureOS) [out](https://linmob.net/videos/#postmarketos-plasma-mobile).)_

Therefore, the new day is going to be _wednesday_. It feels strange, yes, but it's the best of all the bad options I've had available. 

### What else should change?

Lately I have been trying to order the articles and videos by additional topical subheadings. This is something I want to pursue further. I also want to change my approach to _Comments in italics_, and only comment, where that comment actually adds value.

### Feedback welcome!

What do you think about this change? Do you have suggestions or ideas, how I could improve LinBits further?
Please send me an [email](mailto:linbits@linmob.net) or hit me up on [Twitter](https://twitter.com/linmobblog/) or [Mastodon](https://fosstodon.org/@linmob)!
