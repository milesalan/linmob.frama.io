+++
title = "LinBits 26: Weekly Linux Phone news / media roundup (week 53)"
aliases = ["2021/01/03/linbits26-weekly-linux-phone-news-week53.html", "linbits26"]
author = "Peter"
date = "2021-01-03T22:35:00Z"
layout = "post"
[taxonomies]
tags = ["Pine64", "PinePhone", "Arch Linux ARM", "DanctNIX Mobile", "Phosh", "Mobian", "Plasma Mobile", "Phosh", "libhandy", "rc3", "Purism", "Librem 5", "Camera", "Calendori"]
categories = ["weekly update"]
+++

_It's sunday. Now what happened since last sunday?_

A quiet week: Conference talks, Maemo Leste Update, App Showcases and more! _Commentary in italics._
<!-- more -->


### Software development and releases
* [Cawbird v1.3](https://nitter.net/CawbirdClient/status/1344690581931495425#m) and [1.3.1](https://nitter.net/CawbirdClient/status/1345838296342130691#m) have been released, working a lot better on the PinePhone.

### Worth reading 
* TuxPhones: [Running Linux on your smartphone: everything you need to know in 2021](https://tuxphones.com/2021-everything-running-linux-smartphone-guide/). _Great post by Raffaele, don't know how I missed it for last LinBits!_
* Guido Günther: [phosh overview](https://honk.sigxcpu.org/con/phosh_overview.html). _Who better to write a overview of Phosh?_
* Maemo Leste: [Fourteenth Update (July, August, September, October, November, December) 2020](https://maemo-leste.github.io/maemo-leste-fourteenth-update-july-august-september-october-november-december-2020.html). _Great update blog post by the Maemo Leste project._
* dimitris.cc: [Calindori and online calendars](https://dimitris.cc/kde/2020/12/30/Online_Calendars.html). _Nice workaround to get sync working for Calindori. This makes the PinePhone one step closer to being a daily driver._
* Pixls.us: [G’MIC experiments on Manjaro ARM Pinephone](https://discuss.pixls.us/t/gmic-experiments-on-manjaro-arm-pinephone/22120). _If you want to play more with your camera, this should be worth a read._
* Purism App Showcase:
  * [PasswordSafe](https://puri.sm/posts/app-showcase-password-safe/)
  * [Backups](https://puri.sm/posts/backups/) _Unfortunately, upstream Déjà Dup is not yet mobile compliant ([Purism branched it](https://source.puri.sm/Librem5/deja-dup/-/tree/pureos/40.5+2484+git5b578dfa-1pureos0)), so this won't work on every distribution yet._
* /u/bloggerdan: Pinephone as a daily driver? A Week with Mobian: [Day 1](https://teddit.net/r/PINE64official/comments/kjpyhf/pinephone_as_a_daily_driver_a_week_with_mobian/), [Day 2](https://teddit.net/r/PINE64official/comments/klfuku/pinephone_as_a_daily_driver_a_week_with_mobian/), [Day 4](https://teddit.net/r/PINE64official/comments/kn9wr5/pinephone_as_a_daily_driver_a_week_with_mobian/), [Day 5](https://teddit.net/r/PINE64official/comments/ko1z66/pinephone_as_a_daily_driver_a_week_with_mobian/), [Day 6](https://teddit.net/r/PINE64official/comments/kp4hdi/pinephone_as_a_daily_driver_a_week_with_mobian/), [Day 7](https://teddit.net/r/PINE64official/comments/kpjihb/pinephone_as_a_daily_driver_a_week_with_mobian/). _Interesting series!_

### Worth watching
* Linux Lounge: [Taking Photos on the PinePhone - PinePhone Camera Review](https://peertube.co.uk/videos/watch/de008bd7-ba7e-4dfb-87fa-4fba1e8dd85f)
* Gardiner Bryant: [The libhandy INFECTION?](https://lbry.tv/@TheLinuxGamer:f/The-Libhandy-INFESTATION:4). _A video about the Solus libhandy controversy. I like how nuanced Gardiner goes through this._
* HackersGame: [The System Admin's Multi-tool](https://lbry.tv/@HackersGame:4/the-system-admin-s-multi-tool:2). _Nice video about fun use cases for the Librem 5 or also the PinePhone._
* /u/redwisdomlight: [Librem 5 encounters part 13 initial success then turns into a failure 😞](https://teddit.net/r/Purism/comments/klt22s/librem_5_encounters_part_13_initial_success_then/)

#### rC3
* Cathal Mc Daid: [Watching the Watchers - How Surveillance Companies track you using Mobile Networks ](https://media.ccc.de/v/rc3-11511-watching_the_watchers_-_how_surveillance_companies_track_you_using_mobile_networks). 
* jn: [Porting Linux to your favorite obscure Arm SoC](https://media.ccc.de/v/rc3-80571-porting_linux_to_your_favorite_obscure_arm_soc). _This does not play for me, I hope that we're going to see a recording (not relive) eventually._
* jiska, Gerbert and Matthias: [Very Pwnable Network (VPN)](https://media.ccc.de/v/rc3-228534-very_pwnable_network_vpn#t=2328). _Beware of closed source VPN client software!_

#### What I forgot last year: 
* OpenAlt: [Martin Botka, AngeloGioacchino Del Regno, Konrad Dybcio: Qualcomm SoC upstreaming adventures in 2020](https://www.youtube.com/watch?v=xu-3CmRefvc). _I somehow missed this. The talk is a bit long, but it's worth it._

### Stuff I did

#### Content
I have uploaded one new video about installing the PinePhone from the PinePhone. Yes, it can be your only computer. 
* PeerTube: [PinePhone: Flashing the PinePhone from the PinePhone](https://devtube.dev-wiki.de/videos/watch/010aa5d1-ecdf-434e-aa92-ebad781d75af)
* LBRY: [PinePhone: Flashing the PinePhone from the PinePhone](https://www.youtube.com/watch?v=VPV024bRZ_4)
* YouTube: [PinePhone: Flashing the PinePhone from the PinePhone](https://www.youtube.com/watch?v=VPV024bRZ_4)

Also, I finally did something with that old, broken Psion Series 5: [PinePhone QWERTY the stupid way](https://devtube.dev-wiki.de/videos/watch/70e4e4bb-291e-42eb-aca7-804763e839ff).

#### Busywork
I finally went ahead and reinstalled my [ArchLinuxARM/DanctNIX](http://mobile.danctnix.org) mobile install. I backed up with Deja-Dup (after scale-to-fit) and was able to successfully restore that backup once I connected to my Lap Dock, as I was unable to choose individual files to be restored with just the touchscreen (there's a "mobilized" version of Deja-Dup by Purism, but no easy way to run it on Arch). 
I am going to write a blog post about my setup this week.
 
#### LINMOBapps
Not much happened here, just [a few additions and some maintenance](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master). Please [do contribute](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/tasks.md)!
