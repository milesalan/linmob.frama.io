+++
layout = "post"
title = "Thoughts on the PinePhone KDE Community Edition announcement"
aliases = ["2020/11/16/pinephone-kde-community-announcement.html"]
date =  "2020-11-16T16:00:00Z"
author = "Peter"
[taxonomies]
tags = ["PinePhone", "Community Editions", "KDE", "Plasma Mobile", "KDE Neon", "distributions", "postmarketOS", "Manjaro", "PineCone", "upgrade boards"]
categories = ["commentary"]
[extra]
thumbnail = "pmOS-edge-plasma-lockscreen.jpg"
+++

_I know, this is yesterdays news. But I didn't want to  just parrot the Press Release, and that takes time._
<!--/-->
## The KDE Announcement

![Plasma Mobile lockscreen on postmarketOS edge](pmOS-edge-plasma-lockscreen.jpg)

Yesterday, on November 15th, 2020, PINE64 published another one of their long monthly [update blog posts](https://www.pine64.org/2020/11/15/november-update-kde-pinephone-ce-and-a-peek-into-the-future/) (for the first time with a corresponding video ([LBRY](https://lbry.tv/@PINE64:a/november-community-update-kde-pinephone:7), [YouTube](https://www.youtube.com/watch?v=4su-YdjTYtY)) by the awesome PizzaLovingNerd), and at the same time, [KDE e.V. announced that the next PinePhone community edition](https://kde.org/announcements/pinephone-plasma-mobile-edition/) is going to ship with KDE software. 

It’s going to be the same PinePhone you know at the same price points, but with that nice KDE logo on the back.[^1] This is great news, another Community Edition shipping Phosh would have been quite boring. Also, I stand by my [earlier statement](https://linmob.net/2020/09/05/pinephone-building-plasma-mobile-apps-from-the-aur.html) that I like many of the mobile KDE apps, including, but not limited to Angelfish, KTrip and KWeather. Now, given the state of Plasma Mobile on the PinePhone, this may seem surprising to everybody that watched my 3 weeks old video[^2] on the very topic.

First, let me assure you, that my video happened to have bad timing. The situation has improved since.[^3] And given that these new PinePhones go on pre-order December 1st and are likely going to ship next year[^4], there is still time to iron out more bugs, develop further features, and so on.

When I got contacted with the press release, I immediately asked: Which distribution is this going to ship with?

Remember: Currently, we have 3 or 4 (depending on how you count), distributions that ship Plasma Mobile:
- KDE Neon, which is developer focused and gets rebuilt daily and thus often breaks,
- postmarketOS _stable (20.05)_ and _edge_ can be built via `pmbootstrap` or downloaded with Plasma Mobile,
- and Manjaro, who have been focussing on Phosh lately, because that's what they shipped on their Community Edition.

The answer I got was that the "distribution of choice is not decided" at the moment and that they are going to make their decision public as soon as we can. Now, of the above, only postmarketOS edge is really compelling &mdash; the Plasma Mobile release (5.19) in _stable_ is just too old at this point. But postmarketOS _edge_ also breaks from time to time while new releases are being merged, which might not make it the perfect choice.

If I had to bet any money on what they are going to do, I would bet in on a new "User Edition" of KDE Neon, based on Ubuntu 20.04  LTS that basically works just like the KDE Neon people know from their Linux desktops and notebooks. The current branch could continue as a "Developer Edition" (it just lacks that label currently, leading to user confusion and dissatisfaction), just as things are on "regular computers". They would basically solve three problems at once:

1. There is no current, stable and user focused Plasma Mobile distribution currently,
2. most users trying KDE Neon on their PinePhones expect a different, user-focused experience,
3. the project could control the experience and would not be relying on a 3rd party that might have different goals or priorities.

This would then only leave one problem: Someone (a person or, better yet, a team) has to create and maintain this distribution, and it should not slow down the overall progress of the whole Plasma Mobile effort. Also, hardware infrastructure requirements may lead to additional costs. I hope that they are going to able to solve this problem, maybe the 10 USD per device sold that KDE e.V. is going to receive can actually help here. 

## Further PINE64 announcements

That aside, PINE64 announced future Qi Wireless charging back covers for the PinePhone and pre-announced products around a new chipset, the Rockchip RK3566, that is going to be at the heart of future Single Board Computers in 2021 and other "non-Pro" PINE64 devices (which can be read as PinePhone) later on. _PineCone_, which is going to be a blob-free[^5] option for WiFi and Bluetooth on future PINE64 products, is certainly one to watch. 

And if you are an early adopter and have a Braveheart or UBports Community Edition PinePhone, don't miss that you can upgrade your PinePhone by purchasing a 3GB RAM/32GB eMMC PinePhone mainboard at a discount.


[^1]: Unless another hardware bug surfaces, this is going to be the same 1.2b hardware revision that’s currently available as Manjaro Community edition.
[^2]: "The current state of Plasma Mobile on the PinePhone", October 23rd, 2020, [PeerTube](https://devtube.dev-wiki.de/videos/watch/a4f1e6ef-41f5-41ab-a8bf-0e80e3c3d513), [LBRY](https://lbry.tv/@linmob:3/the-current-state-of-plasma-mobile-on:3), [YouTube](https://www.youtube.com/watch?v=Pz9DgFRmOMQ).
[^3]: See my video on convergence with Manjaro Plasma Mobile, which shows Plasma Mobile running a lot smoother: [PeerTube](https://devtube.dev-wiki.de/videos/watch/1d7249d4-e577-4c8c-9f64-74855792f019), [LBRY](https://lbry.tv/@linmob:3/manjaro-plasma-mobile-on-the-pinephone:e), [Youtube](https://www.youtube.com/watch?v=OgEnSd5w4cQ). My upcoming postmarketOS edge Plasma Mobile convergence video is going to confirm this.
[^4]: There is no shipment date announced yet, so I am just guessing here based on previous Community Editions.
[^5]: This means that &mdash; different from the proprietary firmware required to operate the PinePhone's Realtek RTL8723CS WiFi+BT solution &mdash; you are going to operate the PineCone with only open-source software and firmware.
