+++
title = "Cancelled, released and whatever else"
aliases = ["2008/12/cancelled-released-and-whatever-else.html"]
date = "2008-12-03T13:15:00Z"
layout = "post"
[taxonomies]
categories = ["commentary"]
tags = ["A910", "Android", "FSO", "GTA03", "i-geekmod", "iPhone", "Motorola", "Nokia", "OpenEZX", "openmoko", "UIQ"]
[extra]
author = "Peter"
+++
No, I don't have great news, but I just thought that a little write up of news of the mobile devices world might be nice. 
<!-- more -->
First of all, in case the Motorola fanboys belonging to my readers didn't notice: <a href="http://www.mobile-review.com/articles/2008/cancel-phone-en.shtml">Some pictures of the cancelled RAZR3 &#8220;Ruby&#8221; appeared recently</a>&mdash;it wasn't running Linux, but UIQ (as Motorola did apparently never manage to make MotoMAGX 3G capable), so this news won't kill anybody interested in linux on mobile devices. 

Nokia has launched its real answer to iPhone (I guess the well informed reader already heard about <a href="http://www.iphonelinux.org/index.php/Main_Page">iPhone Linux</a>) just yesterday. No, it doesn't run Maemo/Qt Extended (there might be products doing so in the future), but S60&#160;5th 
Edition (keep in mind that Nokia has plans to open-source Symbian) doesn't seem to be a bad deal, anyway (at least it isn't Windows Mobile). I have to admit that I like this N97, not a big surprise as I like the Nokia 5800 as well. 

Android for the OpenMoko FreeRunner is about to be usable in short term, <a href="http://onlinedev.blogspot.com/2008/12/porting-android-phase-5-how-close.html">there'll be images containing a softkeyboard soon</a>&mdash;U believe that this makes the FreeRunner more interesting for Android hype followers (like me^^) and might help its sales.

At least these news make the FreeRunner sound more interesting, it is anyway as there are lots of distributions out in the wild made for it.

But I suppose I'll wait for <a href="http://wiki.openmoko.org/wiki/GTA03">GTA03</a> ;) which is about to feature a new EDGE-capable modem chip (Cinterion mc75) according to Michael Lauer, who has described the difficulties in making ogsmd supporting different modems&mdash;Freescale Neptune, used in Motorolas' EZX phones appears to be a pretty bad one. But read [his blogpost](https://www.vanille.de/blog/ogsmd-and-its-modems/) yourselves.

Concerning EZX: I think I'll publish another igeekmod R2 build and then I'll stop creating builds (at least if I really manage to run moon-buggy in my A910s terminal), at least for some time&mdash;there are chinese guys creating one build after the next one, I don't think I have to keep up with that&mdash;when there'll be great improvements, like multikeypress support and a taskmanager, I will &#8220;igeekmodify&#8221; that firmware&mdash;but that's it. I am looking forward to focus on 2.6&mdash;keep in mind that I am no hacker, please&mdash;to try things, weird stuff and so on, on A910 and E680i.

BTW: If I were a hacker, I would try to port Linux to my Samsung SGH-i780, a device I really like&mdash;but Windows Mobile is, at least from time to time, a pain in the ass.
