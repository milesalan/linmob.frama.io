+++
title = "An update on the reMarkable paper tablet"
aliases = ["2018/11/25/an-update-on-the-remarkable-paper-tablet.html"]
author = "Peter"
date = "2018-11-25T14:05:37Z"
layout = "post"
[taxonomies]
tags = ["cloud", "cloud services", "digital assistant", "digital reading", "eInk", "eInk tablets", "ePaper", "OCR", "reMarkable"]
categories = ["impressions"]
+++

_<a href="https://brimborium.net/a-remarkable-paper-tablet/">In April</a> I wrote down my impressions on the reMarkable Paper Tablet. This is an update to that post._<!-- more -->

### How much did I use it?

For some time, I have used it a lot.But after getting a Microsoft Surface Pro 3 tablet,[^1]  my use the reMarkable dwindeled as that Surface was just a lot more convenient for what I did at the time.[^2] Unfortunately, the Surface started to suffer from "ghost touch" issues, which &mdash; while I still like to do stuff with the pen on it – has made me use the reMarkable more again, with the Surface serving as a laptop replacement.[^3]

### Have there been software updates?

Yes. Bugs concerning WiFi, which had to turn off again after every restart (which occur often on the reMarkable, as it has an option to automagically shut down in order to preserve battery life), now stays in the state you set it to, which is important, as WiFi can drain the battery in no time.

A later update greatly improved the export features, so that you can now reliable work with what you created on the reMarkable on other devices and also means that you can create reliable backups.

Also, they added a cloud based OCR feature, which made me reconsider using their cloud service.[^4] Actually, I wrote this article on the reMarkable, and exported it using the OCR feature.[^5] While I really like the feature, I would have pre­fered to have this feature on device. The Apple Newton could do this reliably in the mid to late 1990s, so the relative underpoweredness of the reMarkable hardware can't be the excuse here.[^6]

All in all, reMarkable (the company) have been working hard to improve the device, and newly introduced bugs have been fixed quickly.

### What do I wish for? 
* Better ways to get files (mainly PDFs) onto the device without using the cloud service, e. g. via USB.
* An independent security audit of their cloud solution in order to improve trust.
* Being able to turn the screen content by 180° degrees. I inadvertently sometimes hit the right page tum button with the palm of my hand, which is annoying every time it happens, since it is a major distraction and has led to "thought-loss".
* Better text highlighting when working with pdf, which sticks to the lines of the highlighted document and can be seen in color after exporting the annotated/highlit document to other devices.</li>
* Awareness of plugged-in USB-keyboards (hiding the on screen software-keyboard) and an expansion of the tiny text editor which was introduced with the OCR feature into an actual basic text editor to turn this into an alternative to the <a href="https://getfreewrite.com/">FreeWrite</a>. Even with a keyboard attached, the reMarkable could be a remarkably distraction free writing tool.

### Anything else?

Yeah, I switched to using another pen, the <a href="https://www.staedtler.com/de/de/noris-digital/">Staedtler Noris digital</a>, which has an iconic pencil look to it. Writing with it feels less like writing with an actual pencil on paper, more like using a felt pen. But I like that I have less "pen anxiety" now, since replacing the Noris would be relatively cheap, and also I felt I went through pen tips way to quickly with the original reMarkable pen.

### Do I recommend it?
Nothing changed here. If you always wanted to have computerized paper and want an eInk-PDF-reader[^7], get one, but do check out comparable devices like the <a href="https://www.youtube.com/watch?v=-REeEzKDn8g">Onyx Boox Note</a> or the really promising-seeming <a href="https://the-digital-reader.com/2018/11/06/boyue-likebook-mimas-has-a-10-3-screen-video/">Boyue Likebook Mimas</a> before.


[^1]: Because it turned out that using the recommended tools (mostly Citavi and Microsoft Office) for university can make life easier, especially as I spend my workdays in Microsoft Word anyway.

[^2]: Marking up PDFs with colors is sometimes more useful than doing it in black and white.

[^3]: Mostly in portrait mode with a ThinkPad USB keyboard attached, which is tremendously great for text writing and editing. Everybody should try this.

[^4]: As I am using more proprietary OSes nowadays (Windows 10 and macOS Mojave), shell scripts to get data onto the device had become really impractical, anyway. BTW: There is this experimental web interface, which can upload files onto the tablet. You just need to drag and drop them onto the website.

[^5]: Which worked quite poorly, but I wrote the text in question using the "virtual pencil", so it is in part my fault.

[^6]: I consider it more likely that the actual reason is per device licensing cost as I would assume that this is not a feature they created in-house, or else they may apply machine learning here.

[^7]: I almost forgot to mention that epub-Support is still pretty terrible…
