+++
title = "Switching Phones without wanting to"
aliases = ["2020/02/01/switching-phones-without-wanting-to.html"]
author = "Peter"
date = "2020-02-01T06:41:00Z"
layout = "post"
[taxonomies]
tags = ["16:9", "2020", "Android", "backup", "iOS", "iPhone", "iPhone SE", "Librem 5", "linux", "mobile device backup", "PinePhone", "Pixel XL", "smartphones", "sustainability"]
categories = ["article", "personal"]
+++

On December 21st, 2020, my Google Pixel XL, which I had used with LineageOS and microG and loved for its great camera, just plainly died. During a phone call.<!-- more --> The screen turned black, and never lit up since. When connected to a computer, it turned out it is stuck in something that is called "HS-USB QDloader 9008 mode".[^1]  And, if all resources I have found yet are to be believed, without specific files that would have to published (or leaked) by the device manufacturer, there is no way to get this fine hardware back into a usable state.[^2]

While trying hard to buy less electronics because of their carbon intensity and the resulting toxic waste, I had already planned a switch of phones in this year, as I had pre-ordered the Purism Librem 5 in late 2017 (?) and a Pine64 PinePhone "Braveheart Edition" later in 2019. But none of these phones were yet in my hands.[^3] All I had in fairly recent hardware were an iPhone SE that I had got myself as a secondary phone for my old number[^4] and then the bq Aquaris X5 I had purchased new in early 2017 for about 120 Euros, which had served me just fine before getting the Pixel XL. In order to avoid switching SIMs I just went ahead and started using the bq Aquaris X5 again. 

Unfortunately, going back to a phone that is worse in almost every single way (performance, camera, display, storage, …) and is not supported by Lineage OS officially any more and thus does not receive any regular security patches feels really like defeat. I found an experimental build of Android 8.1, which ran mostly fine (even though I could not get signature spoofing and thus microG to work), but boy did I hate using it ever more. So I found myself hunting for Pixel XLs on eBay and Pixel 2s, and ... in reality I just mostly used that teeny tiny iPhone SE, since it was just a lot better.[^5] Given that I am set to receive two new phones this year anyway, and the fact that the Pixel XL has no more vendor support, while newer Pixel phones are not supported by my favourite Custom ROM and running stock Android[^6] is completely out of the question for me, I eventually figured I would just go ahead and use this iPhone SE as my main phone for now. Despite sustainability and avoiding excessive consumerism, on motivation was the availablity to easily do local, non-cloud backups with the iPhone, having just lost a bunch of data with my seemingly unfixable Pixel XL.

The main obstacle as I moved the services I use over was battery life. Even with moderate use, the battery was often asking to switch on "Low Power Mode" around 1pm. So there I was, thinking again: Well, maybe I should just pay the money and get myself an iPhone 11. But (aided by being short on money after switching appartments in mid-January) I managed to find an alternative: There are third party battery cases for the SE, that have ok reviews. They aren't as well integrated as Apples own battery cases[^7] I just bought a cheap one of Amazon.[^8]

With this thing on, my iPhone SE earned itself a new nickname: Klumpen.[^9] <em>And it just works.</em> iSH is an okay termux replacement, KyPass is not beautiful, but usable/decent KeePass client. I am fine. And my mobile life is again backed up: If I loose this SE (physically or through sudden device death) I can just get myself another iPhone and restore an encrypted backup.


[^1]: For reference, have a look at this <a href="https://forum.xda-developers.com/pixel-xl/help/help-pixel-xl-hs-usb-qdloader-9008-mode-t3574278/page6">discussion on xda-developers</a>.

[^2]: Actually, I think that there ought to be regulation to make this category of problems user repairable (#righttorepair) or to force the manufacturer to offer fairly priced (compared to used-market device prices) repair options.

[^3]: They still aren't.

[^4]: I bought it for three reasons: 1. Its miniscule size; 2. being able to use iOS again every now and then; 3. Use services that did not work with microG, like car-sharing or eScooter and other mobility services.

[^5]: The SE I use is not perfect though: The front camera has dust between camera and glass and it just has 32GB of storage (the eBay listing said it would have 64 GB, but …).

[^6]: … or Graphene OS, for completely different reasons (services not working that I may want to use).

[^7]: They ironically never made one for the iPhone SE, even though it has the worst SoC-compute/battery capacity ratio of all iPhones ever.

[^8]: <a href="https://www.amazon.de/gp/product/B07S5K37BC/ref=ppx_yo_dt_b_asin_title_o01_s00?ie=UTF8&amp;psc=1">This one.</a>

[^9]: Rough translation: Chunk or nugget.
