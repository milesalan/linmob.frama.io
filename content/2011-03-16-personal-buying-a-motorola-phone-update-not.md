+++
title = "Personal: Buying a Motorola Phone | Update: NOT!"
aliases = ["2011/03/personal-buying-a-motorola-phone.html"]
date = "2011-03-16T17:34:00Z"
[taxonomies]
tags = ["Android 2.1 (Eclair)", "EZX/LJ", "Motorola", "Motorola Flipout", "TI OMAP 3410"]
categories = ["personal", "hardware"]
[extra]
author = "Peter"
+++
__Update:__ _Unfortunately I received an email today telling me that the seller that offered the Flipout at this bargain price hasn't enough Flipouts in stock and is unable to order more. This is really sad as I was looking forward to play with a low cost mid end Android handset made by Motorola. I couldn't find a similar deal to buy a Flipout, yet. If I do, I will get one to fulfill my promise of reviewing and tweaking this little device._

When I first read, that Motorola would make Android phones, I was quite excited. At that time I had three Motorola phones, all EZX and thus running Linux: A first generation device (E680i) and two second generation ones (A910).

Android, as I understood back then, would make these phones a lot more interesting, as there would be tons of native software&mdash;while on EZX it was more about J2ME.

The CLIQ/DEXT, Motorola's first Android phone, wasn't to interesting for me, as I already had a similar, not too different, HTC made phone: The T-Mobile G1.

Then, later, Motorola came up with the first Android 2.0 phone, the Milestone. I was ready to throw money at this device&mdash;but then, out of a sudden, the locked bootloader story spreaded and I was turned off. So turned of, that I didn't buy it, and in fact, no other Android handset since the G1. I didn't like the design of HTCs later phones, and while I really liked what Motorola came up with (especially the Milestone2 and the Defy), I always felt like: Android isn't fun without custom ROMs&mdash;back than I already was a heavy CyanogenMod user.

Now, today, I ordered a Motorola Android phone. It's an ARMv7 powered phone and it was really cheap: 99€ including shipping without any contract. As you might guess, this isn't a Defy, Milestone, Milestone XT720 or even Milestone 2&mdash;it's the Motorola Flipout, a phone with decent specs, as long as you don't mention the screen. But hey, it's small and has a hardware keyboard which seems to be just big enough, and since my G1s hardware keyboard is broken since CeBIT.. well, I just had to buy it.

I will test the device, and if Motoblur is too annoying, I will &#8220;deblur&#8221; it and tweak it as far as possible without a custom ROM&mdash;JIT, Apps2SD and so on.

I am quite excited how this Motorola phone will feel when I'll hold it in my hands. If it is astonishing and I notice that I can live without the latest Android release I might be tempted to get another Motorola Android phone&mdash;or start / support a petition to persuade Motorola to leak open bootloaders for the phones they discontinued (= no updates to later Android releases) way to early.
