+++
title = "A Keyboard for the FreeRunner"
aliases = ["2009/03/keyboard-for-freerunner.html"]
date = "2009-03-04T17:39:00Z"
[taxonomies]
tags = ["gta02", "keyboard", "hardware", "modding", "Neo Freerunner", "soldering",]
categories = ["projects"]
[extra]
author = "Peter"
+++
<a href="100_0410.JPG"><img style="margin: 0pt 0pt 10px 10px;float: right;cursor: pointer;width: 280px;height: 210px" src="100_0410.JPG" alt="FreeRunner on top of my keyboard" border="0" /></a>
Even before I had a FreeRunner, I thought that a device, that aims to be a mobile computer needs a decent keyboard to enable its user to write longer texts. It might be due to my efforts to become a writer, that I am so obsessed of being able to write texts on a mobile device, but hey, now I have a FreeRunner - and after I repaired it with the soldering iron, I don't fear soldering any more.

So I decided to get myself a keyboard, first thinking of those foldable rubber keyboards, but after somebody told me those weren't that great to type on I remembered the foldable keyboard my father has for his Palm m130.
<a href="100_0411.JPG"><img style="margin: 0pt 0pt 10px 10px;float: right;cursor: pointer;width: 280px;height: 210px" src="100_0411.JPG" alt="Closed keyboard next to FreeRunner" border="0" /></a>
So I checked out keyboards of this kind on (german) eBay, and within the rather cheap i found a keyboard for good old Toshiba PDAs (E740, E750). Luckily Fred pointed me to a <a href="http://pinouts.ru/PDA/toshiba_e330_pinout.shtml">pinouts.ru</a>, and guess what, this device is among the few ones featuring USB Host mode - other PDA devices of that time ususally use a serial (RS-232) connection to connect keyboards or other accessoires.

So I bought such a keyboard, and, as I was thinking about how to connect it to the FreeRunner, a USB/VGA adaptor for the earlier mentionned PDA. It turned out that it would be <a href="100_0407.JPG"><img style="margin: 0pt 0pt 10px 10px;float: right;cursor: pointer;width: 280px;height: 210px" src="100_0407.JPG" alt="FreeRunner in use on the keyboard" border="0" /></a>much more difficult to make an adaptor out of the USB/VGA connector than modifying the keyboard (first I was scared to modify  the keyboard because of its mechanics). So I went that way and replaced the Toshiba connector and the keyboard now has a standard USB A plug, which I connect via an mini B to A adaptor to my FreeRunner. The A plug has some advantages, anyway, as it enables you to connect the modified Belkin G700 keyboard to any PC.

Unfortunately I will have to make a special keymap for the device, as the Enter key is being recognized (with a standard keymap) as &#8220;Print&#8221; key - just to name one example.
But besides that, there are no real problems.
<a href="100_0403.JPG"><img style="margin: 0pt 0pt 10px 10px;float: right;cursor: pointer;width: 280px;height: 210px" src="100_0403.JPG" alt="FreeRunner and Keyboard" border="0" /></a>
Modifying the keyboard wasn't really easy, but not that hard - I think I will get another one and make a step by step guide with lots of photos for those who are interested in this.

I have to say that I really like the keyboard (<a href="http://www.kickstartnews.com/reviews/accessories/belkin_pda_keyboard.html">here</a> is a review of it..), but there are some bad points:

* All those keyboards available on german eBay seem to have a german QWERTZ layout, which might be inconvenient for those who are used to other keyboard layouts, but can't stop looking at the keys from time to time.
* It would be nice to charge the FreeRunner/Neo1973, while using the keyboard, which isn't possible with my mod right now (I lead the cable trough the place of the power adaptor)
* A perfect for keyboard would certainly feature an integrated USB hub to connect even more devices (mouse, usb stick)</li></ul>And if course I would still like to have a thumb keyboard or some kind of input device to write SMS on the go&#8230; <a href="http://www.om.vptt.ch/site/?p=663">Maybe the Swisscom guys are working on that ;)</a>

_(Some advice for those who can't want to wait for a step by step guide:_
_Don't open the screws you see when you have the keyboard at maximum width, you don't need to do that. The thin brown wire inside the keyboard is DATA +, the thin yellow is DATA -)_
