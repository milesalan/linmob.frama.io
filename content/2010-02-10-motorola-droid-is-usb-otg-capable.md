+++
title = "Motorola Droid is USB OTG capable"
aliases = ["2010/02/motorola-droid-is-usb-otg-capable.html"]
author = "peter"
date = "2010-02-10T10:49:00Z"
layout = "post"
[taxonomies]
tags = ["Motorola", "motorola droid", "Motorola Milestone", "openmoko", "T-Mobile G1", "usb host", "usb otg"]
categories = ["shortform", "hardware"]
+++
While most mobile devices lovers are eagerly awaiting the Mobile World Congress in Barcelona, Spain, I am happy because I read today that the Motorola Droid is actually able to be a USB Host. Of course you need a special cable to use your Droid even more like a computer thanks to this feature&mdash;but hey, it's great anyway, thanks to <a href="http://www.tombom.co.uk/blog/?p=124">Chris Paget for the cable guide</a>, and Mike Kershaw and Mike Baker, who are Kismet/OpenWRT hackers, for the hacking.
As soon as it is confirmed that the GSM-Droid, the Milestone is USB OTG capable too, I will be tempted to replace my Openmoko FreeRunner and T-Mobile G1 by a &#8220;Milestone&#8221;.
