+++
title = "Android Bootloader locks seem to disappear"
aliases = ["2011/03/28/android-bootloader-locks-seem-to-disappear.html"]
author = "peter"
date = "2011-05-30T19:16:00Z"
layout = "post"
[taxonomies]
tags = ["Android", "hacking", "locked bootloader","htc", "Motorola", "openness", "Samsung", "sony ericsson"]
categories = ["shortform"]
+++
<a href="http://laforge.gnumonks.org/weblog/2011/05/30/#20110530-htc_no_locked_phones">Harald Welte has written an article on a topic I was going to write on, but now that he did, I don&#8217;t have to.</a>
