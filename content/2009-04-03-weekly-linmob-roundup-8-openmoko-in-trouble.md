+++
title = "Weekly linmob roundup (8): Openmoko in trouble?"
aliases = ["2009/04/30/weekly-linmob-roundup-8-openmoko-in-trouble.html"]
author = "peter"
date = "2009-04-03T23:00:00Z"
layout = "post"
[taxonomies]
tags = ["Android", "Documents To Go", "FSO", "GTA03", "OpenExpo", "openmoko", "Plan B"]
categories = ["weekly update"]
+++
_As I don't have much time ATM (will change on monday), I decided to make one article about the recent OpenMoko news and the so called &#8220;weekly roundup&#8221;._

To summarize shortly what happened, I will just take the stuff I posted on <a href="http://identi.ca/linmob">identi.ca</a> this week.

<a href="http://is.gd/q5k1">iPhone OS on #openmoko&mdash;april fools.. ;) #aprilapril</a> The french community made a april fools joke, which was easy to see through, but still funny: iPhone OS ported over to the OpenMoko platform. Actually there is still some Object C written Mac like OS running on a Linux kernel for the Neo1973 (not for the FreeRunner, as there is no &#8220;Big Endian&#8221; kernel available for it) called OpenStep / mySTEP.

<a href="http://is.gd/qlMH">DocumentsToGo for Android</a> will make the Android platform more interesting for business users as it enables you to view and edit MS Office 2007 documents.

Two videos about OpenMoko at the Embedded Systems Conference, San Jose, California: 
* <a href="http://is.gd/qlSY">Sean Moss-Pultz on ESC'09: Openmoko is the Anti-iPhone, still no 3G</a> and
* <a href="http://is.gd/qmfs">Openmoko mobile platform presented at Embedded Systems Conference</a>
Sean Moss-Pultz (Openmoko Inc.'s CEO |<a href="http://threads.moss-pultz.com/">check out his private page</a>) points out, what the Openmoko FreeRunner is: Mainly a cheap, portable development board and not a smartphone.

We all ([meaning all the people interested in Openmoko and free embedded hardware) had high hopes for the follow up device to the Neo FreeRunner (GTA03), which long time codename was GTA03, e.g. because of an ARM11 SoC, Quadband EDGE and a new case design. At the <a href="http://www.openexpo.ch/en/">OpenExpo in Bern</a>, Switzerland, Sean Moss-Pultz of OpenMoko announced that the rumored GTA03 is postphoned, meaning cancelled, due to low OpenMoko resources. <a href="http://translate.google.de/translate?u=http%3A%2F%2Fwww.bernerzeitung.ch%2Fdigital%2Fmobil%2FVorlaeufig-kein-weiteres-LinuxTelefon%2Fstory%2F15176367&amp;sl=de&amp;tl=en&amp;hl=de&amp;ie=UTF-8">As the &#8220;Berner Zeitung&#8221; tells us</a>, after 3000 units of the Neo1973, 10000 units of the FreeRunner were sold. It looks like that they actually mix up GTA03 and the FreeRunner, as software development for GTA02 goes on, though the community will have to get involved even more, Openmoko has to &#8220;fire&#8221; some people to stay alive.

Openmoko will work release another device, &#8220;Plan B&#8221;, instead, which&mdash;as this mailing list post by Steve Mosher (Openmoko Inc.'s VP of marketing) describes&mdash;isn't a phone and needs only on third of resources of GTA03 and is aimed towards mass market. But hey, <a href="http://lists.openmoko.org/pipermail/community/2009-April/044915.html">I will just copy parts of what Steve wrote</a>:

<blockquote>Our biggest challenge was to make a choice about how to spend the balance of 2009.

There were two paths: 
* A: Fulfill our promises on FreeRunner and launch GTA03
* B: Fulfill our promises on FreeRunner and launch project B.

We will talk more about project B in the coming months, but these salient facts should be able to guide any budding executives out there.

1. GTA03 was in constant flux as a design.
2. GTA03 schedule was consequently always slipping.
3. The resources required for GTA03 are 3X those required for Project B.
4. We don't have 3X.

So, we picked plan B.
</blockquote>

But what about a future open mobile computing / communication device by Openmoko? This is what Steve tells:

<blockquote>Now comes the question, what about GTA03? how do we get there? And when? 
And what is it?

Well my basic argument was and is this:

First we attend to the issues that still remain with the GTA02. That's why the VP of marketing ( of all people) is working on the buzz fix problem. Second we complete project B. When we've done that, then we get to eat dessert. Essentially, I made the same argument I heard so many times on this list: &#8220;How do expect us to buy a GTA03 when you've yet to deliver on all the promise of FreeRunner?&#8221; And I took the arguments I heard from disty seriously, &#8220;how do you expect us to buy FR, when GTA03 is right around the corner?&#8221; And I accepted the arguments I heard from Engineers I respect who questioned the viability of the GTA03 in the market place. All of those arguments said &#8220;put a bullet in its brain pan!&#8221;

So, what about GTA03? As it was defined, it is dead. So how do we get to a new GTA03? Two requirements: continue to improve GTA02; deliver on project B. What is GTA03 and when do we get there? There are a number of independent efforts out there that are pitching me ideas for GTA03. I talked to sean a bit about this and I'd like to try to open up more of the design process and the marketing process to the community. Perhaps on a separate list. Some of these discussions have already started.

What can you do to help?

1. Move GTA02 code upstream.
2. Stay Involved.
3. Continue work on applications
4. Buy a FreeRunner.
5. Get involved in GTA03 discussions </blockquote>

[BTW: <a href="http://www.openmoko.com/">There is a limited $299 offer of the FreeRunner</a>]

Actually I really hope that &#8220;Plan B&#8221; (be it a GPS device, a MID, a multimedia device, an ebook reader or whatever) will be the commercial success Openmoko needs badly, so that there will be a follow up to the GTA02. This struggling of Openmoko doesn't surprise me, that's business: Considering they sold their 13000 devices they sold since 2007 at an average of 300$ , this is just $ 3,900,000&mdash;and you have to see that these aren't the earnings, as it doesn't contain the costs for the hardware&mdash;let's assume that the earnings are just one to four tenth of that. You can't employ many people for a long time with that money (though we shouldn't forget about the vertical &#8220;<a href="http://www.dash.net/">Dash Express</a>&#8221;)&mdash;Openmoko needs a CashCow or Star and it needs it badly.

But let's stop these speculations now and see whether there is more to tell.

While Sean Moss-Pultz talk (<a href="http://www.openexpo.ch/fileadmin/documents/2009Bern/Slides/BusinessTrack/01_SeanMossPultz.pdf">get the slides here</a>) at OpenExpo isn't on video in <a href="http://www.youtube.com/user/openexpo">OpenExpos YouTube channel</a>,  a german <a href="http://www.youtube.com/watch?v=ZAZzKR03piY">talk (</a><a href="http://www.openexpo.ch/fileadmin/documents/2009Bern/Slides/TechnologyTrack/02_MichaelLauer.pdf">slides</a>) by Michael &#8220;mickeyl&#8221; Lauer, freelancer who was employed at Openmoko some time ago, about <a href="http://http//www.freesmartphone.org/index.php/Main_Page">FreeSmartphone.Org (FSO)</a> is.

And don't forget to take a look at the latest<a href="http://wiki.openmoko.org/wiki/Community_Updates/April_05%2C_2009"> Openmoko community update</a>!

I guess that's it. If I forgot something (not that unlikely this week), maybe I will add it on tuesday ;) 

_Hope to see you again the next saturday&mdash;and feel free to comment!_
