+++
title = "Breaking news: TIZEN is LiMo+MeeGo+HTML5"
aliases = ["2011/09/28/breaking-news-tizen-is-limo-meego-html5.html"]
author = "peter"
date = "2011-09-28T11:07:00Z"
layout = "post"
[taxonomies]
tags = ["HP webOS", "HTML 5", "html5", "LiMo", "MeeGo", "platforms", "Tizen", "webos"]
categories = ["software", "commentary"]
+++
_It's a nice day, the weather in Munich is awesome, but I am staying in during lunch break to write this article on what just happened today._

Today <a href="http://www.tizen.org">Tizen</a> was announced. Tizen is yet another Linux based operating system, which replaces (read: probably merges) <a href="http://limofoundation.org/">LiMo</a> and <a href="http://www.meego.com">MeeGo</a> into one common platform which is supposed to be optimized for HTML5 apps. 
<!-- more -->

<img style="clear:left;float:left;margin-right:1em;margin-bottom:1em" height="101" width="316" src="tizen.gif" />
While MeeGo relying pretty heavily on Qt and LiMo was known for it's use of GTK+ (in its fourth release GTK+ was joined by <abbr title="Enlightenment Foundation Libraries">EFL</abbr>, but as there are no known devices build around the LiMo r4 platform, let's forget this here ;) ), this is a change to, let's call it &#8220;Linux + Something (doesn't really matter, as all of the afore mentioned Toolkits/Frameworks include their own flavour of WebKit) + WebKit.

The first release of Tizen, which is backed by Intel, Samsung, The Linux Foundation and most likely other players is going to be released alongside an SDK in the __first quarter of 2012__. Tizen is meant to run on a variety of different classes of devices, namely Smartphones, Tablets, Netbooks, In-Vehicle Infotainment Systems and Smart TVs. 

### Comment

I am not really sure what to make out of all this. It feels like an insane stunt. While it certainly makes sense to merge two foundering platforms, the HTML5 move seems odd and nothing else, considering that webOS, which was build using these exact technologies, is dying, partly because of its extensive use of these technologies which simply aren't that mature yet (webOS always had speed issues). There is one more concern: Is there the room for yet another platform? With Windows 8 coming to ARM, Android being really huge and still growing (despite all the issues it is facing), there is not that much room for another player, as Apple has a large share of the market, too.And then there is Chrome OS, which is basically a glorified web browser, a competitor which may be (when first mass market aimed Tizen devices will surface, it will likely be late 2012 or 2013) different and stronger than we expect it to be right now.

However, I wish the new Tizen project the very best luck and success, because I believe that the market and the users need a truly open alternative. Tizen could fill this vacancy, let's hope it will do!

__SOURCES:__ <a href="https://www.tizen.org/blogs/dawnfoster/2011/welcome-tizen">Tizen.org</a>, <a href="https://meego.com/community/blogs/imad/2011/whats-next-meego">MeeGo</a>, <a href="http://limofoundation.org/en/limotizen.html">LiMo</a>
