+++
title = "A910 again"
aliases = ["2008/01/a910-again.html"]
author = "Peter"
date = "2008-01-08T10:33:00Z"
layout = "post"
[taxonomies]
tags = ["A910", "modding", "OpenEZX"]
categories = ["personal", "projects"]
+++
After long time without writing about freeing the A910s WLan, I'm writing again.
<!-- more -->

No: I wasn't successful yet, but if you do nothing, you cannot get it to work - I didn't spend much time on the problem since my last post.

I wasn't motivated to do so, don't ask me why, one point is, that I donated my old one to OpenEZX. Well, maybe there will be even easy to use images for this device some day. The problem is: There is no big community, it doesn't seem that there are many A910s sold to users that care about other things then phoning and sms&mdash;maybe due to the fact it is sold for small money with BT fusion/UNIK.

After my first attempts to mod the firmware failed, I think that scripting could be the solution. Best thing would be a special scripted application that just frees WLan and creates a route that works for Java apps and Opera, just execute and it works.

The problem is: I don't know enough about that stuff, Bernard knows more, but he has not enough time to do it.
And as my sundays are completely free this year, I'll try to work on it every sunday. What else should I do with that time&#8230;

And if this all doesn't work (maybe even if it does) I'll get my self an E28 E2831, sold in France as &#8220;TWIN tact&#8221; by &#8220;NEUF&#8221;.
