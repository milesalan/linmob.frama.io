+++
title = "A910 will be able to use Wifi to surf the internet in approximately 8 weeks"
aliases = ["2008/04/a910-will-be-able-to-use-wifi-to-surf.html"]
date = "2008-04-11T11:36:00Z"
[taxonomies]
tags = ["A910", "A910i", "EZX/LJ", "SBF_Backup"]
categories = ["projects"]
[extra]
author = "Peter"
2021_commentary = "With hindsight, I find my own beta-blah insufferable."
+++
... and this is a pessimistic approximation.
<!-- more -->
After I failed to mod A910 into A910i by just replacing files and linking some of them to fit CG sizes, I took A910s latest firmware (the latest I have) as a base to just port some parts of A910i firmware into a new A910 firmware which I call A910-R57.geekmod. I will release it, as soon as I got WiFi working&mdash;as a beta. ATM I am creating alpha after alpha, and after porting A910i's WiFi app, it is at least possible to connect to WiFi networks successfully&mdash;native apps (ping, telnet) work, Opera doesn't work yet.
I already tried to use a different apmd to overclock my phone, but as it drains battery too quick and doesn't have a real effect on the phones' UI-speed, I won't release an overclocked firmware&mdash;I think it might be better to enable the users to use SWAP easily, as A910 has only 1-5&#160;MB Ram (of 48MB) free. This might speed up the phone. There will be some more gimmicks in my &#8220;geekmod&#8221;, but I'll tell you later&mdash;you have to stay tuned. Integrating features is mostly limited by available space in flash memory&#8230;

ATM I am missing some configuration files of A910i firmware to get Opera to work&mdash;they seem to hide in /ezxlocal and /ezx_user. There is a<a href="http://www.e2mod.com/content/view/241/28/"> tool to extract them (for E2)</a>, which should work on A910i. I would really appreciate it, if some A910i user would extract them and give me a link to download them, as it seems to be the easiest way to get A910's WiFi ready for internet access (the other ways seem to be a lot harder).

Installation instructions:

<blockquote>In order to install it, you just have to extract the rar-file and put SBF Backup.mpkg on your phones memory card. Then you start up your phone and choose the file in filemanager and attempt to open it, there'll be a menu which will ask you, where you want to install that programm&mdash;I'd recommend to choose the memory card.

After installation (which will take some time) you'll find a new item in main menu, choose it.

After some time (I recommend just waiting 20 minutes to ensure it's successful (you can use your phone in that time, though it might be quite slow) you'll find a file called backup.sbf in your memory cards root folder.</blockquote>
