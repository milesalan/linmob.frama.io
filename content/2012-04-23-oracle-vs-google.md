+++
title = "Oracle vs Google"
aliases = ["2012/04/23/oracle-vs-google.html"]
author = "peter"
date = "2012-04-23T05:49:00Z"
layout = "post"
[taxonomies]
tags = ["Android", "law", "Oracle vs. Google", "patent law",]
categories = ["shortform"]
+++
<a href='http://www.fosspatents.com/2012/04/pragmatic-approach-to-copyrightability.html'>Oracle vs Google</a><div class="link_description"><p>Florian Müller, anti-software-patent activist on the ongoing case, whether Google has actually infringed Oracles Java Patents with Android.</p>

<p>*Just kidding: If Android is being banned from market, there might be a real world market chance for Tizen, Boot2Gecko and Open webOS&#8230; or maybe Nokia is bought because of its Maemo/MeeGo assets.</p></div>
