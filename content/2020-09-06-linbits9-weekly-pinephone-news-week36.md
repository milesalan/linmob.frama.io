+++
title = "LinBits 9: Weekly PinePhone news / media roundup (week 36)"
aliases = ["2020/09/06/linbits9-weekly-pinephone-news-week36.html", "linbits9"]
author = "Peter"
date = "2020-09-06T17:50:00Z"
layout = "post"
[taxonomies]
tags = ["Pine64", "PinePhone", "Ubuntu Touch", "Arch Linux ARM", "Anbox", "adb", "App Lists", "Camera", "Nemo Mobile", "Manjaro", "Lomiri", "postmarketOS", "PinePhone", "Unboxings"]
categories = ["weekly update"]
+++

_It's sunday. Now what happened since last sunday?_

p-boot, a first Manjaro Lomiri image, upcoming camera support improvements and much more. _Commentary in italics._
<!-- more -->

### Software releases and improvements
* [megi has released code to his p-boot](https://xnux.eu/p-boot/) PinePhone multi boot loader, which has a GUI to select an OS. _Make sure to watch the video linked below._
* Manjaro have released a [new dev image featuring Plasma Mobile](https://osdn.net/projects/manjaro-arm/storage/pinephone/plasma-mobile/dev/200906/) and their [first image with Lomiri](https://osdn.net/projects/manjaro-arm/storage/pinephone/lomiri/alpha0-20200816/).
* Mobian [have announced support](https://fosstodon.org/@mobian/104802333632393413) for the Convergence Package PinePhones starting with the 20200903 nightly.
* Nemo Mobile [have announced](https://twitter.com/neochapay/status/1301801770126118912) that their next release will be on a Fedora. The migration progress can be followed [here](https://build.opensuse.org/project/subprojects/home:neochapay:nemo)
* [Marius Gripsgard has shared progress on the PinePhone camera](https://twitter.com/Mariogrip/status/1302410923383103488), which looks quite impressive. Previously, [Lukasz Erecinski from Pine64 had shared his impressions with an experimental Qt 5.12 based UBports image](https://twitter.com/LukaszErecinsk1/status/1301306880216707077?s=20).
* UBports [have announced a slight delay for their OTA 13 update](https://ubports.com/blog/ubports-blogs-news-1/post/call-for-testing-ubuntu-touch-ota-13-3717). It's going to be released on September 9th.  
* Also, postmarketOS developer Martijn Braam [has shared progress on supporting the front camera](https://twitter.com/braam_martijn/status/1302301625265119234). _Don’t miss the video linked below._

### Hardware News
* Pine64: [PinePhone Manjaro Community Edition](https://www.pine64.org/2020/08/31/pinephone-manjaro-community-edition/).

### Worth reading
* Jason Evangelho for Forbes: [Meet The New Manjaro Community Edition PinePhone, Starting At $149](https://www.forbes.com/sites/jasonevangelho/2020/08/31/meet-the-new-manjaro-community-edition-pinephone-starting-at-149/). _Just one article on the announcement, there have been a lot._
* Fossbytes: [Meet P-Boot: A New Pico-Sized Bootloader For Linux-Based PinePhone](https://fossbytes.com/meet-p-boot-a-new-pico-sized-bootloader-for-linux-based-pinephone/). _Missed this one, but it is informative enough to add it._
* 5ilver on reddit: [Stuck a piece of paper under the speaker contacts to stop that horrible whining sound. Sweet relief at last!](https://old.reddit.com/r/PinePhoneOfficial/comments/ilfbwl/stuck_a_piece_of_paper_under_the_speaker_contacts/). _Not much of a read, more a "look at", but.. I ran out of categories!_
* [Hacker News Comments on the announcement of the Manjaro Community Edition](https://news.ycombinator.com/item?id=24329900). _Some are worth reading._

### Worth listening
* [Alan Pope](https://popey.com/) has shared his impressions of the PinePhone on the [latest episode of the Ubuntu Podcast](https://ubuntupodcast.org/2020/09/03/s13e24-acorn/) starting at minute 20:23.

### Worth watching
* DanctNIX: [DanctNIX Project Update - August 2020](https://youtu.be/VGpfleG8TMg). _A nice update._
* Howto: Digital Privacy and Infosec: [Pinephone Howto: gPodder Podcast & App review](https://youtu.be/a7Of3deW9AE). _I usually prefer GNOME Podcasts and build it from source this weeks because of rumors of integrated directories (Nope!), but the gPodder desktop app definitely has more features._
* Howto: Digital Privacy & Infosec: [Pinephone Ubuntu Touch Updates: Camera Works, Smooth performance, Apps, Security Enhancements & More](https://www.youtube.com/watch?v=qU62D65qrd4). _Nice overview of how Ubuntu Touch has progressed lately. Unfortunately I don't think the release used for this video is mentioned._
* Martijn Braam: [Camera switching on the PinePhone](https://www.youtube.com/watch?v=jlf8ZyfkwBw). _This is not released yet, but it is great to see progress on the camera front._
* sadfwesv: [PinePhone - Multi-Boot of 9 distros on a single SD card](https://www.youtube.com/watch?v=NqyuKysK6ag). _Short but nice._
* ETA Prime: [Pine Phone First look, An Open Source Linux Powered Smart Phone!](https://www.youtube.com/watch?v=zlI14bb1zEM). _The postmarketOS CE PinePhones are arriving internationally._
* 801 Labs: [Pine64 PinePhone review by bashNinja](https://youtu.be/-z-NugbLH5Q). _Calling the first use of a device a review is a bit of a stretch, but it is worth a watch nontheless._ 

### What did I do?

I did not achieve much this week:
* I spent quite some time on a video showing Anbox on ArchLinux, which is being published as I write this,
* tried to build Tinymail (and failed), but [succeeded with Qt apps](https://linmob.net/2020/09/05/pinephone-building-plasma-mobile-apps-from-the-aur.html),
* no progress on the app directory :(.
 
