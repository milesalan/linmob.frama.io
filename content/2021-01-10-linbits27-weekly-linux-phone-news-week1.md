+++
title = "LinBits 27: Weekly Linux Phone news / media roundup (week 1)"
aliases = ["2021/01/10/linbits27-weekly-linux-phone-news-week1.html", "linbits27"]
author = "Peter"
date = "2021-01-10T21:30:00Z"
layout = "post"
[taxonomies]
tags = ["Pine64", "PinePhone", "Arch Linux ARM", "DanctNIX Mobile", "Phosh", "Phoc", "Mobian", "Plasma Mobile", "Lomiri", "Manjaro", "Purism", "Librem 5", "Quectel EG25-G"]
categories = ["weekly update"]

+++

_It's sunday. Now what happened since last sunday?_

KWin improvements, new Manjaro Lomiri dev builds, Linux 5.10 on the PinePhone's Quectel EG25-G and more. _Commentary in italics._
<!-- more -->

### Software development and releases
* Phoc 0.6.0 [has been released](https://source.puri.sm/Librem5/phoc/-/releases/v0.6.0). 
* The Quectel EG25-G Modem [can run Linux 5.10 now](https://nitter.net/thepine64/status/1346582145557524488).
* Plasma Mobile: [KWin improvements make quite the difference](https://nitter.net/thepine64/status/1347241731801178113). _See also Nate Grahams article below._
* Manjaro Lomiri [seems to get going again](https://nitter.net/ManjaroLinux/status/1347568314692861957), as two dev images where released in the past week. _[Click this link](https://osdn.net/projects/manjaro-arm/storage/pinephone/lomiri/dev-20210110/) to get to the latest image._
* [postmarketOS is switching to eg25-manager](https://freeradical.zone/@craftyguy/105522734134837770), which should improve the PinePhones reliability as a phone considerably.

### Worth reading 
* FOSS2go: [PinePhone modem can now run Linux kernel 5.10](https://foss2go.com/pinephone-modem-can-now-run-linux-kernel-5-10/).
* LinuxSmartphones: [Linux phone news roundup (1-06-2020)](https://linuxsmartphones.com/linux-phone-news-roundup-1-06-2020/).
  * FOSS2go: [Manjaro ARM developer build of Lomiri for PinePhone is out](https://foss2go.com/manjaro-arm-developer-build-of-lomiri-for-pinephone-is-out/). _FOSS2go's take on the same issue._
* FOSS2go: [Manjaro ARM developer build of Lomiri for PinePhone is out](https://foss2go.com/manjaro-arm-developer-build-of-lomiri-for-pinephone-is-out/). _I am going to try this soon._ 
  * LinuxSmartphones: [There’s a new Manjaro ARM Lomiri dev build for the PinePhone](https://linuxsmartphones.com/theres-a-new-manjaro-arm-lomiri-dev-build-for-the-pinephone/).  _Brad's take._
* LinuxSmartphones: [Can a Linux phone replace my iPhone or Android device?](https://linuxsmartphones.com/can-a-linux-phone-replace-my-iphone-or-android-device/). _Nice write-up!_
* Phoronix: [The Qt Company Is Tomorrow Moving Qt 5.15 To Its Commercial-Only LTS Phase](https://www.phoronix.com/scan.php?page=news_item&px=Qt-5.15-LTS-Commercial-Phase). _I don't know if and how this is going to affect Plasma Desktop and Mobile, but it's definitely worth noting._
* Purism: [Purism and Linux 5.8](https://puri.sm/posts/purism-and-linux-5-8/). _Boy, that post is late! 5.10 is already released, but it's still interresting._
* Purism App Spotlight:
  * [Weather](https://puri.sm/posts/app-showcase-weather/), 
  * [Sound Recorder](https://puri.sm/posts/app-spotlight-sound-recorder/).
* /u/bloggerdan: Pinephone as a daily driver? A Non-predetermined Amount of Time* with Mobian: [Day 8](https://teddit.net/r/PINE64official/comments/krdx2p/pinephone_as_a_daily_driver_a_week_with_mobian/), [Day 9](https://teddit.net/r/PINE64official/comments/ktbaud/pinephone_as_a_daily_driver_a_nonpredetermined/), [Day 10](https://teddit.net/r/PINE64official/comments/ktvcny/pinephone_as_a_daily_driver_a_nonpredetermined/). _Interesting series._
* Nate Graham: [This week in KDE: new KWin compositing, new Kickoff, new recording level visualization!](https://pointieststick.com/2021/01/08/this-week-in-kde-new-kwin-compositing-new-kickoff-new-recording-level-visualization/). _This is the backstory to the compositor improvement that helped with Plasma Mobiles' overall performance._
* GNOME Shell and Mutter development blog: [A shell UX update](https://blogs.gnome.org/shell-dev/2021/01/07/a-shell-ux-update/). _This is also relevant for the PinePhone and Librem 5, as it could be handy when connected to a larger screen or for those who insist on running full GNOME on their PinePhone._

### Worth noting
* As [@whatsmieasms points out on Twitter](https://twitter.com/whatsmieasms/status/1346569435625230337), Galaxy S20 Ultra Battery cases almost fit the PinePhone. _I'd recommend waiting for PINE64's official keyboard case which is going to have an integrated battery, but if you can't wait and need more battery life, this might be worth considering._

### Worth watching
* LinuxLounge: [PinePhone Accessories Unboxing! (Clear Case, Glass Screen Protector and Plain Back Cover)](https://peertube.co.uk/videos/watch/14b78b66-b7bd-41c4-b245-bc87eb89b545). _If you have wondered what the official PinePhone accessories are like, this video should be helpful._
* Pedro Francisco: [Sailfish, Manjaro Phosh y Manjaro Plasma Mobile en el PinePhone](https://video.hardlimit.com/videos/watch/9044d105-bfb1-4e42-9d55-190fb0107b52). _Spanish video about three different PinePhone OS._
* Terminal_Heat_Sink: [PinePhone Pine64 Screen Repair / Replacement / Fix Display Artifacts](https://www.youtube.com/watch?v=5CgdAMhqSeA). _If you wanted to know how you could replace a broken PinePhone display, this is how you would do it._
* UltimatePisman: [【その他】PinephoneというLinuxスマホをパソコンに化してみたｗｗ](https://www.youtube.com/watch?v=tGkHFAnmBxA). _A japanese video showing GIMP and PinePhone convergence._
* Privacy & Tech Tips: [Howto: Portsentry On Linux/Pinephone Detect Stealth Portscans, Have Attacker IP Emailed To Us](https://lbry.tv/@RTP:9/howto-portsentry-on-linux-pinephone-to:3). _I actually learned something when I watched this video._


### Stuff I did

#### Content
I wrote a [blog post](https://linmob.net/2021/01/09/my-setup-with-danctnix-archlinuxarm.html) about my setup, which ended up being surprisingly long one.
I have uploaded one new video about "Firefox Profiles and Site Specific Browsers on the PinePhone". As always, you can watch it on 
* [PeerTube](https://devtube.dev-wiki.de/videos/watch/165ca7c9-aa35-4b07-867f-74064542c9fd),
* [LBRY](https://lbry.tv/@linmob:3/firefox-profiles-and-site-specific:c),
* and [YouTube](https://www.youtube.com/watch?v=h7aLE8jBOPc).

 
#### LINMOBapps
I added some scripts, .desktop files and my config.xml for SuperTuxKart [to the repository](https://framagit.org/linmobapps/linmobapps.frama.io). That aside, just [a few additions and some maintenance happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master). Please [do contribute](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/tasks.md)!
