+++
title = "Bringing back old content"
aliases = ["2021/05/01/bringing-back-old-content.html"]
author = "Peter"
date = "2021-05-01T10:29:00Z"
layout = "post"
[taxonomies]
tags = ["history", "announcement",]
categories = ["shortform", "internal",]
+++

_Since I [restarted this blog in 2020](https://linmob.net2020/06/11/welcome-to-linmob.html), I aimed to bring the old content back._ It took a long time, and while it all still needs work [^1], I can finally report some progress: A lot of the old content has been brought back. Some of it, especially posts of the brief "hosted on tumblr" era this blog had, that were just video reposts, did not make it. But a lot made it. 
<!-- more -->
This definitely helped me remembering why I had stopped this blog: Too many great projects had withered away or just been slashed: [Openmoko](https://linmob.net/openmoko-plan-b-and-sean-moss-pultzs-talk-from/), [Nokia switching to Microsoft](https://linmob.net/the-nokia-wp7-announcement-a-late-comment/), [webOS being killed by Apotheker](https://linmob.net/hp-announces-to-kill-webos-devices-looks-for-licensees/), too name a few&mdash;and add the fact that Google seemed friendlier then and that there were tons of _better_ Android blogs.

Also, this blog is now being generated with [Zola](https://getzola.org). It's great! I plan to publish my adaption of the [Minima](https://github.com/jekyll/minima) theme&mdash;as soon as it's slightly less of a work in progress (just look at that ugly footnote below!).

_Have fun exploring the new old content and stay tuned for more new!_

[^1]: The [taxonomy](https://linmob.net/tags/) needs work, broken links need to be fixed (or redirected to archive.org), images need to be reuploaded and embedded.
