+++
title = "Imerj / frog design 2 in 1 Smartpad"
aliases = ["2011/06/26/imerj-frog-design-2-in-1-smartpad.html"]
author = "peter"
date = "2011-06-26T14:13:00Z"
layout = "post"
[taxonomies]
categories = ["hardware", "commentary"]
tags = ["2 in 1 smartpad", "ATandT", "engadget", "english", "first thoughts", "frog", "imerj", "kyocera echo", "prototype", "TI OMAP 4"]
+++
As you may have noticed, I prefer not to cover each and every device with a short article, I rather write about the few devices, that really seem to make a difference to me along with the devices that I actually buy. 

One of these interesting devices I didn't cover because it seemed far to unlikely to ever even get my hands on it was the <a href="http://www.echobykyocera.com">Kyocera Echo</a>, a very interesting 2 screen Android smartphone, made for the US operator sprint, build with two 3.5&#8221; inch displays, powered by a first gen CDMA/EVDO Snapdragon CPU (MSM8650). However, this first attempt at a dual screen smartphone seemed to have some caveats, including the fact that it was just on sale on one US network.

The guys over at Engadget (which while certainly having suffered from quite an exodus of editors and writers still is a great online publication on tech) now <a href="http://www.engadget.com/2011/06/23/prototype-dual-screened-2-in-1-android-smartpad-from-imerj-previ/">got their hands on a similar device</a>, which is a lot more interesting, just because it will most likely be an AT&amp;T device - AT&amp;T has a WCDMA/GSM network and thus this device will, even if marketed only in the US, be usable (after an unlock) almost everywhere in the world.

Specs have been bumped up in comparison to the &#8220;Echo&#8221;. The screens on this still feature the same WVGA resolution, size is increased to 4&#8221; though, which ads up to one splitted screen of 6&#8221; (compared to 4,7&#8221; on the &#8220;Echo&#8221;, the device has a simpler folding mechanism, is thinner (folded: 14mm, as &#8220;tablet&#8221; 7mm ) and has the horsepower of a TI OMAP4 chip at yet undisclosed/unfinalized speed in it - which means it's totally on par with similar superphone solutions and most likely even faster than those running on a nVidia Tegra 2 SoC. 

How you use the device is simple and fascinating, yet hard to describe. That's why I urge you to check out <a href="http://www.imerjdesign.com/">Imerj Designs website</a> and the afore mentioned <a href="http://www.engadget.com/2011/06/23/prototype-dual-screened-2-in-1-android-smartpad-from-imerj-previ/">engadget article</a> now to get an idea about the device.

While certainly not much more than a niche solution (at least as long there is this devider between the two screens), this proposal of a device certainly is a charming thing. It offers a lot more than a stardard (Android) superphone and isn't that much bigger in terms of size - it will most likely be more expensive and battery life is something to be concerned about with such a thin, dual screen device, though.
