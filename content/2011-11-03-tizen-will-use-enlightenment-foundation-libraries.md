+++
title = "Tizen will use Enlightenment Foundation Libraries"
aliases = ["2011/11/03/a-big-part-of-tizen-will-be-to-a-have-framework.html", "2011/10/24/tizen-will-use-enlightenment-foundation-libraries.html"]
author = "peter"
date = "2011-11-03T06:12:00Z"
layout = "post"
[taxonomies]
tags = ["EFL", "future", "MeeGo", "platforms", "Quote", "Qt", "Tizen"]
categories = ["shortform"]
+++
<blockquote>A big part of Tizen will be to have a framework and the corresponding SDK to support HTML5-WAC applications. Native applications development should also be supported through the usage of the EFL (Enlightenment Foundation Libraries&mdash;<a href="http://en.wikipedia.org/wiki/Enlightenment_Foundation_Libraries">http://en.wikipedia.org/wiki/Enlightenment_Foundation_Libraries</a>) with the SDK. So we could suppose that the reference UI of the system will also be based on the EFL.</blockquote>

<p class="attribution"><a href="http://lists.meego.com/pipermail/meego-dev/2011-October/484354.html">Florent Viard on the MeeGo Mailing List</a>

Of course this all pretty unconfirmed, but still, if you have ever seen what a performance EFL delivers on rather poor hardware (I am talking of the Openmoko FreeRunner here, of experiences I made years ago, so it has likely matured since thanks to Samsungs founding), this sounds promising. BTW, there will be a Tizen event in China in early December, so we may be knowing more soon.
