+++
title = "Merging A910i extractions with A910 Firmware - first attempt failed"
aliases = ["2008/03/merging-a910i-extractions-with-a910.html"]
date = "2008-03-26T13:37:00Z"
[taxonomies]
categories = ["projects"]
tags = ["A910", "A910i", "modding"]
[extra]
author = "peter"
+++
Yesterday night I couldn't sleep, as my neighbours were having a party. So I just tried to merge an A910 flash file with gamas' A910i extractions. I used EZXCodec for that, a software developed for windows to edit firmware which runs well with wine.
<!-- more -->

As I realized that codegroup sizes differ up to 2 megabytes (CG44 e.g. is  ~2MB bigger on A910i) between A910 and A910i, I had to edit CG starting adresses - lots of hex-calculations, and: Flashfile size was OK afterwards. But flashing failed somewhere close to 35% - as far as I remember (I'll have to watch out for some logs of that flashtool).

Now I'll try to contact some people which know more than I do and probably, I'll check for reasons of these CG-filesize differences (analyzing CG contents and configuration files) in my next sleepless nights, but there's one thing I already found out: NeufTV is realized with MPlayer/ZMPlayer as a preloaded Qt app.

Might be I need a real A910i `*.sbf (e.g.: R58_G_10.01.378P)` file for success.
