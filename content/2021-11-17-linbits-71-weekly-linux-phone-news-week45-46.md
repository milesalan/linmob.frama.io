+++
title = "LinBits 71: Weekly Linux Phone news / media roundup (week 45/46)"
date = "2021-11-17T21:34:00Z"
draft = false
[taxonomies]
tags = ["PinePhone", "PinePhone Pro", "Manjaro",  "Nemo Mobile", "Librem 5", "Purism", "PINE64 Community Update","Tablets", "JingPad", "LINMOBapps"]
categories = ["weekly update"]
[extra]
author = "Peter"
+++

_What's new with Linux on Mobile?_ Sxmo goes Wayland with 1.6.0, a Chatty beta delivers MMS, Megapixels adds a flash and lots of tablet stuff!<!-- more --> _Commentary in italics._ 


### Software releases
* [Megapixels 1.4.0 and two subsequent bug fix releases](https://gitlab.com/postmarketOS/megapixels/-/tags) have been released. New features include LED and Display-based flash support, and more.
* [Amazfish 2.0.3 has been tagged](https://github.com/piggz/harbour-amazfish/releases/tag/2.0.3), delivering [support for the new InfiniTime motion service](https://twitter.com/adampigg/status/1460735342156423173), so you can have a look at the steps you walk on your Linux Phone (currently SailfishOS only).
* [Sxmo 1.6.0 is out](https://lists.sr.ht/~mil/sxmo-announce/%3C31IGOQIT71NT5.2LYA6VY1SCLPK%40stacyharper.net%3E), delivering Wayland!
* Chatty 0.5.0~beta, the next pre-release of Phosh's default messaging app, [has been tagged](https://source.puri.sm/Librem5/chatty/-/tags/v0.5.0_beta), delivering MMS functionality! There's much more, make sure to go and read the [changelog](https://source.puri.sm/Librem5/chatty/-/commit/7095860abce9c8c7787515cdca950ccb656a32f1).

### Worth noting
* Work on gestures for Phosh [is happening](https://fosstodon.org/web/@agx@librem.one/107270696986030097).
* Signal users, rejoice! One more option for using [Signal on Linux Phones](https://teddit.net/r/PINE64official/comments/qthw58/looking_for_some_basic_feedback_on_native_signal/)!

### Worth reading

#### PinePhone Hardware mods
* Izzo: [Improve your PinePhone eMMC speed](https://izzo.pro/pinephone-vccq-mod/). _Great article! I am not sure whether I'll attempt the hardware mod any time soon, but it is awesome to have the option!_
* TuxPhones: [Doubling the PinePhone storage perf with a soldering iron and a steady hand](https://tuxphones.com/pinephone-storage-performance-pinecil-vccq-emmc-mod/). _Same topic, great explainer of the hardware mod by Caleb!_

#### Mainline ports
* TuxPhones: [Another Xiaomi device platform is getting Linux support](https://tuxphones.com/xiaomi-redmi-note-7-linux-phone-mainline-kernel/). _Great work by Danct12!_

#### Downstream ports
* Liliputing: [New software for old phones: Pixel 2 gets Ubuntu Touch and unofficial Android 12 ports](https://liliputing.com/2021/11/new-software-for-old-phones-pixel-2-gets-android-12-and-ubuntu-touch-ports.html). _Great to see the Pixel 2 seeing some love after being abandoned by Google!_

#### Tablet Hardware
* Jeff Geerling: [CutiePi - a Raspberry Pi CM4 Linux Tablet](https://www.jeffgeerling.com/blog/2021/cutiepi-raspberry-pi-cm4-linux-tablet). _Sadly performance (in benchmarks, less in "real use", according to the video review) seems to suffer from thermal problems – maybe a nominally slower, compatible SoM like the [PINE64 SOQuartz](https://wiki.pine64.org/wiki/SOQuartz) or the [Radxa CM3](https://wiki.radxa.com/Rock3/CM3) could actually have better sustained performance in this form factor. (Please note, this won't be plug and play and just working. You'll need some advanced kernel compilation if not hacking skills at least to make this hypothetical a reality.)_

#### ARMv9 Bring-Up
* Phoronix: [Arm Cortex-A710 Support Merged Into GCC 12 Compiler](https://www.phoronix.com/scan.php?page=news_item&px=Cortex-A710-GCC-12-Lands).

#### PinePhone Pro Bring-Up
* xnux.eu log: [A bunch of mostly Pinephone Pro news](https://xnux.eu/log/#054). _Great progress by Megi!_

#### ARMv7 Wind-Down
* Phoronix: [Fedora Drafts Plans For Retiring ARMv7 Support](https://www.phoronix.com/scan.php?page=news_item&px=Fedora-37-Drop-ARMv7). _ARMv7 devices are still usable for many tasks, so this is sad to see, but it's also understandable._

#### Software Progress
* This Week in GNOME: [#18 Delicious toasts](https://thisweek.gnome.org/posts/2021/11/twig-18/).
* Nate Graham: [This week in KDE: Primarily Centered Hamburgers](https://pointieststick.com/2021/11/12/this-week-in-kde-primarily-centered-hamburgers/).
* Nate Graham: [Be flexible to win big](https://pointieststick.com/2021/11/13/be-flexible-to-win-big/). _Plasma running on more devices as a default is a definitive success!_
* Claudio Cambra: [Kalendar v0.2.0 is out now, adding drag-and-drop, improved calendar management, and lots of bug-fixes — Kalendar devlog 22](https://claudiocambra.com/2021/11/13/kalendar-v0-2-0-is-out-now-adding-drag-and-drop-improved-calendar-management-and-lots-of-bug-fixes-kalendar-devlog-22/). _Great progress!_
* Robert: [Calculator and GTK4](http://curiositydrivendevelopment.blogspot.com/2021/11/calculator-and-gtk4.html). _I hope mobile support is going to stay._

#### Librem 5 with PureOS Byzantium
* Purism: [November Librem 5 update: Byzantium Released](https://puri.sm/posts/november-librem-5-update-byzantium-released/). _To be fair: Byzantium is massively better than Amber, and Purism keeps doing great work to make Mobile Linux a lot more viable!_

#### PINE64 Community Update
* PINE64: [November update: first impressions](https://www.pine64.org/2021/11/15/november-update-first-impressions/). _Nice update! Make sure to read Lukasz's PinePhone Pro impressions. I am looking forward to being able to order the keyboard and the PinePhone Pro!_
  * xda-developers: [PinePhone’s official keyboard add-on will turn it into a tiny Linux PC](https://www.xda-developers.com/pinephones-official-keyboard-add-on-will-turn-it-into-a-tiny-linux-pc/). _True._

#### More Mobile Linux Round-Ups
* Liliputing: [Linux Smartphone News Roundup: PinePhone Keyboard, PinePhone eMMC speed hack, and more](https://liliputing.com/2021/11/linux-smartphone-news-roundup-pinephone-keyboard-pinephone-emmc-speed-hack-and-more.html). _Great roundup! Thanks for the shoutout!_

### Worth listening
* PEBKAC: [PEBKAC S01E04 Mid November Updates!](https://pebkac.show/2021/11/15/pebkac-s01e04-mid-november-updates/). _Well done, and thanks for the mention! (I felt that I got nothing done lately, this nugded me to revisit that assumption)._

### Worth watching
#### PinePhone Pro
* Manjaro Linux: [Pinephone vs. Pinephone-Pro - Benchmarking Apps](https://www.youtube.com/watch?v=tpxzqz-WqhE).
* Danct12: [PinePhone Pro Developer Edition Unboxing](https://www.youtube.com/watch?v=obnvV30rft4).
* PINE64: [PinePhone Pro First Impressions](https://www.youtube.com/watch?v=46khsEjkqPU)

#### PinePhone Hardware mod
* \_symmetrist\_: [PinePhone Vccq mod](https://twitter.com/_symmetrist_/status/1459589852266176514). 

#### PinePhone Commentary
* Linux Lounge: [The PinePhone Isn’t a Consumer Ready Device Yet (And That’s Okay!)](https://peertube.co.uk/videos/watch/410c7a5d-221e-4f6f-a841-c285ee0d5162) _It's okay, indeed!_

#### PinePhone Gaming
* PanzerSajt: [Minecraft Java running on PinePhone](https://www.youtube.com/watch?v=O1y1O-EoW_k).

#### Tablet Hardware Review
* Jeff Geerling: [Finally! A Raspberry Pi Linux Tablet that works!](https://www.youtube.com/watch?v=t-ZQ9LRdXSk)

#### JingPad corner (see also shorts)
* Niccolò Ve: [JingPad A1 Unboxing!](https://tube.kockatoo.org/w/pAzho3YM7xTVM1mzGpCkVe).
* The Linux Experiment: [JingPad Roadmap and updates, installing other distros, open source status, pen latency, and more](https://www.youtube.com/watch?v=Vj3QueLJfSY).
* LearnLInuxTV: [The JingPad A1 Linux Tablet, Full Review!](https://www.youtube.com/watch?v=eiSSAaLSwhA). 


#### OpenAlt Conference[^1]
* OpenAlt: [Jozef Mlich: Nemomobile](https://www.youtube.com/watch?v=Ry84A-hTXRw&t=23920s).
* OpenAlt: [Martin Botka, AngeloGioacchino Del Regno, Marijn Suijten: SoMainline's 2021: Rowing into the upstream](https://www.youtube.com/watch?v=Ry84A-hTXRw&t=16760s). _Same video, different time stamp!_
* OpenAlt: [Elzbieta Godlewska: Learn with FOSS communities or how to turn a passion into a profession](https://www.youtube.com/watch?v=Z878W0YGd7w&t=23350s). _Not mobile specific, but sometimes we need to go broader, right?!_

#### PINE64 Community Update
* PINE64: [November Update: First Impressions](https://odysee.com/@PINE64:a/november-update-first-impressions:b). _Great video by PizzaLovingNerd!_

#### PINE64 Community Update Commentary
* Twinntech: [November review Pinephone status.](https://www.youtube.com/watch?v=x0JbvtZl1i8) _Is it unfair to call him a single-issue guy? Nice cat though!_

#### Byzantine Marketing ;-)
* Purism: [November Librem 5 update: Byzantium Released](https://www.youtube.com/watch?v=UUm22aFJ5uo).

#### Android on PinePhone
* 蕭博文powen: [Android 11 on my pinephone](https://www.youtube.com/watch?v=r5j9kZ-gE4U).
* 蕭博文powen: [Full progress of flashing android 11 to pinephone with MacOS using Glodroid](https://www.youtube.com/watch?v=UDY988kbdQU).

####  Nemo Mobile Shorts Shorts
*  Сергей Чуплыгин: [Pure Maps on nemomobile](https://www.youtube.com/watch?v=OYacHR0Z1AI).
*  Сергей Чуплыгин: [Glacier Package manager demo](https://www.youtube.com/watch?v=ypxTJtwqHqo).
*  Сергей Чуплыгин: [polkit+package manager](https://www.youtube.com/watch?v=L6T9YrD4YP4).

#### Regular Shorts
* PizzaLovingNerd: [Spot - GNOME's Spotify App #shorts #linux](https://www.youtube.com/watch?v=HiVokRn8fxc)
* NOT A FBI Honey pot: [Linux user....kinda gets android to work on pinephone......](https://www.youtube.com/watch?v=3FrW3J0IK9M)
* Massimo Antonio Carofano: [SimCity BuildIT Android app native running on JingPad A1 with JingOS ARM (Ubuntu Based)!!](https://www.youtube.com/watch?v=CsV91LsXJbk).
* Massimo Antonio Carofano: [Google Earth for Android running on JingPad A1 with JingOS ARM (Ubuntu Based)](https://www.youtube.com/watch?v=SkTUE06N0po).
* J Collins: [Using 5G on my Jingpad A1 wifi only model.](https://www.youtube.com/watch?v=lH9TnZJPINs),
* Baonks81: [pmOS onboard login screen](https://www.youtube.com/watch?v=zN3DFGGwsx8). _Unless I am completely mistaken, this is a Nexus 7 (2012)._
* Baonks81: [Ubuntu 21.10 Impish Indri Nexus 7 2012 wifi rev.E1565 kernel-5.15.0-rc4](https://www.youtube.com/watch?v=XKZG655V5PQ).

### Stuff I did

#### Content
Nothing.

#### LINMOBapps and Linux Phone Apps

While LinuxPhoneApps "alpha" activity is still at zero, classic "LINMOBapps" (which you can also find by going to [LinuxPhoneApps.org](https://linuxphoneapps.org)), saw a few changes. BigB added one app,
* [Sonically](https://itmodulo.eu/git/LinuxOnMobile/Sonically),  a native GTK3 client implementing the subsonic API for audio streaming. _Thanks for the Merge Request, BigB!_
Also, I added 
* [signal-rs](https://git.sr.ht/~nicohman/signal-rs/), a WIP Rust/QML signal client. 

[Read here what (else) happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master) on LINMOBapps this week. And please [do contribute!](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/CONTRIBUTING.md)

[^1]: This section is a week late, sorry! I only included English talks.
