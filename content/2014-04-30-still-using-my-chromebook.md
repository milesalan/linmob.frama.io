+++
title = "Still using my Chromebook"
aliases = ["/2014/04/30/still-using-my-chromebook"]
date = "2014-04-30T12:13:00Z"
author = "peter"

layout = "post"
[taxonomies]
tags = ["Arch Linux ARM", "chrome os", "chromebook", "chromebook 3G", "computing", "linmob", "linux", "SAMSUNG XE303C12"]
categories = ["commentary"]
+++

While I am quite bored with most new tech announcements (especially smartphones), I realize that I still care most about small affordable laptops or convertibles. Tablets are great, I enjoy Apples iPad Air, but it has its limits, and so do Android tablets&mdash;limits in productivity. Of course it is entirely possible to do blogging or almost every other task that doesn't require special business software on a tablet&mdash;but it just isn't a great experience. Well, most likely it is just unwillingness to adjust&mdash;when you can choose between using an application that you know since almost forever, or a new app, that maybe is&mdash;at least for the advanced stuff&mdash;a little complicated and does not grow on you within minutes, then you might find yourself wanting the experience that you are used to. At least that's the case with me.

As the headline says I am still using my Samsung ARM Chromebook with 3G. It is my main laptop now, which seems strange, given the fact that an iPad Air or the LG G2 most likely deliver better (benchmarked) performance than this laptop. And then there is this OS&mdash;Chrome OS. Well, nah&mdash;I mostly boot into Arch Linux ARM running from a 32 GB SDHC card. I made it so that it is almost just as user friendly[^2] and has a broad set of applications I am used to[^2]. While it is definitely not the fastest computer under the sun, it performs decent enough&mdash;the keyboard, the touchpad, the screen, the overall performance&mdash;nothing is really top notch, but it is more than good enough for me. BTW: The killer feature is, still, the following: No fan = no (constant) noise.

While it is great to be able to take the SDHC card out of the Chromebook, boot it into Chrome OS, launch guest mode and hand it over to a random person in order to provide that person with a way to access the internet, it feels decidedly hacky. And I like to have the robustness of Chrome OS at hand, which BTW evolved notably since mid 2013. But then the real question is: Would I purchase another (next gen) ARM Chromebook, like one of the announced and soon shipping "SAMSUNG Chromebook 2" with 8-core Exynos?

The answer is: It is unlikely. While&mdash;as mentioned above&mdash;most things work, getting an ARM-powered laptop set up to work a 100% fine with GNU/Linux seems almost impossible today. Rather essential stuff like standby is hit and miss, from time to time my Chromebook doesn't wake up properly. Accelerated graphics (I am not talking about gaming, but much rather about video playback here) or using a newer kernel: Painful to impossible. And as fanless Bay Trail netbooks/subnotebooks become available out there, that&mdash;depending how well the UEFI plays with Linux&mdash;are supposedly almost painless in that regard I would rather go for one of these if I had to upgrade.

But fortunately I don't have to. And so I am sticking to my XE303C12H01DE.

<em>(More on laptops soon.)</em>

[^1]: Thanks to using XFCE with NetworkManager and Modem Manager.
[^2]: Starting from Firefox, including LibreOffice, Gimp, Inkscape and even great stuff like LyX for LaTeX.
