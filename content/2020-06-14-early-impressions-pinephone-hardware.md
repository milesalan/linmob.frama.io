+++
title = "PinePhone Hardware - Early Impressions"
aliases = ["2020/06/14/early-impressions-pinephone-hardware.html"]
date = "2020-06-14T16:42:18Z"
layout = "post"
[taxonomies]
tags = ["PinePhone", "impressions", "PINE64"]
categories = ["impressions"]
+++
After almost a week playing with the Pine64 [PinePhone][pinephone] I want to share some overall impressions. As [software][pinephone-software] is at least partially not quite adapted to this [new 1.2 hardware revision][pinephone-revisions], I will limit myself to the hardware for this post and proceed from the outside to the inside.
<!-- more -->
The great thing about the PinePhone upon taking it out of the box is: This feels just like a smartphone. It does not feel like some strange thick tinkerphone. Surely, it does not follow the latest design trends, but the bezels around the screen and the screen to body ratio is certainly not terrible either. The pastic feels plasticky, but it does not&mdash;at least in my opinion&mdash;feel cheap. Overall, considering the price of device and the economies of scales of the production of this device, I really think that this phone feels amazingly well made. 

_So let's get to the specifications:_ The screen is pretty big with 5.95", but it does not stand out as a giant among the smartphones of 2020. A 720x1440 px resolution is not what I would call super high res, and it does not meet Apples retina criterium at 271 ppi. But it certainly is good enough, and it is a overall a screen that I would describe as fine. Sure, a higher res could have been better, but you have to take the other parts of this phone into your consideration. The Allwinner A64 SoC has a ARM Mali 400 MP2 GPU, which is not exactly the fanciest&mdash;it was announced [in mid 2008][gpu-announced]. A higher screen resolution would have put the GPU under even more pressure&mdash;I think Pine64 chose wisely when they went with the 720p screen. The [Quectel EG25-G][quectel-modem] modem is also a great choice, as it supports many different bands, enabling Pine64 to not have to make different SKUs for different continents.

One thing I wasn't that happy about when the PinePhone was announced were the choice of [chipset][allwinner-a64-sunxi] (although it was not that surprising, as you could call Pine64 PineA64 if you wanted to, as all their devices without the "pro" label use that SoC), and the amount of RAM which is not too plenty by todays standards at 2 GB. I was even more sceptical because of the Wireless LAN chipset: The PinePhone has a Realtek Wifi+BT chipset (RTL8723CS), and my experience with Realtek Wifi had always been bad at best. After using it for a couple of days I must say, that all these choices are fine. You just have to consider the price, and hey, it all works.

One possible shortcoming of the design is one I really did not expect: Pine64 decided to adopt a battery design used by the Samsung Galaxy J7 2015, a midrange Android phone. The battery is not small with 10-11 WH capacity, but just consider that [UBports have boasted lately][gripsgard-tweet] that they managed to get idle time to up to 14 hours. So, if you are looking forward to getting all day battery life while using your PinePhone a lot... well, that may take a while or be just plain impossible. Unfortunately I could not find extended batteries for the J7 2015, but still, that might become a thing in the future. We'll see. 
Also, there's a design bug, that renders [USB-OTG DisplayPort Alt mode broken][megi].

Overall, I must say that I am quite _happy_ with my purchase. The PinePhone is a great device for the price&mdash;and it is only going to get better.

[pinephone]: https://www.pine64.org/pinephone/ 
[pinephone-revisions]: https://wiki.pine64.org/index.php?title=PinePhone#Hardware_Revisions
[pinephone-software]: https://wiki.pine64.org/index.php?title=PinePhone_Software_Releases  
[gpu-announced]: https://www.notebookcheck.net/ARM-Mali-400-MP2.116279.0.html
[quectel-modem]: https://www.quectel.com/product/eg25g.htm
[allwinner-a64-sunxi]: https://linux-sunxi.org/A64
[gripsgard-tweet]: https://twitter.com/Mariogrip/status/1271248916349140994
[megi]: http://xnux.eu/devices/pine64-pinephone.html
