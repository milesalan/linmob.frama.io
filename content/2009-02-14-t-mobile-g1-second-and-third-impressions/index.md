+++
title = "T-Mobile G1 - Second and third impressions"
aliases = ["2009/02/14/t-mobile-g1-second-and-third-impressions.html"]
layout = "post"
date = "2009-02-14T09:38:00Z"
author = "peter"
[taxonomies]
tags = ["Android", "gta02", "Neo Freerunner", "openmoko", "T-Mobile G1", "HTC Dream",]
categories = ["hardware", "impressions",]
+++

24 hours ago I finally got it, my G1. I really like it and I feel that I am close to fanboyism, which makes me feel a need to critisize things.
But there isn't much to critisize: It is a great device, it feels good to have it in your hand&mdash;comparing it to the defected Neo Freerunner I just received minutes ago (and which I hope is repairable), which isn't much bigger, it feels a lot less bulky, not even bulky at all. Then there are lots of funny programs in the Android Market, which let you explore the capabilities of the device: It is fun to use it.

But besides the G1's openness, there are more things to critize, e.g. the time the battery lasts: I had to charge the device twice yesterday. Sure, you can blame that to me, as I couldn't take my hands of, and yes, lacking a data plan I used WLan a lot&mdash;but GPS was rarely used, just to name an example. Anyway: Android is great, it is probably the best software platform for mobile devices I have ever seen.

{{ gallery() }}

But it isn't all like gold: Yesterday evening I was trying to use it without the touchscreen (as you might remember, there were non-touchscreen Android devices announced back on MWC 2008 (and we might see some on MWC 2009), I experienced some points I had to use the touchscreen, e.g. in 3rd party applications. Of course this is ok in an application like Steel, which is, for those who don't know, a browser featuring a soft keyboard&mdash;it is an application aimed at touchscreen use. But while trying to find something in the market, I that problem, too, wasn't to bad, touching the screen once helped me out. But anyway: This is an inconsistancy in my eyes, and we all don't like them.

What I often forgot while playing around and trying this and that, was the &#8220;MENU&#8221;-key, which made me miss some options: I always had to remember that there is such a thing. That is not a great problem, as I will certainly get used to it, but it shows that Android isn't perfectly intuitive. While most people will be able to do a lot with the device while holding it in their hands for the first time, you might tell them, that whenever they don't find something they should try the menu button.

Well, that's it for now. I hope you enjoy life as much as I do. ;)
