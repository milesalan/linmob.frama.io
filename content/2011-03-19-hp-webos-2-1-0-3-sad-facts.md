+++
title = "HP webOS 2.1.0 - 3 sad facts"
aliases = ["2011/03/19/hp-webos-2-1-0-3-sad-facts.html"]
author = "peter"
date = "2011-03-19T11:58:00Z"
layout = "post"
[taxonomies]
tags = ["facts", "webOS", "platforms", "webOS 2.1.0"]
categories = ["shortform", "software",] 
+++
* Linux Kernel is still based on 2.6.24 (on Prē Plus and Prē²) - no change since initial webOS release here
* according to html5test.com, webOS 2.1.0 WebKit still performs (at 170 points) worse than WebKit in Android 2.3.2 (182 points&mdash;webOS 1.4.5 scored 132 points
* while webOS uses open source components like gstreamer for multimedia support, it doesn't support free formats like Ogg Vorbis / Theora, WebM or FLAC.
* <del>as the SDK for webOS 2.1.0 was just released to the public today (previously a 2.x SDK had been available under NDA only), there are about 2000 applications in the german AppCatalog as of today</del> Not true, forgot to activate paid apps in Preware&mdash;it's 6295 Apps in Germany today (3/20/2011)&mdash;not too bad!
