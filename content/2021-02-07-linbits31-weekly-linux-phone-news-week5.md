+++
title = "LinBits 31: Weekly Linux Phone news / media roundup (week 5)"
aliases = ["2021/02/07/linbits31-weekly-linux-phone-news-week5.html", "linbits31"]
author = "Peter"
date = "2021-02-07T22:38:00Z"
layout = "post"
[taxonomies]
tags = ["Pine64", "PinePhone", "Purism", "Librem 5", "LINMOBapps", "bq Aquaris X5", "postmarketOS", "SailfishOS", "Maui", "ExpidusOS", "FOSDEM2021"]
categories = ["weekly update"]
+++

_It's sunday. Now what happened since last sunday?_

The end of Community Editions, a new SailfishOS release, Lisperati1000 and more! _Commentary in italics._
<!-- more -->

### Software development and releases

* Sailfish OS Koli 4.0.1 [has been announced](https://forum.sailfishos.org/t/release-notes-koli-4-0-1/4542). _This is for Early Access subscribers only for now, I'll report (and likely try it) once this new release that features multiple big improvements (including a modernization to Gecko 60 for the default browser) makes it to the PinePhone._
* A new development image of OpenSUSE for the PinePhone featuring Plasma Mobile (and new Phosh images) [have been released](https://twitter.com/hadrianweb/status/1356904307539460098).
* Angelfish Browser 1.7.1 [has been released](https://invent.kde.org/plasma-mobile/plasma-angelfish/-/releases/v1.7.1), fixing a few bugs.
* Tootle 1.0 [has been released](https://github.com/bleakgrey/tootle/releases/tag/1.0), fixing a few critical bugs. _It's really good now, IMHO!_
* [Kaidan](https://www.kaidan.im/2021/02/02/kaidan-0.7.0/), the XMPP messenger that works well on Plasma Mobile, have released version 0.7.0.  _Sadly, no words on [OMEMO support](https://invent.kde.org/network/kaidan/-/issues/254), which I consider an essential feature. But [there's hope it's going to be in one of their next releases](https://www.kaidan.im/2021/01/07/end-to-end-encryption/)._

### Worth noting
* The german IT Security blog [kuketz-blog.de](https://www.kuketz-blog.de/messenger-matrix-im-html-format-verfuegbar-deutsch-und-englisch/) have released their ["Messenger Matrix" in English](https://media.kuketz.de/blog/messenger-matrix/messenger-matrix-en.html).  (link the english map). _Highly recommended!_
* The [Ubuntu Touch Devices page](https://devices.ubuntu-touch.io/) has seen a major overhaul. _It does not just look better, it also works better._

### Worth reading 

* nxos.org: [Maui Weekly Report 8](https://nxos.org/maui/maui-weekly-report-8/). _Go read this, it's very informative on the present and future of Maui._
* Julian's Code Corner: [NlNet grant for Fractal](https://blogs.gnome.org/jsparber/2021/02/01/nlnet-grant-for-fractal/). _Looks like Fractal, the GNOME Matrix client, is going to have a bright future!_
* Mobile NixOS: [January 2021 round-up](https://mobile.nixos.org/news/2021-02-02-january-2021-round-up.html). _Nice progress report!_
* r/ExpidusOS: [Mobile development and packaging the repository](https://www.reddit.com/r/ExpidusOS/comments/lbj1sk/mobile_development_and_packaging_the_repository/). _This is a Void Linux based distribution that is going to come to the PinePhone with an Xfce based UI. Consider me interested!_
* Purism: [Librem 5 News Summary: January 2021](https://puri.sm/posts/librem-5-news-summary-january-2021/). _I hope they are going to keep doing this, a monthly update will certainly help waiting customers to feel somewhat informed. But there's something else in here, let me quote: "Orders after [October 20th, 2017] will need to wait a bit longer for estimates until we have ensured we have secured CPU supply to fulfill them." Looks like I did the right thing in getting a second Librem 5 when I could, as this sounds like my October 23rd, 2017 order could be due in June or even later._
* Linux Smartphones: [Hackers develop open source firmware for the PinePhone modem, use it to make phone calls](https://linuxsmartphones.com/hackers-develop-open-source-firmware-for-the-pinephone-modem-use-it-to-make-phone-calls/). _Mind you, this does not mean that the modem is fully free now. It's impressive nontheless._
* PINE64: [The end of Community Editions](https://www.pine64.org/2021/02/02/the-end-of-community-editions/). _I am looking forward to the future of the PinePhone. Which OS will PINE64 ship?_
  * Linux Smartphones: [PinePhone Community Edition program comes to an end](https://linuxsmartphones.com/pinephone-community-edition-program-comes-to-an-end/). _Brad's take._
  * FOSS2go: [Pinephone with Mobian is the last of the Community Edition series!](https://foss2go.com/pinephone-with-mobian-is-the-last-of-the-community-edition-series/). _Radowslaw's take._
* Liliputing: [Lisperati1000 is a DIY portable programming workstation with a mechanical keyboard](https://liliputing.com/2021/02/lisperati1000-is-a-diy-portable-programming-workstation-with-a-mechanical-keyboard.html). _I love little handheld projects like this!_
* Keim/Yoon/Karabiyik: [Digital Forensics Analysis of Ubuntu Touch on PinePhone](https://www.mdpi.com/2079-9292/10/3/343). _Speaking of Ubuntu Touch: This is a scientific paper about the security of Ubuntu Touch on the PinePhone._
* Disputatio Ebrius: [PinePhone Arrival](https://mstdn.design/pinephone-arrival). _Some First impressions._
* CNX Software: [FOSDEM 2021 Online February 6-7 – Hardware, Embedded & IoT talks](https://www.cnx-software.com/2021/02/01/fosdem-2021-online-february-6-7-hardware-embedded-iot-talks/). _I went through FOSDEM's schedule and was disappointed to not find any specific talks about Linux Phones. The KDE Stand demoed PinePhone related developments though, which was nice. Overall, attending FOSDEM through Matrix was quite nice, and the talks mentioned in this article are likely quite interesting._

### Worth watching

* HomebrewXS: [PinePhone Manjaro CE Unboxing - HomebrewXS](https://www.youtube.com/watch?v=_nlp-qpTfD8). _This feels like a fairly late unboxing of the Manjaro CE, but why not?_
* HomebrewXS: [openSUSE Plasma on the PinePhone - HomebrewXS](https://www.youtube.com/watch?v=InDND27va0Y). _OpenSUSE's new Plasma spin in action!_
* Linux Lounge: [PinePhone Camera Now Has Hardware Acceleration On Ubuntu Touch!](https://peertube.co.uk/videos/watch/26b27191-c39e-4bd0-9468-af7e4c943669). _Nice demo of the new accelerated view finder._
* Luca Sans S: [Smoothest pinephone os -- pmOS sxmo](https://www.youtube.com/watch?v=Q6L9K0EW3qg). _Totally smooth, but kinda hard to use. Really nice video though, it's always nice to see genuine enthusiasm!_
* Nathan Thomas: [Doom running on the pine phone](https://www.youtube.com/watch?v=qz6JdagePMM). _It works. Of course it does!_ 
* Mealynn Kraffel: [Guitar Amp on the PinePhone using Guitarix](https://www.youtube.com/watch?v=wPrg7n_M6I0). _Impressive! I could never have demoed that!_
* Privacy & Tech Tips: [KDE Plasma Mobile: Pinephone KDE Connect Working (Remote Control) Manjaro + Bluetooth Demo](https://odysee.com/@RTP:9/pinephone-kde-connect-working-remote:3). _Nice video on the progress of Manjaro Plasma Mobile._
* Privacy & Tech Tips: [Pinephone: Manjaro's KDE Plasma Mobile Image: Demo Of Calls/Txts, Cameras](https://www.youtube.com/watch?v=0Z83jddcoaA). _More on the same topic._

#### KDE CE Unboxing corner
* BoltzBrain: [Pinephone Manjaro KDE CE (linuxphone) - unboxing and testing with Vufine+ wearable Display](https://odysee.com/@Boltz_Brain:9/Pinephone:e). _Nice one._
* Alex423: [PinePhone KDE Community Edition - Unboxing](https://www.youtube.com/watch?v=-bOWr8y39EY). _Just another unboxing._


### Stuff I did

#### Content

I made one video, it's called "Fedora on the PinePhone: Pipewire Calling!", watch it on
  * [PeerTube](https://devtube.dev-wiki.de/videos/watch/982d6f1c-91be-4426-be9e-6f0ecfe9b3ad),
  * [Odysee](https://odysee.com/@linmob:3/fedora-on-the-pinephone-pipewire-calling:1),
  * and [YouTube](https://www.youtube.com/watch?v=TcLnC74Vo7c).

#### Thanks!

BTW: More than 1000 people are now subscribed to my YouTube channel! Thank you all so much!
Sadly, my video output is likely to decrease in the coming weeks, unless I seriously manage to decrease the time creating a video takes. One of the reasons for this is [PineTalk](https://www.pine64.org/pinetalk/), another is that I just won't be able to spend that much time on videos going forward. Also, I really want to spend more time in creating content for this blog that is not video content. _That said, I am hoping to be able to do a "Librem 5 vs. PinePhone livestream in the 3rd week of february, likely on Wednesday the 17th &mdash; just so you have something to look forward to._

#### Random

I installed postmarketOS Edge with Phosh on my Librem 5 Evergreen, which worked nicely. I had tried Plasma Mobile first, but that would not work at the time. postmarketOS edge, which I also updated on my bq Aquaris X5 has really made amazing progress since I last used it.
The highlight of my postmarketOS experiments [is certainly Nheko](https://fosstodon.org/@linmob/105686343450550320), which had been unusable the last time I tried it on the PinePhone and now is really nice. It's not perfect and could scroll smoother here and there, and very minor scaling improvements could be done here and there, but it's without question the most feature complete native Linux Matrix app I have used.

#### LINMOBapps

This week I added a whooping 9 apps to LINMOBapps, including 3 GTK pass frontends (all written in Python). [and some maintenance happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master). I also added some more [PKGBUILDs](https://framagit.org/linmobapps/linmobapps.frama.io/-/tree/master/PKGBUILDs). Please [do contribute](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/tasks.md)!
