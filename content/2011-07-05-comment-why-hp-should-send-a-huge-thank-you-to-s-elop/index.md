+++
title = "Why HP should send a huge Thank You to S. Elop"
aliases = ["2011/07/comment-why-hp-should-send-huge-thank.html"]
date = "2011-07-05T20:02:00Z"
[taxonomies]
tags = ["HP Pre3", "HP Touchpad", "webOS", "MeeGo", "Nokia", "nokia n9", "Nokia N950", "palm pre", "Steven Elop",]
categories = ["commentary"]
[extra]
author = "Peter"
+++
Let's face it: Many people like the Nokia MeeGo 1.2 Harmattan / Maemo 6  presented on the Nokia N9&mdash;if you read the negative comments, these show concerns about buying a device which is abandoned on release, they dislike the fact it uses last years hardware platform or ask the “What about apps?” question&mdash;if Nokia hadn't discontinued the MeeGo platform, one of these negative points would be valid, and the other two would be addressed by future devices over time (I think I even read once that OMAP4 is pin compatible to OMAP3). 
<!-- more -->
  <center>
  <img src="webos-2.1.0-bug.png">
  <p><small>One of too many webOS 2.1.0 bugs</small></p>
  </center>

But let's think about to whom the the Nokia MeeGo platform would have been a strong competitor. Many of you may think Android, and while this absolutely right in the long term, in short and mid term Nokia's Meego platform would have been more of a competitor to the smaller smartphone operating systems / ecosystems, such as Windows Phone 7 or HP webOS, which are chosen by their loyalists because of their usability, which is less flawed than Androids (menu button, multi tasking).

Nokias MeeGo platform is as it is (in its nearly abandoned state) already a strong competitor to what HP webOS is like&mdash;both systems are all about gesture powered multitasking. Let's look deeper. HP webOS is&mdash;and I am sorry to say that&mdash;is flawed by the web technologies it's using&mdash;JavaScript still is not running as fast as seasoned programming languages, and besides that, there will be some work necessary to speed up system services&mdash;just compare the HP TouchPad's Sun Spider benchmark scores. The TouchPad is running the most advanced version of webOS and surely requires some more polishing, as this is the first public release we are dealing with here (and no, it's not the hardware's fault, I've seen benchmarks were the Qualcomm platform smoked the Tegra2 used in most Android tablets out there).

<center><img src="pallenberg-benchmarks.jpg" /></center>

MeeGo, as being a still rather new development on top of very seasoned technologies, performs a lot better (which has a huge impact, you have to consider that these devices are mobile, and less CPU usage is automatically connected with better battery life..), and as Nokia's MeeGo UI is more than competitive, this platform with Nokia’s experience in the mobile sector (which while HP / Palm have some too should be superior) could have easily made it to the third position on the market, with a huge gap to Android and iOS still, but better than the rest. Nokia opted against a clear MeeGo push though, so this is all theory (most likely they would have stuck to the way inferior Symbian too much, anyway)&mdash;thanks to S. Elop,HP has another chance. 

Success, however, is something that requires more than just good ideas, even more than astonishing products (that HP doesn't have yet, the TouchPad, Veer and Pre³ are in many ways inferior). An alliance with Samsung (that build smartphones with Android, Windows Phone 7, Bada and LiMo already) could help with great hardware, but possibly killed HPs own margins and besides that, it's the software which until now still isn't more than a set of brilliant usability concepts that is sort of usable. HP has the chance to fix their issues, which are many &mdash;as Elop gave them some more time.
