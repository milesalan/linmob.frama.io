+++
title = "Flashing Biktorgj's Modem Firmware"
aliases = ["2021/05/26/flashing-biktorgjs-modem-firmware.html"]
date = "2021-05-26T15:49:18Z"
updated = "2022-03-03T20:00:00Z"
[taxonomies]
tags = ["PinePhone", "open source modem firmware", "Quectel EG25-G",]
categories = ["howto"]
[extra]
author = "Peter"
update_note = "Added link to newer post that describes the new install process."
+++

If you've been an avid reader of the recent monthly [PINE64 Community Updates](https://linmob.net/tags/pine64-community-update/), you may have heard of  Biktorgj's effort [to come up with an open-source firmware for the PinePhone's Quectel EG25-G LTE modem](https://github.com/Biktorgj/pinephone_modem_sdk). This is a guide on how to install it.
<!-- more -->
<mark>Thanks to Dylan van Assche, there's now an easier way to install this firmware – [read my post detailing the process instead](https://linmob.net/easily-upgrading-pinephone-pro-modem-firmware/)!</mark>

<strong>From [release 0.3.0](https://github.com/Biktorgj/pinephone_modem_sdk/releases/tag/0.3.0) onwards, the instructions below are outdated. You just need to get the required tools and run the `flashall` script.</strong>

I've been wanting to try the alternate, free-software modem firmware Biktorgj created for a while. I had a hard time doing this, as I did not bother to set 'minicom' up correctly at first. Why Minicom? Without having ADB set up (read [Megi's website](https://xnux.eu/devices/feature/modem-pp.html#toc-unlock-adb-access) for how to do that[^1]), you need to use [Minicom](https://salsa.debian.org/minicom-team/minicom) or [atinout](http://atinout.sourceforge.net/index.html) to send your phones Quectel EG25-G modem into *fastboot mode* in order to re-flash it.

While I went with Minicom originally (listen to the chapter "Custom PinePhone Firmware tryout progress" in [PineTalk 009 for more](https://www.pine64.org/podcast/009-eventful-no-9/)), I had trouble following my own footsteps for my second PinePhone. Therefore, I wrote up on how to this with 'atinout' instead.

If you still want to do this with Minicom (which has the advantage of being packaged for a larger number of distributions[^2]), check out the guide on [bloggerbust.ca](https://bloggerbust.ca/post/how-to-configure-minicom-to-connect-over-usb-serial-uart/)) and remember to use `/dev/ttyUSB2` instead of `/dev/ttyUSB0`.

### Warning and advice

Please only do this, when you know that you are playing with experimental software and [understand the instructions to reinstall the official firmware](https://github.com/Biktorgj/quectel_eg25_recovery). Also, I advise you to connect your phone to a charger and do this over SSH, which, depending on your distribution will need to set up. If you can figure out setting SSH properly, I trust you to generally be competent enough to follow these instructions.

### Prerequisites

#### Downloading the firmware

Let's download the firmware now. Go to the release page in a browser on your PinePhone, and download the `package.tar.gz` file, or copy the URL into your ssh connection and download it with `wget`.

Either way, I recommend to put the file into a new, empty folder and extract it there.

#### Installing 'atinout'

Installing _atinout_ is easy on the distributions that [have it packaged in their repositories](https://repology.org/project/atinout/versions), which are postmarketOS, openSUSE, Fedora (34 and Rawhide). It's also in the AUR, [check my instructions](https://linmob.net/pinephone-building-plasma-mobile-apps-from-the-aur/#preparations) if you have not enabled it yet on your Arch or Manjaro install. On others, you will have to build it from source, rough steps for that are outlined on [askubuntu](https://askubuntu.com/questions/391320/balance-of-gsm-usb-modem-using-ussd/659164#659164). Or you just try running postmarketOS of a SD card to do this&mdash;it's excellent and I recommend you to try it :)
 
#### Installing 'fastboot'

The package to install *fastboot* is named 'android-tools' in many distributions, like Manjaro, postmarketOS and Arch, but this may differ in your distribution.

### Switching to fastboot mode

With everything installed, it's time to put the modem into fastboot mode. But let's test first, whether we have the correct device:

~~~
echo 'AT' | sudo atinout - /dev/ttyUSB2 -
~~~

If the phone answers `OK`, we're good to go:

~~~
echo 'AT+QFASTBOOT' | sudo atinout - /dev/ttyUSB2 -
~~~

This command, depending on your shell and distribution may return something. Most likely, it is going to fail, when you try to run it again, which is likely good. To confirm that your modem entered fastboot mode, run:

~~~
fastboot devices
~~~

If you read "no permissions", which may well happen, depending on your distribution and the groups your user has been assigned, run it _(and all the following fastboot commands)_ again with sudo:

~~~
sudo fastboot devices
~~~

Now you should be seeing something like this:

~~~~
$ sudo fastboot devices
23edcc83        fastboot
~~~~

You made it! Now let's install this firmware!

### Installing the firmware

_If you have cold feed, run `fastboot reboot` (likely with a `sudo` in front of it) and your modem should return to normal operation._ 



Now you can follow the further instructions on the [release page of Biktorgj's project](https://github.com/Biktorgj/pinephone_modem_sdk/releases).
I quoted the relevant part here for your convenience (again, remember to prepend `sudo` if necessary for your setup):

<blockquote>

5. Once in fastboot mode, first flash the unlocked bootloader:

    ~~~
    fastboot flash aboot appsboot.mbn
    ~~~

    After that `fastboot reboot ; fastboot oem stay`

6. After flashing and rebooting to the bootloader the modem will say FAILED (remote: 'Waiting for orders!')

7. Now you can flash everything:

    ~~~
    fastboot flash boot boot-mdm9607.img
    ~~~ 

    If you get an error flashing the kernel, run fastboot flash:raw : `fastboot flash:raw boot boot-mdm9607.img`

    ~~~
    fastboot flash recovery recovery.img
    ~~~
 
    ~~~
    fastboot flash system rootfs-mdm9607.ubi
    ~~~
    
    ~~~
    fastboot flash recoveryfs recoveryfs.ubi
    ~~~

8. After you flashed everything, you can run `fastboot reboot` and wait for it to come back.

</blockquote>

Enjoy! 

BTW: I had to use `fastboot flash:raw boot boot-mdm9607.img` for the first step under '7.', whether you'll need to do the same depends on the version of 'fastboot' your distribution is offering. 


_Remember: If you run into problems, just restore the [vendor firmware](https://github.com/Biktorgj/quectel_eg25_recovery)._


[^1]: Please don't ask me for help following Megi's instructions, I did not manage to comprehend them enough to follow them.

[^2]: [Minicom](https://salsa.debian.org/minicom-team/minicom) is available for just about any distribution, [the package name is always minicom](https://repology.org/project/minicom/versions).
