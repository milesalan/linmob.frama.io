+++
title = "Geeksphone Keon and Peak"
aliases = ["2013/01/24/geeksphone-known-for-their-open-android-phones,html"]
author = "peter"
date = "2013-01-24T20:22:00Z"
layout = "post"
[taxonomies]
tags = ["developer phones", "Firefox OS", "geeksphone", "GeeksPhone Keon", "GeeksPhone Peak",]
categories = ["shortform", "hardware",]
+++
Geeksphone, known for their open Android phones called “One” and “Zero”, is no more in the Android market, but have released the first two Firefox OS developer phones called "[Keon](https://web.archive.org/web/20130601063753/http://shop.geeksphone.com/en/phones/1-keon.html)" and "[Peak](https://web.archive.org/web/20130605024918/http://shop.geeksphone.com/en/phones/5-peak.html)". And:  You can actually purchase them!

Both look nice, but aren't exactly high-end devices, in fact, with only 512 MB RAM (and as they are build with Qualcomm's SoCs, this means even less RAM for actual application) and thus not too interesting.
