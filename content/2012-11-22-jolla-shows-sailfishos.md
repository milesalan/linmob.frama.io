+++
title = "Jolla shows SailfishOS"
author = "peter"
date = "2012-11-22T15:10:00Z"
layout = "post"
[taxonomies]
tags = ["Jolla", "linux", "MeeGo", "Mer", "Myriad", "platforms", "Video", "Sailfish OS", "smartphones", "software"]
categories = ["software", "shortform",]
+++
<a href="http://www.jolla.com">Jolla</a> has presented its <a href="http://www.sailfishos.org">Sailfish OS</a> to the world. Sadly, they didn't announce any hardware, so for now it's just another OS without hardware, like Open webOS, Tizen, Firefox OS and so on&#8230;

_Make sure [to watch the video](https://www.youtube.com/watch?v=u8skFXWR2y8)!_
