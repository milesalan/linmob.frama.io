+++
title = "New stuff..."
aliases = ["2007/11/new-stuff.html"]
author = "peter"
comments = true
date = "2007-11-26T09:40:00Z"
layout = "post"
[taxomonies]
tags = ["Motorola A910", "OpenEZX",]
categories = ["projects", "software",]
+++

No, it's not that I have got working WiFi on my A910 now, but I have got some other news:

This weekend I managed to try out OpenMoko on my Motorola A780 via _ezx-boot-usb_. Works nicely.
<!-- more -->

And I just bought some new stuff yesterday, some Christmas presents for myself: An Motorola E680i (I wanted to have this phone since I saw it for the first time&mdash;but it was so hard to get one. And I got myself another Motorola A910. I really don't understand why these phones are that cheap&mdash;with shipping this one will cost me 25 GBP, that's around 35€, a price you can only get defected Siemens' and ugly old Nokia's for&mdash;normally.

Well, OK, WiFi still doesn't work for browsing. But it will someday.
Maybe you're asking yourselves, why did this guy buy another one? Well, the outer display of my old one broke without me doing anything. But the rest still works, and so I'm thinking of a donation to OpenEZX project, if they want to have it. I tried to boot some zImages for Mototola A1200 and E2, always green screen (except with one image by Alex Zhang), always kernel panic. But I'm not sure, whether my memory card is really working properly, as I was unable to access it from PC yesterday (Win XP, but some errors under Linux, too). Would be really great to have a booting free kernel. Maybe we have and I was to stupid, I'll try again today or tomorrow&#8230;

I think I'll finally have to setup some cross-compiling toolchain or whatever they call it.

But unfortunately:

The USBNET bug isn't fixed yet in Ubuntu, I am looking for another (live-)distribution (building a new 2.6.23.8 kernel didn't help), at least I can access my A780 via _usbppp_. I'll try to install leoppp on my A910, too&mdash;if it works, that'd be enough for the moment.
