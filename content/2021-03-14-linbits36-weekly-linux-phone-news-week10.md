+++
title = "LinBits 36: Weekly Linux Phone news / media roundup (week 10)"
aliases = ["2021/03/14/linbits37-weekly-linux-phone-news-week10.html", "linbits36"]
author = "Peter"
date = "2021-03-14T21:57:00Z"
layout = "post"
[taxonomies]
tags = ["PINE64", "PinePhone", "Purism", "Librem 5", "LINMOBapps", "PineTalk", "Wine", "Sxmo", "Arch Linux ARM", "Ubuntu Touch"]
categories = ["weekly update"]

+++

_It's sunday. Now what happened since last sunday?_

Sxmo on Arch Linux ARM, UBports OTA 16, Libhandy 1.2 and more! _Commentary in italics._
<!-- more -->

### Software development and releases
* [Kongress 1.0.1 is available](https://dimitris.cc/kde/2021/03/09/Kongress_1_0_1.html). _Just a bug fix release._
* [AudioTube](https://invent.kde.org/jbbgameich/audiotube), a new app to play Music from YouTube, [has been announced](https://jbbgameich.github.io/kde/2021/03/13/audiotube.html).
* [GNOME Podcasts 0.4.9](https://gitlab.gnome.org/World/podcasts/-/releases#0.4.9) has been released.
* Libhandy 1.2 [has been released](https://gitlab.gnome.org/GNOME/libhandy/-/blob/master/NEWS). 
* The [Ubuntu Touch OTA-16 Changelog](https://forums.ubports.com/topic/5837/ubuntu-touch-ota-16-changelog) has been published on the UBports forums. _This is an impressive change log. Please note that the usual OTA schedule does not apply to the PinePhone._
* [sxmo-alarm](https://github.com/justinesmithies/sxmo-alarm), a script to get Sxmo running on Arch Linux ARM, has been released. 


### Worth noting
* /u/ilovelinuxporn: [Wine x86 running at near-native speeds without qemu! (Tutorial coming soon)](https://teddit.net/r/PINE64official/comments/m3vb3n/wine_x86_running_at_nearnative_speeds_without/). _This is something to watch._
* /u/Moxvallix: [A list of PinePhone compatible games (contribution welcome)](https://teddit.net/r/PinePhoneOfficial/comments/m1wfem/a_list_of_pinephone_compatible_games_contribution/). _Nice list!_


### Worth reading 
* Adrien Plazas: [lWhat's New in Libhandy 1.2](https://adrienplazas.com/blog/2021/03/12/libhandy-1-2.html).
* FOSS Adventures: [Real Linux on a Smartphone: PinePhone and openSUSE Tumbleweed](https://www.fossadventures.com/real-linux-on-a-smartphone-pinephone-and-opensuse-tumbleweed/). _A nice post on an OpenSUSE focused blog._
* InfoWorld: [PinePhone: A Linux smartphone a developer could love](https://www.infoworld.com/article/3610602/pinephone-a-linux-smartphone-a-developer-could-love.html). _Nice introductory article._
* Purism: [App Showcase: Tootle](https://puri.sm/posts/app-showcase-tootle/). _Tootle has really come along nicely._
* fedora Community Blog: [“Houston, we have video”: Matrix video calls on the PinePhone](https://communityblog.fedoraproject.org/matrix-video-calls-pinephone/). _Another rehash of last weeks news._

### Worth listening
* [PineTalk 004: GPU-o-Rama](https://www.pine64.org/2021/03/12/004-gpu-o-rama/). _In this fourth episode, [Ezra](https://elatronion.com) and I discuss news and community suggestions. Give it a listen!_

### Worth watching

* TechHut: [Nextcloud + Minecraft Server on a Linux Phone!? PINEPHONE Stress Test!](https://www.youtube.com/watch?v=wSKHbYVvKF0). _It's a nice idea, but I would rather recommend to run these services on a Single Board Computer._
* Glenn Hancock: [Mobian on the Pinephone Review 2021](https://www.youtube.com/watch?v=kAT6cS2vyXM). _Nice video!_
* Glenn Hancock: [Pine64 Pine Phone Motherboard replacement](https://www.youtube.com/watch?v=aZsKefuPQK4). _How to disassemble a PinePhone to send in the mainboard._
* Privacy & Tech Tips: [Howto: Compiling Sourcecode On LInux/Pinephone/Pinetab/Raspberry Pi](https://www.youtube.com/watch?v=JDJJP0Wzg4M). _Nice tutorial! A natural follow up to this video would be videos that instruct people to use cmake or meson and ninja._
* Lucas Pires Camargo: [booting postmarketOS on the Nokia N9](https://www.youtube.com/watch?v=CL6JDwNQRig). _Nice to see postmarketOS booting on Nokias last Linux Phone._
* Bryan Olson: [Pine Phone. The possibilities are endless!](https://www.youtube.com/watch?v=ClB6RTrisco). _A UBports edition, now? Interesting._
* UBports: [Ubuntu Touch Q&A 96](https://www.youtube.com/watch?v=a0ZuxdwlAsQ). _Another interesting update._


### Stuff I did

#### Content

No content this week. Sorry.

#### LINMOBapps

Three new apps have been added, one via merge request (thanks, cahfofpai!) one on external suggestion by its author (thanks, The3DmaN!) and [a little maintenance happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master). I also added some more [PKGBUILDs](https://framagit.org/linmobapps/linmobapps.frama.io/-/tree/master/PKGBUILDs). Please [do contribute](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/tasks.md)! 
