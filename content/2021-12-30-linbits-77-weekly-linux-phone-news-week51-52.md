+++
title = "LinBits 77: Weekly Linux Phone news / media roundup (week 51/52)"
date = "2021-12-30T18:00:00Z"
draft = false
[taxonomies]
tags = ["PinePhone","PinePhone Pro","Mobian","postmarketOS","GTK4","Maui Shell","Purism","LINMOBapps",]
categories = ["weekly update"]
[extra]
author = "Peter"
+++

_The end of the year in Linux Phones, let's have a last LinBits:_ A new Maui Shell for phones and tablets, postmarketOS v21.12, Mobian on the PinePhone Pro, GTK 4.6.0, an app-list replacement prototype and more!
<!-- more --> _Commentary in italics._ 

### Software releases
* Maui Shell, see more below: [Text](#maui-shell) and [video](#maui-shell-demo-videos).
* Biktorgj’s [open-source modem firmware 0.5.0](https://github.com/Biktorgj/pinephone_modem_sdk/releases/tag/0.5.0) has been released. fixing some more things.
* postmarketOS have released [their next stable release, v21.12](https://postmarketos.org/blog/2021/12/29/v21.12-release/). This is based on Alpine 3.15, delivering Sxmo 1.6, Plasma Mobile Gear 21.12 and Phosh 0.14.0 among other updated packages. Also, TTYescape has been added, which is basically a way to fix things on TTY when your GUI should fail without using SSH or a hardware keyboard. Device support has improved, 9 new devices (5 Snapdragon 410 powered ones) have been added. Sadly, the good old Nokia N900 (2009) was dropped, but that’s just what happens when nobody steps up. If you want to hold on to your N900, [Maemo Leste still support it](https://leste.maemo.org/Nokia_N900). _If you have not used postmarketOS in a while (or ever) and have a compatible device, this release might be a good time – stable is less prone to breakages, and with service packs you still get new software every then and now._
* GTK 4.6.0, a new major GTK 4 release, is out! _More dust settling as GNOME 42 is nearing!_

### Worth noting
* PINE64 are having a [PinePhone Community Survey](https://www.pine64.org/2021/12/29/pinephone-community-poll/)! Make sure to take part!
* If you’re waiting for better battery life on your Librem 5, follow [these issues](https://source.puri.sm/Librem5/linux-next/-/merge_requests?label_name%5B%5D=suspend)!


### Worth reading

#### Software Progress
* This Week in GNOME: [#24 Future of Files](https://thisweek.gnome.org/posts/2021/12/twig-24/). _I need to try that GTK4 port of Tangram!_
* antoniof: [The icon view is dead, long live the icon view!](https://blogs.gnome.org/antoniof/2021/12/29/the-icon-view-is-dead-long-live-the-icon-view/).
* Claudio Cambra: [Happy holidays! Kalendar v0.4.0 is our gift, and it brings new views, improved performance, and many bugfixes – Kalendar develop 24](https://claudiocambra.com/2021/12/25/happy-holidays-kalendar-v0-4-0-is-our-gift-and-it-brings-new-views-improved-performance-and-many-many-bugfixes-kalendar-devlog-24/).
* kver: [Better Adaptive Icons for 2022](https://kver.ca/2021/12/better-adaptive-icons-for-2022/?utm_source=atom_feed)

#### Distro Progress
* Mobian blog: [The PinePhone Pro is here (and it runs Mobian!)](https://blog.mobian-project.org/posts/2021/12/28/pinephone-pro/). _Great! The only thing that saddens me a bit that parts of this can be interpreted as further mainlining of the OG PinePhone being quite unlikely._
* Jozef Mlich: [NemoMobile in December/2021 part two](https://blog.mlich.cz/2021/12/nemomobile-in-december-2021-part-two/). _Great progress!_

#### Call for Testing
* UBports: [Ubuntu Touch OTA-21 Call for Testing](https://ubports.com/de/blog/ubports-blogs-nachrichten-1/post/ubuntu-touch-ota-21-call-for-testing-3796). _Do help out by trying new stuff early!_

#### Maui Shell
* NitruxOS blog: [Introducing Maui Shell](https://nxos.org/maui/introducing-maui-shell/). _This is definitely something to look forward to! I always need to physically interact with UIs to know how much I like them, but this seems really promising!_
  * Liliputing: [Maui Shell is a convergent desktop for Linux phones, tablets, laptops, and desktop computers](https://liliputing.com/2021/12/maui-shell-is-a-convergent-desktop-for-linux-phones-tablets-laptops-and-desktop-computers.html)

#### Backups: Something one should always do, and if not, now
* Purism: [How to Create Librem File Backups](https://puri.sm/posts/how-to-create-librem-file-backups/).

#### Hardware corner
* TuxPhones: [A SHIFT in perspective, is this the next step for Linux phones?](https://tuxphones.com/the-shift-we-need/). _Nice article about an interesting phone (I likely won’t buy)!._
  * Liliputing: [Shift6mq is a modular, repairable (and somewhat Linux-friendly) smartphone from Germany](https://liliputing.com/2021/12/shift6mq-is-a-modular-repairable-and-somewhat-linux-friendly-smartphone-from-germany.html). _Brad’s take._

#### Other roundups
* Fossphones.com: [Linux Phone News - December 28th, 2021](https://fossphones.com/1228-news.html). _Well done!_

### Off topic
* Drew DeVault: [Please don't use Discord for FOSS projects](https://drewdevault.com/2021/12/28/Dont-use-Discord-for-FOSS.html).

### Worth listening
* PineTalk Season 2 Episode 4: [Behind the curtains](https://www.pine64.org/2021/12/28/s02e04-pinetalk-behind-the-curtains/). _Great show!_
* Destination Linux: [Linux in 2021: Year In Review! \| Destination Linux 258](https://destinationlinux.org/episode-258/).

### Worth watching

#### PinePhone Emulation Gaming
* Danct12: [PICO-8 Emulator on PinePhone (with keyboard case)](https://tilvids.com/w/k2Lj4GbqRzf8uWykWhu9wx).

#### Maui Shell Demo Videos
* Camilo Higuita: [Cask - Maui Shell - Phone](https://www.youtube.com/watch?v=efB9hX_2lZU). _I like the concept, but can't full grasp it yet._
* Camilo Higuita: [Cask - Maui Shell - Tablet](https://www.youtube.com/watch?v=FxedAIpF3hc)
* Camilo Higuita: [Cask Tablet mode](https://www.youtube.com/watch?v=Yxklj0OTGto). _On device demos are always nicer, and this looks promising!_

#### Ubuntu Touch
* Digital Wandering: [Using Android apps on Ubuntu Touch ((WAYDROID))](https://www.youtube.com/watch?v=IMATJ-jpl1Y).
#### Shorts

#### Shorts
* NOT A FBI Honey pot: [Linux user test the Pinephone's browser privacy.....#shorts](https://www.youtube.com/watch?v=U0U09AGyPDU). _Technically, this is all running with Waydroid, so I am not sure what’s so PinePhone-y about this._

### Thank you!

Thank you for following these updates in 2021, and for all the kind feedback. This is the last update to carry the LinBits name, fear not, these posts are going to continue, just under a different name, without numbering. To celebrate the new name, I’ll most likely take next week off – I do need a break. But don't worry, this roundups are going to be continued.

### Stuff I did

#### Random
* Following last weeks "[[Draft/Discussion] Future: Turning this list into LinuxPhoneApps.org](https://framagit.org/linmobapps/linmobapps.frama.io/-/issues/10)", I've built something that you can find at [https://linuxphoneapps.frama.io](https://linuxphoneapps.frama.io). It has multiple issues, e.g. as the app list performs as poorly as ever and there's no real working road formward, but at least this could be a stop-gap solution that allows for more explanatory content and other things this project needs, e.g. it's own blog. _Please get in touch if you've got thoughts!_

#### LINMOBapps and Linux Phone Apps
Aside from the website-building above, this week two apps were added:
* [Messenger-GTK](https://gitlab.com/gnunet-messenger/messenger-gtk), an _(awesome)_ example GUI application for the GNUnet Messenger service using libgnunetchat, and
* [Flowtime](https://github.com/Diego-Ivan/Flowtime), a productivity timer app, similar to pomodoro timers.

There's plenty back log to still work through, but I guess I better take it easy.

[Read here what (else) happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master) on LINMOBapps this week. And please [do contribute!](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/CONTRIBUTING.md)
