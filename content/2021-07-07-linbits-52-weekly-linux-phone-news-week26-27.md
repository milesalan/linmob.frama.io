+++
title = "LinBits 52: Weekly Linux Phone news / media roundup (week 26/27)"
date = "2021-07-07T21:35:00Z"
[taxonomies]
tags = ["PINE64", "PinePhone",  "PineTalk", "Ubuntu Touch", "postmarketOS",]
categories = ["weekly update"]
[extra]
author = "Peter"
+++

_It's Wednesday. Now what happened since last Wednesday?_

postmarketOS 21.06, accelerated video playback with Clapper and more!<!-- more --> _Commentary in italics._ 

### Software releases
* [postmarketOS 21.06](https://postmarketos.org/blog/2021/07/04/v21.06-release/) has been released, basing on Alpine 3.14 and supporting 4 additional (15 total) devices. _It's awesome to see this progress._

### Worth noting
* A few ARM specific PKGBUILD scripts have been removed from the AUR, as this [Reddit thread](https://www.reddit.com/r/archlinuxarm/comments/odx7bg/arch_linux_arm_aur_equivalent/) points out. _While this makes sense with Arch Linux ARM being a seperate project from Arch Linux, it's still sad and feels slightly unnecessary._ 
* If you want to check out accelerated video playback without messing up your systems `/usr/local`, you can try building the [Flatpak of livi/µPlayer](https://social.librem.one/@agx/106534519111169979).

### Worth reading
#### Long reads
* Dalton Durst: [The Endless Conundrum of creating a secure PinePhone ](https://daltondur.st/secure_pinephone_1/). _Yes, this is technically to old to be included, but it's definitely worth reading. Dalton not only is a very nice guy, he also knows what he's talking about here: There are trade-offs that go along with verified boot, making it not too desirable._

#### Software progress
* Jozef Mlich: [Advances of Nemomobile in July/2021](https://blog.mlich.cz/2021/07/advances-of-nemomobile-in-july-2021/).
* Alejandro Domínguez: [Small progress update of my 2021’s GSoC with Fractal-next](https://aledomu.github.io/gnome/small-progress-update-of-my-2021-gsoc-with-fractal-next/).
* Georges Stavracas: [Dark & light style selector in To Do](https://feaneron.com/2021/07/02/dark-light-style-selector-in-to-do/). _I hope that they will work on mobile compatibility, too. GNOME To Do 40.1 almost fits the screen, the lists feature does not work right with Squeekboard &mdash; it's so close!_
* Claudio Cambra: [Editing and deleting mistakes… and events too — Kalendar week 4 (GSoC 2021)](https://claudiocambra.com/2021/07/04/editing-and-deleting-mistakes-and-events-too-kalendar-week-4-gsoc-2021/).
* Brian Daniels: [Hardware Accelerated Video Decoding on the PinePhone with Clapper](http://briandaniels.me/2021/07/06/hardware-accelerated-video-playback-on-the-pinephone-with-clapper.html). _Last week we had an initial hack, now there's a nice, existing app adopting this. Awesome!_ 
  * Linux Smartphones: [Hardware accelerated video playback now possible on the PinePhon](https://linuxsmartphones.com/hardware-accelerated-video-playback-now-possible-on-the-pinephon/). _Brad's take!_
  
#### Personal Progress
* Carl Schwan: [KDE Documentation & New Job at Nextcloud](https://carlschwan.eu/2021/07/05/kde-documentation-new-job-at-nextcloud/). _I wish him all the best (and selfishly hope he still finds time for his Plasma (Mobile) apps)!_
#### Hardware 
* Linux Smartphones: [Volla Phone X rugged phone is available for pre-order with Ubuntu Touch or Android](https://linuxsmartphones.com/volla-phone-x-rugged-phone-is-available-for-pre-order-with-ubuntu-or-android/). _It's basically the same as the previous Volla Phone, but more rugged and with an even larger battery._
### Worth listening
* Pinetalk [012: Privacy and Security with RTP](https://www.pine64.org/podcast/012-privacy-and-security-with-rtp/). _Have fun listening! Aside from the title, we discuss recent community news and questions that revolve around future PINE64 devices we would like to see._

### Worth watching

#### Ubuntu Touch corner
* UBports: [Ubuntu Touch Q&A 103](https://www.youtube.com/watch?v=ScNeoHry4uk). _Dalton and Alfred again, this time discussing 20.04 (Focal Fossa) progress, initial Open-Store support, clickable progress. Alfred build something called [UBports PDK](https://github.com/ubports/ubports-pdk) to have a better time building software for Ubuntu Touch, which runs amazingly fast on an Apple M1 system. Questions revolve around Rust, AGPS, Morph Browser and security._
* Digital Wandering: [Ubuntu Touch - Choosing a Device (part 2) - featuring the Nexus 5, Pixel 2, and Pixel 3a](https://www.youtube.com/watch?v=BUfPAixcdvI).
* AdhesionTek: [Ubuntu Touch Hands-On: Your phone is your PC, or is it?](https://www.youtube.com/watch?v=ymgpLhe3yPs). _Ugh. I was debating whether I should include this, as it's one more video not understanding while not having 'apt' outside of the Libertine container is a good idea, and it's one more video that thinks dumping on community projects and comparing them to trillion-dollar-company-products would be at all a fair thing to do._

#### Jing Corner
* JingOS: [JingPad Project Monthly Report \| June 2021, JingPad Keyboard EVT, JingOS ARM Alpha, Android ROM...](https://www.youtube.com/watch?v=wnegDC7y_KY).

#### PinePhone Palace
* RTP: [Pinephone: Improve Photos With This GIMP](https://odysee.com/@RTP:9/pinephone-improve-photos-with-gimp:1). _Yes, GIMP on the PinePhone!_
* RTP: [USB Key + Password LUKS Required Scripts (For Linux Phones and Devices)](https://odysee.com/@RTP:9/usb-key-+-password-luks-required-scripts:8).
* Сергей Чуплыгин: [Nemomobile keyboard trackpoint](https://www.youtube.com/watch?v=HoS55Bo0fCU). _Neat!_
* PizzaLovingNerd: [I installed Minecraft on a PinePhone](https://www.youtube.com/watch?v=bCO1CYvffQw). _Spoiler: It's complicated, but not impossible._
* Brian Daniels: [Hardware Video Decoding on the PinePhone with Clapper](https://www.youtube.com/watch?v=BvmRV6IIGGo). _Same video as last week, but now with Clapper instead of gst-playbin-1.0. Awesome!_
* Raspberry Pi Projects and More: [A Raspberry Pi in the Form of a Phone! (Kind of) - PinePhone Setup Guide!](https://www.youtube.com/watch?v=ahfJJ-cu5vw) _Nice guide!_

### Stuff I did

I wrote a blog post on [GNU/Linux on Tablets: Hardware](https://linmob.net/gnu-linux-on-tablets-hardware/).

### Stuff I did

#### Content

Nothing. Well, PineTalk ;-)

#### LINMOBapps
LINMOBapps did not get any love this week. No new apps were added.

[Read here what (else) happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master). 
Please [do contribute!](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/CONTRIBUTING.md)
