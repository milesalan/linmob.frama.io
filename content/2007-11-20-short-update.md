+++
title = "Short update"
aliases = ["2007/11/20/short-update.html"]
author = "peter"
comments = true
date = "2007-11-20T09:00:00Z"
layout = "post"
[taxonomies]
tags = ["EZX/LJ", "modding", "Motorola A910"]
categories = ["projects", "software",]
+++

Recently, I did some work on CG44, and tried to flash it into my A910&mdash;size was correct, and no, i did not forget to ad 1000xff file header. But: It didn't work. After flashing, my phone didn't start up.
<!-- more -->

So decided to do these mods I want to do by putting additional files on my memory card (maybe copying them to (at least the most important ones ezxlocal) and then to do some scripting, maybe there is a way for autostart.

We'll see. Greatest probs ATM are not enough time, _usbnet_ being broken in Ubuntu Gutsy and sometimes a lack of know how (Google often helps&#8230;)
