+++
title = "Motorola A910: Checking out firmware"
aliases = ["2007/09/motorola-a910-checking-out-firmware.html"]
author = "peter"
date = "2007-09-24T17:14:00Z"
layout = "post"
[taxonomies]
tags = ["EZX/LJ", "ezx_wlan.cfg", "modding", "UMA"]
categories = ["hardware", "projects",]
+++
_Introduction: Motorola A910 is a Linux Phone which features WiFi for UMA purposes. The problem with this WiFi is, that it only works with certain networks. As (most) software for Motorola E2 works, I checked out lots of sites to learn about modifying firmware._
<!-- more -->
Yesterday I started to work on A910 again, after I was off for a week, meeting parents and grandparents. As I luckily found someone, who gave me a few firmware files for A910, I extracted the codegroups and had a look at them on Linux. The probably most interesting one is CG43, as it contains the main file system, `/bin/`, `/lib/`, `SYSqtapp`&mdash;but I'll get back to this in some days, when I have my phone flashed to an own firmware, which I want to include some of the modifications some freaks made for Motorola ROKR E2. But this might take some time, as I have to prepare for some exams at university.

CG42 contains `/usr/setup/`, and there is a file called &#8220;ezx_wlan.cfg&#8221;. Line 4 of this file contains the following text:
~~~~~
WiFiUIConnectUMAN = 1</blockquote>
~~~~~
Maybe changing the value here is an easy way to full WiFi connectivity. I'll try, as soon as I am sure how to do that without killing the handset.
