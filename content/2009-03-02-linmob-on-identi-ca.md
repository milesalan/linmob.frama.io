+++
title = "linmob on identi.ca"
aliases = ["2009/03/02/linmob-on-identi-ca"]
author = "peter"
date = "2009-03-02T11:45:00Z"
layout = "post"
[taxonomies]
tags = ["cebit", "identi.ca", "microblogging"]
categories = ["internal"]
+++
<p>You <del>might</del> have certainly heard of this micro blogging stuff and I intend to use it from now on, as I experienced that it can be useful and is fun&mdash;using my personal twitter account.

That's why I have signed on identi.ca, to link to news&mdash;and to cover events, like [CeBIT](http://www.cebit.de)... so watch out for

[http://identi.ca/linmob](http://identi.ca/linmob)
