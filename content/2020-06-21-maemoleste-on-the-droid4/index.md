+++
title = "Maemo Leste on the Droid 4"
aliases = ["2020/06/21/maemoleste-on-the-droid4.html"]
author = "Peter"
date = "2020-06-21T14:00:18Z"
layout = "post"
[taxonomies]
tags = ["Droid 4", "Maemo Leste", "Devuan", "Debian", "postmarketOS", "Hildon", "sustainability", "PowerVR"]
categories = ["impressions"]
+++
_I am still not ready to write something substantial regarding software on the [PinePhone][pinephone]. How could I, having not yet tested Maemo Leste on it?_

I have had a Droid 4 since [2012 (partially broken)](https://brimborium.net/zurueck-zur-tastatur/#more-3343), and while the display is not great (it is a 4.0" WVGA _pentile LCD_), I always liked the idea of having a slide out keyboard since having owned the first official Android phone, the T-Mobile G1/HTC Dream. <!-- more -->Also, the Droid 4 was, combined with Motorolas LapDock product, on of the first convergent phones. Unfortunately, at least running the original Android software, it was not a convergent experience that felt fast or even very usable. It worked, but it did not work well enough&mdash;and at least my german (?) LapDock variant terrible key placement, making touch typing quite error prone. I could not get used to it.

Time passed, and, not giving up on hardware keyboards, at some point after having had aquired first a working unit in 2015 and then a converted Motorola PhotonQ (a sprint phone that did not originally feature a sim slot), I went with "normal" smartphones again, using iOS or Android with the AOSP keyboard, usually spiced up with the proprietary swyping lib to be found in GApps packages. I had not yet completely forgotten about the Droid 4 when I saw a [Fosdem talk](https://archive.fosdem.org/2018/schedule/event/hwenablement_mainline_linux_on_motorola_droid_4/) showing that it now had pretty great mainline linux support. PostmarketOS supported it (but at the time I totally failed to use `pmbootstrap`), and a project that did not (and does not) waste too much time with building an awesome website: [Maemo Leste][leste].

Now, in 2020, Maemo Leste have published a [really awesome image](https://maedevu.maemo.org/images/droid4/20200323/) ( _Edit 6/26/2020_: shortly after publication, Maemo Leste published [updated images](http://maedevu.maemo.org/images/droid4/20200624/), see also [their blog post](https://maemo-leste.github.io/maemo-leste-thirteenth-update-april-may-june-2020.html)) that even has accelerated graphics ([and more][leste-droid4]) with its mainline kernel, which is quite a feat for a device suffering from Imageons PowerVR graphics. Now, Maemo Lestes' Hildon interface, which everyone who ever used a Nokia N900 likely still remembers, runs fluid and is finally really usable without waiting for skipped animations. It's not glitch-free though, but honestly, I don't mind&mdash;it is such a giant leap forward nontheless.

So what's lacking? Like most (if not all) other community build GNU/Linux distributions, Maemo Leste does not come with all that many applications that are specifically built for it&mdash;there's [one repo][leste-extras] that pretty gives you a browsable list of what you might want to try first. [Clarification: You can install/update these specific apps and themes via the app Application Manager on the phone.] Unfortunately, audio playback still has to be figured out the old way with Alsa, and there is no GUI for calling or texting, even though both work fine with ofono.
But, if you are fine with these caveats, with Hildon and the hardware keyboard leading to a landscape orientation in usage, you can run some desktop apps quite well, e.g. `nheko`, a matrix client; browsers like `dillo` or `surf2`; or even `emacs`. A literal ton of Devuan packages are just an `apt install` away, although, in order to get a launcher in Hildon, you need to `cp` or `ln -s` the corresponding desktop files from `/usr/share/applications` to `/usr/share/applications/hildon`.

Besides graphical desktop apps, you can use a ton of "terminal ui" applications, as soon as you have installed the `leste-config-droid4` package, which enables typing some more characters like `:` using the hardware keyboard (.

_Conclusion:_ Maemo Leste is _the_ project to check out if you have a Motorola Droid 4 catching dust somewhere (or buy one of eBay).

### Important Links:
* [Maemo Leste Status](https://leste.maemo.org/Status)
* [Droid 4 Status and installation instructions][leste-droid4]
* [further supported devices (including PinePhone)](https://leste.maemo.org/Category:Device)
* [Maemo Leste application wishlist](https://leste.maemo.org/Wishlist)
* [github.com/maemo-leste](https://github.com/maemo-leste/)
* [Bugtracker](https://github.com/maemo-leste/bugtracker/issues)
* [Project Milestones](https://github.com/maemo-leste/bugtracker/milestones)

### Stay informed / get in touch!
You can follow their [Blog](https://maemo-leste.github.io/), subscribe to their [mailing list](https://lists.dyne.org/lurker/list/maemo-leste.en.html) or chat away on IRC __#maemo-leste__ (on freenode).

### Bad Photography

![Droid 4 Maemo Leste Launcher (with some self-installed apps)](202006-droid4-launcher.webp)

![Droid 4 Maemo Leste Brightness Settings Screen](202006-droid4-settings.webp)

![Droid 4 Maemo Leste Task Switcher](202006-droid4-multitasking.webp)

### Also on Droid 4
If you want to play with postmarketOS, you [can also do so on a Motorola Droid 4](https://wiki.postmarketos.org/wiki/Motorola_Droid_4_(motorola-maserati)).

[pinephone]: https://www.pine64.org/pinephone/
[leste]: https://leste.maemo.org/Main_Page
[leste-extras]: https://github.com/maemo-leste-extras
[leste-droid4]: https://leste.maemo.org/Motorola_Droid_4
