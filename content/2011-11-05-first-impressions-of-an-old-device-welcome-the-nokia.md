+++
title = "First Impressions of an old device: Welcome the Nokia N900"
aliases = ["2011/11/05/first-impressions-of-an-old-device-welcome-the-nokia.html", "2011/11/05/first-impressions-of-an-old-device-welcome-the-nokia-n900.html"]
author = "peter"
date = "2011-11-05T08:05:00Z"
layout = "post"
[taxonomies]
tags = ["Maemo", "MeeGo", "nokia n9", "Nokia N900", "platforms", "TI OMAP 3430"]
categories = ["impressions"]
+++
_Recently, while being really excited about the beautiful Nokia N9, I realized that the prizes for used N900s had finally dropped into regions, where it was an instant purchase for me. So I went ahead and bought a device, which I hadn't really ever seen that often._

<img src="20111105-nokia-n900.jpg" alt="Photo of the Nokia N900" />

In fact, while taking it out of it's box, I was almost overwhelmed by the good condition my used unit was in, and while taking the hefty device in my hand, I finally realized what this skinny nerd girl, which is the only person among the people I call friends that has ever had an N900, had meant: The N900's thickness, surely not looking good on spec sheet or photos, doesn't to it no harm once you hold it and use it.

The soft touch feel of the device is really enjoyable, and aside, the software looks really good on the 3.5&#8221; WVGA screen. And not only that, while you notice that the N900 has a resistive touch screen, you will soon realize, that it doesn't kill this device, in fact I am sure that as soon as you start exploring possibilities like that debian chroot option, you will actually be happy that Nokia made this decision.

Naturally, the speed of the late 2009 N900 doesn't compare well to todays top notch competition' still the combination of a TI OMAP 3430 with 256MB RAM is likely way more fun on this device than on a equally specced Motorola Milestone and surely more fun than on a orginally Palm Pre.

Asking myself, why I didn't get that thing right at launch is a question I asked myself later last night. As far as I remember it simply was the superficial comparison (photos and spec lists), that the N900 really lost to the Motorola Milestone for then obvious reasons. Today, it is different: If you are really up for making a bargain on a used and aged device (which is not the worst thing one can do, as last years top notch hardware is usually better made than this years mid level thing), you should rather consider the N900. I know, this feels odd in a post titled &#8220;First impressions&#8221;, but even though both devicea have arrived at a dead end talking of software, the N900 is way better than a poorly supported Android phone.

Maemo 5 really is as promsing as it always seemed to me from looking at videos. Really, I will never umderstand Nokias' decision to kill software platforms shortly after or at launch. Not that Maemo 6 Harmattan, a.k.a. &#8220;MeeGo 1.2&#8221; on the N9 looks even more compelling&mdash;but seriously, it took them to effing long to get out the door with it, far longer than I assume it would have taken to make a smaller N900 in an E7 shape running a Maemo 5 iteratin ironing out lacking features like a portrait mode. I could go on ranting on this issue, as I could really rant myself into a rage over many bad decisions of Android manufacturers.

To wrap this up: My overall first impression of this N900 is very positive. I like it, it was a good decision to spend roughly 130 Euro for it.

_This article was written on the N900._
