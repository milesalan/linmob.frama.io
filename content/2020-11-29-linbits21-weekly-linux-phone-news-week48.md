+++
title = "LinBits 21: Weekly Linux Phone news / media roundup (week 48)"
aliases = ["2020/11/29/linbits21-weekly-linux-phone-news-week48.html", "linbits21"]
author = "Peter"
date = "2020-11-29T18:55:00Z"
layout = "post"
[taxonomies]
tags = ["Pine64", "PinePhone", "Arch Linux ARM", "DanctNIX Mobile", "Manjaro", "Jumpdrive", "Plasma Mobile", "Phosh", "Lomiri", "Purism", "Librem 5", "Mainboard replacement", "Jolla", "PineCone", "Games"]
categories = ["weekly update"]
+++

_It's sunday. Now what happened since last sunday?_

New distribution releases, a new p-boot-demo and more. _Commentary in italics._
<!-- more -->

### Software releases and improvements

* [Arch Linux ARM/DanctNIX mobile has seen a new release this sunday](https://github.com/dreemurrs-embedded/Pine64-Arch/releases/tag/20201129), which brings a lot of minor updates that affect multitasking (swipe up to close an app) and convergence fixes, that should improve the overall experience quite a bit.
* Manjaro have released some new builds: [Phosh beta 3](https://forum.manjaro.org/t/manjaro-arm-beta3-with-phosh-pinephone/39446), [Lomiri Alpha 2](https://forum.manjaro.org/t/manjaro-arm-alpha2-with-lomiri-pinephone/39561). _For videos of these releases, see below. And if you are looking for newer Plasma Mobile images, make sure to check the [Resources](https://linmob.net/resources/) page._
* Megi has released a [new version of his p-boot-demo image](https://xnux.eu/p-boot-demo/), featuring updated (although already partially outdated) distributions. _Most impressive are the improvements to p-boot, the privacy mode is really quite something and way more useful than the DIP switches. Also great: Booting to eMMC (meaning you don't need to remove the SD card to go back to your main OS) and the integration of JumpDrive, which is helpful if you want to copy files from your computer over USB by just plugging the PinePhone (aside from it's main use of installing distributions to the eMMC/internal storage)._

### Worth reading
* xnux.eu log: [New multi-distro and p-boot released, WiFi power savings](https://xnux.eu/log/#025) _p-boot see above and below, the 0.35 Watt less WiFi power usage will help surely help with battery life._
* Purism: [Mobile/Desktop Convergence](https://puri.sm/posts/mobile-desktop-convergence/). _Great post and videos. The performance they manage to squeeze out the Librem 5 here looks impressive, making me look forward to receiving my October 2017 preorder soon even more._
* LinuxSmartphones: [PinePhone becomes a DIY Linux laptop with this 3D printed keyboard](https://linuxsmartphones.com/pinephone-becomes-a-diy-linux-laptop-with-this-3d-printed-keyboard/). _
* LinuxSmartphones: [Manjaro ARM Beta3 for PinePhone brings camera, phone call, and gesture improvements](https://linuxsmartphones.com/manjaro-arm-beta3-for-pinephone-brings-camera-phone-call-and-gesture-improvements/).
* Jolla Blog: [The Original Jolla Phone turns 7 today](https://blog.jolla.com/jolla7/). _This is a sad one. There won't be further updates to the original Jolla Phone after 7 years &mdash; but it's also great: Which other smartphone has ever gotten 7 years of software updates?_
* Lup Yuen LEE: [Quick Peek of PineCone BL602 RISC-V Evaluation Board](https://lupyuen.github.io/articles/pinecone). _If you are interested in different Bluetooth or WiFi devices, make sure to read this._

### Worth watching

* PixelOutlaw: [PinePhone (Mobian) Running Emacs and Lisp](https://www.youtube.com/watch?v=sV9loJmPyDY). _This is nerdy and awesome._
* KDAB: [QtDD - Kirigami: convergence with Desktop as first-class](https://www.youtube.com/watch?v=2YkKRqV55vI). _If you want to learn something about Kirigami, the key framework of Plasma Mobile, this video is worth your time._
* Privacy & Tech Tips: [Protect Your IP Address: Setup Firefox w/Tor Proxy (Pinephone Mobian Linux Phosh Here- Any OS Works)](https://www.youtube.com/watch?v=CWtJLpTHkcU). _If you want extra privacy, this video is going to help you out._
* The Linux Experiment: [Create your own Linux ecosystem with Nextcloud, DavX5 and KDE Connect](https://www.youtube.com/watch?v=oGYppxV-kFE). _Nextcloud and KDE Connect are projects you should truly check out. If you want to truly self host at home, keep in mind that there are simple avenues like [YunoHost](https://yunohost.org/#/) or [Freedombone](https://freedombone.net/)._ 

#### Manjaro videos 
* Tong Zou: [Pine64 PinePhone unboxing + overview - a phone that runs Manjaro Linux!](https://www.youtube.com/watch?v=YqQlHM4EhGU). _Great video!_
* GeoTechLand: [Manjaro Phosh – Calls, Text & Camera On Pinephone](https://tilvids.com/videos/watch/70c54ce8-85e9-46ef-873b-157392ebb122). _Nice demo &mdash; and also why I don't do such demos._
* Manjaro Linux: [Reveal of stand-by time (battery life) of a Pinephone 1.2a via Lomiri/Manjaro](https://youtube.com/watch?v=aLRwWiqWFOo). _This battery life demo was quite impressive. It's not totally surprising though &mdash; in my testing of Manjaro Lomiri, it's always been very aggressive with going to standby, rendering using SSH a real pain with Alpha 1. I almost wondered whether some of the battery life improvements from Ubuntu Touch [Dalton Durst recently talked about](https://www.youtube.com/watch?v=tt1U677h5kY) have been carried over to Manjaro Lomiri, but on second thought I don't think so._

#### p-boot-demo section
* sadfwesv: [p-boot's PinePhone air-gap privacy mode / on any distro](https://www.youtube.com/watch?v=kyvFNGUxtIY).
* LinuxSmartphones: [Video: Megi’s multi-boot image for the PinePhone (with 17 Linux distros)](https://linuxsmartphones.com/video-megis-multi-boot-image-for-the-pinephone-with-17-linux-distros/).
* Avisando: [PinePhone multi-distro demo image (2)](https://www.youtube.com/watch?v=3krX-sRtDXw).

#### Game corner
* Bhushan Shah: [Playing Blobwars: Metal Blob Solid on Pine64 PinePhone](https://www.youtube.com/watch?v=rEawlcUgg_s).
* Alexmitter: [MCPE Pinephone](https://youtube.com/watch?v=pVRQ9Qr-Uuc). _Yes, that's Minecraft Pocket Edition on the PinePhone._

#### PinePhone mainboard upgrade section
* Brian Daniels: [Braveheart PinePhone Mainboard Upgrade](https://www.youtube.com/watch?v=J3AJEF7akkw).
* Arbitrary Tech: [Replacing Pinephone Motherboard](https://www.youtube.com/watch?v=xQ0AW2Dekgg).

### Stuff I did

#### Content
* I made a short video about Manjaro Lomiri Alpha 2. Watch it on [PeerTube](https://devtube.dev-wiki.de/videos/watch/571cfca0-2bc5-4c79-9568-96f919ad7af7), [LBRY](https://lbry.tv/@linmob:3/pinephone-manjaro-lomiri-alpha-2:0) or [YouTube](https://www.youtube.com/watch?v=KJHn-Oec-YI).
* I had a [very successful phone call](https://fosstodon.org/@linmob/105277976855494017) on my PinePhone. 
 
#### LINMOBapps
Not much happened here, [just managed to add some apps](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master).  The game-split is going to eventually happen. 
