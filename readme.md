# linmob.net

This repository generates the page seen under [LINMOB.net](https://linmob.net).
It's hosted on Framagit these days, after being hosted on GitHub and subsequently [Uberspace](https://uberspace.de) before.

## To Do / Stuff I'd like to add

* comments (via fraangut, email template with posttitle or that private disqus clone),
* try to integrate (some or all) posts of @linmmobblog or @linmob@fosstodon.org with this blog,
* video hosting,
* provide a link to the source markdown file to allow fixes to articles via merge request.

## Notes for myself

Run Zola site locally:
`$ zola serve`

